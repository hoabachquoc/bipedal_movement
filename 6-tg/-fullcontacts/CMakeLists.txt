CMAKE_MINIMUM_REQUIRED(VERSION 2.6)

# Define project
PROJECT (Oneleg6)
#SET(CMAKE_GENERATOR "Unix Makefiles")
SET(CMAKE_CXX_FLAGS  "-std=c++11 -D__USE_LONG_INTEGERS__")
#SET(CMAKE_CXX_FLAGS  "-D__USE_LONG_INTEGERS__")
SET(CMAKE_BUILD_TYPE Release)

SET(OPENSIM_INSTALL_DIR $ENV{OPENSIM_HOME})
SET(qpOASES_INSTALL_DIR $ENV{qpOASES_HOME})

# Change name of build target
SET(TARGET simulate CACHE TYPE STRING)

# Identify the cpp file(s) that were to be built
FILE(GLOB SOURCE_FILES *.h *.cpp)
SET(SOURCE ${SOURCE_FILES})

# Location of headers
SET(SIMTK_HEADERS_DIR ${OPENSIM_INSTALL_DIR}/sdk/include/SimTK/simbody)
SET(OPENSIM_HEADERS_DIR ${OPENSIM_INSTALL_DIR}/sdk/include)
SET(qpOASES_HEADERS_DIR ${qpOASES_INSTALL_DIR}/include)
INCLUDE_DIRECTORIES(${SIMTK_HEADERS_DIR} ${OPENSIM_HEADERS_DIR} ${qpOASES_HEADERS_DIR}${OpenSim_SOURCE_DIR} ${OpenSim_SOURCE_DIR}/Vendors ${OPENSIM_INSTALL_DIR})

# Libraries and dlls
SET(qpOASES_LIBS_DIR ${qpOASES_INSTALL_DIR}/bin)
SET(OPENSIM_LIBS_DIR ${OPENSIM_INSTALL_DIR}/lib)
SET(OPENSIM_DLLS_DIR ${OPENSIM_INSTALL_DIR}/bin)
LINK_DIRECTORIES(${OPENSIM_LIBS_DIR} ${OPENSIM_DLLS_DIR} ${qpOASES_LIBS_DIR})


ADD_EXECUTABLE(${TARGET} ${SOURCE})

TARGET_LINK_LIBRARIES(${TARGET}
	debug osimCommon_d optimized osimCommon
	debug osimSimulation_d optimized osimSimulation
	debug osimAnalyses_d optimized osimAnalyses
	debug osimActuators_d optimized osimActuators
	debug osimTools_d optimized osimTools
	debug SimTKcommon_d optimized SimTKcommon
	debug SimTKmath_d optimized  SimTKmath
	debug SimTKsimbody_d optimized SimTKsimbody
	qpOASES
)

# This block copies the additional files into the running directory
# For example vtp, obj files. Add to the end for more extentions
FILE(GLOB DATA_FILES *.vtp *.obj *.osim)
FOREACH (dataFile ${DATA_FILES})
 ADD_CUSTOM_COMMAND(
    TARGET ${TARGET}
    COMMAND ${CMAKE_COMMAND}
    ARGS -E copy
    ${dataFile}
	${Oneleg6_BINARY_DIR})
 ENDFOREACH (dataFile) 

MARK_AS_ADVANCED(CMAKE_INSTALL_PREFIX)
MARK_AS_ADVANCED(EXECUTABLE_OUTPUT_PATH)
MARK_AS_ADVANCED(LIBRARY_OUTPUT_PATH)
