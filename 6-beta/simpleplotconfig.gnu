reset
set terminal png 
set output "trajectory.png"
set title 'Trajectory of the end-effector'
set ylabel 'X (m)'
set xlabel 'Y (m)'
set grid
set key outside below; set key title "Legend"; set key box reverse; set key box lw 2 lc 4
plot "build/refPosCartesian.sto" using 2:3 with lines lw 4 lc rgb "red" title "reference",\
     "build/posCartesian.sto" using 2:3 with lines lw 2 lc rgb "blue" title "result"

reset
set terminal png
set output "ankleControls.png"
set title 'Ankle Controls'
set ylabel 'control'
set xlabel 'Time (s)'
set grid
plot "build/controls.sto" using 1:2 with lines lw 2 lc rgb "red" title "Left ankle muscle",\
     "build/controls.sto" using 1:3 with lines lw 2 lc rgb "blue" title "Right ankle muscle"

reset
set terminal png
set output "kneeControls.png"
set title 'Knee Controls'
set ylabel 'control'
set xlabel 'Time (s)'
set grid
plot "build/controls.sto" using 1:4 with lines lw 2 lc rgb "orange" title "Left knee muscle",\
     "build/controls.sto" using 1:5 with lines lw 2 lc rgb "cyan" title "Right knee muscle"

reset
set terminal png
set output "ankleForces.png"
set title 'Ankle muscle forces'
set ylabel 'Force (N)'
set xlabel 'Time (s)'
set grid
plot "build/forces.sto" using 1:2 with lines lw 2 lc rgb "red" title "Left ankle muscle",\
     "build/forces.sto" using 1:3 with lines lw 2 lc rgb "blue" title "Right ankle muscle"

reset
set terminal png
set output "kneeForces.png"
set title 'Knee muscle forces'
set ylabel 'Force (N)'
set xlabel 'Time (s)'
set grid
plot "build/forces.sto" using 1:4 with lines lw 2 lc rgb "red" title "Left knee muscle",\
     "build/forces.sto" using 1:5 with lines lw 2 lc rgb "blue" title "Right knee muscle"

reset
set terminal png
set output "torques.png"
set title 'torques'
set ylabel 'torque (Nm)'
set xlabel 'Time (s)'
set grid
plot "build/torques.sto" using 1:2 with lines lw 2 lc rgb "red" title "ankle",\
     "build/torques.sto" using 1:3 with lines lw 2 lc rgb "blue" title "knee"

reset
set terminal png
set output "gravity.png"
set title 'gravity'
set ylabel 'torques (Nm)'
set xlabel 'Time (s)'
set grid
plot "build/gravity.sto" using 1:2 with lines lw 2 lc rgb "red" title "ankle",\
     "build/gravity.sto" using 1:3 with lines lw 2 lc rgb "blue" title "knee"

reset
set terminal png
set output "angles.png"
set title 'Joint angles'
set ylabel 'Angle (rad)'
set xlabel 'Time (s)'
set grid
plot "build/states.sto" using 1:7 with lines lw 2 lc rgb "red" title "Ankle",\
     "build/states.sto" using 1:8 with lines lw 2 lc rgb "blue" title "Knee"
