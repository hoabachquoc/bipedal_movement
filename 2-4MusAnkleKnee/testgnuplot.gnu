# set terminal pngcairo  transparent enhanced font "arial,10" fontscale 1.0 size 600, 400 
set terminal png
set output "dashtypes2.png"
unset border
unset key
set label 1 "dt 1" at screen 0.15, 0.85, 0 left norotate back nopoint
set label 2 "dt 2" at screen 0.15, 0.8, 0 left norotate back nopoint
set label 3 "dt 3" at screen 0.15, 0.75, 0 left norotate back nopoint
set label 4 "dt 4" at screen 0.15, 0.7, 0 left norotate back nopoint
set label 5 "dt 5" at screen 0.15, 0.65, 0 left norotate back nopoint
set label 6 "dt 6" at screen 0.15, 0.6, 0 left norotate back nopoint
set label 7 "dt 7" at screen 0.15, 0.55, 0 left norotate back nopoint
set label 8 "dt 8" at screen 0.15, 0.5, 0 left norotate back nopoint
set label 9 "dt 9" at screen 0.15, 0.45, 0 left norotate back nopoint
set label 10 "dt 10" at screen 0.15, 0.4, 0 left norotate back nopoint
set label 11 "dt \".\"" at screen 0.15, 0.3, 0 left norotate back nopoint
set label 12 "dt \"-\"" at screen 0.15, 0.25, 0 left norotate back nopoint
set label 13 "dt \"._\"" at screen 0.15, 0.2, 0 left norotate back noenhanced nopoint
set label 14 "dt \"..- \"" at screen 0.15, 0.15, 0 left norotate back nopoint
set label 101 "Terminal's native dashtypes" at screen 0.1, 0.9, 0 left norotate back nopoint
set label 102 "Custom dashtypes" at screen 0.1, 0.35, 0 left norotate back nopoint
set arrow 1 from screen 0.25, 0.85, 0 to screen 0.8, 0.85, 0 nohead back nofilled linecolor rgb "#009e73"  linewidth 1.000 dashtype 1
set arrow 2 from screen 0.25, 0.8, 0 to screen 0.8, 0.8, 0 nohead back nofilled linecolor rgb "#009e73"  linewidth 2.000 dashtype '-'
set arrow 3 from screen 0.25, 0.75, 0 to screen 0.8, 0.75, 0 nohead back nofilled linecolor rgb "#009e73"  linewidth 3.000 dashtype 3
set arrow 4 from screen 0.25, 0.7, 0 to screen 0.8, 0.7, 0 nohead back nofilled linecolor rgb "#009e73"  linewidth 4.000 dashtype 4
set arrow 5 from screen 0.25, 0.65, 0 to screen 0.8, 0.65, 0 nohead back nofilled linecolor rgb "#009e73"  linewidth 5.000 dashtype 5
set arrow 6 from screen 0.25, 0.6, 0 to screen 0.8, 0.6, 0 nohead back nofilled linecolor rgb "#009e73"  linewidth 1.000 dashtype 6
set arrow 7 from screen 0.25, 0.55, 0 to screen 0.8, 0.55, 0 nohead back nofilled linecolor rgb "#009e73"  linewidth 2.000 dashtype 7
set arrow 8 from screen 0.25, 0.5, 0 to screen 0.8, 0.5, 0 nohead back nofilled linecolor rgb "#009e73"  linewidth 3.000 dashtype 8
set arrow 9 from screen 0.25, 0.45, 0 to screen 0.8, 0.45, 0 nohead back nofilled linecolor rgb "#009e73"  linewidth 4.000 dashtype 9
set arrow 10 from screen 0.25, 0.4, 0 to screen 0.8, 0.4, 0 nohead back nofilled linecolor rgb "#009e73"  linewidth 5.000 dashtype 10
set arrow 11 from screen 0.25, 0.3, 0 to screen 0.8, 0.3, 0 nohead back nofilled linecolor rgb "#56b4e9"  linewidth 1.000 dashtype "."
set arrow 12 from screen 0.25, 0.25, 0 to screen 0.8, 0.25, 0 nohead back nofilled linecolor rgb "#56b4e9"  linewidth 2.000 dashtype "-"
set arrow 13 from screen 0.25, 0.2, 0 to screen 0.8, 0.2, 0 nohead back nofilled linecolor rgb "#56b4e9"  linewidth 3.000 dashtype "._"
set arrow 14 from screen 0.25, 0.15, 0 to screen 0.8, 0.15, 0 nohead back nofilled linecolor rgb "#56b4e9"  linewidth 4.000 dashtype "..- "
set style data lines
unset xtics
unset ytics
set yrange [ 0.00000 : 1.00000 ] noreverse nowriteback
y = 0.0999999999999998
new = "..- "
DEBUG_TERM_HTIC = 119
DEBUG_TERM_VTIC = 119
plot 0 with lines lt nodraw

