reset
# set term postscript eps size 1024, 720 color blacktext "Helvetica" 24
# set output "PosEef.eps"
# set title 'Position of the end-effector'
# set ylabel 'Position (m)'
# set xlabel 'Time (s)'
# set grid
# set key outside below; set key title "Legend"; set key box reverse; set key box lw 2 lc 4
# plot "PosCartesian.sto" using 1:2 with lines lc rgb "red" title "X",\
#      "RefPosCartesian.sto" using 1:2 with lines lc rgb "cyan" title "refX",\
#      "PosCartesian.sto" using 1:3 with lines lc rgb "blue" title "Y",\
#      "RefPosCartesian.sto" using 1:3 with lines lc rgb "orange" title "refY" 


reset
set terminal png 
set output "PosEef.png"
set title 'Position of the end-effector'
set ylabel 'Position (m)'
set xlabel 'Time (s)'
set grid
set key outside below; set key title "Legend"; set key box reverse; set key box lw 2 lc 4
plot "PosCartesian.sto" using 1:2 with lines lw 2 lc rgb "red" title "X",\
     "RefPosCartesian.sto" using 1:2 with lines lw 4 dt '-' lc rgb "cyan" title "refX",\
     "PosCartesian.sto" using 1:3 with lines lw 2 lc rgb "blue" title "Y",\
     "RefPosCartesian.sto" using 1:3 with lines lw 4 dt '-' lc rgb "orange" title "refY" 

reset

set terminal png
set output "VelEefX.png"
set title 'Velocity X of the end-effector'
set ylabel 'Velocity (m/s)'
set xlabel 'Time (s)'
set grid
plot "VelCartesian.sto" using 1:2 with lines lw 1 lc rgb "cyan" title "X",\
     "RefVelCartesian.sto" using 1:2 with lines dt '-' lw 1 lc rgb "orange" title "refX" 


reset
set terminal png
set output "VelEefY.png"
set title 'Velocity Y of the end-effector'
set ylabel 'Velocity (m/s)'
set xlabel 'Time (s)'
set grid
plot "VelCartesian.sto" using 1:3 with lines lw 1 lc rgb "cyan" title "Y",\
     "RefVelCartesian.sto" using 1:3 with lines dt '-' lw 1 lc rgb "orange" title "refY" 


reset
# set terminal png'
# set output "AccEef.png"
# set title 'Acceleration of the end-effector'
# set ylabel 'Acceleration (m/s²)'
# set xlabel 'Time (s)'
# set grid
# plot "AccCartesian.sto" using 1:2 with lines lc rgb "red" title "X",\
#      "AccCartesian.sto" using 1:3 with lines lc rgb "blue" title "Y"


reset
set terminal png
set output "AccEefX.png"
set title 'Acceleration X of the end-effector'
set ylabel 'Acceleration (m/s²)'
set xlabel 'Time (s)'
set grid
plot "AccCartesian.sto" using 1:2 with lines lw 1 lc rgb "blue" title "X",\
     "RefAccCartesian.sto" using 1:2 with lines dt '-' lw 1 lc rgb "red" title "refX"

reset
set terminal png
set output "AccEefY.png"
set title 'Acceleration Y of the end-effector'
set ylabel 'Acceleration (m/s²)'
set xlabel 'Time (s)'
set grid
plot "AccCartesian.sto" using 1:3 with lines lw 1 lc rgb "blue" title "Y",\
     "RefAccCartesian.sto" using 1:3 with lines dt '-' lw 1 lc rgb "red" title "refY"


reset
set terminal png
set output "Forces.png"
set title 'Muscle force'
set ylabel 'Force (N)'
set xlabel 'Time (s)'
set grid
plot "Forces.sto" using 1:2 with lines lw 1 lc rgb "red" title "Left ankle muscle",\
     "Forces.sto" using 1:3 with lines lw 1 lc rgb "blue" title "Right ankle muscle",\
     "Forces.sto" using 1:4 with lines lw 1 lc rgb "orange" title "Left knee muscle",\
     "Forces.sto" using 1:5 with lines lw 1 lc rgb "cyan" title "Right knee muscle"

reset
set terminal png
set output "AnkleForces.png"
set title 'Ankle muscle forces'
set ylabel 'Force (N)'
set xlabel 'Time (s)'
set grid
plot "Forces.sto" using 1:2 with lines lw 1 lc rgb "red" title "Left ankle muscle",\
     "Forces.sto" using 1:3 with lines lw 1 lc rgb "blue" title "Right ankle muscle",\

reset
set terminal png
set output "KneeForces.png"
set title 'Knee muscle forces'
set ylabel 'Force (N)'
set xlabel 'Time (s)'
set grid
plot "Forces.sto" using 1:4 with lines lw 1 lc rgb "red" title "Left knee muscle",\
     "Forces.sto" using 1:5 with lines lw 1 lc rgb "blue" title "Right knee muscle"

reset
set terminal png
set output "Angles.png"
set title 'Joint angles'
set ylabel 'Angle (rad)'
set xlabel 'Time (s)'
set grid
plot "States.sto" using 1:2 with lines lw 1 lc rgb "red" title "Ankle",\
     "States.sto" using 1:3 with lines lw 1 lc rgb "blue" title "Knee"

reset
set terminal png
set output "AngularVel.png"
set title 'Angular Velocity'
set ylabel 'Angular Velocity (rad/s)'
set xlabel 'Time (s)'
set grid
plot "States.sto" using 1:4 with lines lw 1 lc rgb "red" title "Ankle",\
     "States.sto" using 1:5 with lines lw 1 lc rgb "blue" title "Knee"
