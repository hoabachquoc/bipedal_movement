reset
# set term postscript eps size 1024, 720 color blacktext "Helvetica" 24
# set output "PosEef.eps"
# set title 'Position of the end-effector'
# set ylabel 'Position (m)'
# set xlabel 'Time (s)'
# set grid
# set key outside below; set key title "Legend"; set key box reverse; set key box lw 2 lc 4
# plot "build/PosCartesian.sto" using 1:2 with lines lc rgb "red" title "X",\
#      "build/RefPosCartesian.sto" using 1:2 with lines lc rgb "cyan" title "refX",\
#      "build/PosCartesian.sto" using 1:3 with lines lc rgb "blue" title "Y",\
#      "build/RefPosCartesian.sto" using 1:3 with lines lc rgb "orange" title "refY" 


reset
set terminal png 
set output "PosEef.png"
set title 'Position of the end-effector'
set ylabel 'Position (m)'
set xlabel 'Time (s)'
set grid
set key outside below; set key title "Legend"; set key box reverse; set key box lw 2 lc 4
plot "build/RefPosCartesian.sto" using 1:2 with lines lw 4 lc rgb "cyan" title "refX",\
     "build/PosCartesian.sto" using 1:2 with lines lw 3 lc rgb "red" title "X",\
     "build/RefPosCartesian.sto" using 1:3 with lines dt '-.' lw 3 lc rgb "orange" title "refY",\
     "build/PosCartesian.sto" using 1:3 with lines lw 2 lc rgb "blue" title "Y"

reset
set terminal png 
set output "Trajectory.png"
set title 'Trajectory of the end-effector'
set ylabel 'X (m)'
set xlabel 'Y (m)'
set grid
set key outside below; set key title "Legend"; set key box reverse; set key box lw 2 lc 4
plot "build/RefPosCartesian.sto" using 2:3 with lines lw 4 lc rgb "red" title "reference",\
     "build/PosCartesian.sto" using 2:3 with lines lw 2 lc rgb "blue" title "result"

reset
set terminal png
set output "VelEefX.png"
set title 'Velocity X of the end-effector'
set ylabel 'Velocity (m/s)'
set xlabel 'Time (s)'
set grid
plot "build/VelCartesian.sto" using 1:2 with lines lw 2 lc rgb "cyan" title "X",\
     "build/RefVelCartesian.sto" using 1:2 with lines dt '-.' lw 2 lc rgb "orange" title "refX" 


reset
set terminal png
set output "VelEefY.png"
set title 'Velocity Y of the end-effector'
set ylabel 'Velocity (m/s)'
set xlabel 'Time (s)'
set grid
plot "build/VelCartesian.sto" using 1:3 with lines lw 2 lc rgb "cyan" title "Y",\
     "build/RefVelCartesian.sto" using 1:3 with lines dt '-.' lw 2 lc rgb "orange" title "refY" 


reset
# set terminal png'
# set output "AccEef.png"
# set title 'Acceleration of the end-effector'
# set ylabel 'Acceleration (m/s²)'
# set xlabel 'Time (s)'
# set grid
# plot "build/AccCartesian.sto" using 1:2 with lines lc rgb "red" title "X",\
#      "build/AccCartesian.sto" using 1:3 with lines lc rgb "blue" title "Y"


reset
set terminal png
set output "AccEefX.png"
set title 'Acceleration X of the end-effector'
set ylabel 'Acceleration (m/s²)'
set xlabel 'Time (s)'
# set yrange [-0.5:0.5]
set grid
plot "build/AccCartesian.sto" using 1:2 with lines lw 2 lc rgb "blue" title "X",\
     "build/RefAccCartesian.sto" using 1:2 with lines dt '-.' lw 2 lc rgb "red" title "refX"


reset
set terminal png
set output "AccEefY.png"
set title 'Acceleration Y of the end-effector'
set ylabel 'Acceleration (m/s²)'
set xlabel 'Time (s)'
# set yrange [-0.5:0.5]
set grid
plot "build/AccCartesian.sto" using 1:3 with lines lw 2 lc rgb "blue" title "Y",\
     "build/RefAccCartesian.sto" using 1:3 with lines dt '-.' lw 2 lc rgb "red" title "refY"


reset
set terminal png
set output "Forces.png"
set title 'Muscle force'
set ylabel 'Force (N)'
set xlabel 'Time (s)'
set grid
plot "build/Forces.sto" using 1:2 with lines lw 2 lc rgb "red" title "Left ankle muscle",\
     "build/Forces.sto" using 1:3 with lines lw 2 lc rgb "blue" title "Right ankle muscle",\
     "build/Forces.sto" using 1:4 with lines lw 2 lc rgb "orange" title "Left knee muscle",\
     "build/Forces.sto" using 1:5 with lines lw 2 lc rgb "cyan" title "Right knee muscle"

reset
set terminal png
set output "AnkleForces.png"
set title 'Ankle muscle forces'
set ylabel 'Force (N)'
set xlabel 'Time (s)'
set grid
plot "build/Forces.sto" using 1:2 with lines lw 2 lc rgb "red" title "Left ankle muscle",\
     "build/Forces.sto" using 1:3 with lines lw 2 lc rgb "blue" title "Right ankle muscle",\

reset
set terminal png
set output "KneeForces.png"
set title 'Knee muscle forces'
set ylabel 'Force (N)'
set xlabel 'Time (s)'
set grid
plot "build/Forces.sto" using 1:4 with lines lw 2 lc rgb "red" title "Left knee muscle",\
     "build/Forces.sto" using 1:5 with lines lw 2 lc rgb "blue" title "Right knee muscle"

reset
set terminal png
set output "Angles.png"
set title 'Joint angles'
set ylabel 'Angle (rad)'
set xlabel 'Time (s)'
set grid
plot "build/States.sto" using 1:2 with lines lw 2 lc rgb "red" title "Ankle",\
     "build/States.sto" using 1:3 with lines lw 2 lc rgb "blue" title "Knee"

reset
set terminal png
set output "AngularVel.png"
set title 'Angular Velocity'
set ylabel 'Angular Velocity (rad/s)'
set xlabel 'Time (s)'
set grid
plot "build/States.sto" using 1:4 with lines lw 2 lc rgb "red" title "Ankle",\
     "build/States.sto" using 1:5 with lines lw 2 lc rgb "blue" title "Knee"
