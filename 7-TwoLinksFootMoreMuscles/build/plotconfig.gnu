# reset
# set term postscript eps size 1024, 720 color blacktext "Helvetica" 24
# set output "PosEef.eps"
# set title 'Position of the end-effector'
# set ylabel 'Position (m)'
# set xlabel 'Time (s)'
# set grid
# set key outside below; set key title "Legend"; set key box reverse; set key box lw 2 lc 4
# plot "PosCartesian.sto" using 1:2 with lines lc rgb "red" title "X",\
#      "RefPosCartesian.sto" using 1:2 with lines lc rgb "cyan" title "refX",\
#      "PosCartesian.sto" using 1:3 with lines lc rgb "blue" title "Y",\
#      "RefPosCartesian.sto" using 1:3 with lines lc rgb "orange" title "refY" 


reset
set terminal png 
set output "PosEef.png"
set title 'Position of the end-effector'
set ylabel 'Position (m)'
set xlabel 'Time (s)'
set grid
set key outside below; set key title "Legend"; set key box reverse; set key box lw 2 lc 4
plot "PosCartesian.sto" using 1:2 with lines lw 2 lc rgb "red" title "X",\
     "RefPosCartesian.sto" using 1:2 with lines dt '-' lw 2 lc rgb "green" title "refX",\
     "PosCartesian.sto" using 1:3 with lines lw 2 lc rgb "blue" title "Y",\
     "RefPosCartesian.sto" using 1:3 with lines dt '-' lw 2 lc rgb "black" title "refY" 

reset
set terminal png
set output "VelEefX.png"
set title 'Velocity X of the end-effector'
set ylabel 'Velocity (m/s)'
set xlabel 'Time (s)'
set grid
set key outside below; set key title "Legend"; set key box reverse; set key box lw 2 lc 4
plot "VelCartesian.sto" using 1:2 with lines lw 2 lc rgb "cyan" title "X",\
     "RefVelCartesian.sto" using 1:2 with lines dt '-' lw 2 lc rgb "orange" title "refX" 


reset
set terminal png
set output "VelEefY.png"
set title 'Velocity Y of the end-effector'
set ylabel 'Velocity (m/s)'
set xlabel 'Time (s)'
set grid
set key outside below; set key title "Legend"; set key box reverse; set key box lw 2 lc 4
plot "VelCartesian.sto" using 1:3 with lines lw 2 lc rgb "cyan" title "Y",\
     "RefVelCartesian.sto" using 1:3 with lines dt '-' lw 2 lc rgb "orange" title "refY" 


reset
# set terminal png'
# set output "AccEef.png"
# set title 'Acceleration of the end-effector'
# set ylabel 'Acceleration (m/s²)'
# set xlabel 'Time (s)'
# set grid
# plot "AccCartesian.sto" using 1:2 with lines lc rgb "red" title "X",\
#      "AccCartesian.sto" using 1:3 with lines lc rgb "blue" title "Y"


reset
set terminal png
set output "AccEefX.png"
set title 'Acceleration X of the end-effector'
set ylabel 'Acceleration (m/s²)'
set xlabel 'Time (s)'
# set yrange [-0.5:0.5]
set grid
set key outside below; set key title "Legend"; set key box reverse; set key box lw 2 lc 4
plot "AccCartesian.sto" using 1:2 with lines lw 2 lc rgb "blue" title "X",\
     "RefAccCartesian.sto" using 1:2 with lines dt '-' lw 2 lc rgb "red" title "refX"


reset
set terminal png
set output "AccEefY.png"
set title 'Acceleration Y of the end-effector'
set ylabel 'Acceleration (m/s²)'
set xlabel 'Time (s)'
# set yrange [-0.5:0.5]
set grid
set key outside below; set key title "Legend"; set key box reverse; set key box lw 2 lc 4
plot "AccCartesian.sto" using 1:3 with lines lw 2 lc rgb "blue" title "Y",\
     "RefAccCartesian.sto" using 1:3 with lines dt '-' lw 2 lc rgb "red" title "refY"


reset
set terminal png
set output "Forces.png"
set title 'Muscle force'
set ylabel 'Force (N)'
set xlabel 'Time (s)'
set grid
plot "Forces.sto" using 1:2 with lines lw 2 lc rgb "red" title "Left ankle muscle",\
     "Forces.sto" using 1:3 with lines lw 2 lc rgb "blue" title "Right ankle muscle",\
     "Forces.sto" using 1:4 with lines lw 2 lc rgb "orange" title "Left knee muscle",\
     "Forces.sto" using 1:5 with lines lw 2 lc rgb "green" title "Right knee muscle",\
     "Forces.sto" using 1:6 with lines lw 2 lc rgb "black" title "Left Biarticular muscle",\
     "Forces.sto" using 1:7 with lines lw 2 lc rgb "magenta" title "Right Biarticular muscle"

reset
set terminal png
set output "AnkleForces.png"
set title 'Ankle muscle forces'
set ylabel 'Force (N)'
set xlabel 'Time (s)'
set grid
set key outside below; set key title "Legend"; set key box reverse; set key box lw 2 lc 4
plot "Forces.sto" using 1:2 with lines lw 2 lc rgb "red" title "Left ankle muscle",\
     "Forces.sto" using 1:3 with lines lw 2 lc rgb "blue" title "Right ankle muscle",\

reset
set terminal png
set output "KneeForces.png"
set title 'Knee muscle forces'
set ylabel 'Force (N)'
set xlabel 'Time (s)'
set grid
set key outside below; set key title "Legend"; set key box reverse; set key box lw 2 lc 4
plot "Forces.sto" using 1:4 with lines lw 2 lc rgb "red" title "Left knee muscle",\
     "Forces.sto" using 1:5 with lines lw 2 lc rgb "blue" title "Right knee muscle"

reset
set terminal png
set output "BiarticularForces.png"
set title 'Biarticular forces'
set ylabel 'Force (N)'
set xlabel 'Time (s)'
set grid
set key outside below; set key title "Legend"; set key box reverse; set key box lw 2 lc 4
plot "Forces.sto" using 1:6 with lines lw 2 lc rgb "red" title "Left biart muscle",\
     "Forces.sto" using 1:7 with lines lw 2 lc rgb "blue" title "Right biart muscle"

reset
set terminal png
set output "AnkleControls.png"
set title 'Ankle muscle controls'
set ylabel 'Control'
set xlabel 'Time (s)'
set grid
set key outside below; set key title "Legend"; set key box reverse; set key box lw 2 lc 4
plot "Controls.sto" using 1:2 with lines lw 2 lc rgb "red" title "Left ankle muscle",\
     "Controls.sto" using 1:3 with lines lw 2 lc rgb "blue" title "Right ankle muscle",\

reset
set terminal png
set output "KneeControls.png"
set title 'Knee muscle controls'
set ylabel 'Control'
set xlabel 'Time (s)'
set grid
set key outside below; set key title "Legend"; set key box reverse; set key box lw 2 lc 4
plot "Controls.sto" using 1:4 with lines lw 2 lc rgb "red" title "Left knee muscle",\
     "Controls.sto" using 1:5 with lines lw 2 lc rgb "blue" title "Right knee muscle"

reset
set terminal png
set output "BiarticularControls.png"
set title 'Biarticular Controls'
set ylabel 'Force (N)'
set xlabel 'Time (s)'
set grid
set key outside below; set key title "Legend"; set key box reverse; set key box lw 2 lc 4
plot "Controls.sto" using 1:6 with lines lw 2 lc rgb "red" title "Left biart muscle",\
     "Controls.sto" using 1:7 with lines lw 2 lc rgb "blue" title "Right biart muscle"

reset
set terminal png
set output "Angles.png"
set title 'Joint angles'
set ylabel 'Angle (rad)'
set xlabel 'Time (s)'
set grid
set key outside below; set key title "Legend"; set key box reverse; set key box lw 2 lc 4
plot "States.sto" using 1:2 with lines lw 2 lc rgb "red" title "Ankle",\
     "States.sto" using 1:3 with lines lw 2 lc rgb "blue" title "Knee"

reset
set terminal png
set output "AngularVel.png"
set title 'Angular Velocity'
set ylabel 'Angular Velocity (rad/s)'
set xlabel 'Time (s)'
set grid
set key outside below; set key title "Legend"; set key box reverse; set key box lw 2 lc 4
plot "States.sto" using 1:4 with lines lw 2 lc rgb "red" title "Ankle",\
     "States.sto" using 1:5 with lines lw 2 lc rgb "blue" title "Knee"

reset
set terminal png 
set output "MomentArms.png"
set title 'Moment Arms'
set ylabel 'Moment arms (m)'
set xlabel 'Time (s)'
set grid
set key outside below; set key title "Legend"; set key box reverse; set key box lw 2 lc 4
plot "MomentArms.sto" using 1:2 with lines lw 2 lc rgb "red" title "leftAnkleMuscle",\
     "MomentArms.sto" using 1:3 with lines lw 2 lc rgb "grey" title "rightAnkleMuslce",\
     "MomentArms.sto" using 1:4 with lines lw 2 lc rgb "blue" title "leftKneeMuscle",\
     "MomentArms.sto" using 1:5 with lines lw 2 lc rgb "orange" title "rightKneeMuscle",\
     "MomentArms.sto" using 1:6 with lines lw 2 lc rgb "yellow" title "LeftBiarticular_ankle",\
     "MomentArms.sto" using 1:7 with lines lw 2 lc rgb "magenta" title "LeftBiarticular_knee",\
     "MomentArms.sto" using 1:8 with lines lw 2 lc rgb "green" title "RightBiarticular_ankle",\
     "MomentArms.sto" using 1:9 with lines lw 2 lc rgb "black" title "RightBiarticular_knee"
