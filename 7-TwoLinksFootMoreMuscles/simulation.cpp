#include <OpenSim/OpenSim.h>
#include <Simbody.h>
#include <cmath>
#include <qpOASES.hpp>

#include <iostream>
#include <fstream>
#include <cstdlib>
#include <vector>

using namespace OpenSim;
using namespace SimTK;
using namespace std;
USING_NAMESPACE_QPOASES


//----------------------------------------- OPTIONS ---------------------------------------------
bool simulation = 1;
bool useVisualizer = 0;		// cause segmentation fault !!!
bool printRefTrajectory = 0;
bool printInitialCtrl = 1;
bool printInitialState = 1;
bool printlog = 0;
bool printDesAcc = 0;
bool printNewCtrls = 1;


/** Set initial state
 * default: data 
 * 1: all 0 
 * 2: knee 0.1
 * 3: all 0
 * 4: knee 0.3
 **/
int setInitialState = 2;	

int setInitCtrl = 3;		// 1: gravity compensation || 2: all 0 || 3: all 0.3


/** Set test case
  * default: data
  * 1: origin (0,0,0)
  * 2: vertical
  * 3: pose
**/
int testCase = 2;
bool dataTestCase = 0;
int TrajLength = 1000;


/* Set qpOASES as optmizer */
bool setqpOASES = 1;
bool printqpOAESOptions = 0;
bool modifyqpOASESOptions = 1;

/** Set OpenSim built-in optimizer
 * 1: Interior Point
 * 2: Limited-memory Broyden-Fletcher-Goldfarb-Shanno (LBFGS)
 * 3: LBFGS with simple bound constraints (LBFGSB)
 * 4: C implementation of sequential quadratic programming (CFSQP)
 * 5: Covariance matrix adaptation, evolution strategy (CMAES)
 **/
int setOptimizerAlgorithm = 1;
bool setCostFuncFormulationType = 0;		// 1: integrator || 0 : qp

//----------------------------------------- WEIGHTS ---------------------------------------------
SimTK::Real weightTerm1 = 500.00;	// left ankle muscle
SimTK::Real weightTerm2 = 500.00;	// right ankle muscle
SimTK::Real weightTerm3 = 5.00;	// left knee muscle
SimTK::Real weightTerm4 = 5.00;	// right knee muscle
SimTK::Real weightTerm5 = 5.00;	// left biarticular muscle
SimTK::Real weightTerm6 = 5.00;	// right biarticular muscle

//---------------------------------------- END-EFFECTOR -----------------------------------------
Vec3 eefPoint(0,0.5,0);		// in body frame

//------------------------------------- INTEGRATOR OPTIONS --------------------------------------
unsigned stepNum = 1;
const Real stepSize = 1e-3;
double initialStepTime = 0.0;
double finalStepTime = initialStepTime + stepSize;


//------------------------------------- FEEDBACK CONTROLERS -------------------------------------
Mat33 Kp(36); // position gain
Mat33 Kv(12); // velocity gain

//-------------------------------------- MODEL PROPERTIES ---------------------------------------
double linkageLength = 0.5;
double positionCoM = linkageLength/2;
double linkageMass = 1;
Mat33 inertialLinkage;
double g = -9.80655;

//---------------------------------------- MISCELLANEOUS ----------------------------------------
SimTK::Matrix analyticMassMatrix(2,2);


//===============================================================================================
//				     TRAJECTORY GENERATION - KDL
//===============================================================================================
int calcTrajVecLength() {
   string s;
   int sTotal(0);

   ifstream in;
   in.open("velocity.dat");

   while(!in.eof()) {
      getline(in, s);
      sTotal ++;	
   }
   return sTotal - 1;
}

void printvector(vector<double> &vect) {
   for (vector<double>::const_iterator iter = vect.begin(); iter != vect.end(); ++iter)
      cout << *iter << endl;
}

void generateTrajectry ( Vector_<Vec3> &refPos, Vector_<Vec3> &refVel, Vector_<Vec3> &refAcc ) {
   // Read reference acceleration
   ifstream acc_in("acceleration.dat");
   if (!acc_in) {
      cerr << "Uh oh, file .dat could not be opened for reading!" << endl;
      exit(1);
   }
   // Create the vector, initialized from the numbers in the file:
   std::vector<double> accData (( std::istream_iterator<double>(acc_in)),
	 std::istream_iterator<double>());
   int i = 0;
   for (int count = 0; count < accData.size(); count+=6) {
      refAcc[i] = Vec3( accData[count], accData[count+1], 0); i++;
   }

   // Read reference velocity
   ifstream vel_in("velocity.dat");
   if (!vel_in) {
      cerr << "Uh oh, file .dat could not be opened for reading!" << endl;
      exit(1);
   }
   // Create the vector, initialized from the numbers in the file:
   std::vector<double> velData (( std::istream_iterator<double>(vel_in)),
	 std::istream_iterator<double>());
   i = 0;
   for (int count = 0; count < velData.size(); count+=6) {
      refVel[i] = Vec3( velData[count], velData[count+1], 0);
      i++;
   }

   // Read reference position
   ifstream traj_in("position.dat");
   if (!traj_in) {
      cerr << "Uh oh, file .dat could not be opened for reading!" << endl;
      exit(1);
   }

   std::vector<double> posData (( std::istream_iterator<double>(traj_in)),
	 std::istream_iterator<double>());
   int j = 0;
   for (int count = 0; count < posData .size(); count+=16) {
      refPos[j] = Vec3( posData [count+3], posData [count+7], 0);
      j++;
   }
}

//===============================================================================================
// 			                            OPTIMIZATION SYSTEM
//===============================================================================================

SimTK::Vector momentArmsSet(State& si, Model& osimModel) {
   // Prepare actuators - dynamic cast
   PathActuator& leftAnkleMuscle = dynamic_cast<PathActuator&>
      (osimModel.getActuators().get(0));
   PathActuator& rightAnkleMuscle = dynamic_cast<PathActuator&>
      (osimModel.getActuators().get(1));
   PathActuator& leftKneeMuscle = dynamic_cast<PathActuator&>
      (osimModel.getActuators().get(2));
   PathActuator& rightKneeMuscle = dynamic_cast<PathActuator&>
      (osimModel.getActuators().get(3));
   PathActuator& leftBiarticularMuscle = dynamic_cast<PathActuator&>
      (osimModel.getActuators().get(4));
   PathActuator& rightBiarticularMuscle = dynamic_cast<PathActuator&>
      (osimModel.getActuators().get(5));

   // Prepare coordinates
   Coordinate& ankle_ = osimModel.getCoordinateSet().get(6);
   Coordinate& knee_ = osimModel.getCoordinateSet().get(7);

   // Moment arm vector
   int nbAct = osimModel.getNumControls();
   SimTK::Vector momentArms(nbAct+2);
   momentArms (0) = leftAnkleMuscle.computeMomentArm(si, ankle_);
   momentArms (1) = rightAnkleMuscle.computeMomentArm(si, ankle_);
   momentArms (2) = leftKneeMuscle.computeMomentArm(si, knee_);
   momentArms (3) = rightKneeMuscle.computeMomentArm(si, knee_);
   momentArms (4) = leftBiarticularMuscle.computeMomentArm(si, ankle_);
   momentArms (5) = leftBiarticularMuscle.computeMomentArm(si, knee_);
   momentArms (6) = rightBiarticularMuscle.computeMomentArm(si, ankle_);
   momentArms (7) = rightBiarticularMuscle.computeMomentArm(si, knee_);
   return momentArms;
}

SimTK::Matrix selection_matrix (State& si, Model& osimModel) {
   SimTK::Vector momentArms = momentArmsSet(si, osimModel);
   int nbU = si.getNU();
   int nbAct = osimModel.getNumControls();
   SimTK::Matrix selection_Mat (nbU,nbAct);
   // First 6 rows are 0 - free joint
   selection_Mat = 0;
   selection_Mat (6,0) = momentArms(0);
   selection_Mat (6,1) = momentArms(1);
   selection_Mat (6,4) = momentArms(4);
   selection_Mat (6,5) = momentArms(6);
   selection_Mat (7,2) = momentArms(2);
   selection_Mat (7,3) = momentArms(3);
   selection_Mat (7,4) = momentArms(5);
   selection_Mat (7,5) = momentArms(7);
   return selection_Mat;
}

SimTK::Matrix regulationMatrix (Model& osimModel){
   int nbAct = osimModel.getNumControls();
   SimTK::Matrix regulationMat(nbAct,nbAct);
   regulationMat = weightTerm1;
   regulationMat(1,1) = weightTerm2;
   regulationMat(2,2) = weightTerm3;
   regulationMat(3,3) = weightTerm4;
   regulationMat(4,4) = weightTerm5;
   regulationMat(5,5) = weightTerm6;
   return regulationMat;
}
//------------------------------ QUADRATIC PROGRAMING FORMULATION -------------------------------

/** This function calculates
  * Matrix H in which integrated the regulation terms
  * Vector g
**/
void QPformulation(State& si, Model& osimModel, Vec3& desInput, SimTK::Matrix& H_, SimTK::Vector& g_) {
   const SimbodyMatterSubsystem& matter = osimModel.getMatterSubsystem();
   const int nb = matter.getNumBodies();

   SimTK::Matrix Minv;
   matter.calcMInv(si,Minv);

   SimTK::Matrix JS;
   matter.calcStationJacobian
      (si,MobilizedBodyIndex(3),eefPoint,JS);

   SimTK::Vector gr;
   matter.multiplyBySystemJacobianTranspose(
	 si, osimModel.getGravityForce().getBodyForces(si), gr);

   SimTK::Vector cor;
   matter.calcResidualForceIgnoringConstraints(
	 si, SimTK::Vector(0), SimTK::Vector_<SimTK::SpatialVec>(0),
	 SimTK::Vector(0),cor);

   SimTK::Vector f_app =  gr - cor;

   SimTK::Vector MInvf_app;
   matter.multiplyByMInv(si, f_app, MInvf_app);

   Vec3 JMInvf_app = matter.multiplyByStationJacobian
      (si, MobilizedBodyIndex(3), eefPoint, MInvf_app);

   SimTK::Vec3 JSDotu = matter.calcBiasForStationJacobian
      (si, MobilizedBodyIndex(3), eefPoint);

   SimTK::Matrix selection_Mat = selection_matrix (si, osimModel);

   // Acceleration errors in form of ax + b
   SimTK::Matrix a_ = JS * Minv * selection_Mat *
      osimModel.getActuators().get(0).getOptimalForce();
   SimTK::Vec3 b3_ =  JMInvf_app + JSDotu - desInput;
   SimTK::Vector b_(3);
   b_(0) = b3_(0);
   b_(1) = b3_(1);
   b_(2) = b3_(2);

   // Calculate H combined with regulation terms
   H_ = ~a_ * a_;
   H_ = H_ + regulationMatrix(osimModel);
   g_ = ~a_ * b_;
}


//------------------------------------ QPOASES CLASS --------------------------------------------

class OneLegqpOASESOpt{

   private:
      State& si;
      Model& osimModel;
      Vec3& desAcc;
   public:

      OneLegqpOASESOpt(State& s, Model& aModel, Vec3& adesAcc): 
	 si(s), osimModel(aModel), desAcc(adesAcc) { }

      // Task: Move to a point
      Vector optimize(){
	 int size = osimModel.getActuators().getSize();
	 real_t H[size*size];
	 real_t g[size];
	 real_t lb[size];
	 real_t ub[size];

	 SimTK::Matrix H_;
	 Vector g_;
	 QPformulation(si, osimModel, desAcc, H_, g_);

	 for (int i = 0; i < size; i++) {
	    for (int j = 0; j < size; j++)
	       H[i*size+j] = H_(i,j);
	    g[i] = g_(i);
	    lb[i] = osimModel.getActuators().get(i).getMinControl();
	    ub[i] = osimModel.getActuators().get(i).getMaxControl();
	 }

	 /* Setting up QProblem object. */
	 QProblemB example(size);
	 Options options;
	 options.printLevel = PrintLevel(0);

	 if (modifyqpOASESOptions) {
	    options.numRefinementSteps = 2;
	    options.enableDriftCorrection = 1;
	    options.enableRegularisation = BT_TRUE;
	    options.numRegularisationSteps = 2;
	    options.initialRamping = 0.2;
	    options.finalRamping = 0.4;
	 }
	 example.setOptions( options );

	 if (printqpOAESOptions) {
	    cout << "--enable Ramping : " << options.enableRamping << endl;
	    cout << "--enable Far Bounds : " << options.enableFarBounds << endl;
	    cout << "--enable Flipping Bounds : " << options.enableFlippingBounds << endl;
	    cout << "--enable Regularisation : " << options.enableRegularisation << endl;
	    cout << "--enable Full LI Tests: " << options.enableFullLITests << endl;
	    /* cout << "--enable Full NZC Tests: " << options.enableFullNZCTests << endl; */
	    cout << "--enable Drift Correction: " << options.enableDriftCorrection << endl;
	    cout << "--enable Cholesky Refactorisation: " << options.enableCholeskyRefactorisation << endl;
	    cout << "--enable Equalities: " << options.enableEqualities << endl;
	    cout << "--termination Tolerance: " << options.terminationTolerance << endl;
	    cout << "--bound Tolerance: " << options.boundTolerance << endl;
	    cout << "--bound Relaxation: " << options.boundRelaxation << endl;
	    cout << "--epsNum: " << options.epsNum << endl;
	    cout << "--epsDen: " << options.epsDen << endl;
	    cout << "--max Primal Jump: " << options.maxPrimalJump << endl;
	    cout << "--max Dual Jump: " << options.maxDualJump << endl;
	    cout << "--initialRamping: " << options.initialRamping << endl;
	    cout << "--finalRamping: " << options.finalRamping << endl;
	    cout << "--initialFarBounds: " << options.initialFarBounds << endl;
	    cout << "--growFarBounds: " << options.growFarBounds << endl;
	    cout << "--initialStatusBounds: " << options.initialStatusBounds << endl;
	    cout << "--epsFlipping: " << options.epsFlipping << endl;
	    cout << "--numRegularisationSteps: " << options.numRegularisationSteps << endl;
	    cout << "--epsRegularisation: " << options.epsRegularisation << endl;
	    cout << "--numRefinementSteps: " << options.numRefinementSteps << endl;
	    cout << "--epsIterRef: " << options.epsIterRef << endl;
	    cout << "--epsLITests: " << options.epsLITests << endl;
	    cout << "--epsNZCTests: " << options.epsNZCTests << endl;
	    cout << "--enableInertiaCorrection: " << options.enableInertiaCorrection << endl;
	    /* cout << "--rCondMin: " << options.rCondMin << endl; */
	 }
	 /* Solve first QP. */
	 int_t nWSR = 100;
	 example.init( H,g,lb,ub,nWSR );

	 /* Get and print solution of first QP. */
	 real_t xOpt[size];
	 example.getPrimalSolution( xOpt );

	 /* real_t yOpt[2+1]; */
	 /* example.getDualSolution( yOpt ); */
	 /* printf( "\nxOpt = [ %e, %e ];  yOpt = [ %e, %e, %e ];  objVal = %e\n\n", */ 
	 /* xOpt[0],xOpt[1],yOpt[0],yOpt[1],yOpt[2],example.getObjVal() ); */
	 /* printf( "\nxOpt = [ %e, %e ]; objVal = %e\n\n", xOpt[0],xOpt[1],example.getObjVal() ); */

	 /* example.printOptions(); */
	 /*example.printProperties();*/

	 /*getGlobalMessageHandler()->listAllMessages();*/
	 Vector controlOpt(size);
	 for (int i = 0; i < size; i++)
	    controlOpt(i) = xOpt[i];
	 return controlOpt;
      }
};


//-------------------------------- OPENSIM BUILT IN OPTIMIZATION --------------------------------

class OpensimOptimizationSystem : public OptimizerSystem {

   private:
      int numControls;
      State& si;
      Model& osimModel;
      Vec3& desInput;

   public:
      OpensimOptimizationSystem(int numParameters, State& s, Model& aModel, Vec3& desAcc): 
	 numControls(numParameters), OptimizerSystem(numParameters), 
	 si(s), osimModel(aModel), desInput(desAcc) { }

      int objectiveFunc(  const Vector &newControls, bool new_coefficients, Real& f ) const {

	 if (setCostFuncFormulationType) 
	 {
	    // make a copy of out initial states
	    State s = si;

	    // Update the control values
	    //newControls.dump("New Controls In:");
	    osimModel.updDefaultControls() = newControls;

	    // Create the integrator for the simulation.
	    RungeKuttaMersonIntegrator integrator(osimModel.getMultibodySystem());
	    integrator.setAccuracy(1.0e-4);
	    integrator.setAllowInterpolation(false);

	    // Integration without manager
	    /* integrator.initialize(si); */
	    /* integrator.stepBy(stepSize); */

	    // Create a manager to run the simulation
	    Manager manager(osimModel, integrator);
	    manager.setInitialTime(initialStepTime);
	    manager.setFinalTime(finalStepTime);
	    manager.integrate(s);

	    //osimModel.getControls(s).dump("Model Controls:");
	    osimModel.getMultibodySystem().realize(s, Stage::Acceleration);

	    Vec3 curAcc;
	    // Don't forget state s for god sake !!!
	    osimModel.getSimbodyEngine().getAcceleration
	       (s, osimModel.getBodySet().get("linkage2"), eefPoint, curAcc);

	    Vec3 err;
	    err = desInput - curAcc;

	    // Sum of forces produced by actuators
	    for (int actNb = 0; actNb < osimModel.getNumControls(); actNb ++)
	    {
	       f += osimModel.getActuators().get(actNb).getControl(s) * 
		  osimModel.getActuators().get(actNb).getOptimalForce();
	    }

	    /* f = f * weightTerm + dot(err,err); */
	 }
	 else
	 {
	    SimTK::Matrix H_;
	    SimTK::Vector g_;
	    QPformulation(si, osimModel, desInput, H_, g_);

	    // SimTK bullshit bug: vector*matrix*vector can't give an double/int
	    SimTK::Vector stupidVector = ~newControls * H_ * newControls;
	    f = 1/2 * stupidVector(0) + ~newControls * g_ ;
	 }
	 return(0);
      }	

};

//===============================================================================================
// 				              MAIN
//===============================================================================================


int main()
{
   try {
      std::clock_t startTime = std::clock();

      //----------------------------------------- LOAD MODEL ------------------------------------------
      Model osimModel("TwoLinksFootMoreMuscles.osim");
      osimModel.setUseVisualizer(useVisualizer);

      //--------------------------------------- INITIALIZATION ----------------------------------------
      SimTK::State& si = osimModel.initSystem();
      /* cout << "initialized system" << endl; */

      //------------------------------------- REFERENCE TRAJECTORY ------------------------------------
      int trajVecLength = calcTrajVecLength();

      Vector_<Vec3> refPos(trajVecLength);
      Vector_<Vec3> refVel(trajVecLength);
      Vector_<Vec3> refAcc(trajVecLength);

      /* Choose a type of reference
       * default : trajectory from data files
       * 1 : destination point at the origin
       * 2 : destination point at the top of the linkage 2 when the system is straight up vertically
       **/ 

      int trajVecLength;
      if (dataTestCase) { trajVecLength = calcTrajVecLength(); }
      else { trajVecLength = TrajLength; }
      Vector_<Vec3> refPos(trajVecLength);
      Vector_<Vec3> refVel(trajVecLength);
      Vector_<Vec3> refAcc(trajVecLength);

      switch (testCase) {
	 default:
	    generateTrajectry(refPos, refVel, refAcc);
	    break;
	 case 1:
	    for (int count = 0; count < trajVecLength; ++count) {
	       refPos[count] = Vec3(0); 
	       refVel[count] = Vec3(0);
	       refAcc[count] = Vec3(0); 
	    }
	    break;
	 case 2:
	    for (int count = 0; count < trajVecLength; ++count) {
	       refPos[count] = Vec3(0,1.005,0); 
	       refVel[count] = Vec3(0);
	       refAcc[count] = Vec3(0); 
	    }
	    break;
	 case 3:
	    for (int count = 0; count < trajVecLength; ++count) {
	       refPos[count] = Vec3(0.05,0.905,0); 
	       refVel[count] = Vec3(0);
	       refAcc[count] = Vec3(0); 
	    }
	    break;
      }

      if (printRefTrajectory)
      {
	 for (int count = 0; count < trajVecLength; ++count) {
	    cout << "refPos " << count << " " << refPos[count] << endl;
	    cout << "refVel " << count << " " << refVel[count] << endl;
	    cout << "refAcc " << count << " " << refAcc[count] << endl;
	 }
      }

      //--------------------------------- INITIAL ANGLE POSITIONS -------------------------------------
      double anklePos_init;
      double ankleVel_init;
      double kneePos_init;
      double kneeVel_init;
      double hipPos_init;
      double hipVel_init;

      switch (setInitialState) {
	 default:
	    anklePos_init = -20*SimTK::Pi/180;
	    ankleVel_init = 0;
	    kneePos_init = -30*SimTK::Pi/180;
	    kneeVel_init = 0;
	    /* hipPos_init = 10*SimTK::Pi/180; */
	    /* hipVel_init = 0; */
	    break;
	 case 1:
	    anklePos_init = 0;
	    ankleVel_init = 0;
	    kneePos_init = 0;
	    kneeVel_init = 0;
	    /* hipPos_init = 0; */
	    /* hipVel_init = 0; */
	    break;
	 case 2:
	    anklePos_init = 0;
	    ankleVel_init = 0;
	    kneePos_init = 0.1;
	    kneeVel_init = 0;
	    /* hipPos_init = 0; */
	    /* hipVel_init = 0; */
	    break;
	 case 3:
	    anklePos_init = 0;
	    ankleVel_init = 0;
	    kneePos_init = 0;
	    kneeVel_init = 0;
	    /* hipPos_init = 0.1; */
	    /* hipVel_init = 0; */
	    break;
	 case 4:
	    anklePos_init = 0;
	    ankleVel_init = 0;
	    kneePos_init = 0.3;
	    kneeVel_init = 0;
	    /* hipPos_init = 0.1; */
	    /* hipVel_init = 0.1; */
	    break;
      }

      osimModel.getCoordinateSet().get("ankle_rotZ").setValue(si, anklePos_init);
      osimModel.getCoordinateSet().get("knee_rotZ").setValue(si, kneePos_init);
      osimModel.getCoordinateSet().get("ankle_rotZ").setSpeedValue(si, ankleVel_init);
      osimModel.getCoordinateSet().get("knee_rotZ").setSpeedValue(si, kneeVel_init);

      cout << "--nbU = " << si.getNU() << endl;
      cout << "--nbQ = " << si.getNU() << endl;
      cout << "--nbY = " << si.getNY() << endl;
      //--------------------------------------- ACTUATORS ---------------------------------------------
      int numControls = osimModel.getNumControls();
      cout << "numControls = " << numControls << endl;

      // Get control bound set and initialize controls
      Vector lower_bounds(numControls);
      Vector upper_bounds(numControls);
      Vector initialControls(numControls);

      for (int i = 0; i < numControls; i++) {
	 lower_bounds(i) = osimModel.getActuators().get(i).getMinControl();
	 upper_bounds(i) = osimModel.getActuators().get(i).getMaxControl();
	 switch (setInitCtrl) {
	    case 2:
	       initialControls(i) = 0;
	       break;
	    case 3:
	       initialControls(i) = 0.3;
	       break;
	 }
      }

      osimModel.updDefaultControls() = initialControls;

      if (printInitialCtrl)
      { cout << "Initial controls = " << osimModel.getDefaultControls() << endl; }

      //--------------------------------------- VISUALIZER --------------------------------------------
      if (useVisualizer) {
	 osimModel.updMatterSubsystem().setShowDefaultGeometry(true);
	 Visualizer& viz = osimModel.updVisualizer().updSimbodyVisualizer();
	 viz.setBackgroundType(viz.SolidColor);
	 viz.setBackgroundColor(Cyan);
      }

      // Angular state variables
      Vec3 eefPos;
      Vec3 eefVel;
      osimModel.getMultibodySystem().realize(si, Stage::Acceleration);
      osimModel.getSimbodyEngine().getPosition
	 (si, osimModel.getBodySet().get("linkage2"), eefPoint, eefPos);
      osimModel.getSimbodyEngine().getVelocity
	 (si, osimModel.getBodySet().get("linkage2"), eefPoint, eefVel);

      if (printInitialState) {
	 cout << "Initial end-effector position  = " << eefPos << endl;
	 cout << "Initial end-effector velocity  = " << eefVel << endl;
      }


      //---------------------------------------- LOG FILES --------------------------------------------
      // Construct control storage
      Array<string> columnLabels;
      Storage* _controlStore = new Storage(1023,"controls");
      columnLabels.append("time");
      for(int i=0;i<osimModel.getActuators().getSize();i++)
	 columnLabels.append(osimModel.getActuators().get(i).getName());
      _controlStore->setColumnLabels(columnLabels);

      // Construct force storage
      Storage* _forceStore = new Storage(1023,"forces");
      _forceStore->setColumnLabels(columnLabels);

      // Initialize storage files
      Vector updctrls(osimModel.getActuators().getSize());
      Vector updforces(osimModel.getActuators().getSize());
      osimModel.getMultibodySystem().realize(si, Stage::Acceleration);

      for (int i = 0; i < osimModel.getActuators().getSize(); i++) {
	 updctrls[i] = osimModel.getActuators().get(i).getControl(si);
	 updforces[i] = updctrls[i] * osimModel.getActuators().get(i).getOptimalForce();
      } 
      /* cout << updctrls << endl; */

      _controlStore->store(stepNum, 0.0,osimModel.getActuators().getSize(), &updctrls[0]);
      _controlStore->print("Controls.sto");
      _forceStore->store(stepNum, 0.0,osimModel.getActuators().getSize(), &updforces[0]);
      _forceStore->print("Forces.sto");

      // Construct Cartesian state storage of the end-effector
      Array<string> posColumnLabels;
      Storage* _posCartesianStore = new Storage(1023,"PosCartesian");
      posColumnLabels.append("time");
      posColumnLabels.append("pX");
      posColumnLabels.append("pY");
      posColumnLabels.append("pZ");
      _posCartesianStore->setColumnLabels(posColumnLabels);

      Array<string> velColumnLabels;
      Storage* _velCartesianStore = new Storage(1023,"VelCartesian");
      velColumnLabels.append("time");
      velColumnLabels.append("vX");
      velColumnLabels.append("vY");
      velColumnLabels.append("vZ");
      _velCartesianStore->setColumnLabels(velColumnLabels);

      Array<string> accColumnLabels;
      Storage* _accCartesianStore = new Storage(1023,"AccCartesian");
      accColumnLabels.append("time");
      accColumnLabels.append("aX");
      accColumnLabels.append("aY");
      accColumnLabels.append("aZ");
      _accCartesianStore->setColumnLabels(accColumnLabels);

      Array<string> refPosColumnLabels;
      Storage* _refPosCartesianStore = new Storage(1023,"refstatePos");
      refPosColumnLabels.append("time");
      refPosColumnLabels.append("pXr");
      refPosColumnLabels.append("pYr");
      refPosColumnLabels.append("pZr");
      _refPosCartesianStore->setColumnLabels(refPosColumnLabels);

      Array<string> refVelColumnLabels;
      Storage* _refVelCartesianStore = new Storage(1023,"refstateVel");
      refVelColumnLabels.append("time");
      refVelColumnLabels.append("vXr");
      refVelColumnLabels.append("vYr");
      refVelColumnLabels.append("vZr");
      _refVelCartesianStore->setColumnLabels(refVelColumnLabels);

      Array<string> refAccColumnLabels;
      Storage* _refAccCartesianStore = new Storage(1023,"refstateAcc");
      refAccColumnLabels.append("time");
      refAccColumnLabels.append("aXr");
      refAccColumnLabels.append("aYr");
      refAccColumnLabels.append("aZr");
      _refAccCartesianStore->setColumnLabels(refAccColumnLabels);

      // Initialize state storage
      Vec3 updPosCartesian(0);
      Vec3 updVelCartesian(0);
      Vec3 updAccCartesian(0);
      osimModel.getSimbodyEngine().getPosition
	 (si, osimModel.getBodySet().get("linkage2"), eefPoint, updPosCartesian);
      osimModel.getSimbodyEngine().getVelocity
	 (si, osimModel.getBodySet().get("linkage2"), eefPoint, updVelCartesian);
      osimModel.getSimbodyEngine().getAcceleration
	 (si, osimModel.getBodySet().get("linkage2"), eefPoint, updAccCartesian);

      _posCartesianStore->store(stepNum, 0.0, 3, &updPosCartesian[0]);
      _posCartesianStore->print("PosCartesian.sto");
      _velCartesianStore->store(stepNum, 0.0, 3, &updVelCartesian[0]);
      _velCartesianStore->print("VelCartesian.sto");
      _accCartesianStore->store(stepNum, 0.0, 3, &updAccCartesian[0]);
      _accCartesianStore->print("AccCartesian.sto");

      _refPosCartesianStore->store(stepNum, 0.0, 3, &refPos[0].get(0));
      _refPosCartesianStore->print("RefPosCartesian.sto");
      _refVelCartesianStore->store(stepNum, 0.0, 3, &refVel[0].get(0));
      _refVelCartesianStore->print("RefVelCartesian.sto");
      _refAccCartesianStore->store(stepNum, 0.0, 3, &refAcc[0].get(0));
      _refAccCartesianStore->print("RefAccCartesian.sto");

      // Moment arm storage
      Array<string> momentArms;
      Storage* _momentArmsStore = new Storage(1023,"momentArms");
      momentArms.append("time");
      momentArms.append("leftAnkleMuscle");
      momentArms.append("rightAnkleMuscle");
      momentArms.append("leftKneeMuscle");
      momentArms.append("rightKneeMuscle");
      _momentArmsStore->setColumnLabels(momentArms);

      // Initialize moment arm storage
      SimTK::Vector updMomentArms = momentArmsSet(si, osimModel);
      int nbMomentArms = numControls + 2;
      _momentArmsStore->store(stepNum, 0.0, nbMomentArms, &updMomentArms(0));
      _momentArmsStore->print("MomentArms.sto");

      //----------------------------------- INTEGRATOR MANAGER ----------------------------------------
      SimTK::RungeKuttaMersonIntegrator integ(osimModel.getMultibodySystem());
      integ.setAccuracy(1.0e-4);
      Manager manager(osimModel, integ);

      //--------------------------------------- SIMULATION --------------------------------------------
      if (simulation) {
	 for (int count = 0; count < trajVecLength; ++count) {
	    //------------------------------------- TASK SERVOING -------------------------------------------
	    // Compute desired trajectory from PD controller
	    Vec3 desAcc = Kp * (refPos[count] - eefPos) 
	       + Kv * (refVel[count] - eefVel) 
	       + refAcc[count];

	    if (printDesAcc)
	       cout << "desAcc = " << desAcc <<endl;

	    /* cout << "Message: End task servoing" << endl; */

	    //------------------------------------ OPTIMIZATION ---------------------------------------------
	    Vector newControls(osimModel.getActuators().getSize());
	    if (setqpOASES) {
	       OneLegqpOASESOpt sys(si, osimModel, desAcc);
	       newControls = sys.optimize();
	    }
	    else {
	       OptimizerAlgorithm optimizerAlg;
	       switch (setOptimizerAlgorithm) {
		  case 1:
		     optimizerAlg = InteriorPoint;
		     break;
		  case 2:
		     optimizerAlg = LBFGS;
		     break; 
		  case 3:
		     optimizerAlg = LBFGSB;
		     break;
		  case 4:
		     optimizerAlg = CFSQP;
		     break;
		  case 5:
		     optimizerAlg = CMAES;
		     break;
	       }
	       OpensimOptimizationSystem 
		  sys(numControls, si, osimModel, desAcc);
	       sys.setParameterLimits( lower_bounds, upper_bounds );

	       Optimizer opt(sys, optimizerAlg);

	       // Specify settings for the optimizer
	       opt.setConvergenceTolerance(0.001);
	       opt.useNumericalGradient(true);
	       opt.setMaxIterations(200);
	       opt.setLimitedMemoryHistory(500);

	       Real f;	
	       f = opt.optimize(newControls);
	       /* cout <<  "f " << count << " = " << f << endl; */
	    }
	    osimModel.updDefaultControls() = newControls;

	    if (printNewCtrls)
	       cout << "newControls " << count << " = " << newControls << endl;


	    //----------------------------------- ADVANCE TO NEXT STEP --------------------------------------
	    manager.setInitialTime(initialStepTime);
	    manager.setFinalTime(finalStepTime);
	    manager.integrate(si);

	    // Update current state
	    osimModel.getMultibodySystem().realize(si, Stage::Acceleration);
	    osimModel.getSimbodyEngine().getPosition
	       (si, osimModel.getBodySet().get("linkage2"), eefPoint, eefPos);
	    osimModel.getSimbodyEngine().getVelocity
	       (si, osimModel.getBodySet().get("linkage2"), eefPoint, eefVel);

	    /* cout << "Message: Advanced to next step" << endl; */

	    //----------------------------------- UPDATE LOG FILES ------------------------------------------
	    // Store State variables
	    manager.getStateStorage().print("States.sto");
	    /* cout << "Message: Stored State variables" << endl; */

	    // Store controls, forces and moment arms
	    for (int i = 0; i < osimModel.getActuators().getSize(); i++) {
	       updctrls[i] = osimModel.getActuators().get(i).getControl(si); 
	       updforces[i] = updctrls[i] * osimModel.getActuators().get(i).getOptimalForce();
	    }
	    updMomentArms = momentArmsSet(si, osimModel);
	    /* cout << "Message: Moment arm update loop ended" << endl; */

	    _controlStore->store(stepNum, si.getTime(), numControls, &updctrls[0]);
	    _controlStore->print("Controls.sto");
	    _forceStore->store(stepNum, si.getTime(), numControls, &updforces[0]);
	    _forceStore->print("Forces.sto");
	    _momentArmsStore->store(stepNum, si.getTime(), nbMomentArms, &updMomentArms(0));
	    _momentArmsStore->print("MomentArms.sto");

	    /* cout << "Message: Stored controls, forces and moment arms" << endl; */

	    // Store Cartesian state of the end-effector
	    osimModel.getSimbodyEngine().getPosition
	       (si, osimModel.getBodySet().get("linkage2"), eefPoint, updPosCartesian);
	    osimModel.getSimbodyEngine().getVelocity
	       (si, osimModel.getBodySet().get("linkage2"), eefPoint, updVelCartesian);
	    osimModel.getSimbodyEngine().getAcceleration
	       (si, osimModel.getBodySet().get("linkage2"), eefPoint, updAccCartesian);

	    if (printlog) {
	       cout << "pos " << count << "= " << updPosCartesian << 
		  "\t" << "ref pos " << count << "= " << refPos[count] <<
		  "\t" << "vel " << count << "= " << updVelCartesian <<
		  "\t" << "ref vel " << count << "= " << refVel[count] <<
		  endl;
	    }

	    _posCartesianStore->store(stepNum, si.getTime(), 3, &updPosCartesian[0] );
	    _posCartesianStore->print("PosCartesian.sto");
	    _velCartesianStore->store(stepNum, si.getTime(), 3, &updVelCartesian[0] );
	    _velCartesianStore->print("VelCartesian.sto");
	    _accCartesianStore->store(stepNum, si.getTime(), 3, &updAccCartesian[0] );
	    _accCartesianStore->print("AccCartesian.sto");
	    _refPosCartesianStore->store(stepNum, si.getTime(), 3, &refPos[count].get(0) );
	    _refPosCartesianStore->print("RefPosCartesian.sto");
	    _refVelCartesianStore->store(stepNum, si.getTime(), 3, &refVel[count].get(0));
	    _refVelCartesianStore->print("RefVelCartesian.sto");
	    _refAccCartesianStore->store(stepNum, si.getTime(), 3, &refAcc[count].get(0));
	    _refAccCartesianStore->print("RefAccCartesian.sto");

	    /* cout << "Message: Updated log files" << endl; */

	    //-------------------------------------- UPDATE TIME --------------------------------------------
	    initialStepTime += stepSize;
	    finalStepTime = initialStepTime + stepSize;
	 }		
      }
   }
   catch (OpenSim::Exception ex)
   {
      std::cout << ex.getMessage() << std::endl;
      return 1;
   }
   catch (SimTK::Exception::Base ex)
   {
      std::cout << ex.getMessage() << std::endl;
      return 1;
   }
   catch (std::exception ex)
   {
      std::cout << ex.what() << std::endl;
      return 1;
   }
   catch (...)
   {
      std::cout << "UNRECOGNIZED EXCEPTION" << std::endl;
      return 1;
   }
}
