#include <OpenSim/OpenSim.h>
#include <Simbody.h>
#include <cmath>
#include <qpOASES.hpp>

#include <iostream>
#include <fstream>
#include <cstdlib>

using namespace OpenSim;
using namespace SimTK;
using namespace std;
USING_NAMESPACE_QPOASES

int calcTrajVecLength()
{
	string s;
	int sTotal(0);

	ifstream in;
	in.open("velocity.dat");

	while(!in.eof()) {
		getline(in, s);
		sTotal ++;	
	}
	return sTotal - 1;
}

void refVx(vector<double> &vx)
{
	ifstream file_in("velocity.dat");
	if (!file_in)
	{
		cerr << "Uh oh, file .dat could not be opened for reading!" << endl;
		exit(1);
	}

	// Create the vector, initialized from the numbers in the file:
	vector<double> fileData((istream_iterator<double>(file_in)),
			istream_iterator<double>());
	/* vector<double> vx; */
	for (int count = 0; count < fileData.size(); count+=6)
	{
		vx.push_back(fileData[count]);
	}
	/* return vx; */
}

void printvector(vector<double> &vect)
{
	for (vector<double>::const_iterator iter = vect.begin(); iter != vect.end(); ++iter)
		cout << *iter << endl;
}

int main()
{
	int trajVecLength = calcTrajVecLength();
	cout << "trajVecLength = " << trajVecLength << endl;
	/* vector<double> vx = refVx(); */
	vector<double> vectx;
	refVx(vectx);
	printvector(vectx);
}
