#include <OpenSim/OpenSim.h>
#include <Simbody.h>
#include <cmath>
#include <qpOASES.hpp>

#include <iostream>
#include <fstream>
#include <cstdlib>
#include <vector>

using namespace OpenSim;
using namespace SimTK;
using namespace std;
USING_NAMESPACE_QPOASES


//----------------------------------------- OPTIONS --------------------------------------------
bool simulation = 1;
bool useVisualizer = 0;		// cause segmentation fault !!!

/** Set initial state
 * default: data 
 * 1: all 0 
 * 2: knee 0.1
 * 3: all 0
 * 4: knee 0.3
 **/
int setInitialState = 1;

/* Set initial control
 * 1: gravity compensation
 * 2: all 0
 * 3: all 0.3
*/
int setInitCtrl = 2;


/** Set test case
 * default: data
 * 1: origin (0,0,0)
 * 2: vertical
 * 3: pose
 **/
int testCase = 3;
int TrajLength = 500;


//-------------------------------------- OPTIMIZATION SETTINGS ---------------------------------
// Control sampling time
double h = 1e-3;
bool dynamicConstraint = 1;

//-------------------------------------- QPOASES OPTIMIZATION ----------------------------------
bool setqpOASES = 1;

//----------------------------------- BUILT-IN OPTIMIZATION ------------------------------------
/** Set OpenSim built-in optimizer
 * 1: Interior Point
 * 2: Limited-memory Broyden-Fletcher-Goldfarb-Shanno (LBFGS)
 * 3: LBFGS with simple bound constraints (LBFGSB)
 * 4: C implementation of sequential quadratic programming (CFSQP)
 * 5: Covariance matrix adaptation, evolution strategy (CMAES)
 **/
int setOptimizerAlgorithm = 1;
bool setCostFuncFormulationType = 0;		// 1: integrator || 0 : qp

//----------------------------------------- WEIGHTS --------------------------------------------
SimTK::Real randomweight = 1e-3;
/* Task weight */
SimTK::Real taskw  = 1;

/* Control input */
SimTK::Real lanklew = 1e-2;
SimTK::Real ranklew = 1e-2;
SimTK::Real lkneew  = 1e-9;
SimTK::Real rkneew  = 1e-9;

/* Contact wrench */
SimTK::Real wrenchw0 = randomweight;
SimTK::Real wrenchw1 = randomweight;
SimTK::Real wrenchw2 = randomweight;
SimTK::Real wrenchw3 = randomweight;
SimTK::Real wrenchw4 = randomweight;
SimTK::Real wrenchw5 = randomweight;

//---------------------------------------- END-EFFECTOR ----------------------------------------
Vec3 eefPoint(0,0.43,0);		// in body frame

//------------------------------------- CONTACT POINTS -----------------------------------------
/** Contact points in foot frame
 * Contact sphere radius 0.003 m
 * Contact sphere center position in foot frame Y = -0.005
 **/
int nc = 1;
Vec3 lHeelPt(-0.07,-0.008,-0.02);
Vec3 rHeelPt(-0.07,-0.008,0.02);
Vec3 lPadPt(0.06,-0.008,-0.02);
Vec3 rPadPt(0.06,-0.008,0.02);

//------------------------------------- INTEGRATOR OPTIONS -------------------------------------
unsigned stepNum = 1;
const Real stepSize = 1e-3;
double initialStepTime = 0.0;
double finalStepTime = initialStepTime + stepSize;


//------------------------------------- FEEDBACK CONTROLERS ------------------------------------
Mat33 Kp(64); // position gain
Mat33 Kv(16); // velocity gain

//-------------------------------------- MODEL PROPERTIES --------------------------------------

//---------------------------------------- MISCELLANEOUS ---------------------------------------


//==============================================================================================
//				                    TRAJECTORY GENERATION - KDL
//==============================================================================================
int calcTrajVecLength() {
    string s;
    int sTotal(0);

    ifstream in;
    in.open("velocity.dat");

    while(!in.eof()) {
        getline(in, s);
        sTotal ++;	
    }
    return sTotal - 1;
}


void generateTrajectry ( Vector_<Vec3> &refPos, Vector_<Vec3> &refVel, Vector_<Vec3> &refAcc ) {
    // Read reference acceleration
    ifstream acc_in("acceleration.dat");
    if (!acc_in) {
        cerr << "Uh oh, file .dat could not be opened for reading!" << endl;
        exit(1);
    }
    // Create the vector, initialized from the numbers in the file:
    std::vector<double> accData (( std::istream_iterator<double>(acc_in)),
            std::istream_iterator<double>());
    int i = 0;
    for (int count = 0; count < accData.size(); count+=6) {
        refAcc[i] = Vec3( accData[count], accData[count+1], 0); i++;
    }

    // Read reference velocity
    ifstream vel_in("velocity.dat");
    if (!vel_in) {
        cerr << "Uh oh, file .dat could not be opened for reading!" << endl;
        exit(1);
    }
    // Create the vector, initialized from the numbers in the file:
    std::vector<double> velData (( std::istream_iterator<double>(vel_in)),
            std::istream_iterator<double>());
    i = 0;
    for (int count = 0; count < velData.size(); count+=6) {
        refVel[i] = Vec3( velData[count], velData[count+1], 0);
        i++;
    }

    // Read reference position
    ifstream traj_in("position.dat");
    if (!traj_in) {
        cerr << "Uh oh, file .dat could not be opened for reading!" << endl;
        exit(1);
    }

    std::vector<double> posData (( std::istream_iterator<double>(traj_in)),
            std::istream_iterator<double>());
    int j = 0;
    for (int count = 0; count < posData .size(); count+=16) {
        refPos[j] = Vec3( posData [count+3], posData [count+7], 0);
        j++;
    }
}

//==============================================================================================
// 			                            REARRANGE VECTOR
//==============================================================================================

// Convert Vec3 to Vector
SimTK::Vector vec3ToVector(Vec3& vec){
    SimTK::Vector vecOut(3);
    for (int i = 0; i < 3; i++){
        vecOut(i) = vec(i);
    }
    return vecOut;
}

//==============================================================================================
// 			                          OPTIMIZATION SYSTEM
//==============================================================================================

/** Moment arms set
**/
SimTK::Vector momentArmVec(State& si, Model& osimModel) {
    // Prepare actuators - dynamic cast
    PathActuator& leftAnkleMuscle = dynamic_cast<PathActuator&>
        (osimModel.getActuators().get(0));
    PathActuator& rightAnkleMuscle = dynamic_cast<PathActuator&>
        (osimModel.getActuators().get(1));
    PathActuator& leftKneeMuscle = dynamic_cast<PathActuator&>
        (osimModel.getActuators().get(2));
    PathActuator& rightKneeMuscle = dynamic_cast<PathActuator&>
        (osimModel.getActuators().get(3));

    // Prepare coordinates
    Coordinate& ankle_ = osimModel.getCoordinateSet().get(6);
    Coordinate& knee_ = osimModel.getCoordinateSet().get(7);

    // Moment arm vector
    int nbAct = osimModel.getNumControls();
    SimTK::Vector momentArms(nbAct);
    momentArms (0) = leftAnkleMuscle.computeMomentArm(si, ankle_);
    momentArms (1) = rightAnkleMuscle.computeMomentArm(si, ankle_);
    momentArms (2) = leftKneeMuscle.computeMomentArm(si, knee_);
    momentArms (3) = rightKneeMuscle.computeMomentArm(si, knee_);
    return momentArms;
}

/** Matrix of maximal force of proportional path actuators
**/
SimTK::Matrix FmMat (State& si, Model& osimModel) {
    int nbAct = osimModel.getNumControls();
    SimTK::Matrix Fm (nbAct,nbAct);
    Fm = 0;
    for (int i = 0; i < nbAct; i++) {
        Fm(i,i) = osimModel.getActuators().get(i).getOptimalForce();
    }
    return Fm;
}

/** Multiply this matrix (nu x na) to associate generalized coordinates 
  * to muscles which actuate them.
  * The moment arms are already assigned a sign (+/-) according to the rotation
  * generated anti-clockwise/clockwise
  * generally to separate the floating base coordinates with the rest
  * Multiply this matrix with muscles forces vector to produce a torque vector.
**/
SimTK::Matrix momentArmMat (State& si, Model& osimModel) {
    int nbU = si.getNU();
    int nbAct = osimModel.getNumControls();
    SimTK::Vector momentArms = momentArmVec(si, osimModel);
    SimTK::Matrix MA_Mat (nbU,nbAct);
    // First 6 rows are 0 - free joint
    MA_Mat = 0;
    MA_Mat (6,0) = momentArms(0);
    MA_Mat (6,1) = momentArms(1);
    MA_Mat (7,2) = momentArms(2);
    MA_Mat (7,3) = momentArms(3);
    return MA_Mat;
} 
/** Selection matrix for propotional muscles
  * To be multiplied with control vector
**/

SimTK::Matrix selectionMatrixForProportionalMuscles (State& si, Model& osimModel) {
    return momentArmMat(si, osimModel) * FmMat(si, osimModel);
}

// Simple regulation matrix (weight matrix) for only control optimization
SimTK::Matrix simpleRegulationMatrix (Model& osimModel){
    int nbAct = osimModel.getNumControls();

    SimTK::Matrix regulationMat(nbAct,nbAct);

    regulationMat(0,0) = lanklew;
    regulationMat(1,1) = ranklew;
    regulationMat(2,2) = lkneew;
    regulationMat(3,3) = rkneew;

    return regulationMat;
}

// Regulation matrix (weight matrix) for all optimization variables
SimTK::Matrix regulationMatrix (Model& osimModel, State& si){
    int nbAct = osimModel.getNumControls();
    int size = nbAct + 6*nc;

    SimTK::Matrix regulationMat(size,size);
    regulationMat(0,0) = lanklew;
    regulationMat(1,1) = ranklew;
    regulationMat(2,2) = lkneew;
    regulationMat(3,3) = rkneew;

    regulationMat(4,4) = wrenchw0;
    regulationMat(5,5) = wrenchw1;
    regulationMat(6,6) = wrenchw2;
    regulationMat(7,7) = wrenchw3;
    regulationMat(8,8) = wrenchw4;
    regulationMat(9,9) = wrenchw5;

    return regulationMat;
}


/** Mass matrix
*/
SimTK::Matrix MassMatrix(State& si, Model& osimModel) {
    const SimbodyMatterSubsystem& matter = osimModel.getMatterSubsystem();
    SimTK::Matrix M_;
    matter.calcM(si,M_);
    return M_;
}


/** Inverse Mass matrix
*/
SimTK::Matrix InverseMassMatrix(State& si, Model& osimModel) {
    const SimbodyMatterSubsystem& matter = osimModel.getMatterSubsystem();
    SimTK::Matrix Minv;
    matter.calcMInv(si,Minv);
    return Minv;
}


/** Methods q_max, q_min, q_value, nu_value don't take into account the floating base
**/
SimTK::Vector q_max (Model& osimModel) {
    const CoordinateSet& CoordSet = osimModel.getCoordinateSet();
    SimTK::Vector qMax(CoordSet.getSize()-6);
    for (int i = 0; i < CoordSet.getSize()-6; i++) {
        qMax(i) = CoordSet.get(i+6).getRangeMax();
    }
    return qMax;
}

SimTK::Vector q_min (Model& osimModel) {
    const CoordinateSet& CoordSet = osimModel.getCoordinateSet();
    SimTK::Vector qMin(CoordSet.getSize()-6);
    for (int i = 0; i < CoordSet.getSize()-6; i++) {
        qMin(i) = CoordSet.get(i+6).getRangeMin();
    }
    return qMin;
}

SimTK::Vector q_value (Model& osimModel, State& s) {
    const CoordinateSet& CoordSet = osimModel.getCoordinateSet();
    SimTK::Vector qVal(CoordSet.getSize()-6);
    for (int i = 0; i < CoordSet.getSize()-6; i++) {
        qVal(i) = CoordSet.get(i+6).getValue(s);
    }
    return qVal;
}

SimTK::Vector nu_value (Model& osimModel, State& s) {
    const CoordinateSet& CoordSet = osimModel.getCoordinateSet();
    SimTK::Vector nuVal(CoordSet.getSize()-6);
    for (int i = 0; i < CoordSet.getSize()-6; i++) {
        nuVal(i) = CoordSet.get(i+6).getSpeedValue(s);
    }
    return nuVal;
}


//------------------------------ QUADRATIC PROGRAMING FORMULATION ------------------------------

/** This function calculates
 * Matrix H in which integrated the regulation terms
 * Vector g
 **/

/* M(q).du + ~G.mult = g(q) + T + gamma - cor(q,u)
 * --> du = Minv.(T + g + gamma - ~G.mult - cor)
 * Xd = J.u
 * --> Xdd = dJ.u + J.du
 * --> Xdd = dJ.u + J.Minv.(T + g + gamma - ~G.mult - cor)
 * --> Xdd = J.Minv.(T + gamma) + J.Minv.(g - ~G.mult - cor) + dJ.u
 * --> Xdd = J.Minv.S.u + J.Minv.~Jc.Fc + J.Minv.(g - ~G.mult - cor) + dJ.u
 * Xdd_des = Xdd_traj_ + p_gains_.( X_des - X_curr_) + d_gains_.( Xd_des - Xd_curr_)
 * compute min(u) || Xdd - Xdd_des ||² + u^T * W * u
 * Xdd - Xdd_des = P.x + k
 * With P = [0  J.Minv.S  J.Minv.~Jc]
 *      k = J.Minv.(g - ~G.mult - cor) + dJ.u
*/

/******************* Matrix A = [Ad Anuc Awcf]^T *********************/
void A_qpoases(Model& osimModel, State& si, SimTK::Matrix& A_qp) {
    const SimbodyMatterSubsystem& matter = osimModel.getMatterSubsystem();
    int na = osimModel.getNumControls();
    int nu = si.getNU();
    // Frame Jacobian of all contact points (containing both linear and rotational parts)
    SimTK::Matrix Jc;
    matter.calcFrameJacobian(si,MobilizedBodyIndex(1),Vec3(0),Jc);

    A_qp = 0.0;

    // Ad
    A_qp(0,0,nu,na) = selectionMatrixForProportionalMuscles(si,osimModel);
    A_qp(0,na,nu,6*nc) = ~Jc;

    // Awcf - pending for friction cone
}


/************************** ubA - lbA  ******************************/
void ubA_lbA(Model& osimModel, State& si, SimTK::Vector& ubA, SimTK::Vector& lbA) {
    const SimbodyMatterSubsystem& matter = osimModel.getMatterSubsystem();
    int na = osimModel.getNumControls();
    int nu = si.getNU();

    // ubA_d (equality constraint) 
    /** Gravtiy and Coriolis forces are on the rhs of the dynamic equation
      * They have the opposite sign of the actuator forces
    **/
    SimTK::Vector gr;
    matter.multiplyBySystemJacobianTranspose(si, osimModel.getGravityForce().getBodyForces(si), gr);
    SimTK::Vector cor;
    matter.calcResidualForceIgnoringConstraints(si, SimTK::Vector(0), SimTK::Vector_<SimTK::SpatialVec>(0), SimTK::Vector(0),cor);

    SimTK::Vector Mv;
    matter.multiplyByMInv(si, si.getUDot(), Mv);

    ubA(0,nu) = Mv + cor - gr;

    // ubA_wcf - pending for friction cone

    // lbA_nuc; lbA_d
    lbA = ubA;
    // lbA_wcf - pending for friction cone
}

/************************** ub - lb *******************************/
void ub_lb(Model& osimModel, State& si, SimTK::Vector& ub, SimTK::Vector& lb) {
    int na = osimModel.getNumControls();
    // ub_a
    ub(0,na) = 1.0;
    // ub_wc (arbitrary great value)
    ub(na,6*nc) = 1e2;

    // lb_a
    lb(0,na) = 0.0;
    // lb_wc
    lb(na,6*nc) = -1e2;
    // mx, mz, ty >= 0
    lb(na) = 0.0;
    lb(na+2) = 0.0;
    lb(na+4) = 0.0;
}

/** QP formulation with new set of optimization variables [nu,control,omega_c]
**/
void QPformulation(State& si, Model& osimModel, Vec3& desInput, SimTK::Matrix& H, SimTK::Vector& g) {
    const SimbodyMatterSubsystem& matter = osimModel.getMatterSubsystem();
    const int nb = matter.getNumBodies();
    int na = osimModel.getNumControls();

    SimTK::Matrix M_;
    matter.calcM(si,M_);

    // Jacobian at the end-effector - Thigh/Body Index 3
    SimTK::Matrix J_eef;
    matter.calcStationJacobian(si,MobilizedBodyIndex(3),eefPoint,J_eef);

    // Frame Jacobian of all contact points (containing both linear and rotational parts)
    SimTK::Matrix Jc;
    matter.calcFrameJacobian(si,MobilizedBodyIndex(1),Vec3(0),Jc);

    /** Gravtiy and Coriolis forces are on the rhs of the dynamic equation
      * They have the opposite sign of the actuator forces
    **/
    SimTK::Vector gr;
    matter.multiplyBySystemJacobianTranspose(si, osimModel.getGravityForce().getBodyForces(si), gr);
    SimTK::Vector cor;
    matter.calcResidualForceIgnoringConstraints(si, SimTK::Vector(0), SimTK::Vector_<SimTK::SpatialVec>(0), SimTK::Vector(0),cor);

    // gravity - coriolis
    SimTK::Vector _f =  gr - cor;

    // M^-1*(gravity - coriolis)
    SimTK::Vector MInvf;
    matter.multiplyByMInv(si, _f, MInvf);

    // J_eff*M^-1*(gravity - coriolis)
    Vec3 J_effMInvf = matter.multiplyByStationJacobian(si, MobilizedBodyIndex(3), eefPoint, MInvf);

    // d(J_eff)*u
    Vec3 J_effDotu = matter.calcBiasForStationJacobian(si, MobilizedBodyIndex(3), eefPoint);

    // Acceleration errors in form of Px + k
    // P = [J_eff*M^-1*D*Fm  J_eff*M^-1*~Jc]
    SimTK::Matrix P(3, na+6*nc);
    P(0,0,3,na) = J_eef * InverseMassMatrix(si,osimModel) * selectionMatrixForProportionalMuscles(si,osimModel);
    P(0,na,3,6*nc) = J_eef * InverseMassMatrix(si,osimModel) * ~Jc;

    // Construct vector k
    Vec3 k3 = J_effMInvf + J_effDotu - desInput;
    SimTK::Vector k = vec3ToVector(k3);

    // Matrix H
    H = ~P * P;
    H = H * taskw + regulationMatrix(osimModel,si);


    // Vector g
    g = ~P * k * taskw;
}


//----------------------------------- QPOASES CLASSES ------------------------------------------

/** QPOASES optimization class with new set of optimization variables [nu,control,omega_c]
**/
class OneLegqpOASESOpt{

    private:
        State& si;
        Model& osimModel;
        Vec3& desAcc;
    public:

        OneLegqpOASESOpt(State& s, Model& aModel, Vec3& adesAcc): 
            si(s), osimModel(aModel), desAcc(adesAcc) { }

        // Task: Move to a point
        SimTK::Vector optimize(){
            int na = osimModel.getNumControls();

            SimTK::Matrix H_;
            SimTK::Vector g_;

            QPformulation(si, osimModel, desAcc, H_, g_);

            // New QP problem with variable [nu,tau,omegac_c]
            int nbTotalOptVar = na+6*nc;

            SimTK::Vector ub_(nbTotalOptVar);
            SimTK::Vector lb_(nbTotalOptVar);
            ub_lb(osimModel, si, ub_, lb_);

            // Convert OpenSim matrices to qpOASES matrices
            real_t H_qp[nbTotalOptVar*nbTotalOptVar];
            real_t g_qp[nbTotalOptVar];
            real_t lb_qp[nbTotalOptVar];
            real_t ub_qp[nbTotalOptVar];

            for (int i = 0; i < nbTotalOptVar; i++) {
                for (int j = 0; j < nbTotalOptVar; j++)
                    H_qp[i*nbTotalOptVar+j] = H_(i,j);
                g_qp[i] = g_(i);
                lb_qp[i] = lb_(i);
                ub_qp[i] = ub_(i);
            }

            // A, lbA, ubA, lb, ub need to specify dimensions
            int nbRwA = si.getNU();
            SimTK::Matrix A_(nbRwA, nbTotalOptVar);
            SimTK::Vector ubA_(nbRwA);
            SimTK::Vector lbA_(nbRwA);

            A_qpoases(osimModel, si, A_);
            ubA_lbA(osimModel, si, ubA_, lbA_);

            real_t A_qp[nbRwA*nbTotalOptVar];
            real_t lbA_qp[nbRwA];
            real_t ubA_qp[nbRwA];


            for (int i = 0; i < nbRwA; i++) {
                for (int j = 0; j < nbTotalOptVar; j++)
                    A_qp[i*nbRwA+j] = A_(i,j);
                lbA_qp[i] = lbA_(i);
                ubA_qp[i] = ubA_(i);
            }

            /* Setting up QProblem object. */
            QProblem twolinksfoot(nbTotalOptVar,nbRwA);
            /* QProblemB twolinksfoot(nbTotalOptVar); */

            Options options;
            options.printLevel = PrintLevel(0);
            twolinksfoot.setOptions(options);

            /* Solve first QP. */
            real_t cputime = 1.0;
            int_t nWSR = 100;
            /* twolinksfoot.init(H_qp,g_qp,lb_qp,ub_qp,nWSR,&cputime); */
            twolinksfoot.init(H_qp,g_qp,A_qp,lb_qp,ub_qp,lbA_qp,ubA_qp,nWSR,&cputime);

            /* Get and print solution of first QP. */
            real_t xOpt[nbTotalOptVar];
            twolinksfoot.getPrimalSolution(xOpt);

            // Retrieve actuator controls
            SimTK::Vector controlOpt(na);
            for (int i = 0; i < na; i++) {
                controlOpt(i) = xOpt[i];
            }
            return controlOpt;
        }
        ~OneLegqpOASESOpt() {}
};


//-------------------------------- OPENSIM BUILT IN OPTIMIZATION -------------------------------

class OpensimOptimizationSystem : public OptimizerSystem {

    private:
        int numControls;
        State& si;
        Model& osimModel;
        Vec3& desInput;

    public:
        OpensimOptimizationSystem(int numParameters, State& s, Model& aModel, Vec3& desAcc): 
            numControls(numParameters), OptimizerSystem(numParameters), 
            si(s), osimModel(aModel), desInput(desAcc) { }

        int objectiveFunc(const SimTK::Vector &newControls, bool new_coefficients, Real& f) const {
            SimTK::Matrix H_;
            SimTK::Vector g_;

            QPformulation(si, osimModel, desInput, H_, g_);

            // SimTK bullshit bug: vector*matrix*vector can't give an double/int
            SimTK::Vector stupidVector = ~newControls * H_ * newControls;
                f = 1/2 * stupidVector(0) + ~newControls * g_ ;
            return(0);
        }
};

//==============================================================================================
// 				                                MAIN
//==============================================================================================


int main()
{
    try {
        std::clock_t startTime = std::clock();

        //------------------------------------- LOAD MODEL -------------------------------------

        Model osimModel("OneLeg.osim");
        osimModel.setUseVisualizer(useVisualizer);

        //----------------------------------- INITIALIZATION -----------------------------------

        SimTK::State& si = osimModel.initSystem();

        //--------------------------------- REFERENCE TRAJECTORY -------------------------------

        /* Choose a type of reference
         * default : trajectory from data files
         * 1 : destination point at the origin
         * 2 : destination point at the top of the linkage 2 when the system is straight up vertically
         **/ 

        int trajVecLength;
        if (testCase == 0) { trajVecLength = calcTrajVecLength(); }
        else { trajVecLength = TrajLength; }
        Vector_<Vec3> refPos(trajVecLength);
        Vector_<Vec3> refVel(trajVecLength);
        Vector_<Vec3> refAcc(trajVecLength);

        switch (testCase) {
            default:
                generateTrajectry(refPos, refVel, refAcc);
                break;
            case 1:
                for (int count = 0; count < trajVecLength; ++count) {
                    refPos[count] = Vec3(-0.07,0.9,0.0); 
                    refVel[count] = Vec3(0);
                    refAcc[count] = Vec3(0); 
                }
                break;
            case 2:
                for (int count = 0; count < trajVecLength; ++count) {
                    refPos[count] = Vec3(-0.01,0.800,0.0); 
                    refVel[count] = Vec3(0);
                    refAcc[count] = Vec3(0); 
                }
                break;
            case 3:
                for (int count = 0; count < trajVecLength; ++count) {
                    refPos[count] = Vec3(0.0,0.41+0.43+0.005+0.09,0.0); 
                    refVel[count] = Vec3(0);
                    refAcc[count] = Vec3(0); 
                }
                break;
        }


        //------------------------------- INITIAL ANGLE POSITIONS ------------------------------

        double anklePos_init;
        double ankleVel_init;
        double kneePos_init;
        double kneeVel_init;

        switch (setInitialState) {
            default:
                anklePos_init = -20*SimTK::Pi/180;
                ankleVel_init = 0;
                kneePos_init = -30*SimTK::Pi/180;
                kneeVel_init = 0;
                break;
            case 1:
                anklePos_init = 0;
                ankleVel_init = 0;
                kneePos_init = 0;
                kneeVel_init = 0;
                break;
            case 2:
                anklePos_init = 0;
                ankleVel_init = 0;
                kneePos_init = 0.1;
                kneeVel_init = 0;
                break;
        }

        osimModel.getCoordinateSet().get("ankle_rotZ").setValue(si, anklePos_init);
        osimModel.getCoordinateSet().get("knee_rotZ").setValue(si, kneePos_init);
        osimModel.getCoordinateSet().get("ankle_rotZ").setSpeedValue(si, ankleVel_init);
        osimModel.getCoordinateSet().get("knee_rotZ").setSpeedValue(si, kneeVel_init);

        //------------------------------------ ACTUATORS ---------------------------------------

        int numControls = osimModel.getNumControls();

        // Get control bound set and initialize controls
        int bound_size = numControls + 6*nc;
        SimTK::Vector lower_bounds(bound_size);
        SimTK::Vector upper_bounds(bound_size);
        ub_lb(osimModel, si, upper_bounds, lower_bounds);

        // Set initial controls
        SimTK::Vector initialControls(numControls);

        switch (setInitCtrl) {
          case 1:
            initialControls(0) = 0;
            initialControls(1) = 0.063;
            initialControls(2) = 0;
            initialControls(3) = 0.063;
            break;
          case 2:
            initialControls(0) = 0;
            initialControls(1) = 0;
            initialControls(2) = 0;
            initialControls(3) = 0;
            break;
          case 3:
            initialControls(0) = 0.3;
            initialControls(1) = 0.3;
            initialControls(2) = 0.3;
            initialControls(3) = 0.3;
            break;
        }
        osimModel.updDefaultControls() = initialControls;

        //------------------------------------ VISUALIZER --------------------------------------
        if (useVisualizer) {
            osimModel.updMatterSubsystem().setShowDefaultGeometry(true);
            Visualizer& viz = osimModel.updVisualizer().updSimbodyVisualizer();
            viz.setBackgroundType(viz.SolidColor);
            viz.setBackgroundColor(Cyan);
        }

        // Angular state variables
        Vec3 eefPos;
        Vec3 eefVel;
        osimModel.getMultibodySystem().realize(si, Stage::Acceleration);
        osimModel.getSimbodyEngine().getPosition
            (si, osimModel.getBodySet().get("linkage2"), eefPoint, eefPos);
        osimModel.getSimbodyEngine().getVelocity
            (si, osimModel.getBodySet().get("linkage2"), eefPoint, eefVel);


        //------------------------------------- LOG FILES --------------------------------------
        // Construct control storage
        Array<string> columnLabels;
        Storage* _controlStore = new Storage(1023,"controls");
        columnLabels.append("time");
        for(int i=0;i<osimModel.getActuators().getSize();i++)
            columnLabels.append(osimModel.getActuators().get(i).getName());
        _controlStore->setColumnLabels(columnLabels);

        // Construct force storage
        Storage* _forceStore = new Storage(1023,"forces");
        _forceStore->setColumnLabels(columnLabels);

        // Initialize storage files
        SimTK::Vector updctrls(osimModel.getActuators().getSize());
        SimTK::Vector updforces(osimModel.getActuators().getSize());
        osimModel.getMultibodySystem().realize(si, Stage::Acceleration);

        for (int i = 0; i < osimModel.getActuators().getSize(); i++) {
            updctrls[i] = osimModel.getActuators().get(i).getControl(si);
            updforces[i] = updctrls[i] * osimModel.getActuators().get(i).getOptimalForce();
        } 
        /* cout << updctrls << endl; */

        _controlStore->store(stepNum, 0.0,osimModel.getActuators().getSize(), &updctrls[0]);
        _controlStore->print("controls.sto");
        _forceStore->store(stepNum, 0.0,osimModel.getActuators().getSize(), &updforces[0]);
        _forceStore->print("forces.sto");

        // Construct Cartesian state storage of the end-effector
        Array<string> posColumnLabels;
        Storage* _posCartesianStore = new Storage(1023,"PosCartesian");
        posColumnLabels.append("time");
        posColumnLabels.append("pX");
        posColumnLabels.append("pY");
        posColumnLabels.append("pZ");
        _posCartesianStore->setColumnLabels(posColumnLabels);

        Array<string> velColumnLabels;
        Storage* _velCartesianStore = new Storage(1023,"VelCartesian");
        velColumnLabels.append("time");
        velColumnLabels.append("vX");
        velColumnLabels.append("vY");
        velColumnLabels.append("vZ");
        _velCartesianStore->setColumnLabels(velColumnLabels);

        Array<string> accColumnLabels;
        Storage* _accCartesianStore = new Storage(1023,"AccCartesian");
        accColumnLabels.append("time");
        accColumnLabels.append("aX");
        accColumnLabels.append("aY");
        accColumnLabels.append("aZ");
        _accCartesianStore->setColumnLabels(accColumnLabels);

        Array<string> refPosColumnLabels;
        Storage* _refPosCartesianStore = new Storage(1023,"refstatePos");
        refPosColumnLabels.append("time");
        refPosColumnLabels.append("pXr");
        refPosColumnLabels.append("pYr");
        refPosColumnLabels.append("pZr");
        _refPosCartesianStore->setColumnLabels(refPosColumnLabels);

        Array<string> refVelColumnLabels;
        Storage* _refVelCartesianStore = new Storage(1023,"refstateVel");
        refVelColumnLabels.append("time");
        refVelColumnLabels.append("vXr");
        refVelColumnLabels.append("vYr");
        refVelColumnLabels.append("vZr");
        _refVelCartesianStore->setColumnLabels(refVelColumnLabels);

        Array<string> refAccColumnLabels;
        Storage* _refAccCartesianStore = new Storage(1023,"refstateAcc");
        refAccColumnLabels.append("time");
        refAccColumnLabels.append("aXr");
        refAccColumnLabels.append("aYr");
        refAccColumnLabels.append("aZr");
        _refAccCartesianStore->setColumnLabels(refAccColumnLabels);

        // Initialize state storage
        Vec3 updPosCartesian(0);
        Vec3 updVelCartesian(0);
        Vec3 updAccCartesian(0);
        osimModel.getSimbodyEngine().getPosition
            (si, osimModel.getBodySet().get("linkage2"), eefPoint, updPosCartesian);
        osimModel.getSimbodyEngine().getVelocity
            (si, osimModel.getBodySet().get("linkage2"), eefPoint, updVelCartesian);
        osimModel.getSimbodyEngine().getAcceleration
            (si, osimModel.getBodySet().get("linkage2"), eefPoint, updAccCartesian);

        _posCartesianStore->store(stepNum, 0.0, 3, &updPosCartesian[0]);
        _posCartesianStore->print("posCartesian.sto");
        _velCartesianStore->store(stepNum, 0.0, 3, &updVelCartesian[0]);
        _velCartesianStore->print("velCartesian.sto");
        _accCartesianStore->store(stepNum, 0.0, 3, &updAccCartesian[0]);
        _accCartesianStore->print("accCartesian.sto");

        _refPosCartesianStore->store(stepNum, 0.0, 3, &refPos[0].get(0));
        _refPosCartesianStore->print("refPosCartesian.sto");
        _refVelCartesianStore->store(stepNum, 0.0, 3, &refVel[0].get(0));
        _refVelCartesianStore->print("refVelCartesian.sto");
        _refAccCartesianStore->store(stepNum, 0.0, 3, &refAcc[0].get(0));
        _refAccCartesianStore->print("refAccCartesian.sto");

        // Moment arm storage
        Array<string> momentArms;
        Storage* _momentArmsStore = new Storage(1023,"momentArms");
        momentArms.append("time");
        momentArms.append("leftAnkleMuscle");
        momentArms.append("rightAnkleMuscle");
        momentArms.append("leftKneeMuscle");
        momentArms.append("rightKneeMuscle");
        _momentArmsStore->setColumnLabels(momentArms);

        // Initialize moment arm storage
        SimTK::Vector updMomentArms = momentArmVec(si, osimModel);
        int nbMomentArms = numControls;
        _momentArmsStore->store(stepNum, 0.0, nbMomentArms, &updMomentArms(0));
        _momentArmsStore->print("momentArms.sto");

        // Torque storage
        Array<string> torques;
        Storage* _torqueStore = new Storage(1023,"torques");
        torques.append("time");
        torques.append("ankle");
        torques.append("knee");
        _torqueStore->setColumnLabels(torques);

        // Initialize torque storage
        SimTK::Vector updTorques(numControls/2);
        updTorques(0) = updctrls(0) * updMomentArms(0) + updctrls(1) * updMomentArms(1);
        updTorques(1) = updctrls(2) * updMomentArms(2) + updctrls(3) * updMomentArms(3);
        _torqueStore->store(stepNum, 0.0, numControls/2, &updTorques(0));
        _torqueStore->print("torques.sto");

        // Gravity storage
        Array<string> gravity;
        Storage* _gravityStore = new Storage(1023,"gravity");
        gravity.append("time");
        gravity.append("ankle");
        gravity.append("knee");
        _gravityStore->setColumnLabels(gravity);

        const SimbodyMatterSubsystem& matter = osimModel.getMatterSubsystem();
        SimTK::Vector gr;
        matter.multiplyBySystemJacobianTranspose(
            si, osimModel.getGravityForce().getBodyForces(si), gr);
        _gravityStore->store(stepNum, 0.0, numControls/2, &gr(6));
        _gravityStore->print("gravity.sto");

        /* // Coriolis storage */
        /* Array<string> coriolis; */
        /* Storage* _coriolisStore = new Storage(1023,"coriolis"); */
        /* coriolis.append("time"); */
        /* coriolis.append("ankle"); */
        /* coriolis.append("knee"); */
        /* _coriolisStore->setColumnLabels(coriolis); */

        /* SimTK::Vector cor; */
        /* matter.calcResidualForceIgnoringConstraints( */
        /*     si, SimTK::Vector(0), SimTK::Vector_<SimTK::SpatialVec>(0), */
        /*     SimTK::Vector(0),cor); */

        /* _coriolisStore->store(stepNum, 0.0, numControls/2, &cor(6)); */
        /* _coriolisStore->print("coriolis.sto"); */

        // Create the force reporter
        ForceReporter* reporter = new ForceReporter(&osimModel);
        osimModel.addAnalysis(reporter);

        //-------------------------------- INTEGRATOR MANAGER ----------------------------------
        SimTK::RungeKuttaMersonIntegrator integ(osimModel.getMultibodySystem());
        integ.setAccuracy(1.0e-4);
        Manager manager(osimModel, integ);

        //------------------------------------ SIMULATION --------------------------------------
        if (simulation) {
            // start timing
            clock_t startTime = clock();

            for (int count = 0; count < trajVecLength; ++count) {

                //--------------------------- TASK SERVOING ------------------------------------

                // Compute desired trajectory from PD controller
                Vec3 desAcc = Kp * (refPos[count] - eefPos) 
                    + Kv * (refVel[count] - eefVel) 
                    + refAcc[count];

                //---------------------------- OPTIMIZATION ------------------------------------

                SimTK::Vector newControls(bound_size, 0.0);

                if (setqpOASES) {
                    OneLegqpOASESOpt sys(si, osimModel, desAcc);
                    osimModel.updDefaultControls() = sys.optimize();
                }
                else {
                    OptimizerAlgorithm optimizerAlg;
                    switch (setOptimizerAlgorithm) {
                        case 1:
                            optimizerAlg = InteriorPoint;
                            break;
                        case 2:
                            optimizerAlg = LBFGS;
                            break; 
                        case 3:
                            optimizerAlg = LBFGSB;
                            break;
                        case 4:
                            optimizerAlg = CFSQP;
                            break;
                        case 5:
                            optimizerAlg = CMAES;
                            break;
                    }
                    OpensimOptimizationSystem sys(bound_size, si, osimModel, desAcc);
                    sys.setParameterLimits(lower_bounds, upper_bounds);

                    Optimizer opt(sys, optimizerAlg);

                    // Specify settings for the optimizer
                    opt.setConvergenceTolerance(0.001);
                    opt.useNumericalGradient(true);
                    opt.setMaxIterations(200);
                    opt.setLimitedMemoryHistory(500);

                    Real f;	
                    f = opt.optimize(newControls);
                    cout << "--> f = " << f << endl;
                }
                /* cout << "--> newControls = " << newControls << endl; */
                cout << "--> newControls = " << osimModel.updDefaultControls() << endl;

                //-------------------------- ADVANCE TO NEXT STEP ------------------------------

                manager.setInitialTime(initialStepTime);
                manager.setFinalTime(finalStepTime);
                manager.integrate(si);

                // Update current state
                osimModel.getMultibodySystem().realize(si, Stage::Acceleration);
                osimModel.getSimbodyEngine().getPosition
                    (si, osimModel.getBodySet().get("linkage2"), eefPoint, eefPos);
                osimModel.getSimbodyEngine().getVelocity
                    (si, osimModel.getBodySet().get("linkage2"), eefPoint, eefVel);

                //---------------------------- UPDATE LOG FILES --------------------------------

                // Save the forces
                reporter->getForceStorage().print("forceReporter.mot"); 

                // Store State variables
                manager.getStateStorage().print("states.sto");
                /* cout << "Message: Stored State variables" << endl; */

                // Store controls, forces and moment arms
                for (int i = 0; i < osimModel.getActuators().getSize(); i++) {
                    updctrls[i] = osimModel.getActuators().get(i).getControl(si); 
                    updforces[i] = updctrls[i] * osimModel.getActuators().get(i).getOptimalForce();
                }
                updMomentArms = momentArmVec(si, osimModel);
                updTorques(0) = updctrls(0) * updMomentArms(0) + updctrls(1) * updMomentArms(1);
                updTorques(1) = updctrls(2) * updMomentArms(2) + updctrls(3) * updMomentArms(3);
                /* cout << "Message: Moment arm update loop ended" << endl; */

                _controlStore->store(stepNum, si.getTime(), numControls, &updctrls[0]);
                _controlStore->print("controls.sto");
                _forceStore->store(stepNum, si.getTime(), numControls, &updforces[0]);
                _forceStore->print("forces.sto");
                _momentArmsStore->store(stepNum, si.getTime(), nbMomentArms, &updMomentArms(0));
                _momentArmsStore->print("momentArms.sto");
                _torqueStore->store(stepNum, si.getTime(), numControls/2, &updTorques(0));
                _torqueStore->print("torques.sto");

                /* cout << "Message: Stored controls, forces and moment arms" << endl; */

                // Store Cartesian state of the end-effector
                osimModel.getSimbodyEngine().getPosition
                    (si, osimModel.getBodySet().get("linkage2"), eefPoint, updPosCartesian);
                osimModel.getSimbodyEngine().getVelocity
                    (si, osimModel.getBodySet().get("linkage2"), eefPoint, updVelCartesian);
                osimModel.getSimbodyEngine().getAcceleration
                    (si, osimModel.getBodySet().get("linkage2"), eefPoint, updAccCartesian);

                _posCartesianStore->store(stepNum, si.getTime(), 3, &updPosCartesian[0] );
                _posCartesianStore->print("posCartesian.sto");
                _velCartesianStore->store(stepNum, si.getTime(), 3, &updVelCartesian[0] );
                _velCartesianStore->print("velCartesian.sto");
                _accCartesianStore->store(stepNum, si.getTime(), 3, &updAccCartesian[0] );
                _accCartesianStore->print("accCartesian.sto");
                _refPosCartesianStore->store(stepNum, si.getTime(), 3, &refPos[count].get(0) );
                _refPosCartesianStore->print("refPosCartesian.sto");
                _refVelCartesianStore->store(stepNum, si.getTime(), 3, &refVel[count].get(0));
                _refVelCartesianStore->print("refVelCartesian.sto");
                _refAccCartesianStore->store(stepNum, si.getTime(), 3, &refAcc[count].get(0));
                _refAccCartesianStore->print("refAccCartesian.sto");

                matter.multiplyBySystemJacobianTranspose(
                    si, osimModel.getGravityForce().getBodyForces(si), gr);
                _gravityStore->store(stepNum, si.getTime(), numControls/2, &gr(6));
                _gravityStore->print("gravity.sto");

                //-------------------------------- UPDATE TIME ---------------------------------

                initialStepTime += stepSize;
                finalStepTime = initialStepTime + stepSize;
            }		
            // end timing
            cout << "simulation time = " << 1.e3*(clock()-startTime)/CLOCKS_PER_SEC << "ms" << endl;
        }
    }
    catch (OpenSim::Exception ex)
    {
        std::cout << ex.getMessage() << std::endl;
        return 1;
    }
    catch (SimTK::Exception::Base ex)
    {
        std::cout << ex.getMessage() << std::endl;
        return 1;
    }
    catch (std::exception ex)
    {
        std::cout << ex.what() << std::endl;
        return 1;
    }
    catch (...)
    {
        std::cout << "UNRECOGNIZED EXCEPTION" << std::endl;
        return 1;
    }
}
