#include <OpenSim/OpenSim.h>
#include <Simbody.h>
#include <cmath>

using namespace OpenSim;
using namespace SimTK;
using namespace std;


int main()
{
	try {

        Model osimModel("leg_tg.osim");
		// enable the model visualizer see the model in action
		osimModel.setUseVisualizer(true);

		/* osimModel.updMatterSubsystem().setShowDefaultGeometry(true); */
		/* Visualizer& viz = osimModel.updVisualizer().updSimbodyVisualizer(); */
		/* viz.setBackgroundType(viz.SolidColor); */
		/* viz.setBackgroundColor(Cyan); */


        // Force reporter
        ForceReporter* reporter = new ForceReporter(&osimModel);
        osimModel.addAnalysis(reporter);

		// Initialize system
		SimTK::State& si = osimModel.initSystem();


        //------------------------------ TEST POSITION FUNCTIONS --------------------------------
        double foot_hy = 0.09;
        double linkage1_length = 0.41;
        double ankle_positionX_in_foot = -0.03;
        double sphere_radius = 0.005;
        double floating_base_origin_ty = sphere_radius + foot_hy + linkage1_length/2;

        cout << "-- floating base origin ty  = " << floating_base_origin_ty << endl;
        cout << "-- eefPos Y calculated = " << floating_base_origin_ty + linkage1_length/2 << endl;

        Vec3 eefPoint (0, linkage1_length, 0);

        Vec3 fb_Pos;
        Vec3 eefPos;
        Transform linkage1_transform;

        osimModel.getMultibodySystem().realize(si, Stage::Position);

        osimModel.getSimbodyEngine().getPosition (si, osimModel.getBodySet().get("linkage1"), eefPoint, eefPos);
        osimModel.getSimbodyEngine().getPosition (si, osimModel.getBodySet().get("linkage1"), Vec3(0, linkage1_length/2, 0), fb_Pos);
        linkage1_transform = osimModel.getSimbodyEngine().getTransform (si, osimModel.getBodySet().get("linkage1"));

        cout << "-- fb_Pos = " << fb_Pos << endl;
        cout << "-- eefPos = " << eefPos << endl;
        cout << "-- linkage 1 transform = " << linkage1_transform << endl;

        /* Vec3 eefPos_ground; */
        /* osimModel.getSimbodyEngine().transformPosition */
        /*     (si, */ 
        /*      osimModel.getBodySet().get("linkage2"), */ 
        /*      eefPos, */
        /*      osimModel.getBodySet().get("ground"), */ 
        /*      eefPos_ground */
        /*     ); */

        /* cout << "-- eefPos_ground = " << eefPos_ground << endl; */

		// Set new initial states
		double anklePos_init = 10 * SimTK::Pi / 180;
		double fj_rz_init = -10 * SimTK::Pi / 180;
        double fj_ty_compensante = (linkage1_length/2) * (1 - cos(fj_rz_init));

		osimModel.getCoordinateSet().get("ankle_rotZ").setValue(si, anklePos_init);
		osimModel.getCoordinateSet().get("fj_rz").setValue(si, fj_rz_init);
		osimModel.getCoordinateSet().get("fj_ty").setValue(si, floating_base_origin_ty - fj_ty_compensante);


        osimModel.getMultibodySystem().realize(si, Stage::Position);

        osimModel.getSimbodyEngine().getPosition (si, osimModel.getBodySet().get("linkage1"), eefPoint, eefPos);
        osimModel.getSimbodyEngine().getPosition (si, osimModel.getBodySet().get("linkage1"), Vec3(0, linkage1_length/2, 0), fb_Pos);
        linkage1_transform = osimModel.getSimbodyEngine().getTransform (si, osimModel.getBodySet().get("linkage1"));

        cout << "-- fj_ty_compensante = " << fj_ty_compensante << endl; 
        cout << "-- eefPos X calculated = " << - linkage1_length/2 * sin (fj_rz_init) << endl;
        cout << "-- eefPos Y calculated = " << sphere_radius + foot_hy + linkage1_length * cos (anklePos_init) << endl;

        cout << "-- fb_Pos = " << fb_Pos << endl;
        cout << "-- eefPos = " << eefPos << endl;
        cout << "-- linkage 1 transform = " << linkage1_transform << endl;

		// Integrator
		SimTK::RungeKuttaMersonIntegrator integ(osimModel.getMultibodySystem());
		integ.setAccuracy(1.0e-4);
		
		// Manager
		Manager manager(osimModel,integ);
		manager.setInitialTime(0.0);
		manager.setFinalTime(2.0);
		manager.integrate(si);
        manager.getStateStorage().print("states_leg_tg.sto");

        reporter->getForceStorage().print("forces_leg_tg.mot");

	}
	catch (OpenSim::Exception ex)
	{
		std::cout << ex.getMessage() << std::endl;
		return 1;
	}
	catch (SimTK::Exception::Base ex)
	{
		std::cout << ex.getMessage() << std::endl;
		return 1;
	}
	catch (std::exception ex)
	{
		std::cout << ex.what() << std::endl;
		return 1;
	}
	catch (...)
	{
		std::cout << "UNRECOGNIZED EXCEPTION" << std::endl;
		return 1;
	}
	std::cout << "OpenSim example completed successfully" << std::endl;
	std::cout << "Press return to continue" << std::endl;
	std::cin.get();
	return 0;
}
