#include <OpenSim/OpenSim.h>
#include <Simbody.h>
#include <cmath>
#include <qpOASES.hpp>

#include <iostream>
#include <fstream>
#include <cstdlib>
#include <vector>

using namespace OpenSim;
using namespace SimTK;
using namespace std;
USING_NAMESPACE_QPOASES


//----------------------------------------- OPTIONS --------------------------------------------
bool simulation = 1;
bool useVisualizer = 0;		// cause segmentation fault !!!
bool verifyInitSys = 0;
bool printRefTrajectory = 0;
bool printInitialCtrl = 0;
bool printInitialState = 0;
bool printlog = 0;
bool printDesAcc = 0;
bool printNewCtrls = 1;
bool printDbgQPForm = 0;
bool printQPmatrices = 0;
bool printContactFr = 0;

/** Set initial state
 * default: data 
 * 1: all 0 (the only available)
 * 2: knee 0.1
 * 3: all 0
 * 4: knee 0.3
 **/
int setInitialState = 1;

/* Set initial control
 * 1: gravity compensation
 * 2: all 0
 * 3: all 0.3
*/
int setInitCtrl = 2;


/** Set test case
 * default: data
 * 1: origin (0,0,0)
 * 2: 1.5m
 * 3: pose
 **/
int testCase = 2;
int TrajLength = 500;


//------------------------------------ OPTIMIZATION SETTINGS -----------------------------------
// Control sampling time
double h = 1e-3;
bool dynamicConstraint = 1;

//------------------------------------ QPOASES OPTIMIZATION ------------------------------------
bool setqpOASES = 1;
bool printqpOAESOptions = 0;
bool modifyqpOASESOptions = 0;
bool printMatrixQPOASES = 0;

//----------------------------------- BUILT-IN OPTIMIZATION ------------------------------------
/** Set OpenSim built-in optimizer
 * 1: Interior Point
 * 2: Limited-memory Broyden-Fletcher-Goldfarb-Shanno (LBFGS)
 * 3: LBFGS with simple bound constraints (LBFGSB)
 * 4: C implementation of sequential quadratic programming (CFSQP)
 * 5: Covariance matrix adaptation, evolution strategy (CMAES)
 **/
int setOptimizerAlgorithm = 3;
bool setCostFuncFormulationType = 0;		// 1: integrator || 0 : qp

//----------------------------------------- WEIGHTS --------------------------------------------
SimTK::Real weightTerm1 = 1;	// go easy with it !!!
SimTK::Real weightTerm2 = 1;	// go easy with it !!!
SimTK::Real weightTerm3 = 1;	// go easy with it !!!
SimTK::Real weightTerm4 = 1;	// go easy with it !!!

//---------------------------------------- END-EFFECTOR ----------------------------------------
Vec3 eefPoint(0,0.5,0);		    // in body frame
Vec3 topHead(0,0.224,0);		// on top of the head

//--------------------------------------- CONTACT POINTS ---------------------------------------
/** Contact points in foot frame
 * Contact sphere radius 0.003 m
 * Contact sphere center position in foot frame Y = -0.005
 **/
int nc = 4;
Vec3 lHeelPt(-0.07,-0.008,-0.02);
Vec3 rHeelPt(-0.07,-0.008,0.02);
Vec3 lPadPt(0.06,-0.008,-0.02);
Vec3 rPadPt(0.06,-0.008,0.02);

//------------------------------------- INTEGRATOR OPTIONS -------------------------------------
unsigned stepNum = 1;
const Real stepSize = 1e-3;
double initialStepTime = 0.0;
double finalStepTime = initialStepTime + stepSize;


//------------------------------------- FEEDBACK CONTROLERS ------------------------------------
Mat33 Kp(64); // position gain
Mat33 Kv(16); // velocity gain

//-------------------------------------- MODEL PROPERTIES --------------------------------------
double g = -9.80655;
bool floatingBase = 0;

//---------------------------------------- MISCELLANEOUS ---------------------------------------
SimTK::Matrix analyticMassMatrix(2,2);


//==============================================================================================
//				                    TRAJECTORY GENERATION - KDL
//==============================================================================================
int calcTrajVecLength() {
    string s;
    int sTotal(0);

    ifstream in;
    in.open("velocity.dat");

    while(!in.eof()) {
        getline(in, s);
        sTotal ++;	
    }
    return sTotal - 1;
}

void printvector(vector<double> &vect) {
    for (vector<double>::const_iterator iter = vect.begin(); iter != vect.end(); ++iter)
        cout << *iter << endl;
}

void generateTrajectry ( Vector_<Vec3> &refPos, Vector_<Vec3> &refVel, Vector_<Vec3> &refAcc ) {
    // Read reference acceleration
    ifstream acc_in("acceleration.dat");
    if (!acc_in) {
        cerr << "Uh oh, file .dat could not be opened for reading!" << endl;
        exit(1);
    }
    // Create the vector, initialized from the numbers in the file:
    std::vector<double> accData (( std::istream_iterator<double>(acc_in)),
            std::istream_iterator<double>());
    int i = 0;
    for (int count = 0; count < accData.size(); count+=6) {
        refAcc[i] = Vec3( accData[count], accData[count+1], 0); i++;
    }

    // Read reference velocity
    ifstream vel_in("velocity.dat");
    if (!vel_in) {
        cerr << "Uh oh, file .dat could not be opened for reading!" << endl;
        exit(1);
    }
    // Create the vector, initialized from the numbers in the file:
    std::vector<double> velData (( std::istream_iterator<double>(vel_in)),
            std::istream_iterator<double>());
    i = 0;
    for (int count = 0; count < velData.size(); count+=6) {
        refVel[i] = Vec3( velData[count], velData[count+1], 0);
        i++;
    }

    // Read reference position
    ifstream traj_in("position.dat");
    if (!traj_in) {
        cerr << "Uh oh, file .dat could not be opened for reading!" << endl;
        exit(1);
    }

    std::vector<double> posData (( std::istream_iterator<double>(traj_in)),
            std::istream_iterator<double>());
    int j = 0;
    for (int count = 0; count < posData .size(); count+=16) {
        refPos[j] = Vec3( posData [count+3], posData [count+7], 0);
        j++;
    }
}

//==============================================================================================
// 			                            REARRANGE VECTOR
//==============================================================================================

// Convert Vec3 to Vector
SimTK::Vector vec3ToVector(Vec3& vec){
    SimTK::Vector vecOut(3);
    for (int i = 0; i < 3; i++){
        vecOut(i) = vec(i);
    }
    return vecOut;
}

// Array of bodies on which are located contact points
// Until now the only body is the foot
Array_<MobilizedBodyIndex> bodyCt() {
    Array_<MobilizedBodyIndex> onBodyB(nc);
    onBodyB[0] = MobilizedBodyIndex(1);
    onBodyB[1] = MobilizedBodyIndex(1);
    onBodyB[2] = MobilizedBodyIndex(1);
    onBodyB[3] = MobilizedBodyIndex(1);
    return onBodyB; 
}

// Array of frame origin points of task frames (contact points)
Array_<Vec3> originCtPtFrm() {
    Array_<Vec3> originAoInB(nc);
    originAoInB[0] = lHeelPt;
    originAoInB[1] = rHeelPt;
    originAoInB[2] = lPadPt;
    originAoInB[3] = rPadPt;
    return originAoInB;
}

//==============================================================================================
// 			                          OPTIMIZATION SYSTEM
//==============================================================================================

/** Foot-ground contact forces **/
SimTK::Vector contactForces(State& si, Model& osimModel) {
    // Retrieve contact forces
    OpenSim::HuntCrossleyForce &lHeelContact = (OpenSim::HuntCrossleyForce &)osimModel.getForceSet().get("LeftHeelContactForce");
    OpenSim::HuntCrossleyForce &rHeelContact = (OpenSim::HuntCrossleyForce &)osimModel.getForceSet().get("RightHeelContactForce");
    OpenSim::HuntCrossleyForce &lPadContact = (OpenSim::HuntCrossleyForce &)osimModel.getForceSet().get("LeftPadContactForce");
    OpenSim::HuntCrossleyForce &rPadContact = (OpenSim::HuntCrossleyForce &)osimModel.getForceSet().get("RightPadContactForce");
    Array<double> lHeelForceRec = lHeelContact.getRecordValues(si);
    Array<double> rHeelForceRec = rHeelContact.getRecordValues(si);
    Array<double> lPadForceRec = lPadContact.getRecordValues(si);
    Array<double> rPadForceRec = rPadContact.getRecordValues(si);
    
    Vec3 lHeelForce(lHeelForceRec[6],lHeelForceRec[7],lHeelForceRec[8]);
    Vec3 lHeelMoment(lHeelForceRec[9],lHeelForceRec[10],lHeelForceRec[11]);

    Vec3 rHeelForce(rHeelForceRec[6],rHeelForceRec[7],rHeelForceRec[8]);
    Vec3 rHeelMoment(rHeelForceRec[9],rHeelForceRec[10],rHeelForceRec[11]);

    Vec3 lPadForce(lPadForceRec[6],lPadForceRec[7],lPadForceRec[8]);
    Vec3 lPadMoment(lPadForceRec[9],lPadForceRec[10],lPadForceRec[11]);

    Vec3 rPadForce(rPadForceRec[6],rPadForceRec[7],rPadForceRec[8]);
    Vec3 rPadMoment(rPadForceRec[9],rPadForceRec[10],rPadForceRec[11]);

    if (printContactFr) {
        cout << "-- lHeelForceRec = " << lHeelForceRec << endl;
        cout << "-- lHeelForce = " << lHeelForce << endl;
        cout << "-- lHeelMoment = " << lHeelMoment << endl;
        cout << "-- rHeelForce = " << rHeelForce << endl;
        cout << "-- rHeelMoment = " << rHeelMoment << endl;
        cout << "-- lPadForce = " << lPadForce << endl;
        cout << "-- lPadMoment = " << lPadMoment << endl;
        cout << "-- rPadForce = " << rPadForce << endl;
        cout << "-- rPadMoment = " << rPadMoment << endl;
    }


    // Generalized forces resulting from foot ground contact forces
    const SimbodyMatterSubsystem& matter = osimModel.getMatterSubsystem();
    /* SimTK::Vector lHeelGnFr; */
    /* matter.multiplyByStationJacobianTranspose(si,MobilizedBodyIndex(1),lHeelPt,lHeelForce,lHeelGnFr); */
    /* SimTK::Vector rHeelGnFr; */
    /* matter.multiplyByStationJacobianTranspose(si,MobilizedBodyIndex(1),rHeelPt,rHeelForce,rHeelGnFr); */
    /* SimTK::Vector lPadGnFr; */
    /* matter.multiplyByStationJacobianTranspose(si,MobilizedBodyIndex(1),lPadPt,lPadForce,lPadGnFr); */
    /* SimTK::Vector rPadGnFr; */
    /* matter.multiplyByStationJacobianTranspose(si,MobilizedBodyIndex(1),rPadPt,rPadForce,rPadGnFr); */

    /* SimTK::Vector contact = lHeelGnFr + rHeelGnFr + lPadGnFr + rPadGnFr; */



    Array_<MobilizedBodyIndex> onBodyB = bodyCt();

    Array_<Vec3> originAoInB = originCtPtFrm();

    Vector_<SpatialVec> F_GAo(nc);
    F_GAo[0] = SpatialVec(lHeelMoment,lHeelForce);
    F_GAo[1] = SpatialVec(rHeelMoment,rHeelForce);
    F_GAo[2] = SpatialVec(lPadMoment,lPadForce);
    F_GAo[3] = SpatialVec(rPadMoment,rPadForce);
    
    SimTK::Vector contact;
    matter.multiplyByFrameJacobianTranspose(si, onBodyB, originAoInB, F_GAo, contact);
    return contact;
}


/** Moment arms set
**/
SimTK::Vector momentArmsSet(State& si, Model& osimModel) {
    // Prepare actuators - dynamic cast
    PathActuator& lTA = dynamic_cast<PathActuator&> (osimModel.getActuators().get(0));
    PathActuator& rTA = dynamic_cast<PathActuator&> (osimModel.getActuators().get(1));
    PathActuator& lSol = dynamic_cast<PathActuator&> (osimModel.getActuators().get(2));
    PathActuator& rSol = dynamic_cast<PathActuator&> (osimModel.getActuators().get(3));
    PathActuator& lVas = dynamic_cast<PathActuator&> (osimModel.getActuators().get(4));
    PathActuator& rVas = dynamic_cast<PathActuator&> (osimModel.getActuators().get(5));
    PathActuator& lGlu = dynamic_cast<PathActuator&> (osimModel.getActuators().get(6));
    PathActuator& rGlu = dynamic_cast<PathActuator&> (osimModel.getActuators().get(7));
    PathActuator& lHFL = dynamic_cast<PathActuator&> (osimModel.getActuators().get(8));
    PathActuator& rHFL = dynamic_cast<PathActuator&> (osimModel.getActuators().get(9));

    PathActuator& lGas = dynamic_cast<PathActuator&> (osimModel.getActuators().get(10));
    PathActuator& rGas = dynamic_cast<PathActuator&> (osimModel.getActuators().get(11));
    PathActuator& lHam = dynamic_cast<PathActuator&> (osimModel.getActuators().get(12));
    PathActuator& rHam = dynamic_cast<PathActuator&> (osimModel.getActuators().get(13));
    PathActuator& lRF = dynamic_cast<PathActuator&> (osimModel.getActuators().get(14));
    PathActuator& rRF = dynamic_cast<PathActuator&> (osimModel.getActuators().get(15));

    // Prepare coordinates
    Coordinate& lAnkle = osimModel.getCoordinateSet().get(0);
    Coordinate& rAnkle = osimModel.getCoordinateSet().get(1);
    Coordinate& lKnee = osimModel.getCoordinateSet().get(2);
    Coordinate& rKnee = osimModel.getCoordinateSet().get(3);
    Coordinate& lHip = osimModel.getCoordinateSet().get(4);
    Coordinate& rHip = osimModel.getCoordinateSet().get(5);

    // Moment arm vector
    // 10 uniaricular path actuators and 6 biarticular actuators
    int nbAct = 22;
    SimTK::Vector momentArms(nbAct);
    momentArms (0) = lTA.computeMomentArm(si, lAnkle);
    momentArms (1) = rTA.computeMomentArm(si, rAnkle);
    momentArms (2) = lSol.computeMomentArm(si, lAnkle);
    momentArms (3) = rSol.computeMomentArm(si, rAnkle);
    momentArms (4) = lVas.computeMomentArm(si, lKnee);
    momentArms (5) = rVas.computeMomentArm(si, rKnee);
    momentArms (6) = lGlu.computeMomentArm(si, lHip);
    momentArms (7) = rGlu.computeMomentArm(si, rHip);
    momentArms (8) = lHFL.computeMomentArm(si, lHip);
    momentArms (9) = rHFL.computeMomentArm(si, rHip);

    // lGas : 10
    momentArms (10) = lGas.computeMomentArm(si, lAnkle);
    momentArms (11) = lGas.computeMomentArm(si, lKnee);
    // rGas : 11
    momentArms (12) = rGas.computeMomentArm(si, rAnkle);
    momentArms (13) = rGas.computeMomentArm(si, rKnee);

    // lHam : 12
    momentArms (14) = lHam.computeMomentArm(si, lKnee);
    momentArms (15) = lHam.computeMomentArm(si, lHip);
    // rHam : 13
    momentArms (16) = rHam.computeMomentArm(si, rKnee);
    momentArms (17) = rHam.computeMomentArm(si, rHip);

    // lRF : 14
    momentArms (18) = lRF.computeMomentArm(si, lKnee);
    momentArms (19) = lRF.computeMomentArm(si, lHip);
    // rRF : 15
    momentArms (20) = rRF.computeMomentArm(si, rKnee);
    momentArms (21) = rRF.computeMomentArm(si, rHip);
    return momentArms;
}

/** Activate certain muscles to which associated a moment arm
  * The moment arms are already assigned a sign (+/-) according to the rotation
  * generated anti-clockwise/clockwise
  * Multiply this square matrix with muscles forces vector to produce a torque vector.
  * In case of coordinate actuators, moment arm is 1.
**/
SimTK::Matrix assignMomentArms (State& si, Model& osimModel) {
    SimTK::Vector momentArms = momentArmsSet(si, osimModel);
    int nbAct = osimModel.getNumControls();
    // Take into account biarticular actuators
    nbAct = nbAct + 6;
    SimTK::Matrix selectedMuscles (nbAct,nbAct);
    selectedMuscles = 0;
    for (int i = 0; i < nbAct; i++) {
        if (i < momentArms.size()) {
            selectedMuscles(i,i) = momentArms(i);
        }
        else {
            selectedMuscles(i,i) = 1;
        }
    }
    return selectedMuscles;
}

/** Multiply this matrix (nu x na) to associate generalized coordinates 
  * to muscles which actuate them.
  * generally to separate the floating base coordinates with the rest
**/
SimTK::Matrix selectCoord (State& si, Model& osimModel) {
    int nbU = si.getNU();
    int nbAct = osimModel.getNumControls() + 6;
    SimTK::Matrix selectedCoord (nbU,nbAct);
    // First 6 rows are 0 - free joint
    selectedCoord = 0;
    selectedCoord (6,0) = 1;
    selectedCoord (6,1) = 1;
    selectedCoord (7,2) = 1;
    selectedCoord (7,3) = 1;
    return selectedCoord;
} 
/** Selection matrix for propotional muscles
  * To be multiplied with control vector
**/

SimTK::Matrix selectionMatrixForProportionalMuscles (State& si, Model& osimModel) {
    return selectCoord(si, osimModel) * assignMomentArms(si, osimModel) * osimModel.getActuators().get(0).getOptimalForce();
}

/* Square matrix of optimal forces
*/

SimTK::Matrix optFrMat(Model& osimModel) {
    int nbAct = osimModel.getNumControls();
    SimTK::Matrix optFr(nbAct,nbAct);
    optFr = 0;
    for (int i = 0; i < nbAct; i++) {
        optFr(i,i) = osimModel.getActuators().get(i).getOptimalForce();
    }
    return optFr;
}

/** Selection matrix for actuators of different types
  * To be multiplied with control vector and square matrix of optimal forces
**/
SimTK::Matrix selectionMatrixForActuators (State& si, Model& osimModel) {
    SimTK::Vector momentArms = momentArmsSet(si, osimModel);
    int nbU = si.getNU();
    int nbAct = osimModel.getNumControls();
    SimTK::Matrix selectionMat(nbU,nbAct);
    selectionMat = 0;

    // left ankle
    selectionMat(0,0) = momentArms(0);
    selectionMat(0,2) = momentArms(2);
    selectionMat(0,10) = momentArms(10);
    // right ankle
    selectionMat(1,1) = momentArms(1);
    selectionMat(1,3) = momentArms(3);
    selectionMat(1,11) = momentArms(11);
    // left knee
    selectionMat(2,4) = momentArms(4);
    selectionMat(2,10) = momentArms(11);
    selectionMat(2,12) = momentArms(14);
    selectionMat(2,14) = momentArms(18);
    // right knee
    selectionMat(3,5) = momentArms(5);
    selectionMat(3,11) = momentArms(13);
    selectionMat(3,13) = momentArms(16);
    selectionMat(3,15) = momentArms(20);
    // left hip
    selectionMat(4,6) = momentArms(6);
    selectionMat(4,8) = momentArms(8);
    selectionMat(4,12) = momentArms(15);
    selectionMat(4,14) = momentArms(19);
    // right hip
    selectionMat(5,7) = momentArms(7);
    selectionMat(5,9) = momentArms(9);
    selectionMat(5,13) = momentArms(17);
    selectionMat(5,15) = momentArms(21);
    // neck
    selectionMat(6,20) = 1;
    // left shoulder
    selectionMat(7,16) = 1;
    // right shoulder
    selectionMat(8,17) = 1;
    // left elbow
    selectionMat(9,18) = 1;
    // right elbow
    selectionMat(10,19) = 1;

    return selectionMat;
}

// Regulation matrix (weight matrix)
SimTK::Matrix regulationMatrix (Model& osimModel){
    int nbAct = osimModel.getNumControls();
    SimTK::Matrix regulationMat(nbAct,nbAct);
    regulationMat = weightTerm1;
    regulationMat(1,1) = weightTerm2;
    regulationMat(2,2) = weightTerm3;
    regulationMat(3,3) = weightTerm4;
    return regulationMat;
}

/** Contact Jacobian composed of only translational part
**/
SimTK::Matrix ContactTranslationalJacobian(State& si, Model& osimModel) {
    const SimbodyMatterSubsystem& matter = osimModel.getMatterSubsystem();
    int nu = si.getNU();
    SimTK::Matrix Jc(3*nc, nu);
    matter.calcStationJacobian(si, bodyCt(), originCtPtFrm(), Jc);
    return Jc;
}


/** Jdotu at contact points composed of only translational part
**/
SimTK::Vector jdotuContact(State& si, Model& osimModel) {
    const SimbodyMatterSubsystem& matter = osimModel.getMatterSubsystem();
    SimTK::Vector jdotuct(3*nc);
    matter.calcBiasForStationJacobian(si, bodyCt(), originCtPtFrm(), jdotuct);
    return jdotuct;
}


/** Mass matrix
*/
SimTK::Matrix MassMatrix(State& si, Model& osimModel) {
    const SimbodyMatterSubsystem& matter = osimModel.getMatterSubsystem();
    SimTK::Matrix M_;
    matter.calcM(si,M_);
    return M_;
}


/** Inverse Mass matrix
*/
SimTK::Matrix InverseMassMatrix(State& si, Model& osimModel) {
    const SimbodyMatterSubsystem& matter = osimModel.getMatterSubsystem();
    SimTK::Matrix Minv;
    matter.calcMInv(si,Minv);
    return Minv;
}


/** Methods q_max, q_min, q_value, nu_value don't take into account the floating base
**/
SimTK::Vector q_max (Model& osimModel) {
    int nbFBCoord;
    if (floatingBase) {nbFBCoord = 6;}
    else {nbFBCoord = 0;}
    const CoordinateSet& CoordSet = osimModel.getCoordinateSet();
    SimTK::Vector qMax(CoordSet.getSize()-nbFBCoord);
    for (int i = 0; i < CoordSet.getSize()-nbFBCoord; i++) {
        qMax(i) = CoordSet.get(i+nbFBCoord).getRangeMax();
    }
    return qMax;
}

SimTK::Vector q_min (Model& osimModel) {
    int nbFBCoord;
    if (floatingBase) {nbFBCoord = 6;}
    else {nbFBCoord = 0;}
    const CoordinateSet& CoordSet = osimModel.getCoordinateSet();
    SimTK::Vector qMin(CoordSet.getSize()-nbFBCoord);
    for (int i = 0; i < CoordSet.getSize()-nbFBCoord; i++) {
        qMin(i) = CoordSet.get(i+nbFBCoord).getRangeMin();
    }
    return qMin;
}

SimTK::Vector q_value (Model& osimModel, State& s) {
    int nbFBCoord;
    if (floatingBase) {nbFBCoord = 6;}
    else {nbFBCoord = 0;}
    const CoordinateSet& CoordSet = osimModel.getCoordinateSet();
    SimTK::Vector qVal(CoordSet.getSize()-nbFBCoord);
    for (int i = 0; i < CoordSet.getSize()-nbFBCoord; i++) {
        qVal(i) = CoordSet.get(i+nbFBCoord).getValue(s);
    }
    return qVal;
}

SimTK::Vector nu_value (Model& osimModel, State& s) {
    int nbFBCoord;
    if (floatingBase) {nbFBCoord = 6;}
    else {nbFBCoord = 0;}
    const CoordinateSet& CoordSet = osimModel.getCoordinateSet();
    SimTK::Vector nuVal(CoordSet.getSize()-nbFBCoord);
    for (int i = 0; i < CoordSet.getSize()-nbFBCoord; i++) {
        nuVal(i) = CoordSet.get(i+nbFBCoord).getSpeedValue(s);
    }
    return nuVal;
}



//------------------------------ QUADRATIC PROGRAMING FORMULATION ------------------------------

/** This function calculates
 * Matrix H in which integrated the regulation terms
 * Vector g
 **/

// M(q).qdd = B(qd) + G(q) + T
// --> qdd = Minv.( T + B + G)
// Xd = J.qd
// --> Xdd = Jdot.qdot + J.qdd
// --> Xdd = Jdot.qdot + J.Minv.( T + B + G)
// --> Xdd = Jdot.qdot + J.Minv.T + J.Minv.(B + G )
// Xdd_des = Xdd_traj_ + p_gains_.( X_des - X_curr_) + d_gains_.( Xd_des - Xd_curr_)
// ==> We want to compute min(T) || Xdd - Xdd_des ||²
// If with replace, we can put it in the form ax + b
// With a = J.Minv
//      b = J.Minv.(B + G ) + Jdot.qdot - Xdd_des
// In reality, a is multiplied further by matrix of boundaries of forces (optimal forces)
// and selection matrix


/** QP with only actuator controls as optimization parameters
**/
void QPformulation(State& si, Model& osimModel, Vec3& desInput, SimTK::Matrix& H, SimTK::Vector& g, SimTK::Vector& ub, SimTK::Vector& lb) {
    const SimbodyMatterSubsystem& matter = osimModel.getMatterSubsystem();
    const int nb = matter.getNumBodies();

    SimTK::Matrix Minv;
    matter.calcMInv(si,Minv);

    SimTK::Matrix JS;
    matter.calcStationJacobian(si,MobilizedBodyIndex(6),topHead,JS);

    SimTK::Vector gr;
    matter.multiplyBySystemJacobianTranspose(si,osimModel.getGravityForce().getBodyForces(si),gr);

    SimTK::Vector cor;
    matter.calcResidualForceIgnoringConstraints(si,SimTK::Vector(0),SimTK::Vector_<SimTK::SpatialVec>(0),SimTK::Vector(0),cor);

    // Rhs forces
    SimTK::Vector f_app =  gr + cor;

    // Minv.(B+G)
    SimTK::Vector MInvf_app;
    matter.multiplyByMInv(si, f_app, MInvf_app);

    // J.Minv.(B+G)
    Vec3 JMInvf_app = matter.multiplyByStationJacobian(si,MobilizedBodyIndex(6),topHead,MInvf_app);

    // Jdot.qdot
    SimTK::Vec3 JSDotu = matter.calcBiasForStationJacobian(si,MobilizedBodyIndex(6),topHead);

    // Acceleration errors in form of ax + b
    int na = osimModel.getNumControls();
    int nu = si.getNU();
    SimTK::Matrix a_ = JS*Minv*selectionMatrixForActuators(si, osimModel)*optFrMat(osimModel);
    SimTK::Vec3 b3_ =  JMInvf_app + JSDotu - desInput;
    SimTK::Vector b_ = vec3ToVector(b3_);

    // Calculate H combined with regulation terms
    H = ~a_ * a_;
    /* H_ = H_ + regulationMatrix(osimModel); */
    g = ~a_ * b_;

    
    if (printDbgQPForm) { cout << "-- full_formulation" << endl;}


    /************************** ub *******************************/
    // ub_q
    /* ub(0,nu) = 2/h/h*(q_max(osimModel) - q_value(osimModel,si) - h * nu_value(osimModel,si)); */
    // ub_a
    ub = 1.0;
    /* ub(0,na) = 1.0; */
    if (printDbgQPForm) {cout << "-- ub = " << ub << endl;}

    /************************** lb ******************************/
    // lb_q
    /* lb(0,nu) = 2/h/h*(q_min(osimModel) - q_value(osimModel,si) - h * nu_value(osimModel,si)); */
    // lb_a
    lb(0,na-5) = 0.0;
    lb(na-5,5) = -1.0;
    if (printDbgQPForm) {cout << "-- lb = " << lb << endl;}
}

//------------------------------------ QPOASES CLASS -------------------------------------------

class OneLegqpOASESOpt{

    private:
        State& si;
        Model& osimModel;
        Vec3& desAcc;
    public:

        OneLegqpOASESOpt(State& s, Model& aModel, Vec3& adesAcc): 
            si(s), osimModel(aModel), desAcc(adesAcc) { }

        // Task: Move to a point
        Vector optimize(){
            int nbCtrl = osimModel.getActuators().getSize();
            real_t H[nbCtrl*nbCtrl];
            real_t g[nbCtrl];
            real_t lb[nbCtrl];
            real_t ub[nbCtrl];

            SimTK::Matrix H_;
            Vector g_;
            Vector ub_(nbCtrl);
            Vector lb_(nbCtrl);

            QPformulation(si, osimModel, desAcc, H_, g_, ub_, lb_);
            if (printQPmatrices) {
                cout << "-- QPOASES: H_ = " << H_ << endl;
                cout << "-- QPOASES: g_ = " << g_ << endl;
                cout << "-- QPOASES: ub_ = " << ub_ << endl;
                cout << "-- QPOASES: lb_ = " << lb_ << endl;
            } 

            // Construct matrices for QPOASES

            for (int i = 0; i < nbCtrl; i++) {
                for (int j = 0; j < nbCtrl; j++)
                    H[i*nbCtrl+j] = H_(i,j);
                g[i] = g_(i);
                lb[i] = lb_(i);
                ub[i] = ub_(i);
            }
            if (printMatrixQPOASES) {
                cout << "-- QPOASES: H = " << H << endl;
                cout << "-- QPOASES: g = " << g << endl;
                cout << "-- QPOASES: ub = " << ub << endl;
                cout << "-- QPOASES: lb = " << lb << endl;
            }

            /* Setting up QProblem object. */
            QProblemB wholebodyQP(nbCtrl);
            Options options;
            options.printLevel = PrintLevel(0);
            wholebodyQP.setOptions(options);

            /* Solve first QP. */
            real_t cputime = 1.0;
            int_t nWSR = 100;
            wholebodyQP.init(H,g,lb,ub,nWSR,&cputime);

            /* Get and print solution of first QP. */
            real_t xOpt[nbCtrl];
            wholebodyQP.getPrimalSolution(xOpt);

            // Retrive actuator controls
            Vector controlOpt(nbCtrl);
            for (int i = 0; i < nbCtrl; i++)
                controlOpt(i) = xOpt[i];
            return controlOpt;
        }
};


//-------------------------------- OPENSIM BUILT IN OPTIMIZATION -------------------------------

class OpensimOptimizationSystem : public OptimizerSystem {

    private:
        int numControls;
        State& si;
        Model& osimModel;
        Vec3& desInput;

    public:
        OpensimOptimizationSystem(int numParameters, State& s, Model& aModel, Vec3& desAcc): 
            numControls(numParameters), OptimizerSystem(numParameters), 
            si(s), osimModel(aModel), desInput(desAcc) { }

        int objectiveFunc(  const Vector &newControls, bool new_coefficients, Real& f ) const {

            if (setCostFuncFormulationType) 
            {
                // make a copy of out initial states
                State s = si;

                // Update the control values
                //newControls.dump("New Controls In:");
                osimModel.updDefaultControls() = newControls;

                // Create the integrator for the simulation.
                RungeKuttaMersonIntegrator integrator(osimModel.getMultibodySystem());
                integrator.setAccuracy(1.0e-4);
                integrator.setAllowInterpolation(false);

                // Integration without manager
                /* integrator.initialize(si); */
                /* integrator.stepBy(stepSize); */

                // Create a manager to run the simulation
                Manager manager(osimModel, integrator);
                manager.setInitialTime(initialStepTime);
                manager.setFinalTime(finalStepTime);
                manager.integrate(s);

                //osimModel.getControls(s).dump("Model Controls:");
                osimModel.getMultibodySystem().realize(s, Stage::Acceleration);

                Vec3 curAcc;
                // Don't forget state s for god sake !!!
                osimModel.getSimbodyEngine().getAcceleration
                    (s, osimModel.getBodySet().get("head"), topHead, curAcc);

                Vec3 err;
                err = desInput - curAcc;

                // Sum of forces produced by actuators
                /* for (int actNb = 0; actNb < osimModel.getNumControls(); actNb ++) */
                /* { */
                /*     f += osimModel.getActuators().get(actNb).getControl(s) * */ 
                /*         osimModel.getActuators().get(actNb).getOptimalForce(); */
                /* } */

                f = dot(err,err);
            }
            else
            {
                int nbCtrl = osimModel.getActuators().getSize();
                SimTK::Matrix H_;
                SimTK::Vector g_;
                SimTK::Vector ub_(nbCtrl);
                SimTK::Vector lb_(nbCtrl);
                QPformulation(si, osimModel, desInput, H_, g_,ub_,lb_);

                // SimTK bullshit bug: vector*matrix*vector can't give double/int result
                SimTK::Vector stupidVector = ~newControls * H_ * newControls;
                f = 1/2 * stupidVector(0) + ~newControls * g_ ;
            }
            return(0);
        }	

};

//==============================================================================================
// 				                               MAIN
//==============================================================================================


int main()
{
    try {
        std::clock_t startTime = std::clock();

        //----------------------------------------- LOAD MODEL ---------------------------------
        Model osimModel("WholeBodyNoFoot.osim");
        osimModel.setUseVisualizer(useVisualizer);

        //--------------------------------------- INITIALIZATION -------------------------------
        SimTK::State& si = osimModel.initSystem();
        if (verifyInitSys){ cout << "-- Initializing system - done" << endl;}

        //------------------------------------- REFERENCE TRAJECTORY ---------------------------

        /* Choose a type of reference
         * default : trajectory from data files
         * 1 : destination point at the origin
         * 2 : destination point at the top of the linkage 2 when the system is straight up vertically
         **/ 

        int trajVecLength;
        if (testCase == 0) { trajVecLength = calcTrajVecLength(); }
        else { trajVecLength = TrajLength; }
        Vector_<Vec3> refPos(trajVecLength);
        Vector_<Vec3> refVel(trajVecLength);
        Vector_<Vec3> refAcc(trajVecLength);

        switch (testCase) {
            default:
                generateTrajectry(refPos, refVel, refAcc);
                break;
            case 1:
                for (int count = 0; count < trajVecLength; ++count) {
                    refPos[count] = Vec3(0); 
                    refVel[count] = Vec3(0);
                    refAcc[count] = Vec3(0); 
                }
                break;
            case 2:
                for (int count = 0; count < trajVecLength; ++count) {
                    refPos[count] = Vec3(0,1.5,0); 
                    refVel[count] = Vec3(0);
                    refAcc[count] = Vec3(0); 
                }
                break;
            case 3:
                for (int count = 0; count < trajVecLength; ++count) {
                    refPos[count] = Vec3(-0.05,0.905,0); 
                    refVel[count] = Vec3(0);
                    refAcc[count] = Vec3(0); 
                }
                break;
        }

        if (printRefTrajectory)
        {
            cout << "-- Print trajectories from data files" << endl;
            for (int count = 0; count < trajVecLength; ++count) {
                cout << "refPos " << count << " " << refPos[count] << endl;
                cout << "refVel " << count << " " << refVel[count] << endl;
                cout << "refAcc " << count << " " << refAcc[count] << endl;
            }
        }

        //--------------------------------- INITIAL ANGLE POSITIONS ----------------------------
        double anklePos_init;
        double ankleVel_init;
        double kneePos_init;
        double kneeVel_init;
        double hipPos_init;
        double hipVel_init;

        switch (setInitialState) {
            default:
                anklePos_init = -20*SimTK::Pi/180;
                ankleVel_init = 0;
                kneePos_init = -30*SimTK::Pi/180;
                kneeVel_init = 0;
                /* hipPos_init = 10*SimTK::Pi/180; */
                /* hipVel_init = 0; */
                break;
            case 1:
                for (int i = 0; i < si.getNU(); i++) {
                    osimModel.getCoordinateSet().get(i).setValue(si,0.0);
                }
                break;
            case 2:
                anklePos_init = 0;
                ankleVel_init = 0;
                kneePos_init = -0.1;
                kneeVel_init = 0;
                /* hipPos_init = 0; */
                /* hipVel_init = 0; */
                break;
            case 3:
                anklePos_init = 0;
                ankleVel_init = 0;
                kneePos_init = 0;
                kneeVel_init = 0;
                /* hipPos_init = 0.1; */
                /* hipVel_init = 0; */
                break;
            case 4:
                anklePos_init = 0;
                ankleVel_init = 0;
                kneePos_init = 0.3;
                kneeVel_init = 0;
                /* hipPos_init = 0.1; */
                /* hipVel_init = 0.1; */
                break;
        }

        /* osimModel.getCoordinateSet().get("ankle_rotZ").setValue(si, anklePos_init); */
        /* osimModel.getCoordinateSet().get("knee_rotZ").setValue(si, kneePos_init); */
        /* osimModel.getCoordinateSet().get("ankle_rotZ").setSpeedValue(si, ankleVel_init); */
        /* osimModel.getCoordinateSet().get("knee_rotZ").setSpeedValue(si, kneeVel_init); */

        cout << "--nbU = " << si.getNU() << endl;
        cout << "--nbQ = " << si.getNQ() << endl;
        cout << "--nbY = " << si.getNY() << endl;
        cout << "-- actuator set = " << osimModel.getActuators() << endl;
        cout << "-- coordinate set = " << osimModel.getCoordinateSet() << endl;
        cout << "-- joint set = " << osimModel.getJointSet() << endl;
        cout << "-- body set = " << osimModel.getBodySet() << endl;
        cout << "-- class object = " << (osimModel.getActuators().get(2).getConcreteClassName() == "PathActuator") << endl;
        //--------------------------------------- ACTUATORS ------------------------------------
        int numControls = osimModel.getNumControls();
        cout << "numControls = " << numControls << endl;

        // Get control bound set and initialize controls
        Vector lower_bounds(numControls);
        Vector upper_bounds(numControls);
        Vector initialControls(numControls);

        for (int i = 0; i < numControls; i++) {
            lower_bounds(i) = osimModel.getActuators().get(i).getMinControl();
            upper_bounds(i) = osimModel.getActuators().get(i).getMaxControl();
            switch (setInitCtrl) {
                case 2:
                    initialControls(i) = 0;
                    break;
                case 3:
                    initialControls(i) = 0.3;
                    break;
            }
        }

        osimModel.updDefaultControls() = initialControls;

        if (printInitialCtrl)
        { cout << "Initial controls = " << osimModel.getDefaultControls() << endl; }

        //--------------------------------------- VISUALIZER -----------------------------------
        if (useVisualizer) {
            osimModel.updMatterSubsystem().setShowDefaultGeometry(true);
            Visualizer& viz = osimModel.updVisualizer().updSimbodyVisualizer();
            viz.setBackgroundType(viz.SolidColor);
            viz.setBackgroundColor(Cyan);
        }

        // Angular state variables
        Vec3 eefPos;
        Vec3 eefVel;
        osimModel.getMultibodySystem().realize(si, Stage::Acceleration);
        osimModel.getSimbodyEngine().getPosition
            (si, osimModel.getBodySet().get("head"), topHead, eefPos);
        osimModel.getSimbodyEngine().getVelocity
            (si, osimModel.getBodySet().get("head"), topHead, eefVel);

        if (printInitialState) {
            cout << "Initial end-effector position  = " << eefPos << endl;
            cout << "Initial end-effector velocity  = " << eefVel << endl;
        }


        //---------------------------------------- LOG FILES -----------------------------------
        // Construct control storage
        Array<string> columnLabels;
        Storage* _controlStore = new Storage(1023,"controls");
        columnLabels.append("time");
        for(int i=0;i<osimModel.getActuators().getSize();i++)
            columnLabels.append(osimModel.getActuators().get(i).getName());
        _controlStore->setColumnLabels(columnLabels);

        // Construct force storage
        Storage* _forceStore = new Storage(1023,"forces");
        _forceStore->setColumnLabels(columnLabels);

        // Initialize storage files
        Vector updctrls(osimModel.getActuators().getSize());
        Vector updforces(osimModel.getActuators().getSize());
        osimModel.getMultibodySystem().realize(si, Stage::Acceleration);

        for (int i = 0; i < osimModel.getActuators().getSize(); i++) {
            updctrls[i] = osimModel.getActuators().get(i).getControl(si);
            updforces[i] = updctrls[i] * osimModel.getActuators().get(i).getOptimalForce();
        } 
        /* cout << updctrls << endl; */

        _controlStore->store(stepNum, 0.0,osimModel.getActuators().getSize(), &updctrls[0]);
        _controlStore->print("Controls.sto");
        _forceStore->store(stepNum, 0.0,osimModel.getActuators().getSize(), &updforces[0]);
        _forceStore->print("Forces.sto");

        // Construct Cartesian state storage of the end-effector
        Array<string> posColumnLabels;
        Storage* _posCartesianStore = new Storage(1023,"PosCartesian");
        posColumnLabels.append("time");
        posColumnLabels.append("pX");
        posColumnLabels.append("pY");
        posColumnLabels.append("pZ");
        _posCartesianStore->setColumnLabels(posColumnLabels);

        Array<string> velColumnLabels;
        Storage* _velCartesianStore = new Storage(1023,"VelCartesian");
        velColumnLabels.append("time");
        velColumnLabels.append("vX");
        velColumnLabels.append("vY");
        velColumnLabels.append("vZ");
        _velCartesianStore->setColumnLabels(velColumnLabels);

        Array<string> accColumnLabels;
        Storage* _accCartesianStore = new Storage(1023,"AccCartesian");
        accColumnLabels.append("time");
        accColumnLabels.append("aX");
        accColumnLabels.append("aY");
        accColumnLabels.append("aZ");
        _accCartesianStore->setColumnLabels(accColumnLabels);

        Array<string> refPosColumnLabels;
        Storage* _refPosCartesianStore = new Storage(1023,"refstatePos");
        refPosColumnLabels.append("time");
        refPosColumnLabels.append("pXr");
        refPosColumnLabels.append("pYr");
        refPosColumnLabels.append("pZr");
        _refPosCartesianStore->setColumnLabels(refPosColumnLabels);

        Array<string> refVelColumnLabels;
        Storage* _refVelCartesianStore = new Storage(1023,"refstateVel");
        refVelColumnLabels.append("time");
        refVelColumnLabels.append("vXr");
        refVelColumnLabels.append("vYr");
        refVelColumnLabels.append("vZr");
        _refVelCartesianStore->setColumnLabels(refVelColumnLabels);

        Array<string> refAccColumnLabels;
        Storage* _refAccCartesianStore = new Storage(1023,"refstateAcc");
        refAccColumnLabels.append("time");
        refAccColumnLabels.append("aXr");
        refAccColumnLabels.append("aYr");
        refAccColumnLabels.append("aZr");
        _refAccCartesianStore->setColumnLabels(refAccColumnLabels);

        // Initialize state storage
        Vec3 updPosCartesian(0);
        Vec3 updVelCartesian(0);
        Vec3 updAccCartesian(0);
        osimModel.getSimbodyEngine().getPosition
            (si, osimModel.getBodySet().get("head"), topHead, updPosCartesian);
        osimModel.getSimbodyEngine().getVelocity
            (si, osimModel.getBodySet().get("head"), topHead, updVelCartesian);
        osimModel.getSimbodyEngine().getAcceleration
            (si, osimModel.getBodySet().get("head"), topHead, updAccCartesian);

        _posCartesianStore->store(stepNum, 0.0, 3, &updPosCartesian[0]);
        _posCartesianStore->print("PosCartesian.sto");
        _velCartesianStore->store(stepNum, 0.0, 3, &updVelCartesian[0]);
        _velCartesianStore->print("VelCartesian.sto");
        _accCartesianStore->store(stepNum, 0.0, 3, &updAccCartesian[0]);
        _accCartesianStore->print("AccCartesian.sto");

        _refPosCartesianStore->store(stepNum, 0.0, 3, &refPos[0].get(0));
        _refPosCartesianStore->print("RefPosCartesian.sto");
        _refVelCartesianStore->store(stepNum, 0.0, 3, &refVel[0].get(0));
        _refVelCartesianStore->print("RefVelCartesian.sto");
        _refAccCartesianStore->store(stepNum, 0.0, 3, &refAcc[0].get(0));
        _refAccCartesianStore->print("RefAccCartesian.sto");

        // Moment arm storage
        /* Array<string> momentArms; */
        /* Storage* _momentArmsStore = new Storage(1023,"momentArms"); */
        /* momentArms.append("time"); */
        /* momentArms.append("lTA"); */
        /* momentArms.append("rTA"); */
        /* momentArms.append("lSol"); */
        /* momentArms.append("rSol"); */
        /* momentArms.append("lVas"); */
        /* momentArms.append("rVas"); */
        /* momentArms.append("lGlu"); */
        /* momentArms.append("rGlu"); */
        /* momentArms.append("lHFL"); */
        /* momentArms.append("rHFL"); */
        /* _momentArmsStore->setColumnLabels(momentArms); */

        /* // Initialize moment arm storage */
        /* SimTK::Vector updMomentArms = momentArmsSet(si, osimModel); */
        /* int nbMomentArms = numControls; */
        /* _momentArmsStore->store(stepNum, 0.0, nbMomentArms, &updMomentArms(0)); */
        /* _momentArmsStore->print("MomentArms.sto"); */

        // Create the force reporter
        ForceReporter* reporter = new ForceReporter(&osimModel);
        osimModel.addAnalysis(reporter);

        //----------------------------------- INTEGRATOR MANAGER -------------------------------
        SimTK::RungeKuttaMersonIntegrator integ(osimModel.getMultibodySystem());
        integ.setAccuracy(1.0e-4);
        Manager manager(osimModel, integ);

        //--------------------------------------- SIMULATION -----------------------------------
        if (simulation) {
            // start timing
            clock_t startTime = clock();

            for (int count = 0; count < trajVecLength; ++count) {
                //------------------------------------- TASK SERVOING --------------------------
                // Compute desired trajectory from PD controller
                Vec3 desAcc = Kp * (refPos[count] - eefPos) 
                    + Kv * (refVel[count] - eefVel) 
                    + refAcc[count];

                if (printDesAcc)
                    cout << "desAcc = " << desAcc <<endl;

                /* cout << "Message: End task servoing" << endl; */

                //------------------------------------ OPTIMIZATION ----------------------------
                Vector newControls(osimModel.getActuators().getSize());
                if (setqpOASES) {
                    OneLegqpOASESOpt sys(si, osimModel, desAcc);
                    newControls = sys.optimize();
                }
                else {
                    OptimizerAlgorithm optimizerAlg;
                    switch (setOptimizerAlgorithm) {
                        case 1:
                            optimizerAlg = InteriorPoint;
                            break;
                        case 2:
                            optimizerAlg = LBFGS;
                            break; 
                        case 3:
                            optimizerAlg = LBFGSB;
                            break;
                        case 4:
                            optimizerAlg = CFSQP;
                            break;
                        case 5:
                            optimizerAlg = CMAES;
                            break;
                    }
                    OpensimOptimizationSystem 
                        sys(numControls, si, osimModel, desAcc);
                    sys.setParameterLimits( lower_bounds, upper_bounds );

                    Optimizer opt(sys, optimizerAlg);

                    // Specify settings for the optimizer
                    opt.setConvergenceTolerance(0.001);
                    opt.useNumericalGradient(true);
                    opt.setMaxIterations(200);
                    opt.setLimitedMemoryHistory(500);

                    Real f;	
                    f = opt.optimize(newControls);
                    /* cout <<  "f " << count << " = " << f << endl; */
                }
                osimModel.updDefaultControls() = newControls;

                if (printNewCtrls)
                    cout << "newControls " << count << " = " << newControls << endl;


                //----------------------------------- ADVANCE TO NEXT STEP ---------------------
                manager.setInitialTime(initialStepTime);
                manager.setFinalTime(finalStepTime);
                manager.integrate(si);

                // Update current state
                osimModel.getMultibodySystem().realize(si, Stage::Acceleration);
                osimModel.getSimbodyEngine().getPosition
                    (si, osimModel.getBodySet().get("head"), topHead, eefPos);
                osimModel.getSimbodyEngine().getVelocity
                    (si, osimModel.getBodySet().get("head"), topHead, eefVel);

                /* cout << "Message: Advanced to next step" << endl; */

                //----------------------------------- UPDATE LOG FILES -------------------------
                // Save the forces
                reporter->getForceStorage().print("forceReporter.mot"); 

                // Store State variables
                manager.getStateStorage().print("States.sto");
                /* cout << "Message: Stored State variables" << endl; */

                // Store controls, forces and moment arms
                for (int i = 0; i < osimModel.getActuators().getSize(); i++) {
                    updctrls[i] = osimModel.getActuators().get(i).getControl(si); 
                    updforces[i] = updctrls[i] * osimModel.getActuators().get(i).getOptimalForce();
                }
                /* updMomentArms = momentArmsSet(si, osimModel); */
                /* cout << "Message: Moment arm update loop ended" << endl; */

                _controlStore->store(stepNum, si.getTime(), numControls, &updctrls[0]);
                _controlStore->print("Controls.sto");
                _forceStore->store(stepNum, si.getTime(), numControls, &updforces[0]);
                _forceStore->print("Forces.sto");
                /* _momentArmsStore->store(stepNum, si.getTime(), nbMomentArms, &updMomentArms(0)); */
                /* _momentArmsStore->print("MomentArms.sto"); */

                /* cout << "Message: Stored controls, forces and moment arms" << endl; */

                // Store Cartesian state of the end-effector
                osimModel.getSimbodyEngine().getPosition
                    (si, osimModel.getBodySet().get("head"), topHead, updPosCartesian);
                osimModel.getSimbodyEngine().getVelocity
                    (si, osimModel.getBodySet().get("head"), topHead, updVelCartesian);
                osimModel.getSimbodyEngine().getAcceleration
                    (si, osimModel.getBodySet().get("head"), topHead, updAccCartesian);

                if (printlog) {
                    cout << "pos " << count << "= " << updPosCartesian << 
                        "\t" << "ref pos " << count << "= " << refPos[count] <<
                        "\t" << "vel " << count << "= " << updVelCartesian <<
                        "\t" << "ref vel " << count << "= " << refVel[count] <<
                        endl;
                }

                _posCartesianStore->store(stepNum, si.getTime(), 3, &updPosCartesian[0] );
                _posCartesianStore->print("PosCartesian.sto");
                _velCartesianStore->store(stepNum, si.getTime(), 3, &updVelCartesian[0] );
                _velCartesianStore->print("VelCartesian.sto");
                _accCartesianStore->store(stepNum, si.getTime(), 3, &updAccCartesian[0] );
                _accCartesianStore->print("AccCartesian.sto");
                _refPosCartesianStore->store(stepNum, si.getTime(), 3, &refPos[count].get(0) );
                _refPosCartesianStore->print("RefPosCartesian.sto");
                _refVelCartesianStore->store(stepNum, si.getTime(), 3, &refVel[count].get(0));
                _refVelCartesianStore->print("RefVelCartesian.sto");
                _refAccCartesianStore->store(stepNum, si.getTime(), 3, &refAcc[count].get(0));
                _refAccCartesianStore->print("RefAccCartesian.sto");

                /* cout << "Message: Updated log files" << endl; */

                //-------------------------------------- UPDATE TIME ---------------------------
                initialStepTime += stepSize;
                finalStepTime = initialStepTime + stepSize;
            }		
            // end timing
            cout << "simulation time = " << 1.e3*(clock()-startTime)/CLOCKS_PER_SEC << "ms" << endl;
        }
    }
    catch (OpenSim::Exception ex)
    {
        std::cout << ex.getMessage() << std::endl;
        return 1;
    }
    catch (SimTK::Exception::Base ex)
    {
        std::cout << ex.getMessage() << std::endl;
        return 1;
    }
    catch (std::exception ex)
    {
        std::cout << ex.what() << std::endl;
        return 1;
    }
    catch (...)
    {
        std::cout << "UNRECOGNIZED EXCEPTION" << std::endl;
        return 1;
    }
}
