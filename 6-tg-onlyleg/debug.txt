Loaded model Oneleg from file OneLeg_tg_2_changeBase.osim

End-effector reference
-- px_test = 0
-- py_test = 0.935

End-effector initial position
-- eefPos = ~[0,0.935,0]
-- eefVel = ~[0,0,0]

Initial state
-- fj_rz = 0
-- fj_rz_u = 0
-- ankle_rotZ = 0
-- ankle_rotZ_u = 0
-- knee_rotZ = 0
-- knee_rotZ_u = 0

% First 6 elements relate to the floating base

-- gravity = ~[0 0 -0.420705 0 -163.575 0 -0.420705 -0.420705]

-- coriolis = ~[0 0 0 0 0 0 0 0]


-- M = 
[1.22633 0.0195195 0 0 0 0.6331 0 0]
[0.0195195 0.0508563 0 0 0 -0.0429 0 0]
[0 0 1.25095 -0.6331 0.0429 0 0.596568 0.0618832]
[0 0 -0.6331 16.68 0 0 1.6244 0.06435]
[0 0 0.0429 0 16.68 0 0.0429 0.0429]
[0.6331 -0.0429 0 0 0 16.68 0 0]
[0 0 0.596568 1.6244 0.0429 0 0.596568 0.0618832]
[0 0 0.0618832 0.06435 0.0429 0 0.0618832 0.0354997]

-- Jc transpose = 
[1 0 0 0 0 -0.463]
[0 1 0 0 0 -0.03]
[0 0 1 0.463 0.03 0]
[0 0 0 1 0 0]
[0 0 0 0 1 0]
[0 0 0 0 0 1]
[0 0 1 0.463 0.03 0]
[0 0 1 0.053 0.03 0]

-- sign verification for contact wrench: coriolis - gravity = Jc transpose * gamma_contact
~[0 0 0.420705 0 163.575 0 0.420705 0.420705]



