reset
set terminal png 
set output "trajectory.png"
set title 'Trajectory of the end-effector'
set ylabel 'X (m)'
set xlabel 'Y (m)'
set grid
set key outside below; set key title "Legend"; set key box reverse; set key box lw 2 lc 4
plot "build/refPosCartesian.sto" using 2:3 with lines lw 4 lc rgb "red" title "reference",\
     "build/posCartesian.sto" using 2:3 with lines lw 2 lc rgb "blue" title "result"

reset
set terminal png
set output "controls.png"
set title 'Controls'
set ylabel 'control'
set xlabel 'Time (s)'
set grid
plot "build/controls.sto" using 1:2 with lines lw 2 lc rgb "red" title "Ankle"


reset
set terminal png
set output "forces.png"
set title 'Torques'
set ylabel 'Torques (Nm)'
set xlabel 'Time (s)'
set grid
plot "build/forces.sto" using 1:2 with lines lw 2 lc rgb "red" title "Ankle"


reset
set terminal png
set output "angles.png"
set title 'Joint angles'
set ylabel 'Angle (rad)'
set xlabel 'Time (s)'
set grid
plot "build/states.sto" using 1:7 with lines lw 2 lc rgb "red" title "Ankle"
