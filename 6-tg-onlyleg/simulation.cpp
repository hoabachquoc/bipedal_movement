#include <OpenSim/OpenSim.h>
#include <Simbody.h>
#include <cmath>
#include <qpOASES.hpp>

#include <iostream>
#include <fstream>
#include <cstdlib>
#include <vector>

using namespace OpenSim;
using namespace SimTK;
using namespace std;
USING_NAMESPACE_QPOASES


//----------------------------------------- OPTIONS --------------------------------------------
bool simulation = 1;
bool useVisualizer = 1;		// cause segmentation fault !!!

int setInitialState = 1;

/* Choose a task
 * default : trajectory from data files
 * 1 : keep initial pose
 * 2 : destination point at the top of the linkage 2 when the system is straight up vertically
 **/ 
int testCase = 1;

int TrajLength = 500;


/* Set initial control
 * 1: all 0
 * 2: all 0.03
 */
int setInitCtrl = 1;

//-------------------------------------- OPTIMIZATION SETTINGS ---------------------------------
// Control sampling time
double h = 1e-3;
bool dynamicConstraint = 1;

//-------------------------------------- QPOASES OPTIMIZATION ----------------------------------
bool setqpOASES = 0;
bool printqpOASESsolution = 0;

//----------------------------------- BUILT-IN OPTIMIZATION ------------------------------------
/** Set OpenSim built-in optimizer
 * 1: Interior Point
 * 2: Limited-memory Broyden-Fletcher-Goldfarb-Shanno (LBFGS)
 * 3: LBFGS with simple bound constraints (LBFGSB)
 * 4: C implementation of sequential quadratic programming (CFSQP)
 * 5: Covariance matrix adaptation, evolution strategy (CMAES)
 **/
int setOptimizerAlgorithm = 1;
bool setCostFuncFormulationType = 0;		// 0 : qp || 1: integrator

//----------------------------------------- WEIGHTS --------------------------------------------
SimTK::Real randomweight = 1;
/* Task weight */
SimTK::Real taskw  = 1e-6;

/* Control input */
SimTK::Real anklew = 1;
SimTK::Real kneew  = 1;

/* Contact wrench */
SimTK::Real wrenchw0 = randomweight;
SimTK::Real wrenchw1 = randomweight;
SimTK::Real wrenchw2 = randomweight;
SimTK::Real wrenchw3 = randomweight;
SimTK::Real wrenchw4 = randomweight;
SimTK::Real wrenchw5 = randomweight;

//---------------------------------------- END-EFFECTOR ----------------------------------------
Vec3 eefPoint(0,0.41,0);		// in body frame

//------------------------------------- CONTACT POINTS -----------------------------------------
/** Contact points in foot frame
 * Contact sphere radius 0.005 m
 * Contact sphere center position in foot frame Y = -0.005
 **/

bool separateContact = 0;
int nc = 1;     // change to 4 if separateContact = 1
Vec3 lHeelPt(-0.07,-0.008,-0.02);
Vec3 rHeelPt(-0.07,-0.008,0.02);
Vec3 lPadPt(0.06,-0.008,-0.02);
Vec3 rPadPt(0.06,-0.008,0.02);
Vec3 cPt(0.0, -0.008, 0);

//------------------------------------- INTEGRATOR OPTIONS -------------------------------------
unsigned stepNum = 1;
const Real stepSize = 1e-3;
double initialStepTime = 0.0;
double finalStepTime = initialStepTime + stepSize;


//------------------------------------- FEEDBACK CONTROLERS ------------------------------------
Mat33 Kp(400); // position gain
Mat33 Kv(40); // velocity gain

//-------------------------------------- MODEL PROPERTIES --------------------------------------

//---------------------------------------- MISCELLANEOUS ---------------------------------------
bool debugMessages = 0;
bool logMessages = 1;

//==============================================================================================
//				                    TRAJECTORY GENERATION - KDL
//==============================================================================================
int calcTrajVecLength() {
    string s;
    int sTotal(0);

    ifstream in;
    in.open("velocity.dat");

    while(!in.eof()) {
        getline(in, s);
        sTotal ++;	
    }
    return sTotal - 1;
}


void generateTrajectry ( Vector_<Vec3> &refPos, Vector_<Vec3> &refVel, Vector_<Vec3> &refAcc ) {
    // Read reference acceleration
    ifstream acc_in("acceleration.dat");
    if (!acc_in) {
        cerr << "Uh oh, file .dat could not be opened for reading!" << endl;
        exit(1);
    }
    // Create the vector, initialized from the numbers in the file:
    std::vector<double> accData (( std::istream_iterator<double>(acc_in)),
            std::istream_iterator<double>());
    int i = 0;
    for (int count = 0; count < accData.size(); count+=6) {
        refAcc[i] = Vec3( accData[count], accData[count+1], 0); i++;
    }

    // Read reference velocity
    ifstream vel_in("velocity.dat");
    if (!vel_in) {
        cerr << "Uh oh, file .dat could not be opened for reading!" << endl;
        exit(1);
    }
    // Create the vector, initialized from the numbers in the file:
    std::vector<double> velData (( std::istream_iterator<double>(vel_in)),
            std::istream_iterator<double>());
    i = 0;
    for (int count = 0; count < velData.size(); count+=6) {
        refVel[i] = Vec3( velData[count], velData[count+1], 0);
        i++;
    }

    // Read reference position
    ifstream traj_in("position.dat");
    if (!traj_in) {
        cerr << "Uh oh, file .dat could not be opened for reading!" << endl;
        exit(1);
    }

    std::vector<double> posData (( std::istream_iterator<double>(traj_in)),
            std::istream_iterator<double>());
    int j = 0;
    for (int count = 0; count < posData .size(); count+=16) {
        refPos[j] = Vec3( posData [count+3], posData [count+7], 0);
        j++;
    }
}

//==============================================================================================
// 			                            REARRANGE VECTOR
//==============================================================================================

// Convert Vec3 to Vector
SimTK::Vector vec3ToVector(const Vec3& vec){
    SimTK::Vector vecOut(3);
    for (int i = 0; i < 3; i++){
        vecOut(i) = vec(i);
    }
    return vecOut;
}


// Array of bodies on which are located contact points
// So far only the foot (mobilized body index 3) is considered
Array_<MobilizedBodyIndex> bodyCt() {
    Array_<MobilizedBodyIndex> onBodyB(nc);
    onBodyB[0] = MobilizedBodyIndex(3);
    onBodyB[1] = MobilizedBodyIndex(3);
    onBodyB[2] = MobilizedBodyIndex(3);
    onBodyB[3] = MobilizedBodyIndex(3);
    return onBodyB; 
}

// Array of frame origin points of task frames (contact points)
Array_<Vec3> originCtPtFrm() {
    Array_<Vec3> originAoInB(nc);

    originAoInB[0] = lHeelPt;
    originAoInB[1] = rHeelPt;
    originAoInB[2] = lPadPt;
    originAoInB[3] = rPadPt;

    return originAoInB;
}
//==============================================================================================
// 			                          OPTIMIZATION SYSTEM
//==============================================================================================

// Regulation matrix (weight matrix) for all optimization variables
SimTK::Matrix regulationMatrix (Model& osimModel, State& si){
    int size = osimModel.getNumControls() + 6*nc;

    SimTK::Matrix regulationMat(size,size);
    regulationMat = 0;
    regulationMat(0,0) = anklew;

    regulationMat(1,1) = wrenchw0;
    regulationMat(2,2) = wrenchw1;
    regulationMat(3,3) = wrenchw2;
    regulationMat(4,4) = wrenchw3;
    regulationMat(5,5) = wrenchw4;
    regulationMat(6,6) = wrenchw5;

    return regulationMat;
}

SimTK::Vector q_max (Model& osimModel) {
    const CoordinateSet& CoordSet = osimModel.getCoordinateSet();
    int size = CoordSet.getSize();

    SimTK::Vector qMax(size);

    for (int i = 0; i < size; i++)
        qMax(i) = CoordSet.get(i).getRangeMax();

    return qMax;
}

SimTK::Vector q_min (Model& osimModel) {
    const CoordinateSet& CoordSet = osimModel.getCoordinateSet();
    int size = CoordSet.getSize();
    SimTK::Vector qMin(size);

    for (int i = 0; i < size; i++)
        qMin(i) = CoordSet.get(i).getRangeMin();

    return qMin;
}



// Convert spatialVec to Vector
SimTK::Vector convertSpatialVecToVector (const SpatialVec& sVec) {
    SimTK::Vector oVec (6);
    oVec(0) = sVec[0][0];
    oVec(1) = sVec[0][1];
    oVec(2) = sVec[0][2];
    oVec(3) = sVec[1][0];
    oVec(4) = sVec[1][1];
    oVec(5) = sVec[1][2];
    return oVec;
}

SimTK::Vector ub (Model& osimModel) {
    int na = osimModel.getNumControls();
    SimTK::Vector ub (na+6*nc);
    int count = 0;

    // ub_a
    ub (count, na) = 1.0;
    count += na;

    // ub_wc (random positive values)
    ub (count, 6*nc) = 3e2;

    return ub;
}

SimTK::Vector lb (Model& osimModel) {
    int na = osimModel.getNumControls();
    SimTK::Vector lb (na+6*nc);
    int count = 0;

    // lb_a
    lb (count, na) = -1.0;
    count += na;

    // lb_wc
    lb (count, 6*nc) = -3e2;

    // mx, mz, ty >= 0
    for (int i = 0; i < nc; i++) {
        /* lb (count)   = 0.0; */
        /* lb (count+2) = 0.0; */
        lb (count+4) = 0.0;
        count += 6;
    }
    return lb;
}


//------------------------------ QUADRATIC PROGRAMING FORMULATION ------------------------------

/* M(q).du + ~G.mult = g(q) + T + gamma - cor(q,u)
 * --> du = Minv.(T + g + gamma - ~G.mult - cor)
 * Xd = J.u
 * --> Xdd = dJ.u + J.du
 * --> Xdd = dJ.u + J.Minv.(T + g + gamma - cor)
 * --> Xdd = J.Minv.(T + gamma) + J.Minv.(g - cor) + dJ.u
 * --> Xdd = J.Minv.S.u + J.Minv.~Jc.Fc + J.Minv.(g - cor) + dJ.u
 * Xdd_des = Xdd_traj_ + p_gains_.( X_des - X_curr_) + d_gains_.( Xd_des - Xd_curr_)
 * compute min(u) || Xdd - Xdd_des ||² + u^T * W * u
 * Xdd - Xdd_des = P.x + k
 * With P = [J.Minv.S  J.Minv.~Jc]
 *      k = J.Minv.(g - cor) + dJ.u - Xdd_des
 */


//-------------------------------- OPENSIM BUILT IN OPTIMIZATION -------------------------------

class OpensimOptimizationSystem : public OptimizerSystem {
    int m_numParams;
    State& m_s;
    Model& m_osimModel;
    Vec3& m_desAcc;

    public:
    OpensimOptimizationSystem(int numParams, 
            int numberOfEqualityConstraints,
            int numberOfInequalityConstraints,
            State& aState, 
            Model& aModel, 
            Vec3& desAcc):
        OptimizerSystem(numParams),
        m_s(aState),
        m_osimModel(aModel),
        m_desAcc(desAcc)
    {
        setNumEqualityConstraints(numberOfEqualityConstraints);
        setNumInequalityConstraints(numberOfInequalityConstraints);
    }

    const SimbodyMatterSubsystem& matter () const {return m_osimModel.getMatterSubsystem();}
    const MultibodySystem& system () const {return m_osimModel.getMultibodySystem();}

    // return the mobilized body index of the linkage 2 on which is located the end-effector
    const MobilizedBodyIndex eefBod () const {return MobilizedBodyIndex(1); }

    // Selection matrix containing the maximal force of torque generators
    SimTK::Matrix selectionMatrix () const {
        SimTK::Matrix selection(m_s.getNU(), m_osimModel.getNumControls());
        selection = 0;
        selection(6,0) = m_osimModel.getActuators().get(0).getOptimalForce();
        return selection;
    }

    SimTK::Vector gravity () const {
        SimTK::Vector gr;
        matter().multiplyBySystemJacobianTranspose(m_s, m_osimModel.getGravityForce().getBodyForces(m_s), gr);
        return gr;
    }


    SimTK::Vector coriolis () const {
        SimTK::Vector cor;
        matter().calcResidualForceIgnoringConstraints(m_s, SimTK::Vector(0), SimTK::Vector_<SimTK::SpatialVec>(0), SimTK::Vector(0),cor);
        return cor;
    }

    SimTK::Matrix massMatrix () const {
        SimTK::Matrix M_; matter().calcM(m_s, M_);
        return M_;
    }

    SimTK::Matrix invMassMatrix () const {
        SimTK::Matrix Minv; matter().calcMInv(m_s, Minv);
        return Minv;
    }


    SimTK::Matrix contactJacob () const {
        SimTK::Matrix Jc;
        if (separateContact)
            matter().calcFrameJacobian(m_s, bodyCt(), originCtPtFrm(), Jc);
        else
            matter().calcFrameJacobian(m_s, MobilizedBodyIndex(2), cPt, Jc);

        return Jc;
    }

    // Bias term (dJ.u) calculated manually by adding a pertubation
    Vec3 dJacob_u (const MobilizedBodyIndex whichBod, const Vec3& pt, const Vector& speed) const {
        State perturbq = m_s; 

        const Real Delta = 5e-6;
        const SimTK::Vector& q    = m_s.getQ();
        const SimTK::Vector& qdot = m_s.getQDot();

        // Perturbed +:
        perturbq.updQ() = q + Delta*qdot;
        system().realize(perturbq, Stage::Position);
        Vec3 JS2_Pu = matter().multiplyByStationJacobian (perturbq, whichBod, pt, speed);

        // Perturbed -:
        perturbq.updQ() = q - Delta*qdot;
        system().realize(perturbq, Stage::Position);
        Vec3 JS1_Pu = matter().multiplyByStationJacobian (perturbq, whichBod, pt, speed);

        // Estimate JDotu - central difference
        return (JS2_Pu-JS1_Pu)/Delta/2;
    }

    // Estimated generalized accelerations based on the equation of movement
    SimTK::Vector estimated_dnu (const SimTK::Vector& params) const {
        SimTK::Vector torque_act (m_s.getNU(), 0.0);

        for (int i = 0; i < m_s.getNU()-6; i++)
            torque_act(i+6) = params(i);

        return invMassMatrix() * (gravity() 
                - coriolis() 
                + torque_act 
                + ~contactJacob() * params (m_s.getNU()-6, 6*nc)
                );
    }

    // Estimated generalized velocities
    // nu = nu + dnu * dt
    SimTK::Vector estimated_nu (const SimTK::Vector& params) const {
        return m_s.getU() + estimated_dnu (params) * h; 
    }


    // Estimated generalized coordinates
    // q = q + nu * dt + dnu * dt^2 / 2
    SimTK::Vector estimated_q (const SimTK::Vector& params) const {
        return m_osimModel.getStateValues(m_s)(0, m_s.getNU()) + 
            estimated_nu (params) * h + estimated_dnu (params) * h*h/2;
    }


    int objectiveFunc(const SimTK::Vector &newControls, bool new_coefficients, Real& f) const {

        if (setCostFuncFormulationType) {
            // make a copy of out initial states
            State s = m_s;

            // Update the control values
            //newControls.dump("New Controls In:");
            m_osimModel.updDefaultControls() = newControls;

            // Create the integrator for the simulation.
            RungeKuttaMersonIntegrator integrator(m_osimModel.getMultibodySystem());
            integrator.setAccuracy(1.0e-4);
            integrator.setAllowInterpolation(false);

            // Create a manager to run the simulation
            Manager manager(m_osimModel, integrator);
            manager.setInitialTime(initialStepTime);
            manager.setFinalTime(finalStepTime);
            manager.integrate(s);

            //osimModel.getControls(s).dump("Model Controls:");
            m_osimModel.getMultibodySystem().realize(s, Stage::Acceleration);

            Vec3 curAcc;
            // Don't forget state s for god sake !!!
            m_osimModel.getSimbodyEngine().getAcceleration
                (s, m_osimModel.getBodySet().get("linkage1"), eefPoint, curAcc);

            Vec3 err;
            err = m_desAcc - curAcc;
            f = dot (err,err);
            /* f = err(1)*err(1); */
        }
        else {
            const int nb = matter().getNumBodies();
            int na = m_osimModel.getNumControls();

            // Jacobian at the end-effector - Thigh/Body Index 3
            SimTK::Matrix J_eef;
            matter().calcStationJacobian(m_s, eefBod(), eefPoint, J_eef);

            // gravity - coriolis
            SimTK::Vector _f =  gravity() - coriolis();

            // M^-1*(gravity - coriolis)
            SimTK::Vector MInvf;
            matter().multiplyByMInv(m_s, _f, MInvf);

            // J_eff*M^-1*(gravity - coriolis)
            Vec3 J_effMInvf = matter().multiplyByStationJacobian(m_s,
                    eefBod(),
                    eefPoint,
                    MInvf
                    );

            // Acceleration errors in form of Px + k

            // P = [J_eff.M^-1.S  J_eff.M^-1.~Jc]
            SimTK::Matrix P (3, na+6*nc); P = 0.0;
            int P_column_count = 0;

            P (0, P_column_count, 3, na) = J_eef * invMassMatrix() * selectionMatrix();
            P_column_count += na;

            P (0, P_column_count, 3, 6*nc) = J_eef * invMassMatrix() * ~contactJacob();

            // k = J_eef . M^-1 . (gr - cor) + dJ_eff.u - Xdd_des
            SimTK::Vector k = vec3ToVector(J_effMInvf 
                    + dJacob_u(eefBod(), eefPoint, estimated_nu(newControls)) 
                    - m_desAcc);

            if (debugMessages)
                cout << "-- debug P = " << P << endl;

            // Matrix H
            SimTK:: Matrix H = ~P * P;
            H = H * taskw + regulationMatrix (m_osimModel, m_s);
            if (debugMessages)
                cout << " -- debug H = " << H << endl;

            // Vector g
            SimTK::Vector g = ~P * k * taskw;
            /* SimTK::Vector g = ~P * k; */
            if (debugMessages)
                cout << " -- debug g = " << g << endl;


            // cost function: f = chi^T . H . chi + chi^T . g 

            // vector*matrix*vector can't give an double/int in simtk
            SimTK::Vector stupidVector = ~newControls * H * newControls;
            f = 1/2 * stupidVector(0) + ~newControls * g;
        }

        return(0);
    }

    /* 
     ** Method to compute the value of the constraints.
     ** Equality constraints are first followed by the any inequality constraints
     */ 
    int constraintFunc(const SimTK::Vector &newControls, bool new_coefficients, SimTK::Vector &constraints)  const{

        if (debugMessages)
            cout << "\n-- debug newControls = " << newControls << endl;

        int nu = m_s.getNU();
        int count = 0;

        // eq: Linear velocity null at contact points (3*nc)
        SimTK::Vector e_dnu = estimated_dnu(newControls);
        SimTK::Vector e_nu  = estimated_nu (newControls);
        MobilizedBodyIndex footMb = MobilizedBodyIndex(2);

        if (separateContact) {
            Vec3 jc_dnu_lheel = matter().multiplyByStationJacobian (m_s, footMb, lHeelPt, e_dnu);
            Vec3 jc_dnu_rheel = matter().multiplyByStationJacobian (m_s, footMb, rHeelPt, e_dnu);
            Vec3 jc_dnu_lpad  = matter().multiplyByStationJacobian (m_s, footMb, lPadPt, e_dnu);
            Vec3 jc_dnu_rpad  = matter().multiplyByStationJacobian (m_s, footMb, rPadPt, e_dnu);

            Vec3 djc_u_lheel = dJacob_u (footMb, lHeelPt, e_nu);
            Vec3 djc_u_rheel = dJacob_u (footMb, rHeelPt, e_nu);
            Vec3 djc_u_lpad  = dJacob_u (footMb,  lPadPt, e_nu);
            Vec3 djc_u_rpad  = dJacob_u (footMb,  rPadPt, e_nu);

            constraints (count  ,3) = vec3ToVector(jc_dnu_lheel + djc_u_lheel);
            constraints (count+3,3) = vec3ToVector(jc_dnu_rheel + djc_u_rheel);
            constraints (count+6,3) = vec3ToVector(jc_dnu_lpad  + djc_u_lpad);
            constraints (count+9,3) = vec3ToVector(jc_dnu_rpad  + djc_u_rpad);

            if (debugMessages)
                cout << "-- Contact constraints = " << constraints(count, 3*nc) << endl;
        }
        else {
            Vec3 jc_dnu_cPt = matter().multiplyByStationJacobian (m_s, footMb, cPt, e_dnu);
            Vec3 djc_u_cPt  = dJacob_u (footMb, cPt, e_nu);
            constraints (count, 3) = vec3ToVector (jc_dnu_cPt + djc_u_cPt);
            if (debugMessages)
                cout << "-- Contact constraints = " << constraints(count, 3*nc) << endl;
        }
        count += nc * 3;

        // ineq: Bounds for generalized coordinates knee and ankle (2.(nu-6))

        SimTK::Vector est_q = estimated_q(newControls);

        constraints(count     , nu-6) = (est_q - q_min(m_osimModel))(6, nu-6);
        constraints(count+nu-6, nu-6) = (q_max(m_osimModel) - est_q)(6, nu-6);

        if (debugMessages)
            cout << "-- Bound constraints = " << constraints(count, 2*(nu-6)) << endl;

        return(0);
    }
    void testMatrices() const {

        const int nb = matter().getNumBodies();
        int na = m_osimModel.getNumControls();
        SimTK::Vector newControls (na+6*nc, 0.5);

        /* cout << "-- gr_init = " << gravity() << endl; */
        /* cout << "-- cor_init = " << coriolis() << endl; */
        /* cout << "-- M_init = " << massMatrix() << endl; */
        /* cout << "-- Jc_init = " << contactJacob() << endl; */
        /* cout << "-- ~Jc_init = " << ~contactJacob() << endl; */

        /* cout << "-- sign verification for contact wrench: " << endl; */
        /* cout << coriolis() - gravity() << endl; */

        cout << "-- fj_rz = " << m_osimModel.getCoordinateSet().get("fj_rz").getValue(m_s) << endl;
        cout << "-- fj_rz_u = " << m_osimModel.getCoordinateSet().get("fj_rz").getSpeedValue(m_s) << endl;
        cout << "-- ankle_rotZ = " << m_osimModel.getCoordinateSet().get("ankle_rotZ").getValue(m_s) << endl;
        cout << "-- ankle_rotZ_u = " << m_osimModel.getCoordinateSet().get("ankle_rotZ").getSpeedValue(m_s) << endl;


        // Jacobian at the end-effector - Thigh/Body Index 3
        SimTK::Matrix J_eef;
        matter().calcStationJacobian(m_s, eefBod(), eefPoint, J_eef);

        if (debugMessages)
            cout << "-- debug J_eff = " << J_eef << endl;

        // gravity - coriolis
        SimTK::Vector _f =  gravity() - coriolis();

        // M^-1*(gravity - coriolis)
        SimTK::Vector MInvf;
        matter().multiplyByMInv(m_s, _f, MInvf);

        // J_eff*M^-1*(gravity - coriolis)
        Vec3 J_effMInvf = matter().multiplyByStationJacobian(m_s,
                eefBod(),
                eefPoint,
                MInvf
                );

        // Acceleration errors in form of Px + k

        // P = [J_eff.M^-1.S  J_eff.M^-1.~Jc]
        SimTK::Matrix P (3, na+6*nc); P = 0.0;
        int P_column_count = 0;

        P (0, P_column_count, 3, na) = J_eef * invMassMatrix() * selectionMatrix();
        P_column_count += na;

        P (0, P_column_count, 3, 6*nc) = J_eef * invMassMatrix() * ~contactJacob();

        // k = J_eef . M^-1 . (gr - cor) + dJ_eff.u - Xdd_des
        SimTK::Vector k = vec3ToVector(J_effMInvf 
                + dJacob_u(eefBod(), eefPoint, estimated_nu(newControls)) 
                - m_desAcc);

        if (debugMessages) {
            cout << "-- debug P = " << P << endl;
            cout << "-- debug k = " << k << endl;
        }

        // Matrix H
        SimTK:: Matrix H = ~P * P;
        H = H * taskw + regulationMatrix (m_osimModel, m_s);
        if (debugMessages)
            cout << " -- debug H = " << H << endl;

        // Vector g
        SimTK::Vector g = ~P * k * taskw;
        /* SimTK::Vector g = ~P * k; */
        if (debugMessages)
            cout << " -- debug g = " << g << endl;


        // cost function: f = chi^T . H . chi + chi^T . g 

        // vector*matrix*vector can't give an double/int in simtk
        SimTK::Vector stupidVector = ~newControls * H * newControls;
        Real f = 1/2 * stupidVector(0) + ~newControls * g;
        if (debugMessages) {
            cout << " -- debug stupidVector = " << stupidVector << endl;
            cout << " -- debug f = " << f << endl;
        }
    }
};

//==============================================================================================
// 				                                MAIN
//==============================================================================================


int main()
{
    try {
        std::clock_t startTime = std::clock();

        //------------------------------------- LOAD MODEL -------------------------------------

        Model osimModel("leg_tg.osim");
        osimModel.setUseVisualizer(useVisualizer);

        //----------------------------------- INITIALIZATION -----------------------------------

        SimTK::State& si = osimModel.initSystem();

        //------------------------------- INITIAL ANGLE POSITIONS ------------------------------

        double foot_hy = 0.09;
        double linkage1_length = 0.41;
        double ankle_positionX_in_foot = -0.03;
        double sphere_radius = 0.005;


        double fj_rz_init;
        double fj_rz_u_init;
        double anklePos_init;
        double ankleVel_init;
        double fj_ty_init;

        /* Choose an initial static state (no speed)
         * 1 : pose
         * 2 : straight
         **/ 

        switch (setInitialState) {
            case 1:
                fj_rz_init   = -2 * SimTK::Pi / 180;
                fj_rz_u_init =   0;
                anklePos_init = 2 * SimTK::Pi / 180;
                ankleVel_init =  0;
                fj_ty_init = sphere_radius + foot_hy + linkage1_length/2 * cos(fj_rz_init); 
                osimModel.getCoordinateSet().get("fj_ty").setValue(si, fj_ty_init);
                break;
            case 2:
                fj_rz_init   =   0 * SimTK::Pi / 180;
                fj_rz_u_init =   0;
                anklePos_init =  0 * SimTK::Pi / 180;
                ankleVel_init =  0;
                break;
        }

        osimModel.getCoordinateSet().get("fj_rz").setValue(si, fj_rz_init);
        osimModel.getCoordinateSet().get("fj_rz").setSpeedValue(si, fj_rz_u_init);
        osimModel.getCoordinateSet().get("ankle_rotZ").setValue(si, anklePos_init);
        osimModel.getCoordinateSet().get("ankle_rotZ").setSpeedValue(si, ankleVel_init);

        //--------------------------------- REFERENCE TRAJECTORY -------------------------------

        /* Choose a task
         * default : trajectory from data files
         * 1 : keep initial pose
         * 2 : destination point at the top of the linkage 2 when the system is straight up vertically
         **/ 

        double px_test, py_test;

        int trajVecLength;
        if (testCase == 0) { trajVecLength = calcTrajVecLength(); }
        else { trajVecLength = TrajLength; }
        Vector_<Vec3> refPos(trajVecLength);
        Vector_<Vec3> refVel(trajVecLength);
        Vector_<Vec3> refAcc(trajVecLength);

        switch (testCase) {
            case 1:
                px_test =  linkage1_length * sin(anklePos_init);
                py_test =  sphere_radius + foot_hy + linkage1_length * cos(anklePos_init);
                break;
            case 2:
                px_test =  0.0;
                py_test =  sphere_radius + foot_hy + linkage1_length;
                break;
        } 

        cout << "-- px_test = " << px_test << endl;
        cout << "-- py_test = " << py_test << endl;

        for (int count = 0; count < trajVecLength; ++count) {
            refPos[count] = Vec3(px_test, py_test, 0.0); 
            refVel[count] = Vec3(0);
            refAcc[count] = Vec3(0); 
        }


        //------------------------------------ ACTUATORS ---------------------------------------

        int numControls = osimModel.getNumControls();

        // Get control bound set and initialize controls
        int bound_size = numControls + 6*nc;

        // Set initial controls
        SimTK::Vector initialControls(numControls);

        switch (setInitCtrl) {
            case 1:
                initialControls(0) = 0;
                break;
            case 2:
                initialControls(0) = 0.03;
                break;
        }
        osimModel.updDefaultControls() = initialControls;

        //------------------------------------ VISUALIZER --------------------------------------
        /* if (useVisualizer) { */
        /*     osimModel.updMatterSubsystem().setShowDefaultGeometry(true); */
        /*     Visualizer& viz = osimModel.updVisualizer().updSimbodyVisualizer(); */
        /*     viz.setBackgroundType(viz.SolidColor); */
        /*     viz.setBackgroundColor(Cyan); */
        /* } */

        // Angular state variables
        Vec3 eefPos;
        Vec3 eefVel;
        osimModel.getMultibodySystem().realize(si, Stage::Acceleration);
        osimModel.getSimbodyEngine().getPosition
            (si, osimModel.getBodySet().get("linkage1"), eefPoint, eefPos);
        osimModel.getSimbodyEngine().getVelocity
            (si, osimModel.getBodySet().get("linkage1"), eefPoint, eefVel);

        /* Vec3 eefPos_ground; */
        /* osimModel.getSimbodyEngine().transformPosition */
        /*     (si, */ 
        /*      osimModel.getBodySet().get("linkage1"), */ 
        /*      eefPos, */
        /*      osimModel.getBodySet().get("ground"), */ 
        /*      eefPos_ground */
        /*     ); */


        cout << "-- eefPos = " << eefPos << endl;
        cout << "-- eefVel = " << eefVel << endl;
        /* cout << "-- eefPos_ground = " << eefPos_ground << endl; */


        /* Transform linkage1_transform = osimModel.getSimbodyEngine().getTransform */
        /*     (si, osimModel.getBodySet().get("linkage1")); */
        /* cout << "-- linkage 1 transform = " << linkage1_transform << endl; */

        //------------------------------------- LOG FILES --------------------------------------
        // Construct control storage
        Array<string> columnLabels;
        Storage* _controlStore = new Storage(1023,"controls");
        columnLabels.append("time");
        for(int i=0;i<osimModel.getActuators().getSize();i++)
            columnLabels.append(osimModel.getActuators().get(i).getName());
        _controlStore->setColumnLabels(columnLabels);

        // Construct force storage
        Storage* _forceStore = new Storage(1023,"forces");
        _forceStore->setColumnLabels(columnLabels);

        // Initialize storage files
        SimTK::Vector updctrls(osimModel.getActuators().getSize());
        SimTK::Vector updforces(osimModel.getActuators().getSize());
        osimModel.getMultibodySystem().realize(si, Stage::Acceleration);

        for (int i = 0; i < osimModel.getActuators().getSize(); i++) {
            updctrls[i] = osimModel.getActuators().get(i).getControl(si);
            updforces[i] = updctrls[i] * osimModel.getActuators().get(i).getOptimalForce();
        } 

        _controlStore->store(stepNum, 0.0,osimModel.getActuators().getSize(), &updctrls[0]);
        _controlStore->print("controls.sto");
        _forceStore->store(stepNum, 0.0,osimModel.getActuators().getSize(), &updforces[0]);
        _forceStore->print("forces.sto");

        // Construct Cartesian state storage of the end-effector
        Array<string> posColumnLabels;
        Storage* _posCartesianStore = new Storage(1023,"PosCartesian");
        posColumnLabels.append("time");
        posColumnLabels.append("pX");
        posColumnLabels.append("pY");
        posColumnLabels.append("pZ");
        _posCartesianStore->setColumnLabels(posColumnLabels);

        Array<string> velColumnLabels;
        Storage* _velCartesianStore = new Storage(1023,"VelCartesian");
        velColumnLabels.append("time");
        velColumnLabels.append("vX");
        velColumnLabels.append("vY");
        velColumnLabels.append("vZ");
        _velCartesianStore->setColumnLabels(velColumnLabels);

        Array<string> accColumnLabels;
        Storage* _accCartesianStore = new Storage(1023,"AccCartesian");
        accColumnLabels.append("time");
        accColumnLabels.append("aX");
        accColumnLabels.append("aY");
        accColumnLabels.append("aZ");
        _accCartesianStore->setColumnLabels(accColumnLabels);

        Array<string> refPosColumnLabels;
        Storage* _refPosCartesianStore = new Storage(1023,"refstatePos");
        refPosColumnLabels.append("time");
        refPosColumnLabels.append("pXr");
        refPosColumnLabels.append("pYr");
        refPosColumnLabels.append("pZr");
        _refPosCartesianStore->setColumnLabels(refPosColumnLabels);

        Array<string> refVelColumnLabels;
        Storage* _refVelCartesianStore = new Storage(1023,"refstateVel");
        refVelColumnLabels.append("time");
        refVelColumnLabels.append("vXr");
        refVelColumnLabels.append("vYr");
        refVelColumnLabels.append("vZr");
        _refVelCartesianStore->setColumnLabels(refVelColumnLabels);

        Array<string> refAccColumnLabels;
        Storage* _refAccCartesianStore = new Storage(1023,"refstateAcc");
        refAccColumnLabels.append("time");
        refAccColumnLabels.append("aXr");
        refAccColumnLabels.append("aYr");
        refAccColumnLabels.append("aZr");
        _refAccCartesianStore->setColumnLabels(refAccColumnLabels);

        // Initialize state storage
        Vec3 updPosCartesian(0);
        Vec3 updVelCartesian(0);
        Vec3 updAccCartesian(0);
        osimModel.getSimbodyEngine().getPosition
            (si, osimModel.getBodySet().get("linkage1"), eefPoint, updPosCartesian);
        osimModel.getSimbodyEngine().getVelocity
            (si, osimModel.getBodySet().get("linkage1"), eefPoint, updVelCartesian);
        osimModel.getSimbodyEngine().getAcceleration
            (si, osimModel.getBodySet().get("linkage1"), eefPoint, updAccCartesian);

        _posCartesianStore->store(stepNum, 0.0, 3, &updPosCartesian[0]);
        _posCartesianStore->print("posCartesian.sto");
        _velCartesianStore->store(stepNum, 0.0, 3, &updVelCartesian[0]);
        _velCartesianStore->print("velCartesian.sto");
        _accCartesianStore->store(stepNum, 0.0, 3, &updAccCartesian[0]);
        _accCartesianStore->print("accCartesian.sto");

        _refPosCartesianStore->store(stepNum, 0.0, 3, &refPos[0].get(0));
        _refPosCartesianStore->print("refPosCartesian.sto");
        _refVelCartesianStore->store(stepNum, 0.0, 3, &refVel[0].get(0));
        _refVelCartesianStore->print("refVelCartesian.sto");
        _refAccCartesianStore->store(stepNum, 0.0, 3, &refAcc[0].get(0));
        _refAccCartesianStore->print("refAccCartesian.sto");

        // Gravity storage
        /* Array<string> gravity; */
        /* Storage* _gravityStore = new Storage(1023,"gravity"); */
        /* gravity.append("time"); */
        /* gravity.append("ankle"); */
        /* _gravityStore->setColumnLabels(gravity); */

        const SimbodyMatterSubsystem& matter = osimModel.getMatterSubsystem();

        SimTK::Vector gr;
        matter.multiplyBySystemJacobianTranspose(
                si, osimModel.getGravityForce().getBodyForces(si), gr);
        /* _gravityStore->store(stepNum, 0.0, numControls/2, &gr(6)); */
        /* _gravityStore->print("gravity.sto"); */

        /* cout << "-- gravity = " << gr << endl; */

        // Create the force reporter
        ForceReporter* reporter = new ForceReporter(&osimModel);
        osimModel.addAnalysis(reporter);

        //-------------------------------- INTEGRATOR MANAGER ----------------------------------
        SimTK::RungeKuttaMersonIntegrator integ(osimModel.getMultibodySystem());
        integ.setAccuracy(1.0e-4);
        Manager manager(osimModel, integ);

        //----------------------------- TEST OPTIMIZATION MATRICES -----------------------------
        int  test_numberOfEqualityConstraints = 3*nc; 
        int  test_numberOfInequalityConstraints = 2 * (si.getNU()-6);

        // Compute desired trajectory from PD controller
        Vec3 test_desAcc = Kp * (refPos[0] - eefPos) 
            + Kv * (refVel[0] - eefVel) 
            + refAcc[0];

        OpensimOptimizationSystem test_sys(bound_size, 
                test_numberOfEqualityConstraints, 
                test_numberOfInequalityConstraints,
                si, 
                osimModel,
                test_desAcc);

        test_sys.setParameterLimits(lb(osimModel), ub(osimModel));
        test_sys.testMatrices();
        cout << "test_desAcc = " << test_desAcc << endl;

        /* Optimizer test_opt(sys, optimizerAlg); */

        /* // Specify settings for the optimizer */
        /* test_opt.setConvergenceTolerance(0.0001); */
        /* test_opt.useNumericalGradient(true); */
        /* test_opt.useNumericalJacobian(true); */
        /* test_opt.setMaxIterations(2000); */
        /* test_opt.setLimitedMemoryHistory(2000); */

        //------------------------------------ SIMULATION --------------------------------------
        if (simulation) {
            // Equality constraints: dynamic equation, linear velocities at contact points
            int  numberOfEqualityConstraints = 3*nc; 
            int  numberOfInequalityConstraints = 2 * (si.getNU()-6);

            // Optimization variables
            SimTK::Vector newControls(bound_size, 0.0);

            // Initialize optimization variables
            /* newControls (1) = 0; */
            /* newControls (2) = 0.420705/0.03; */
            /* newControls (3) = 0; */
            /* newControls (4) = 0; */
            /* newControls (5) = 0; */
            /* newControls (6) = 163.575; */
            /* newControls (7) = 0; */

            // start timing
            clock_t startTime = clock();

            for (int count = 0; count < trajVecLength; ++count) {

                //--------------------------- TASK SERVOING ------------------------------------

                // Compute desired trajectory from PD controller
                Vec3 desAcc = Kp * (refPos[count] - eefPos) 
                    + Kv * (refVel[count] - eefVel) 
                    + refAcc[count];

                if (logMessages) {cout << "desAcc = " << desAcc << endl;}

                //---------------------------- OPTIMIZATION ------------------------------------


                if (setqpOASES) {
                }
                else {
                    OptimizerAlgorithm optimizerAlg;
                    switch (setOptimizerAlgorithm) {
                        case 1:
                            optimizerAlg = InteriorPoint;
                            break;
                        case 2:
                            optimizerAlg = LBFGS;
                            break; 
                        case 3:
                            optimizerAlg = LBFGSB;
                            break;
                        case 4:
                            optimizerAlg = CFSQP;
                            break;
                        case 5:
                            optimizerAlg = CMAES;
                            break;
                    }
                    OpensimOptimizationSystem sys(bound_size, 
                            numberOfEqualityConstraints, 
                            numberOfInequalityConstraints,
                            si, 
                            osimModel,
                            desAcc);

                    sys.setParameterLimits(lb(osimModel), ub(osimModel));

                    Optimizer opt(sys, optimizerAlg);

                    // Specify settings for the optimizer
                    opt.setConvergenceTolerance(0.0001);
                    opt.useNumericalGradient(true);
                    opt.useNumericalJacobian(true);
                    opt.setMaxIterations(2000);
                    opt.setLimitedMemoryHistory(2000);

                    Real f;	
                    f = opt.optimize(newControls);
                    cout << "--> f = " << f << endl;
                    osimModel.updDefaultControls() = newControls(0, numControls);
                }
                cout << "--> control inputs = " << osimModel.updDefaultControls() << endl;

                //-------------------------- ADVANCE TO NEXT STEP ------------------------------

                manager.setInitialTime(initialStepTime);
                manager.setFinalTime(finalStepTime);
                manager.integrate(si);

                // Update current state
                osimModel.getMultibodySystem().realize(si, Stage::Acceleration);
                osimModel.getSimbodyEngine().getPosition
                    (si, osimModel.getBodySet().get("linkage1"), eefPoint, eefPos);
                osimModel.getSimbodyEngine().getVelocity
                    (si, osimModel.getBodySet().get("linkage1"), eefPoint, eefVel);

                //---------------------------- UPDATE LOG FILES --------------------------------

                // Save the forces
                reporter->getForceStorage().print("forceReporter.mot"); 

                // Store State variables
                manager.getStateStorage().print("states.sto");

                // Store controls, forces and moment arms
                for (int i = 0; i < osimModel.getActuators().getSize(); i++) {
                    updctrls[i] = osimModel.getActuators().get(i).getControl(si); 
                    updforces[i] = updctrls[i] * osimModel.getActuators().get(i).getOptimalForce();
                }
                _controlStore->store(stepNum, si.getTime(), numControls, &updctrls[0]);
                _controlStore->print("controls.sto");
                _forceStore->store(stepNum, si.getTime(), numControls, &updforces[0]);
                _forceStore->print("forces.sto");

                // Store Cartesian state of the end-effector
                osimModel.getSimbodyEngine().getPosition
                    (si, osimModel.getBodySet().get("linkage1"), eefPoint, updPosCartesian);
                osimModel.getSimbodyEngine().getVelocity
                    (si, osimModel.getBodySet().get("linkage1"), eefPoint, updVelCartesian);
                osimModel.getSimbodyEngine().getAcceleration
                    (si, osimModel.getBodySet().get("linkage1"), eefPoint, updAccCartesian);

                _posCartesianStore->store(stepNum, si.getTime(), 3, &updPosCartesian[0] );
                _posCartesianStore->print("posCartesian.sto");
                _velCartesianStore->store(stepNum, si.getTime(), 3, &updVelCartesian[0] );
                _velCartesianStore->print("velCartesian.sto");
                _accCartesianStore->store(stepNum, si.getTime(), 3, &updAccCartesian[0] );
                _accCartesianStore->print("accCartesian.sto");
                _refPosCartesianStore->store(stepNum, si.getTime(), 3, &refPos[count].get(0) );
                _refPosCartesianStore->print("refPosCartesian.sto");
                _refVelCartesianStore->store(stepNum, si.getTime(), 3, &refVel[count].get(0));
                _refVelCartesianStore->print("refVelCartesian.sto");
                _refAccCartesianStore->store(stepNum, si.getTime(), 3, &refAcc[count].get(0));
                _refAccCartesianStore->print("refAccCartesian.sto");

                /* matter.multiplyBySystemJacobianTranspose( */
                /*         si, osimModel.getGravityForce().getBodyForces(si), gr); */
                /* _gravityStore->store(stepNum, si.getTime(), numControls/2, &gr(6)); */
                /* _gravityStore->print("gravity.sto"); */

                //-------------------------------- UPDATE TIME ---------------------------------

                initialStepTime += stepSize;
                finalStepTime = initialStepTime + stepSize;
            }		
            // end timing
            cout << "simulation time = " << 1.e3*(clock()-startTime)/CLOCKS_PER_SEC << "ms" << endl;
        }
    }
    catch (OpenSim::Exception ex)
    {
        std::cout << ex.getMessage() << std::endl;
        return 1;
    }
    catch (SimTK::Exception::Base ex)
    {
        std::cout << ex.getMessage() << std::endl;
        return 1;
    }
    catch (std::exception ex)
    {
        std::cout << ex.what() << std::endl;
        return 1;
    }
    catch (...)
    {
        std::cout << "UNRECOGNIZED EXCEPTION" << std::endl;
        return 1;
    }
}
