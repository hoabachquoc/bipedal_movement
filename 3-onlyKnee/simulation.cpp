#include <OpenSim/OpenSim.h>
#include <Simbody.h>
#include <cmath>
#include <qpOASES.hpp>

#include <iostream>
#include <fstream>
#include <cstdlib>
#include <vector>

using namespace OpenSim;
using namespace SimTK;
using namespace std;
USING_NAMESPACE_QPOASES


//------------------------------------- GENERAL FUNCTIONS ---------------------------------------
bool simulation = 1;
bool useVisualizer = 0;		// cause segmentation fault !!!
bool printRefTrajectory = 0;
bool printInitialCtrl = 1;
bool printInitialState = 1;
bool printlog = 0;
bool printDesAcc = 0;
bool printNewCtrls = 1;

int setInitialState = 2;	// default: data || 1: all 0 || 2: all 0 except knee angle 0.1
int setInitCtrl = 2;		// 1: gravity compensation || 2: all 0 || 3: all 3N
int testCase = 2;		// default: data || 1: goal - origin || 2: goal - vertical top
bool setqpOASES = 1; 		// 1: qpOASES || 0: Interior Point
bool setIpcostFunc = 0;		// 1: integrator || 0 : qp

SimTK::Real weightTerm = 0;	// go easy with it !!!

//---------------------------------------- END-EFFECTOR -----------------------------------------
Vec3 eefPoint(0,0.5,0);

//------------------------------------- INTEGRATOR OPTIONS --------------------------------------
unsigned stepNum = 1;
const Real stepSize = 1e-3;
double initialStepTime = 0.0;
double finalStepTime = initialStepTime + stepSize;


//------------------------------------- FEEDBACK CONTROLERS -------------------------------------
Mat33 Kp(36); // position gain
Mat33 Kv(12); // velocity gain

//-------------------------------------- MODEL PROPERTIES ---------------------------------------
double linkageLength = 0.5;
double positionCoM = linkageLength/2;
double linkageMass = 1;
Mat33 inertialLinkage;
double g = -9.80655;

//---------------------------------------- MISCELLANEOUS ----------------------------------------
SimTK::Matrix analyticMassMatrix(2,2);


//===============================================================================================
//				     TRAJECTORY GENERATION - KDL
//===============================================================================================
int calcTrajVecLength()
{
	string s;
	int sTotal(0);

	ifstream in;
	in.open("velocity.dat");

	while(!in.eof()) {
		getline(in, s);
		sTotal ++;	
	}
	return sTotal - 1;
}

void printvector(vector<double> &vect)
{
	for (vector<double>::const_iterator iter = vect.begin(); iter != vect.end(); ++iter)
		cout << *iter << endl;
}

void generateTrajectry ( Vector_<Vec3> &refPos, Vector_<Vec3> &refVel, Vector_<Vec3> &refAcc )
{
	// Read reference acceleration
	ifstream acc_in("acceleration.dat");
	if (!acc_in)
	{
		cerr << "Uh oh, file .dat could not be opened for reading!" << endl;
		exit(1);
	}
	// Create the vector, initialized from the numbers in the file:
	std::vector<double> accData (( std::istream_iterator<double>(acc_in)),
			std::istream_iterator<double>());
	int i = 0;
	for (int count = 0; count < accData.size(); count+=6)
	{
		refAcc[i] = Vec3( accData[count], accData[count+1], 0);
		i++;
	}

	// Read reference velocity
	ifstream vel_in("velocity.dat");
	if (!vel_in)
	{
		cerr << "Uh oh, file .dat could not be opened for reading!" << endl;
		exit(1);
	}
	// Create the vector, initialized from the numbers in the file:
	std::vector<double> velData (( std::istream_iterator<double>(vel_in)),
			std::istream_iterator<double>());
	i = 0;
	for (int count = 0; count < velData.size(); count+=6)
	{
		refVel[i] = Vec3( velData[count], velData[count+1], 0);
		i++;
	}

	// Read reference position
	ifstream traj_in("position.dat");
	if (!traj_in)
	{
		cerr << "Uh oh, file .dat could not be opened for reading!" << endl;
		exit(1);
	}

	std::vector<double> posData (( std::istream_iterator<double>(traj_in)),
			std::istream_iterator<double>());
	int j = 0;
	for (int count = 0; count < posData .size(); count+=16)
	{
		refPos[j] = Vec3( posData [count+3], posData [count+7], 0);
		j++;
	}
}

//===============================================================================================
// 			               OPTIMIZATION SYSTEM
//===============================================================================================
class OneLegqpOASESOpt{

private:
	int numPar;
	State& si;
	Model& osimModel;
	Vec3& desAcc;
public:

OneLegqpOASESOpt(int numParameters, State& s, Model& aModel, Vec3& adesAcc): 
	numPar(numParameters), si(s), osimModel(aModel), desAcc(adesAcc)
{
}

double getAnkleAngle(){
	return osimModel.getCoordinateSet().get("ankle_rotZ").getValue(si) + SimTK::Pi/2;
	/* return osimModel.getCoordinateSet().get("ankle_rotZ").getValue(si); */
}

double getAnkleAngVel(){
	return osimModel.getCoordinateSet().get("ankle_rotZ").getSpeedValue(si);
}

double getKneeAngle(){
	return osimModel.getCoordinateSet().get("knee_rotZ").getValue(si);
}

double getKneeAngVel(){
	return osimModel.getCoordinateSet().get("knee_rotZ").getSpeedValue(si);
}

Vector calcCoriolisForces() {
	/* SimTK::Matrix coriolisMat(2,2); */
	/* Vector angle_vel(2); */
	/* angle_vel(0) = getAnkleAngVel(); */
	/* angle_vel(1) = getKneeAngVel(); */
	/* osimModel.getBodySet().get("linkage2").getInertia(inertialLinkage); */
	/* double alpha = inertialLinkage(2,2)*2 + linkageMass*positionCoM*positionCoM */
	/* 	+ linkageMass*(linkageLength*linkageLength + positionCoM*positionCoM); */
	/* double beta = linkageMass*linkageLength*positionCoM; */
	/* double delta = inertialLinkage(2,2) + linkageMass*positionCoM*positionCoM; */
	/* double betas2 = beta*sin(getKneeAngle()); */
	/* coriolisMat(0,0) = -betas2*getKneeAngVel(); */
	/* coriolisMat(1,0) =  betas2*getAnkleAngVel(); */
	/* coriolisMat(0,1) = -betas2*(getAnkleAngVel()+getKneeAngVel()); */
	/* coriolisMat(1,1) =  0; */
	/* return coriolisMat * angle_vel; */

	Vector coriolisForces(2);
	coriolisForces(0) = - linkageMass * linkageLength * linkageLength *
			sin(getKneeAngle()) * getAnkleAngVel() * getKneeAngVel() 
	 			- linkageMass * linkageLength * linkageLength/2*
			sin(getKneeAngle()) * getKneeAngVel() * getKneeAngVel();

	coriolisForces(1) = 1/2 * linkageMass * linkageLength * linkageLength *
			sin(getKneeAngle()) * getAnkleAngVel()* getAnkleAngVel();
	return coriolisForces; 
}

Vector calcGravityForces() {
	Vector gravityForces(2);
	gravityForces(0) = linkageMass * g * linkageLength * 0.5 * cos(getAnkleAngle()) 
		+ linkageMass * g * linkageLength * 
			(0.5 * cos(getAnkleAngle() + getKneeAngle()) + cos(getAnkleAngle()));
	gravityForces(1) = linkageMass * g * linkageLength/2 * 
				cos(getAnkleAngle() + getKneeAngle());
	return gravityForces;
}

const SimbodyMatterSubsystem& matterSubsystem() {
	return osimModel.getMatterSubsystem(); 
}

SimTK::Matrix calcInverseMassMatrix(){
	SimTK::Matrix Minv;
	matterSubsystem().calcMInv(si,Minv);
	return Minv;
}

SimTK::Matrix calcStationJacobian(){
	SimTK::Matrix JS;
	matterSubsystem().calcStationJacobian(si,MobilizedBodyIndex(2),eefPoint,JS);
	return JS;
}


    // We just calculated f_residual = M udot + f_inertial - f_applied, with
    // both udot and f_applied zero, i.e. f_residual=f_inertial. That should
    // be the same as what is returned by getTotalCentrifugalForces().
Vector inertialForce() {
	const SimbodyMatterSubsystem& matter = osimModel.getMatterSubsystem();
	const int nb = matter.getNumBodies();
	Vector_<SpatialVec> F_inertial(nb);
	Vector f_inertial;
	for (MobodIndex i(0); i<nb; ++i)
		F_inertial[i] = matter.getTotalCentrifugalForces(si, i);
	matter.multiplyBySystemJacobianTranspose(si, F_inertial, f_inertial);
	return f_inertial;
}

Vector getJSDotQDot() {
	Vec3 JSDotu = matterSubsystem().calcBiasForStationJacobian
		(si, MobilizedBodyIndex(2), eefPoint);
	Vector JSDotQDot(2);
	JSDotQDot(0) = JSDotu(0);
	JSDotQDot(1) = JSDotu(1);
	return JSDotQDot;
}

SimTK::Matrix multipleJS_Minv() {
	return calcStationJacobian() * calcInverseMassMatrix();
}

Vector calcGravity() {
	Vector g;
	osimModel.getMatterSubsystem().multiplyBySystemJacobianTranspose(
		si, osimModel.getGravityForce().getBodyForces(si), g);
	return g;
}

Vector calcCoriolis() {
	Vector c;
	osimModel.getMatterSubsystem().calcResidualForceIgnoringConstraints(
		si, SimTK::Vector(0), SimTK::Vector_<SimTK::SpatialVec>(0),
			SimTK::Vector(0), c);
	return c;
}


// M(q).qdd + B(qd) = G(q) + T
// --> qdd = Minv.( T - B + G)
// Xd = J.qd
// --> Xdd = Jdot.qdot + J.qdd
// --> Xdd = Jdot.qdot + J.Minv.( T - B + G)
// --> Xdd = Jdot.qdot + J.Minv.T + J.Minv.(- B + G )
// Xdd_des = Xdd_traj_ + p_gains_.( X_des - X_curr_) + d_gains_.( Xd_des - Xd_curr_)
// ==> We want to compute min(T) || Xdd - Xdd_des ||²
// If with replace, we can put it in the form ax + b
// With a = J.Minv
//      b = J.Minv.(- B + G ) + Jdot.qdot - Xdd_des

SimTK::Matrix calca_() {
	// Prepare actuators - dynamic cast
	PathActuator& leftKneeMuscle = dynamic_cast<PathActuator&>
		(osimModel.getActuators().get(0));
	PathActuator& rightKneeMuscle = dynamic_cast<PathActuator&>
		(osimModel.getActuators().get(1));

	// Prepare coordinates
	/* Coordinate& ankle_ = osimModel.getCoordinateSet().get(0); */
	Coordinate& knee_ = osimModel.getCoordinateSet().get(1);

	// Moment arm matrix
	SimTK::Matrix selection_Mat (2,2);
	selection_Mat (0,0) = 0;
	selection_Mat (0,1) = 0;
	selection_Mat (1,0) = leftKneeMuscle.computeMomentArm(si, knee_);
	selection_Mat (1,1) = rightKneeMuscle.computeMomentArm(si, knee_);

	return multipleJS_Minv()(0,0,2,2) * selection_Mat
		* osimModel.getActuators().get(0).getOptimalForce();
}

Vector calcb_() {
	Vector desAcc_planXY(2);
	desAcc_planXY(0) = desAcc(0);
	desAcc_planXY(1) = desAcc(1);
	/* Vector f_app = - calcCoriolisForces() + calcGravityForces(); */
	Vector f_app =  - calcCoriolis() + calcGravity();
	/* Vector f_app = - inertialForce() + calcGravity(); */
	Vector MInvf_app;
	matterSubsystem().multiplyByMInv(si, f_app, MInvf_app);
	Vec3 JMInvf_app = matterSubsystem().multiplyByStationJacobian
		(si, MobilizedBodyIndex(2), eefPoint, MInvf_app);
	Vector JMInvf_app2(2);
	JMInvf_app2(0) = JMInvf_app(0);
	JMInvf_app2(1) = JMInvf_app(1);
	return JMInvf_app2 + getJSDotQDot() - desAcc_planXY;
}

SimTK::Matrix calcH_() {
	return ~calca_()*calca_();
}

Vector calcg_() {
	return ~calca_()*calcb_();
}

Vector calclb_() {
	int size = osimModel.getActuators().getSize();
	Vector lb_(size);
	for (int i = 0; i < size; i++)
		lb_(i)=osimModel.getActuators().get(i).getMinControl();
	return lb_;
}

Vector calcub_() {
	int size = osimModel.getActuators().getSize();
	Vector ub_(size);
	for (int i = 0; i < size; i++)
		ub_(i)=osimModel.getActuators().get(i).getMaxControl();
	return ub_;
}

// Task: Move to a point
Vector optimize(){
	real_t H[numPar*numPar];
	real_t g[numPar];
	real_t lb[numPar];
	real_t ub[numPar];

	SimTK::Matrix H_ = calcH_();	
	Vector g_ = calcg_();
	Vector lb_ = calclb_();
	Vector ub_ = calcub_();

	for (int i = 0; i < numPar; i++)
	{
		for (int j = 0; j < numPar; j++)
			H[i*numPar+j] = H_(i,j);
		g[i] = g_(i);
		lb[i] = lb_(i);
		ub[i] = ub_(i);
	}

	/* Setting up QProblem object. */
	QProblemB example(numPar);

	Options options;
	options.printLevel = PrintLevel(0);
	example.setOptions( options );

	/* Solve first QP. */
	int_t nWSR = 100;
	example.init( H,g,lb,ub,nWSR );

	/* Get and print solution of first QP. */
	real_t xOpt[numPar];
	example.getPrimalSolution( xOpt );

	/* real_t yOpt[2+1]; */
	/* example.getDualSolution( yOpt ); */
	/* printf( "\nxOpt = [ %e, %e ];  yOpt = [ %e, %e, %e ];  objVal = %e\n\n", */ 
	/* xOpt[0],xOpt[1],yOpt[0],yOpt[1],yOpt[2],example.getObjVal() ); */
	/* printf( "\nxOpt = [ %e, %e ]; objVal = %e\n\n", xOpt[0],xOpt[1],example.getObjVal() ); */

	/* example.printOptions(); */
	/*example.printProperties();*/

	/*getGlobalMessageHandler()->listAllMessages();*/
	Vector controlOpt(numPar);
	for (int i = 0; i < numPar; i++)
		controlOpt(i) = xOpt[i];
	return controlOpt;
}
};

class OpensimOptimizationSystem : public OptimizerSystem {

private:
	int numControls;
	State& si;
	Model& osimModel;
	Vec3& desInput;

public:
	OpensimOptimizationSystem(int numParameters, State& s, Model& aModel, Vec3& desAcc): 
             numControls(numParameters), OptimizerSystem(numParameters), 
		si(s), osimModel(aModel), desInput(desAcc){}
			 	
int objectiveFunc(  const Vector &newControls, bool new_coefficients, Real& f ) const {

	if (setIpcostFunc) 
	{
		// make a copy of out initial states
		State s = si;

		// Update the control values
		//newControls.dump("New Controls In:");
		osimModel.updDefaultControls() = newControls;

		// Create the integrator for the simulation.
		RungeKuttaMersonIntegrator integrator(osimModel.getMultibodySystem());
		integrator.setAccuracy(1.0e-4);
		integrator.setAllowInterpolation(false);
		
		// Integration without manager
		/* integrator.initialize(si); */
		/* integrator.stepBy(stepSize); */

		// Create a manager to run the simulation
		Manager manager(osimModel, integrator);
		manager.setInitialTime(initialStepTime);
		manager.setFinalTime(finalStepTime);
		manager.integrate(s);

		//osimModel.getControls(s).dump("Model Controls:");
		osimModel.getMultibodySystem().realize(s, Stage::Acceleration);

		Vec3 curAcc;
		// Don't forget state s for god sake !!!
		osimModel.getSimbodyEngine().getAcceleration
			(s, osimModel.getBodySet().get("linkage2"), eefPoint, curAcc);

		Vec3 err;
		err = desInput - curAcc;

		// Sum of forces produced by actuators
		for (int actNb = 0; actNb < osimModel.getNumControls(); actNb ++)
		{
			f += osimModel.getActuators().get(actNb).getControl(s) * 
				osimModel.getActuators().get(actNb).getOptimalForce();
		}
		
		f = f * weightTerm + dot(err,err);
	}
	else
	{
		const SimbodyMatterSubsystem& matter = osimModel.getMatterSubsystem();
		const int nb = matter.getNumBodies();

		SimTK::Matrix Minv;
		matter.calcMInv(si,Minv);

		SimTK::Matrix JS;
		matter.calcStationJacobian
			(si,MobilizedBodyIndex(2),eefPoint,JS);

		SimTK::Vector gr;
		matter.multiplyBySystemJacobianTranspose(
			si, osimModel.getGravityForce().getBodyForces(si), gr);

		SimTK::Vector cor;
		matter.calcResidualForceIgnoringConstraints(
			si, SimTK::Vector(0), SimTK::Vector_<SimTK::SpatialVec>(0),
				SimTK::Vector(0),cor);

		SimTK::Matrix JSmultipleMinv = JS * Minv;
		SimTK::Matrix a_ = JSmultipleMinv(0,0,2,2);

		SimTK::Vector desAcc_planXY(2);
		desAcc_planXY(0) = desInput(0);
		desAcc_planXY(1) = desInput(1);

		SimTK::Vector f_app =  gr - cor;

		SimTK::Vector MInvf_app;
		matter.multiplyByMInv(si, f_app, MInvf_app);

		Vec3 JMInvf_app = matter.multiplyByStationJacobian
			(si, MobilizedBodyIndex(2), eefPoint, MInvf_app);
		SimTK::Vector JMInvf_app2(2);
		JMInvf_app2(0) = JMInvf_app(0);
		JMInvf_app2(1) = JMInvf_app(1);

		SimTK::Vec3 JSDotu = matter.calcBiasForStationJacobian
			(si, MobilizedBodyIndex(2), eefPoint);
		SimTK::Vector JSDotQDot(2);
		JSDotQDot(0) = JSDotu(0);
		JSDotQDot(1) = JSDotu(1);

		// Prepare actuators - dynamic cast
		PathActuator& leftAnkleMuscle = dynamic_cast<PathActuator&>
			(osimModel.getActuators().get(0));
		PathActuator& rightAnkleMuscle = dynamic_cast<PathActuator&>
			(osimModel.getActuators().get(1));
		PathActuator& leftKneeMuscle = dynamic_cast<PathActuator&>
			(osimModel.getActuators().get(2));
		PathActuator& rightKneeMuscle = dynamic_cast<PathActuator&>
			(osimModel.getActuators().get(3));

		// Prepare coordinates
		Coordinate& ankle_ = osimModel.getCoordinateSet().get(0);
		Coordinate& knee_ = osimModel.getCoordinateSet().get(1);

		// Moment arm matrix
		SimTK::Vector b_ =  JMInvf_app2 + JSDotQDot - desAcc_planXY;
		SimTK::Matrix selection_Mat (2,4);
		selection_Mat (0,0) = leftAnkleMuscle.computeMomentArm(si, ankle_);
		selection_Mat (0,1) = rightAnkleMuscle.computeMomentArm(si, ankle_);
		selection_Mat (0,2) = 0;
		selection_Mat (0,3) = 0;
		selection_Mat (1,0) = 0;
		selection_Mat (1,1) = 0;
		selection_Mat (1,2) = leftKneeMuscle.computeMomentArm(si, knee_);
		selection_Mat (1,3) = rightKneeMuscle.computeMomentArm(si, knee_);

		// Cost function
		Vector torque_ = selection_Mat * newControls;
		torque_ = torque_ * osimModel.getActuators().get(0).getOptimalForce();
		f = ~(a_*torque_+b_)*(a_*torque_+b_)
			+ weightTerm * (torque_(0) + torque_(1));
	}
	return(0);
}	

};

//===============================================================================================
// 				              MAIN
//===============================================================================================


int main()
{
	try {
		std::clock_t startTime = std::clock();

//----------------------------------------- LOAD MODEL ------------------------------------------
		Model osimModel("Mux4_noAnkleMus.osim");
		osimModel.setUseVisualizer(useVisualizer);

//--------------------------------------- INITIALIZATION ----------------------------------------
		SimTK::State& si = osimModel.initSystem();
		/* cout << "initialized system" << endl; */

//------------------------------------- REFERENCE TRAJECTORY ------------------------------------
		int trajVecLength = calcTrajVecLength();

		Vector_<Vec3> refPos(trajVecLength);
		Vector_<Vec3> refVel(trajVecLength);
		Vector_<Vec3> refAcc(trajVecLength);

		/* Choose a type of reference
		 * default : trajectory from data files
		 * 1 : destination point at the origin
		 * 2 : destination point at the top of the linkage 2 when the system is straight up vertically
		**/ 

		switch (testCase) {
			default:
				generateTrajectry( refPos, refVel, refAcc );
			break;
			case 1:
				for (int count = 0; count < trajVecLength; ++count)
				{
					refPos[count] = Vec3(0); 
					refVel[count] = Vec3(0);
					refAcc[count] = Vec3(0); 
				}
			break;
			case 2:
				for (int count = 0; count < trajVecLength; ++count)
				{
					refPos[count] = Vec3(0,1,0); 
					refVel[count] = Vec3(0);
					refAcc[count] = Vec3(0); 
				}
			break;
		}

		if (printRefTrajectory)
		{
			for (int count = 0; count < trajVecLength; ++count)
			{
				cout << "refPos " << count << " " << refPos[count] << endl;
				cout << "refVel " << count << " " << refVel[count] << endl;
				cout << "refAcc " << count << " " << refAcc[count] << endl;
			}
		}

//--------------------------------- INITIAL ANGLE POSITIONS -------------------------------------
		double anklePos_init;
		double ankleVel_init;
		double kneePos_init;
		double kneeVel_init;

		switch (setInitialState)
		{
			default:
				anklePos_init = -20*SimTK::Pi/180;
				ankleVel_init = 0;
				kneePos_init = -30*SimTK::Pi/180;
				kneeVel_init = 0;
			break;
			case 1:
				anklePos_init = 0;
				ankleVel_init = 0;
				kneePos_init = 0;
				kneeVel_init = 0;
			break;
			case 2:
				anklePos_init = 0;
				ankleVel_init = 0;
				kneePos_init = 0.01;
				kneeVel_init = 0;
			break;
		}

		osimModel.getCoordinateSet().get("ankle_rotZ").setValue(si, anklePos_init);
		osimModel.getCoordinateSet().get("knee_rotZ").setValue(si, kneePos_init);
		osimModel.getCoordinateSet().get("ankle_rotZ").setSpeedValue(si, ankleVel_init);
		osimModel.getCoordinateSet().get("knee_rotZ").setSpeedValue(si, kneeVel_init);

//--------------------------------------- ACTUATORS ---------------------------------------------
		int numControls = osimModel.getNumControls();
		cout << "numControls = " << numControls << endl;


		// Get maximal force of path actuators
		double maxForce = osimModel.getActuators().get(0).getOptimalForce();
		/* cout << "maxForce = " << maxForce << endl; */

		// Get control bounds
		double lowerBoundCtrl = osimModel.getActuators().get(0).getMinControl();
		double upperBoundCtrl = osimModel.getActuators().get(1).getMaxControl();

		// Calculate initial control according to static equilibrum
		Vector initialControls(numControls, 0.0);
		for (int cnt = 0; cnt < numControls; cnt ++)
		{
			switch (setInitCtrl)
			{
				case 2:
					initialControls[cnt] = 0;
				break;
				case 3:
					initialControls[cnt] = 3;
				break;
			}
		}

		osimModel.updDefaultControls() = initialControls;

		if (printInitialCtrl)
		{
			cout << "Initial controls = " << osimModel.getDefaultControls() << endl;
		}

		//Print the control gains
		/* std::cout << "kp = " << Kp << std::endl; */
		/* std::cout << "kv = " << Kv << std::endl; */

//--------------------------------------- VISUALIZER --------------------------------------------
		if (useVisualizer)
		{
			osimModel.updMatterSubsystem().setShowDefaultGeometry(true);
			Visualizer& viz = osimModel.updVisualizer().updSimbodyVisualizer();
			viz.setBackgroundType(viz.SolidColor);
			viz.setBackgroundColor(Cyan);
		}
		
		// Angular state variables
		Vec3 eefPos;
		Vec3 eefVel;
		osimModel.getMultibodySystem().realize(si, Stage::Acceleration);
		osimModel.getSimbodyEngine().getPosition
			(si, osimModel.getBodySet().get("linkage2"), eefPoint, eefPos);
		osimModel.getSimbodyEngine().getVelocity
			(si, osimModel.getBodySet().get("linkage2"), eefPoint, eefVel);

		if (printInitialState)
		{
			cout << "Initial end-effector position  = " << eefPos << endl;
			cout << "Initial end-effector velocity  = " << eefVel << endl;
		}


//---------------------------------------- LOG FILES --------------------------------------------
		// Construct control storage
		Array<string> columnLabels;
		Storage* _controlStore = new Storage(1023,"controls");
		columnLabels.append("time");
		for(int i=0;i<osimModel.getActuators().getSize();i++)
			columnLabels.append(osimModel.getActuators().get(i).getName());
		_controlStore->setColumnLabels(columnLabels);

		// Construct force storage
		Storage* _forceStore = new Storage(1023,"forces");
		_forceStore->setColumnLabels(columnLabels);

		// Initialize storage files
		Vector updctrls(osimModel.getActuators().getSize());
		Vector updforces(osimModel.getActuators().getSize());
		osimModel.getMultibodySystem().realize(si, Stage::Acceleration);

		for (int i = 0; i < osimModel.getActuators().getSize(); i++) {
			updctrls[i] = osimModel.getActuators().get(i).getControl(si);
			updforces[i] = updctrls[i] * maxForce;
		} 
		/* cout << updctrls << endl; */

		_controlStore->store(stepNum, 0.0,osimModel.getActuators().getSize(), &updctrls[0]);
		_controlStore->print("Controls.sto");
		_forceStore->store(stepNum, 0.0,osimModel.getActuators().getSize(), &updforces[0]);
		_forceStore->print("Forces.sto");

		// Construct Cartesian state storage of the end-effector
		Array<string> posColumnLabels;
		Array<string> velColumnLabels;
		Array<string> accColumnLabels;
		Storage* _posCartesianStore = new Storage(1023,"PosCartesian");
		posColumnLabels.append("time");
		posColumnLabels.append("pX");
		posColumnLabels.append("pY");
		posColumnLabels.append("pZ");
		_posCartesianStore->setColumnLabels(posColumnLabels);
		Storage* _velCartesianStore = new Storage(1023,"VelCartesian");
		velColumnLabels.append("time");
		velColumnLabels.append("vX");
		velColumnLabels.append("vY");
		velColumnLabels.append("vZ");
		_velCartesianStore->setColumnLabels(velColumnLabels);
		Storage* _accCartesianStore = new Storage(1023,"AccCartesian");
		accColumnLabels.append("time");
		accColumnLabels.append("aX");
		accColumnLabels.append("aY");
		accColumnLabels.append("aZ");
		_accCartesianStore->setColumnLabels(accColumnLabels);

		Array<string> refPosColumnLabels;
		Storage* _refPosCartesianStore = new Storage(1023,"refstatePos");
		refPosColumnLabels.append("time");
		refPosColumnLabels.append("pXr");
		refPosColumnLabels.append("pYr");
		refPosColumnLabels.append("pZr");
		_refPosCartesianStore->setColumnLabels(refPosColumnLabels);

		Array<string> refVelColumnLabels;
		Storage* _refVelCartesianStore = new Storage(1023,"refstateVel");
		refVelColumnLabels.append("time");
		refVelColumnLabels.append("vXr");
		refVelColumnLabels.append("vYr");
		refVelColumnLabels.append("vZr");
		_refVelCartesianStore->setColumnLabels(refVelColumnLabels);

		Array<string> refAccColumnLabels;
		Storage* _refAccCartesianStore = new Storage(1023,"refstateAcc");
		refAccColumnLabels.append("time");
		refAccColumnLabels.append("aXr");
		refAccColumnLabels.append("aYr");
		refAccColumnLabels.append("aZr");
		_refAccCartesianStore->setColumnLabels(refAccColumnLabels);

		// Initialize state storage
		Vec3 updPosCartesian(0);
		Vec3 updVelCartesian(0);
		Vec3 updAccCartesian(0);
		osimModel.getSimbodyEngine().getPosition
			(si, osimModel.getBodySet().get("linkage2"), eefPoint, updPosCartesian);
		osimModel.getSimbodyEngine().getVelocity
			(si, osimModel.getBodySet().get("linkage2"), eefPoint, updVelCartesian);
		osimModel.getSimbodyEngine().getAcceleration
			(si, osimModel.getBodySet().get("linkage2"), eefPoint, updAccCartesian);

		_posCartesianStore->store(stepNum, 0.0, 3, &updPosCartesian[0]);
		_posCartesianStore->print("PosCartesian.sto");
		_velCartesianStore->store(stepNum, 0.0, 3, &updVelCartesian[0]);
		_velCartesianStore->print("VelCartesian.sto");
		_accCartesianStore->store(stepNum, 0.0, 3, &updAccCartesian[0]);
		_accCartesianStore->print("AccCartesian.sto");

		_refPosCartesianStore->store(stepNum, 0.0, 3, &refPos[0].get(0));
		_refPosCartesianStore->print("RefPosCartesian.sto");
		_refVelCartesianStore->store(stepNum, 0.0, 3, &refVel[0].get(0));
		_refVelCartesianStore->print("RefVelCartesian.sto");
		_refAccCartesianStore->store(stepNum, 0.0, 3, &refAcc[0].get(0));
		_refAccCartesianStore->print("RefAccCartesian.sto");


//----------------------------------- INTEGRATOR MANAGER ----------------------------------------
		SimTK::RungeKuttaMersonIntegrator integ(osimModel.getMultibodySystem());
		integ.setAccuracy(1.0e-4);
		Manager manager(osimModel, integ);

//--------------------------------------- SIMULATION --------------------------------------------
		if (simulation)
		{
			for (int count = 0; count < trajVecLength; ++count)
			{
//------------------------------------- TASK SERVOING -------------------------------------------
				// Compute desired trajectory from PD controller
				Vec3 desAcc = Kp * (refPos[count] - eefPos) 
						+ Kv * (refVel[count] - eefVel) 
						+ refAcc[count];

				if (printDesAcc)
					cout << "desAcc = " << desAcc <<endl;

				/* cout << "Message: End task servoing" << endl; */

//------------------------------------ OPTIMIZATION ---------------------------------------------
		Vector newControls(osimModel.getActuators().getSize());
		if (setqpOASES) 
		{
			OneLegqpOASESOpt sys(numControls, si, osimModel, desAcc);
			newControls = sys.optimize();
		}
		else
		{
			OpensimOptimizationSystem 
				sys(numControls, si, osimModel, desAcc);
			Vector lower_bounds(numControls, lowerBoundCtrl);
			Vector upper_bounds(numControls, upperBoundCtrl);
			sys.setParameterLimits( lower_bounds, upper_bounds );

			/* Optimizer opt(sys, SimTK::LBFGSB); */
			Optimizer opt(sys, InteriorPoint);

			// Specify settings for the optimizer
			opt.setConvergenceTolerance(0.001);
			opt.useNumericalGradient(true);
			opt.setMaxIterations(200);
			opt.setLimitedMemoryHistory(500);
			
			Real f;	
			f = opt.optimize(newControls);
			/* cout <<  "f " << count << " = " << f << endl; */
		}
		osimModel.updDefaultControls() = newControls;

		if (printNewCtrls)
			cout << "newControls " << count << " = " << newControls << endl;


//----------------------------------- ADVANCE TO NEXT STEP --------------------------------------
		manager.setInitialTime(initialStepTime);
		manager.setFinalTime(finalStepTime);
		manager.integrate(si);

		// Update current state
		osimModel.getMultibodySystem().realize(si, Stage::Acceleration);
		osimModel.getSimbodyEngine().getPosition
			(si, osimModel.getBodySet().get("linkage2"), eefPoint, eefPos);
		osimModel.getSimbodyEngine().getVelocity
			(si, osimModel.getBodySet().get("linkage2"), eefPoint, eefVel);

		/* cout << "Message: Advanced to next step" << endl; */

//----------------------------------- UPDATE LOG FILES ------------------------------------------
// Store State variables
manager.getStateStorage().print("States.sto");
/* cout << "Message: Stored State variables" << endl; */

// Store controls, forces and moment arms
for (int i = 0; i < osimModel.getActuators().getSize(); i++) {
	updctrls[i] = osimModel.getActuators().get(i).getControl(si); 
	updforces[i] = updctrls[i] * maxForce;
}
/* cout << "Message: Moment arm update loop ended" << endl; */

_controlStore->store(stepNum, si.getTime(),osimModel.getActuators().getSize(), &updctrls[0]);
_controlStore->print("Controls.sto");
_forceStore->store(stepNum, si.getTime(),osimModel.getActuators().getSize(), &updforces[0]);
_forceStore->print("Forces.sto");

/* cout << "Message: Stored controls, forces and moment arms" << endl; */

// Store Cartesian state of the end-effector
osimModel.getSimbodyEngine().getPosition
	(si, osimModel.getBodySet().get("linkage2"), eefPoint, updPosCartesian);
osimModel.getSimbodyEngine().getVelocity
	(si, osimModel.getBodySet().get("linkage2"), eefPoint, updVelCartesian);
osimModel.getSimbodyEngine().getAcceleration
	(si, osimModel.getBodySet().get("linkage2"), eefPoint, updAccCartesian);

if (printlog)
{
	cout << "pos " << count << "= " << updPosCartesian << 
		"\t" << "ref pos " << count << "= " << refPos[count] <<
		"\t" << "vel " << count << "= " << updVelCartesian <<
		"\t" << "ref vel " << count << "= " << refVel[count] <<
		endl;
}

_posCartesianStore->store(stepNum, si.getTime(), 3, &updPosCartesian[0] );
_posCartesianStore->print("PosCartesian.sto");
_velCartesianStore->store(stepNum, si.getTime(), 3, &updVelCartesian[0] );
_velCartesianStore->print("VelCartesian.sto");
_accCartesianStore->store(stepNum, si.getTime(), 3, &updAccCartesian[0] );
_accCartesianStore->print("AccCartesian.sto");
_refPosCartesianStore->store(stepNum, si.getTime(), 3, &refPos[count].get(0) );
_refPosCartesianStore->print("RefPosCartesian.sto");
_refVelCartesianStore->store(stepNum, si.getTime(), 3, &refVel[count].get(0));
_refVelCartesianStore->print("RefVelCartesian.sto");
_refAccCartesianStore->store(stepNum, si.getTime(), 3, &refAcc[count].get(0));
_refAccCartesianStore->print("RefAccCartesian.sto");

/* cout << "Message: Updated log files" << endl; */

//-------------------------------------- UPDATE TIME --------------------------------------------
				initialStepTime += stepSize;
				finalStepTime = initialStepTime + stepSize;
			}		
		}
	}
	catch (OpenSim::Exception ex)
	{
		std::cout << ex.getMessage() << std::endl;
		return 1;
	}
	catch (SimTK::Exception::Base ex)
	{
		std::cout << ex.getMessage() << std::endl;
		return 1;
	}
	catch (std::exception ex)
	{
		std::cout << ex.what() << std::endl;
		return 1;
	}
	catch (...)
	{
		std::cout << "UNRECOGNIZED EXCEPTION" << std::endl;
		return 1;
	}
}
