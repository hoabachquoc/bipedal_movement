reset
set terminal png 
set output "PosEef.png"
set title 'Position of the end-effector'
set ylabel 'Position (m)'
set xlabel 'Time (s)'
set grid
plot "<paste time.dat position.dat" using 1:5 with lines lc rgb "red" lw 2 title "X",\
     "<paste time.dat position.dat" using 1:9 with lines lc rgb "blue" lw 2 title "Y" 


set terminal png
set output "VelEef.png"
set title 'Velocity of the end-effector'
set ylabel 'Velocity (m/s)'
set xlabel 'Time (s)'
set grid
plot "<paste time.dat velocity.dat" using 1:2 with lines lc rgb "red" lw 2 title "X",\
     "<paste time.dat velocity.dat" using 1:3 with lines lc rgb "blue" lw 2 title "Y" 


set terminal png
set output "AccEef.png"
set title 'Acceleration X of the end-effector'
set ylabel 'Acceleration (m/s²)'
set xlabel 'Time (s)'
set grid
plot "<paste time.dat acceleration.dat" using 1:2 with lines lc rgb "red" lw 2 title "X",\
     "<paste time.dat acceleration.dat" using 1:3 with lines lc rgb "blue" lw 2 title "Y"
