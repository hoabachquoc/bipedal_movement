/**
 * \file path_example.cpp
 * An example to demonstrate the use of trajectory generation
 * functions.
 *
 * There are is a matlab/octave file in the examples directory to visualise the results
 * of this example program. (visualize_trajectory.m)
 *
 */

#include <frames.hpp>
#include <frames_io.hpp>
#include <trajectory.hpp>
#include <trajectory_segment.hpp>
#include <trajectory_stationary.hpp>
#include <trajectory_composite.hpp>
#include <trajectory_composite.hpp>
#include <velocityprofile_trap.hpp>
#include <path_roundedcomposite.hpp>
#include <rotational_interpolation_sa.hpp>
#include <utilities/error.h>
#include <trajectory_composite.hpp>

int main(int argc,char* argv[]) {
	using namespace KDL;
	// Create the trajectory:
    // use try/catch to catch any exceptions thrown.
    // NOTE:  exceptions will become obsolete in a future version.
	try {
        // Path_RoundedComposite defines the geometric path along
        // which the robot will move.
		//
		Path_RoundedComposite* path1 = new Path_RoundedComposite(0.2,0.01,new RotationalInterpolation_SingleAxis());
		// The routines are now robust against segments that are parallel.
		// When the routines are parallel, no rounding is needed, and no attempt is made
		// add constructing a rounding arc.
		// (It is still not possible when the segments are on top of each other)
		// Note that you can only rotate in a deterministic way over an angle less then M_PI!
		// With an angle == M_PI, you cannot predict over which side will be rotated.
		// With an angle > M_PI, the routine will rotate over 2*M_PI-angle.
		// If you need to rotate over a larger angle, you need to introduce intermediate points.
		// So, there is a common use case for using parallel segments.
		path1->Add(Frame(Rotation::RPY(M_PI,0,0), Vector(0.55,0.75,0)));
		path1->Add(Frame(Rotation::RPY(M_PI,0,0), Vector(0.0,0.9,0)));
		path1->Add(Frame(Rotation::RPY(M_PI,0,0), Vector(-0.55,0.75,0)));

		// always call Finish() at the end, otherwise the last segment will not be added.
		path1->Finish();
		
		// Path 2
		Path_RoundedComposite* path2 = new Path_RoundedComposite(0.2,0.01,new RotationalInterpolation_SingleAxis());
		path2->Add(Frame(Rotation::RPY(M_PI/2,0,0), Vector(-0.55,0.75,0)));
		path2->Add(Frame(Rotation::RPY(M_PI/4,0,0), Vector(0.0,0.7,0)));
		path2->Add(Frame(Rotation::RPY(M_PI/2,0,0), Vector(0.55,0.75,0)));
		path2->Finish();

		// Trajectory defines a motion of the robot along a path.
		// This defines a trapezoidal velocity profile.
		// Velocity_Trap(max Vec, max Acc)
		VelocityProfile* velpref1 = new VelocityProfile_Trap(0.6,0.5);
		velpref1->SetProfile(0,path1->PathLength());  
		Trajectory* traject1 = new Trajectory_Segment(path1, velpref1);

		VelocityProfile* velpref2 = new VelocityProfile_Trap(0.6,0.8);
		velpref2->SetProfile(0,path2->PathLength());  
		Trajectory* traject2 = new Trajectory_Segment(path2, velpref2);

		Trajectory_Composite* ctraject = new Trajectory_Composite();
		ctraject->Add(traject1);
		ctraject->Add(traject2);
		ctraject->Add(new Trajectory_Stationary(0.3, Frame(Rotation::RPY(M_PI/4,0,0), Vector(0.55,0.75,0))));

		/* std::cout << "Trajectory duration : " << traject->Duration() << std::endl; */

//------------------------------------- WRITE TRAJECTORIES --------------------------------------

		double dt = 0.001;
		std::ofstream ofp("./position.dat");
		std::ofstream oft("./time.dat");
		for (double t = 0.0; t <= ctraject->Duration(); t+= dt) {
			Frame current_pose;
			current_pose = ctraject->Pos(t);
			for (int i=0;i<4;++i)
				for (int j=0;j<4;++j)
					ofp << current_pose(i,j) << "\t";
			ofp << "\n";
			oft << t << "\n";
		}
		ofp.close();
		oft.close();

		std::ofstream ofv("./velocity.dat");
		for (double t=0.0; t <= ctraject->Duration(); t+= dt) {
			Twist current_vel;
			current_vel = ctraject->Vel(t);
			for (int i=0;i<6;++i)
				ofv << current_vel(i) << "\t";
			ofv << "\n";
		}
		ofv.close();

		std::ofstream ofa("./acceleration.dat");
		for (double t=0.0; t <= ctraject->Duration(); t+= dt) {
			Twist current_acc;
			current_acc = ctraject->Acc(t);
			for (int i=0;i<6;++i)
				ofa << current_acc(i) << "\t";
			ofa << "\n";
		}
		ofa.close();

		// you can get some meta-info on the path:
		/* for (int segmentnr=0;  segmentnr < path->GetNrOfSegments(); segmentnr++) { */
		/* 	double starts,ends; */
		/* 	Path::IdentifierType pathtype; */
		/* 	if (segmentnr==0) { */
		/* 		starts = 0.0; */
		/* 	} else { */
		/* 		starts = path->GetLengthToEndOfSegment(segmentnr-1); */
		/* 	} */
		/* 	ends = path->GetLengthToEndOfSegment(segmentnr); */
		/* 	pathtype = path->GetSegment(segmentnr)->getIdentifier(); */
		/* 	std::cout << "segment " << segmentnr << " runs from s="<<starts << " to s=" <<ends; */
		/* 	switch(pathtype) { */
		/* 		case Path::ID_CIRCLE: */
		/* 			std::cout << " circle"; */
		/* 			break; */
		/* 		case Path::ID_LINE: */
		/* 			std::cout << " line "; */
		/* 			break; */
		/* 		default: */
		/* 			std::cout << " unknown "; */
		/* 			break; */
		/* 	} */
		/* 	std::cout << std::endl; */
		/* } */
        std::cout << " position written to the ./position.dat file " << std::endl;
        std::cout << " velocity written to the ./velocity.dat file " << std::endl;
        std::cout << " acceleration written to the ./acceleration.dat file " << std::endl;

        delete ctraject;
	} catch(Error& error) {
		std::cout <<"I encountered this error : " << error.Description() << std::endl;
		std::cout << "with the following type " << error.GetType() << std::endl;
	}

}


