#include <OpenSim/OpenSim.h>
#include <Simbody.h>

#include <cmath>


using namespace OpenSim;
using namespace SimTK;
using namespace std;


int main()
{
	try {
		Model osimModel("TwoLinksWrapObj.osim");
		osimModel.setUseVisualizer(true);
		
		// Initialize system
		SimTK::State& si = osimModel.initSystem();
		
		// Pin joint initial states
		CoordinateSet &coordinates = osimModel.updCoordinateSet();
		coordinates[0].setValue(si, -Pi/3, false);
		coordinates[1].setValue(si, 2*Pi/3, false);

		// Setup integrator and manager
		SimTK::RungeKuttaMersonIntegrator integrator(osimModel.getMultibodySystem());
		integrator.setAccuracy(1.0e-3);

		Manager manager(osimModel, integrator);
		
		double t0(0.0), tf(15);
		manager.setInitialTime(t0);
		manager.setFinalTime(tf);
		std::cout<<"\n\nIntegrating from " << t0 << " to " << tf << std::endl;
		manager.integrate(si);
	}
	catch (OpenSim::Exception ex)
	{
		std::cout << ex.getMessage() << std::endl;
		return 1;
	}
	catch (SimTK::Exception::Base ex)
	{
		std::cout << ex.getMessage() << std::endl;
		return 1;
	}
	catch (std::exception ex)
	{
		std::cout << ex.what() << std::endl;
		return 1;
	}
	catch (...)
	{
		std::cout << "UNRECOGNIZED EXCEPTION" << std::endl;
		return 1;
	}
	std::cout << "OpenSim example completed successfully" << std::endl;
	std::cout << "Press return to continue" << std::endl;
	std::cin.get();
	return 0;
}
