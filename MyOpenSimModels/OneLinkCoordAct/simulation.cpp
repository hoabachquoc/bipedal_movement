#include <OpenSim/OpenSim.h>
#include <Simbody.h>
#include <cmath>

using namespace OpenSim;
using namespace SimTK;
using namespace std;

int stepCount = 0;
unsigned stepNum = 1;
const Real stepSize = 0.01;
double initialStepTime = 0.0;
double finalStepTime = initialStepTime + stepSize;
double bestSoFar = Infinity;

double anklePos_init = -SimTK::Pi/4;
double ankleVel_init = 0;
double initialCtrlValue = 0.5;

// Set gain for the feedforward and feedback controllers
double Kp(1600.0); // position gain
double Kv(80.0); // velocity gain

SimTK::Real weightTerm = 1.0;


//=======================================================================
// 						OPTIMIZATION SYSTEM
//=======================================================================

class OneLegOptimizationSystem : public OptimizerSystem {
	
	private:
		int numKnobs;
		State& si;
		Model& osimModel;
		Vec3& desTraj;

	public:

		/* Constructor class. Parameters passed are accessed in the objectiveFunc() class. */
		OneLegOptimizationSystem(int numParameters, State& s, Model& aModel, Vec3& adesTraj): 
             numKnobs(numParameters), OptimizerSystem(numParameters), si(s), osimModel(aModel), desTraj(adesTraj)
		{
		}
			 	
	int objectiveFunc(const Vector &newControls, bool new_coefficients, Real& f) const {
		
		// make a copy of out initial states
        State s = si;
		osimModel.getMultibodySystem().realize(s, Stage::Acceleration);
		
		// Update the control values
		osimModel.updDefaultControls() = newControls;

		// Get Actuator Set
		const Set<Actuator> &actSet = osimModel.getActuators();
		cout << "\nobjective evaluation #: " << stepCount << " start." << "\nNew controls = " << 
			osimModel.getControls(s) << std::endl;

//------------------------------- INTEGRATOR ----------------------------------
		RungeKuttaMersonIntegrator integrator(osimModel.getMultibodySystem());
		integrator.setAccuracy(1.0e-4);
		Manager manager(osimModel, integrator);
		manager.setInitialTime(initialStepTime);
		manager.setFinalTime(finalStepTime);
		manager.integrate(s);
		
//		integrator.setAllowInterpolation(false);
//		integrator.initialize(s);
//		integrator.stepBy(stepSize);


//------------------------------- OBJECTIVE FUNCTION ----------------------------------
		osimModel.getMultibodySystem().realizeModel(s);
		osimModel.getMultibodySystem().realize(s, Stage::Acceleration);
		double angAcc = osimModel.getCoordinateSet().get("ankle_rotZ").getAccelerationValue(s);
		double angPos = osimModel.getCoordinateSet().get("ankle_rotZ").getValue(s);

		f = pow(desTraj[2] - angAcc, 2);

		stepCount++;
		
        if( f < bestSoFar){
			bestSoFar = f;
			cout << "objective evaluation #: " << stepCount << " end." << 
			"\nDesired trajectories = " << desTraj <<
			"\nAngle position = " << angPos << "\nQuadratic sum of state errors  = " << f << std::endl;
		}
		return(0);
   }
};


//=======================================================================
// 									MAIN
//=======================================================================


int main()
{
	try {
//------------------------------- LOAD MODEL ----------------------------------
		Model osimModel("OneLinkCoordAct.osim");
		osimModel.setUseVisualizer(true);

//------------------------------- INITIALIZATION ------------------------------
		SimTK::State& si = osimModel.initSystem();

		// Set pin joint initial states
		osimModel.getCoordinateSet().get("ankle_rotZ").setValue(si, anklePos_init);
		osimModel.getCoordinateSet().get("ankle_rotZ").setSpeedValue(si, ankleVel_init);
		
		// The number of controls will equal the number of actuators in the model
		int numControls = osimModel.getNumControls();		
		
		// Initialize coordinate actuators
		Vector initialControls(numControls, initialCtrlValue);
		osimModel.setControls(si,initialControls);

		// Configure the visualizer.
//		osimModel.updMatterSubsystem().setShowDefaultGeometry(true);
//		Visualizer& viz = osimModel.updVisualizer().updSimbodyVisualizer();
//		viz.setBackgroundType(viz.SolidColor);
//		viz.setBackgroundColor(Cyan);

		//Print the control gains
		std::cout << "kp = " << Kp << std::endl;
		std::cout << "kv = " << Kv << std::endl;

		// Angular state variables
		double angPos = anklePos_init;
		double angVel;
		
//-------------------------- REFERENCE TRAJECTORIES ---------------------------
		Vec3 refTraj(0);
		cout << "Reference trajectory = " << refTraj << endl;

//------------------------------- INTEGRATOR ----------------------------------
		SimTK::RungeKuttaMersonIntegrator integ(osimModel.getMultibodySystem());
		integ.setAccuracy(1.0e-4);
		integ.setAllowInterpolation(false);
		integ.initialize(si);

//------------------------------- SIMULATION ----------------------------------
		do
		{
			cout << "\n-----------" << "STEP - " << stepNum << "-------------"<< endl;
			
//------------------------------ TASK SERVOING --------------------------------
			// Update current state
			State& aState = integ.updAdvancedState();
			osimModel.getMultibodySystem().realize(aState, Stage::Acceleration);
			angPos = osimModel.getCoordinateSet().get("ankle_rotZ").getValue(aState);
			angVel = osimModel.getCoordinateSet().get("ankle_rotZ").getSpeedValue(aState);
			cout << "Pos - iteration K = " << angPos << endl;
			cout << "Vel - iteration K = " << angVel << endl;

			// Compute desired trajectories from PD controller
			double desAngAcc = refTraj[2] + Kp * (refTraj[0] - angPos) + Kv * (refTraj[1] - angVel);
			Vec3 desTraj;
			desTraj[0] = refTraj[0];
			desTraj[1] = refTraj[0];
			desTraj[2] = desAngAcc;
			
//------------------------------- OPTIMIZATION ---------------------------------
			// Initialize the optimizer system
			// Only one optimization parameter
			OneLegOptimizationSystem sys(numControls, aState, osimModel, desTraj);
			Real f = Infinity;
			// Update torque control
			Vector controls = osimModel.updControls(aState);
			Vector lower_bounds(numControls, -1.0);
			Vector upper_bounds(numControls, 1.0);

			sys.setParameterLimits(lower_bounds, upper_bounds);
			
			// Create an optimizer. Pass in our OptimizerSystem
			// and the name of the optimization algorithm.
			Optimizer opt(sys, SimTK::InteriorPoint);

			// Specify settings for the optimizer
			opt.setConvergenceTolerance(0.01);
			opt.useNumericalGradient(true);
			opt.setMaxIterations(2000);
			opt.setLimitedMemoryHistory(500);
				
			// Optimize
			f = opt.optimize(controls);
			cout << "Cost fnc = " << f << endl;
			cout << "Controls to advance from iteration K to K+1 = " << controls << endl;

//------------------------------- ADVANCE TO NEXT STEP ----------------------------
			// Update the control values
			controls = 0;
			osimModel.updControls(aState) = controls;

			// Advance to the next iteration
			integ.stepBy(stepSize);
			cout << "controls after integration to K+1 = " << osimModel.updControls(aState) << endl;

			// Save results
			osimModel.printControlStorage("Actuator_controls.sto");
			
//------------------------------- UPDATE COUNTERS ----------------------------------
			++stepNum;
			initialStepTime += stepSize;
			finalStepTime = initialStepTime + stepSize;
		}
		while (pow(angPos-refTraj[0], 2) > 1e-3);
	}
	catch (OpenSim::Exception ex)
	{
		std::cout << ex.getMessage() << std::endl;
		return 1;
	}
	catch (SimTK::Exception::Base ex)
	{
		std::cout << ex.getMessage() << std::endl;
		return 1;
	}
	catch (std::exception ex)
	{
		std::cout << ex.what() << std::endl;
		return 1;
	}
	catch (...)
	{
		std::cout << "UNRECOGNIZED EXCEPTION" << std::endl;
		return 1;
	}
	std::cout << "OpenSim example completed successfully" << std::endl;
	std::cout << "Press return to continue" << std::endl;
	std::cin.get();
	return 0;
}
