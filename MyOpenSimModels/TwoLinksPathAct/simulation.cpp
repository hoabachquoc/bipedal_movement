#include <OpenSim/OpenSim.h>
#include <Simbody.h>

#include <cmath>


using namespace OpenSim;
using namespace SimTK;
using namespace std;


int main()
{
	try {
		Model osimModel;
		osimModel.setName("TwoLinksPathAct");

		double Pi = SimTK::Pi;

		// Get the ground body
		OpenSim::Body& ground = osimModel.getGroundBody();
		ground.addDisplayGeometry("checkered_floor.vtp");
		ground.setName("ground");
		osimModel.setGravity(Vec3(0, -9.80665, 0));

		// create linkage body
		double linkageMass = 1;
		double linkageLength = 0.5;
		double linkageDiameter = 0.06;

		Vec3 linkageDimensions(linkageDiameter, linkageLength, linkageDiameter);
		Vec3 linkageMassCenter(0,linkageLength/2,0);
		Inertia linkageInertia = Inertia::cylinderAlongY(linkageDiameter/2.0, linkageLength/2.0);

		OpenSim::Body* linkage1 = new OpenSim::Body("linkage1", linkageMass, linkageMassCenter, linkageMass*linkageInertia);

		// Graphical representation
		linkage1->addDisplayGeometry("cylinder.vtp");
		// This cylinder.vtp geometry is 1 meter tall, 1 meter diameter.  Scale and shift it to look pretty
		GeometrySet& geometry = linkage1->updDisplayer()->updGeometrySet();
		DisplayGeometry& thinCylinder = geometry[0];
		thinCylinder.setScaleFactors(linkageDimensions);
		thinCylinder.setTransform(Transform(Vec3(0.0,linkageLength/2.0,0.0)));
//		linkage1->addDisplayGeometry("sphere.vtp");
//		geometry[1].setScaleFactors(Vec3(0.1));

		// Creat a second linkage body
		OpenSim::Body* linkage2 = new OpenSim::Body(*linkage1);
		linkage2->setName("linkage2");

		// Create 1 degree-of-freedom pin joints between the bodies to creat a kinematic chain from ground through the linkages
		Vec3 orientationInGround(0); Vec3 locationInGround(0);
		Vec3 locationInParent(0.0, linkageLength, 0.0);
		Vec3 orientationInParent(0.0);
		Vec3 orientationInChild(0); Vec3 locationInChild(0);
		
		// Joints
		PinJoint *ankle = new PinJoint("ankle", ground, locationInGround, orientationInGround, 
												*linkage1, locationInChild, orientationInChild);

		PinJoint *knee = new PinJoint("knee", *linkage1, locationInParent, orientationInParent, 
											*linkage2, locationInChild, orientationInChild);
			

		double ankle_range[2] = {-Pi/2, Pi/2};
		double knee_range[2] = {-Pi, Pi};
		CoordinateSet& ankleCoordinateSet = ankle->upd_CoordinateSet();
		ankleCoordinateSet[0].setName("ankle_rotZ");
		ankleCoordinateSet[0].setRange(ankle_range);

		CoordinateSet& kneeCoordinateSet = knee->upd_CoordinateSet();
		kneeCoordinateSet[0].setName("knee_rotZ");
		kneeCoordinateSet[0].setRange(knee_range);


		// Add the bodies to the model (be careful of the order !!!)
		osimModel.addBody(linkage1);
		osimModel.addBody(linkage2);


		//=======================================================================
		// Section: Actuators
		//=======================================================================

		PathActuator *muscle11 = new PathActuator();
		muscle11->setName("muscle11");
		muscle11->addNewPathPoint("mus11pt1", ground, Vec3(-0.1,0,0));
		muscle11->addNewPathPoint("mus11pt2", *linkage1, Vec3(-0.03,0.25,0));
		muscle11->setOptimalForce(100);
		muscle11->setMinControl(0);
		muscle11->setMaxControl(1);

		PathActuator *muscle12 = new PathActuator();
		muscle12->setName("muscle12");
		muscle12->addNewPathPoint("mus12pt1", ground, Vec3(0.1,0,0));
		muscle12->addNewPathPoint("mus12pt2", *linkage1, Vec3(0.03,0.25,0));
		muscle12->setOptimalForce(100);
		muscle12->setMinControl(0);
		muscle12->setMaxControl(1);
		
		PathActuator *muscle21 = new PathActuator();
		muscle21->setName("muscle21");
		muscle21->addNewPathPoint("mus21pt1", *linkage1, Vec3(-0.03,0.3,0));
		muscle21->addNewPathPoint("mus21pt2", *linkage2, Vec3(-0.03,0.25,0));
		muscle21->setOptimalForce(100);
		muscle21->setMinControl(0);
		muscle21->setMaxControl(1);

		PathActuator *muscle22 = new PathActuator();
		muscle22->setName("muscle22");
		muscle22->addNewPathPoint("mus22pt1", *linkage1, Vec3(0.03,0.4,0));
		muscle22->addNewPathPoint("mus22pt2", *linkage2, Vec3(0.03,0.3,0));
		muscle22->setOptimalForce(100);
		muscle22->setMinControl(0);
		muscle22->setMaxControl(1);

		// Add the two muscles (as forces) to the model
		osimModel.addForce(muscle11);
		osimModel.addForce(muscle12);
		osimModel.addForce(muscle21);
		osimModel.addForce(muscle22);

		// Save the model to a file
		osimModel.print("TwoLinksPathAct.osim");

		
		osimModel.setUseVisualizer(true);
		
		// Initialize system
		SimTK::State& si = osimModel.initSystem();
		
		// Pin joint initial states
		CoordinateSet &coordinates = osimModel.updCoordinateSet();
		coordinates[0].setValue(si, -Pi/3, false);
		coordinates[1].setValue(si, 2*Pi/3, false);

		// Setup integrator and manager
		SimTK::RungeKuttaMersonIntegrator integrator(osimModel.getMultibodySystem());
		integrator.setAccuracy(1.0e-3);

		Manager manager(osimModel, integrator);
		
		double t0(0.0), tf(5);
		manager.setInitialTime(t0);
		manager.setFinalTime(tf);
		std::cout<<"\n\nIntegrating from " << t0 << " to " << tf << std::endl;
		manager.integrate(si);
		
	}
	catch (OpenSim::Exception ex)
	{
		std::cout << ex.getMessage() << std::endl;
		return 1;
	}
	catch (SimTK::Exception::Base ex)
	{
		std::cout << ex.getMessage() << std::endl;
		return 1;
	}
	catch (std::exception ex)
	{
		std::cout << ex.what() << std::endl;
		return 1;
	}
	catch (...)
	{
		std::cout << "UNRECOGNIZED EXCEPTION" << std::endl;
		return 1;
	}
	std::cout << "OpenSim example completed successfully" << std::endl;
	std::cout << "Press return to continue" << std::endl;
	std::cin.get();
	return 0;
}
