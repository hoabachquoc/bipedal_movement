#include <OpenSim/OpenSim.h>
#include <Simbody.h>
#include <cmath>
/* #include <qpOASES.hpp> */

#include <iostream>
#include <fstream>
#include <cstdlib>
#include <vector>

using namespace OpenSim;
using namespace SimTK;
using namespace std;

//------------------------------------ MODEL PROPERTIES ------------------------------------------
double linkageLength = 0.5;
double positionCoM = linkageLength/2;
double linkageMass = 1;
double linkageDiameter = 0.06;
Mat33 inertiaLinkage;
double g = -9.80665;

//-------------------------------------- END-EFFECTOR --------------------------------------------
Vec3 eefPoint(0,0.5,0);

//===============================================================================================
//					TEST FUNCTIONS
//===============================================================================================
class testFunctions{

    private:
        State& si;
        Model& osimModel;
        Vec3& desAcc;
    public:

        testFunctions(State& s, Model& aModel, Vec3& adesAcc): 
            si(s), osimModel(aModel), desAcc(adesAcc)
    {
    }
        double getAnkleAngle(){
            return osimModel.getCoordinateSet().get("ankle_rotZ").getValue(si) + SimTK::Pi/2;
        }

        double getAnkleAngVel(){
            return osimModel.getCoordinateSet().get("ankle_rotZ").getSpeedValue(si);
        }

        double getKneeAngle(){
            return osimModel.getCoordinateSet().get("knee_rotZ").getValue(si);
        }

        double getKneeAngVel(){
            return osimModel.getCoordinateSet().get("knee_rotZ").getSpeedValue(si);
        }

        Vector calcCoriolisForces() {
            SimTK::Matrix coriolisMat(2,2);
            Vector angle_vel(2);
            angle_vel(0) = getAnkleAngVel();
            angle_vel(1) = getKneeAngVel();
            osimModel.getBodySet().get("linkage2").getInertia(inertiaLinkage);

            double alpha = inertiaLinkage(2,2)*2 
                            + linkageMass*positionCoM*positionCoM
                            + linkageMass*(linkageLength*linkageLength+positionCoM*positionCoM);

            double beta = linkageMass*linkageLength*positionCoM;
            double delta = inertiaLinkage(2,2) + linkageMass*positionCoM*positionCoM;
            double betas2 = beta*sin(getKneeAngle());

            coriolisMat(0,0) = -betas2*getKneeAngVel();
            coriolisMat(1,0) =  betas2*getAnkleAngVel();
            coriolisMat(0,1) = -betas2*(getAnkleAngVel()+getKneeAngVel());
            coriolisMat(1,1) =  0;
            return coriolisMat * angle_vel;
        }

        Vector calcGravityForces() {
            Vector gravityForces(2);
            gravityForces(0) = linkageMass * g * linkageLength * ( 
                    0.5 * cos(getAnkleAngle()) + 0.5 * cos (getAnkleAngle() + getKneeAngle()) 
                    + cos(getAnkleAngle()) );
            gravityForces(1) = linkageMass * g * linkageLength/2 * cos(getAnkleAngle() + getKneeAngle());
            return gravityForces;
        }

        const SimbodyMatterSubsystem& matterSubsystem() {
            return osimModel.getMatterSubsystem(); 
        }

        const SpatialVec Jdotu () {
            return matterSubsystem().getTotalCoriolisAcceleration
                (si, MobilizedBodyIndex(2));
        }

        RowVector_<Vec3> getStationJacobianRowVector(){
            RowVector_<Vec3> JS;
            matterSubsystem().calcStationJacobian
                (si, MobilizedBodyIndex(2), eefPoint, JS);
            return JS;
        }

        SimTK::Matrix getStationJacobian(){
            SimTK::Matrix JS;
            matterSubsystem().calcStationJacobian
                (si, MobilizedBodyIndex(2), eefPoint, JS);
            return JS;
        }

        double getCoriolisForceVec() {
            SpatialVec CFSp = 
                matterSubsystem().getTotalCentrifugalForces(si, MobilizedBodyIndex(2));

            double CForce = CFSp(1)(1);
            /* CForce(0) = CFSp(1).get(0); */
            /* CForce(1) = CFSp(1).get(1); */
            return CForce;
        }

        SpatialVec getCoriolisForceSpaVec() {
            matterSubsystem().getTotalCentrifugalForces(si, MobilizedBodyIndex(2));
        }


        Vec3 getJSDotQDotStation() {
            Vec3 JSDotu = matterSubsystem().calcBiasForStationJacobian
                (si, MobilizedBodyIndex(2), eefPoint);
            return JSDotu;
        }

        Vector getJSDotQDotStationNoBias() {
            SimTK::Matrix JS;
            matterSubsystem().calcStationJacobian
                (si, MobilizedBodyIndex(2), eefPoint, JS);

            Vector u(2);
            u(0) = osimModel.getCoordinateSet().get("ankle_rotZ").getSpeedValue(si);
            u(1) = osimModel.getCoordinateSet().get("knee_rotZ").getSpeedValue(si);
            return JS * u;
        }

        Vector_<SpatialVec> getJSDotQDotSystem() {
            Vector_<SpatialVec> JSDotuSys;
            matterSubsystem().calcBiasForSystemJacobian(si, JSDotuSys);
            return JSDotuSys;
        }

        SpatialVec GyroForceBody () {
            return matterSubsystem().getGyroscopicForce (si, MobilizedBodyIndex(2));
        }


        Vector calcGravity() {
            Vector g;
            osimModel.getMatterSubsystem().multiplyBySystemJacobianTranspose(
                    si, osimModel.getGravityForce().getBodyForces(si), g);
            return g;
        }

        Vector calcCoriolis() {
            Vector c;
            osimModel.getMatterSubsystem().calcResidualForceIgnoringConstraints(
                    si, SimTK::Vector(0), SimTK::Vector_<SimTK::SpatialVec>(0),
                    SimTK::Vector(0), c);
            return c;
        }

};

int main()
{
    try {
        //=======================================================================
        // Section: Create Model
        //=======================================================================

        Model osimModel;
        osimModel.setName("osimModel");

        double Pi = SimTK::Pi;

        // Get the ground body
        OpenSim::Body& ground = osimModel.getGroundBody();
        ground.addDisplayGeometry("checkered_floor.vtp");
        ground.setName("ground");
        osimModel.setGravity(Vec3(0, g, 0));

        // Linkage 1
        Vec3 linkageMassCenter(0,linkageLength/2,0);
        Inertia linkageInertia = Inertia::cylinderAlongY(linkageDiameter/2.0, linkageLength/2.0) * linkageMass;
        OpenSim::Body* linkage1 = new OpenSim::Body("linkage1", linkageMass, linkageMassCenter, linkageInertia);

        // Graphical representation
        linkage1->addDisplayGeometry("cylinder.vtp");

        // Scale and shift it to look pretty
        GeometrySet& geometry = linkage1->updDisplayer()->updGeometrySet();
        DisplayGeometry& thinCylinder = geometry[0];
        Vec3 linkageDimensions(linkageDiameter, linkageLength, linkageDiameter);
        thinCylinder.setScaleFactors(linkageDimensions);
        thinCylinder.setTransform(Transform(Vec3(0.0,linkageLength/2.0,0.0)));	//don't mess with it!

        // Sphere at joint
        linkage1->addDisplayGeometry("sphere.vtp");
        geometry[1].setScaleFactors(Vec3(0.1));

        // Linkage 2 
        OpenSim::Body* linkage2 = new OpenSim::Body(*linkage1);
        linkage2->setName("linkage2");

        // Create 1 degree-of-freedom pin joints between the bodies to creat a kinematic chain from ground through the linkages
        Vec3 orientationInGround(0); Vec3 locationInGround(0);
        Vec3 locationInParent(0.0, linkageLength, 0.0); Vec3 orientationInParent(0.0);
        Vec3 orientationInChild(0); Vec3 locationInChild(0);

        // Joints
        PinJoint *ankle = new PinJoint("ankle", ground, locationInGround, orientationInGround, 
                *linkage1, locationInChild, orientationInChild);

        PinJoint *knee = new PinJoint("knee", *linkage1, locationInParent, orientationInParent, 
                *linkage2, locationInChild, orientationInChild);


        double ankle_range[2] = {-Pi/2, Pi/2};
        double knee_range[2] = {-Pi, Pi};
        CoordinateSet& ankleCoordinateSet = ankle->upd_CoordinateSet();
        ankleCoordinateSet[0].setName("ankle_rotZ");
        ankleCoordinateSet[0].setRange(ankle_range);

        CoordinateSet& kneeCoordinateSet = knee->upd_CoordinateSet();
        kneeCoordinateSet[0].setName("knee_rotZ");
        kneeCoordinateSet[0].setRange(knee_range);


        // Add the bodies to the model (be careful of the order !!!)
        osimModel.addBody(linkage1);
        osimModel.addBody(linkage2);


        //=======================================================================
        // Section: Actuators
        //=======================================================================

        CoordinateActuator *ankleAct = new CoordinateActuator("ankle_rotZ");
        ankleAct->setName("ankleAct");
        ankleAct->setOptimalForce(100.0);
        ankleAct->setMinControl(-1.0);
        ankleAct->setMaxControl(1.0);

        CoordinateActuator *kneeAct = new CoordinateActuator("knee_rotZ");
        kneeAct->setName("kneeAct");
        kneeAct->setOptimalForce(100.0);
        kneeAct->setMinControl(-1.0);
        kneeAct->setMaxControl(1.0);


        // Add the two muscles (as forces) to the model
        osimModel.addForce(ankleAct); osimModel.addForce(kneeAct); 
        // Save the model to a file
        osimModel.print("TwoLinksCoordAct.osim");

        //------------------------------------ TEST INTEGRATOR -------------------------------------------
        osimModel.setUseVisualizer(true);

        // Initialize system
        SimTK::State& si = osimModel.initSystem();

        // Pin joint initial states
        CoordinateSet &coordinates = osimModel.updCoordinateSet();
        coordinates[0].setValue(si, -Pi/3, false);
        coordinates[1].setValue(si, 2*Pi/3, false);

        // Setup integrator and manager
        SimTK::RungeKuttaMersonIntegrator integrator(osimModel.getMultibodySystem());
        integrator.setAccuracy(1.0e-3);

        Manager manager(osimModel, integrator);

        double t0(0.0), tf(0.1);

        int count = 0;
        while (tf < 1.0) {
            manager.setInitialTime(t0);
            manager.setFinalTime(tf);
            std::cout<<"\n\nIntegrating from " << t0 << " to " << tf << std::endl;
            manager.integrate(si);
            t0 += 0.1;
            tf += 0.1;

            osimModel.getMultibodySystem().realize(si, Stage::Acceleration);

            //--------------------------------- TEST FUNCTIONS ----------------------------------

            Vec3 desAcc(0);
            testFunctions testFunc(si, osimModel, desAcc); 
            cout << "-- calcCoriolisForces = " << testFunc.calcCoriolisForces() << endl;
            cout << "-- calcGravityForces = " << testFunc.calcGravityForces() << endl;
            cout << "-- Gravity(Stanev) = " << testFunc.calcGravity() << endl;
            cout << "-- Coriolis(Stanev) = " << testFunc.calcCoriolis() << endl;

            const SimbodyMatterSubsystem& matter = osimModel.getMatterSubsystem();
            const int nb = matter.getNumBodies();

            // coriolis term calculated by getTotalCentrifugalForces
            Vector_<SpatialVec> F_inertial(nb);
            Vector f_inertial;
            for (MobodIndex i(0); i<nb; ++i)
                F_inertial[i] = matter.getTotalCentrifugalForces(si, i);
            matter.multiplyBySystemJacobianTranspose(si, F_inertial, f_inertial);

            Vector Mdu;
            matter.multiplyByM(si, si.getUDot(), Mdu);
            cout << "Mdt - f_inertial - gr(stanev) = " << Mdu - f_inertial - testFunc.calcGravity() << endl;
            manager.getStateStorage().print("States.sto");
        }
    }
    catch (OpenSim::Exception ex)
    {
        std::cout << ex.getMessage() << std::endl;
        return 1;
    }
    catch (SimTK::Exception::Base ex)
    {
        std::cout << ex.getMessage() << std::endl;
        return 1;
    }
    catch (std::exception ex)
    {
        std::cout << ex.what() << std::endl;
        return 1;
    }
    catch (...)
    {
        std::cout << "UNRECOGNIZED EXCEPTION" << std::endl;
        return 1;
    }
    std::cout << "OpenSim example completed successfully" << std::endl;
    std::cout << "Press return to continue" << std::endl;
    std::cin.get();
    return 0;
}
