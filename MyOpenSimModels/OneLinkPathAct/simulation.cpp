#include <OpenSim/OpenSim.h>
#include <Simbody.h>

using namespace OpenSim;
using namespace SimTK;
using namespace std;


int main()
{
	try {

		Model osimModel;
		osimModel.setName("osimModel");

		double Pi = SimTK::Pi;

		// Get the ground body
		OpenSim::Body ground = osimModel.getGroundBody();
		ground.addDisplayGeometry("checkered_floor.vtp");
		ground.setName("ground");
		osimModel.setGravity(Vec3(0, -9.80665, 0));

		// create linkage body
		double linkageMass = 0.05;
		double linkageLength = 0.5;
		double linkageDiameter = 0.06;

		Vec3 linkageDimensions(linkageDiameter, linkageLength, linkageDiameter);
		Vec3 linkageMassCenter(0,linkageLength/2,0);
		Inertia linkageInertia = Inertia::cylinderAlongY(linkageDiameter/2.0, linkageLength/2.0) * linkageMass;
		OpenSim::Body* linkage1 = new OpenSim::Body("linkage1",
													linkageMass,
													linkageMassCenter,
													linkageInertia);

		// Graphical representation
		linkage1->addDisplayGeometry("cylinder.vtp");
		// This cylinder.vtp geometry is 1 meter tall, 1 meter diameter.  Scale and shift 
		GeometrySet& geometry = linkage1->updDisplayer()->updGeometrySet();
		DisplayGeometry& thinCylinder = geometry[0];
		thinCylinder.setScaleFactors(linkageDimensions);
		thinCylinder.setTransform(Transform(Vec3(0.0,linkageLength/2.0,0.0)));
		linkage1->addDisplayGeometry("sphere.vtp");
		geometry[1].setScaleFactors(Vec3(0.1));


		// Create 1 degree-of-freedom pin joints between the bodies to creat a kinematic chain from ground through the linkage
		Vec3 orientationInGround(0); 
		Vec3 locationInGround(0);
		Vec3 locationInLinkage1(0.0);
		Vec3 orientationInLinkage1(0); 
		
		PinJoint *ankle = new PinJoint("ankle", 
									ground,locationInGround,orientationInGround, 
									*linkage1,locationInLinkage1,orientationInLinkage1);

		double range[2] = {-SimTK::Pi, SimTK::Pi};
		CoordinateSet& ankleCoordinateSet = ankle->upd_CoordinateSet();
		ankleCoordinateSet[0].setName("ankle_rotZ");
		ankleCoordinateSet[0].setRange(range);


		// Add the bodies to the model (be careful of the order !!!)
		osimModel.addBody(linkage1);


		//=======================================================================
		// Section: Actuators
		//=======================================================================

//		PathActuator *muscle11 = new PathActuator();
//		muscle11->setName("muscle11");
//		muscle11->addNewPathPoint("mus11pt1", ground, Vec3(-0.05,0,0));
//		muscle11->addNewPathPoint("mus11pt2", *linkage1, Vec3(0,0.25,0));
//		muscle11->setOptimalForce(5);
//		muscle11->setMinControl(0);
//		muscle11->setMaxControl(1);

		PathActuator *muscle12 = new PathActuator();
		muscle12->setName("muscle12");
		muscle12->addNewPathPoint("mus12pt1", ground, Vec3(0.1,0,0));
		muscle12->addNewPathPoint("mus12pt2", *linkage1, Vec3(0,0.25,0));
		muscle12->setOptimalForce(5);
		muscle12->setMinControl(0);
		muscle12->setMaxControl(1);


		// Add the two muscles (as forces) to the model
//		osimModel.addForce(muscle11);
		osimModel.addForce(muscle12);


		// the prescribed controller sets the controls as functions of time
		PrescribedController *legController = new PrescribedController();
		// give the legController control over all (two) model actuators
		legController->setActuators(osimModel.updActuators());

		// specify some control nodes 
		double t[] = {0.0, 0.4, 0.7, 1.0, 1.5, 2.0};
        double x[] = {0.3, 0.5, 0.8, 1.0, 0.4, 0.6};

		// specify the control function for each actuator
//		legController->prescribeControlForActuator("muscle11", new Constant(0.0));
//		legController->prescribeControlForActuator("muscle12", new PiecewiseLinearFunction(6, t, x));
		legController->prescribeControlForActuator("muscle12", new Constant(0.36));

		// add the controller to the model
		osimModel.addController(legController);



        // Save the model to a file
        osimModel.print("OneLinkPathAct.osim");
		

		// enable the model visualizer see the model in action
		osimModel.setUseVisualizer(true);

		// Initialize system
		SimTK::State& si = osimModel.initSystem();

		// Set pin joint initial states
		double anklePos_init = Pi/4;
		osimModel.getCoordinateSet().get("ankle_rotZ").setValue(si, anklePos_init);

		// Configure the visualizer.
		osimModel.updMatterSubsystem().setShowDefaultGeometry(true);
		Visualizer& viz = osimModel.updVisualizer().updSimbodyVisualizer();
		viz.setBackgroundType(viz.SolidColor);
		viz.setBackgroundColor(Cyan);
		
		// Integrator
		SimTK::RungeKuttaMersonIntegrator integ(osimModel.getMultibodySystem());
		integ.setAccuracy(1.0e-4);
		
		// Manager
		Manager manager(osimModel,integ);
		manager.setInitialTime(0.0);
		manager.setFinalTime(2.0);
		manager.integrate(si);

	}
	catch (OpenSim::Exception ex)
	{
		std::cout << ex.getMessage() << std::endl;
		return 1;
	}
	catch (SimTK::Exception::Base ex)
	{
		std::cout << ex.getMessage() << std::endl;
		return 1;
	}
	catch (std::exception ex)
	{
		std::cout << ex.what() << std::endl;
		return 1;
	}
	catch (...)
	{
		std::cout << "UNRECOGNIZED EXCEPTION" << std::endl;
		return 1;
	}
	std::cout << "OpenSim example completed successfully" << std::endl;
	std::cout << "Press return to continue" << std::endl;
	std::cin.get();
	return 0;
}
