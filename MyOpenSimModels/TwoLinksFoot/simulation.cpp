#include <OpenSim/OpenSim.h>
#include <Simbody.h>
#include <iostream>

using namespace OpenSim;
using namespace SimTK;
using namespace std;

bool modelChoice = 1;
bool printContactFr = 0;
bool printContactFrInLoop = 0;
bool printResidualForce = 1;
bool printConstraintMult = 0;
bool printCentrifugalForce = 0;
bool printTreeEqMobForces = 0;
bool compareRigidBodyForcesToEoMTerms = 0;
bool compareToRigidBodyForces = 0;

//------------------------------------------- CONTACT ------------------------------------------
int nc = 4;
Vec3 lHeelPt(-0.07,-0.008,-0.02);
Vec3 rHeelPt(-0.07,-0.008,0.02);
Vec3 lPadPt(0.06,-0.008,-0.02);
Vec3 rPadPt(0.06,-0.008,0.02);
Vec3 eefPoint(0,0.43,0);

// Array of bodies on which are located contact points
// Until now the only body is the foot
Array_<MobilizedBodyIndex> bodyCt() {
  Array_<MobilizedBodyIndex> onBodyB(nc);
  onBodyB[0] = MobilizedBodyIndex(3);
  onBodyB[1] = MobilizedBodyIndex(3);
  onBodyB[2] = MobilizedBodyIndex(3);
  onBodyB[3] = MobilizedBodyIndex(3);
  return onBodyB; 
}

// Array of frame origin points of task frames (contact points)
Array_<Vec3> originCtPtFrm() {
  Array_<Vec3> originAoInB(nc);
  /* originAoInB[0] = lHeelPt; */
  /* originAoInB[1] = rHeelPt; */
  /* originAoInB[2] = lPadPt; */
  /* originAoInB[3] = rPadPt; */

  originAoInB[0] = Vec3(0);
  originAoInB[1] = Vec3(0);
  originAoInB[2] = Vec3(0);
  originAoInB[3] = Vec3(0);

  return originAoInB;
}

Vector_<SpatialVec> contactForcesGroundFrame(State& si, Model& osimModel) {
  // Retrieve contact forces

  Array<double> lHeelForceRec = osimModel.getForceSet().get("LeftHeelContactForce").getRecordValues(si);
  Array<double> rHeelForceRec = osimModel.getForceSet().get("RightHeelContactForce").getRecordValues(si);
  Array<double> lPadForceRec = osimModel.getForceSet().get("LeftPadContactForce").getRecordValues(si);
  Array<double> rPadForceRec = osimModel.getForceSet().get("RightPadContactForce").getRecordValues(si);

  Vec3 lHeelForce(lHeelForceRec[6],lHeelForceRec[7],lHeelForceRec[8]);
  Vec3 lHeelMoment(lHeelForceRec[9],lHeelForceRec[10],lHeelForceRec[11]);

  Vec3 rHeelForce(rHeelForceRec[6],rHeelForceRec[7],rHeelForceRec[8]);
  Vec3 rHeelMoment(rHeelForceRec[9],rHeelForceRec[10],rHeelForceRec[11]);

  Vec3 lPadForce(lPadForceRec[6],lPadForceRec[7],lPadForceRec[8]);
  Vec3 lPadMoment(lPadForceRec[9],lPadForceRec[10],lPadForceRec[11]);

  Vec3 rPadForce(rPadForceRec[6],rPadForceRec[7],rPadForceRec[8]);
  Vec3 rPadMoment(rPadForceRec[9],rPadForceRec[10],rPadForceRec[11]);

  if (printContactFr) {
    cout << "-- lHeelForceRec = " << lHeelForceRec << endl;
    cout << "-- lHeelForce = " << lHeelForce << endl;
    cout << "-- lHeelMoment = " << lHeelMoment << endl;
    cout << "-- rHeelForce = " << rHeelForce << endl;
    cout << "-- rHeelMoment = " << rHeelMoment << endl;
    cout << "-- lPadForce = " << lPadForce << endl;
    cout << "-- lPadMoment = " << lPadMoment << endl;
    cout << "-- rPadForce = " << rPadForce << endl;
    cout << "-- rPadMoment = " << rPadMoment << endl;
  }

  Vector_<SpatialVec> F_GAo(nc);
  F_GAo[0] = SpatialVec(lHeelMoment,lHeelForce);
  F_GAo[1] = SpatialVec(rHeelMoment,rHeelForce);
  F_GAo[2] = SpatialVec(lPadMoment,lPadForce);
  F_GAo[3] = SpatialVec(rPadMoment,rPadForce);

  return F_GAo;
}


SpatialVec totalContactForces(State& si, Model& osimModel) {
  Vector_<SpatialVec> contactForce = contactForcesGroundFrame(si,osimModel);
  SpatialVec totalCtForce = contactForce(0) + contactForce(1) + contactForce(2) + contactForce(3);
  return totalCtForce;
}

SimTK::Vector contactForces(State& si, Model& osimModel) {
  // Generalized forces resulting from foot ground contact forces
  const SimbodyMatterSubsystem& matter = osimModel.getMatterSubsystem();
  Array_<MobilizedBodyIndex> onBodyB = bodyCt();
  Array_<Vec3> originAoInB = originCtPtFrm();
  Vector_<SpatialVec> F_GAo = contactForcesGroundFrame(si, osimModel);

  SimTK::Vector contact;
  matter.multiplyByFrameJacobianTranspose(si, onBodyB, originAoInB, F_GAo, contact);
  return contact;
}

// Convert spatialVec to Vector
SimTK::Vector convertSpatialVecToVector (const SpatialVec& sVec) {
    SimTK::Vector oVec (6);
    oVec(0) = sVec[0][0];
    oVec(1) = sVec[0][1];
    oVec(2) = sVec[0][2];
    oVec(3) = sVec[1][0];
    oVec(4) = sVec[1][1];
    oVec(5) = sVec[1][2];
    return oVec;
}


SimTK::Matrix contactJacob_base (const State& m_s, const Model& m_osimModel, const Vec3& point) {
    SimTK::Matrix JcP_b (6, 6);
    JcP_b = 0;
    JcP_b (0, 0, 3, 3) = 1;
    JcP_b (3, 3, 3, 3) = 1;

    // Contact point origin
    Vec3 cPos;
    m_osimModel.getSimbodyEngine().getPosition
        (m_s, m_osimModel.getBodySet().get("foot"), point, cPos);

    // Floating base origin
    Vec3 oPos;
    m_osimModel.getSimbodyEngine().getPosition
        /* (m_s, m_osimModel.getBodySet().get("foot"), Vec3(0), oPos); */
        (m_s, m_osimModel.getBodySet().get("linkage2"), Vec3(0), oPos);

    Vec3 updPosCartesian = cPos - oPos;

    JcP_b (4, 0) = -updPosCartesian(2);
    JcP_b (5, 0) = +updPosCartesian(1);
    JcP_b (3, 1) = +updPosCartesian(2);
    JcP_b (5, 1) = -updPosCartesian(0);
    JcP_b (3, 2) = -updPosCartesian(1);
    JcP_b (4, 2) = +updPosCartesian(0);

    return JcP_b;
}


//------------------------------------------- CONTACT ------------------------------------------

int main()
{
  try {
    Model osimModel("OneLeg_tg_2_changeBase.osim");
    /* Model osimModel("TwoLinksFoot.osim"); */
    osimModel.setUseVisualizer(false);

    // Initialize system
    SimTK::State& si = osimModel.initSystem();

    // Pin joint initial states
    /* CoordinateSet &coordinates = osimModel.updCoordinateSet(); */
    /* coordinates[6].setValue(si, 0.2, true); */
    /* coordinates[7].setValue(si, -0.2, true); */

    cout << "-- coordinate set: " << osimModel.updCoordinateSet() << endl;
    cout << "-- body set: " << osimModel.updBodySet() << endl;
    /* cout << "-- originCtPtFrm = " << originCtPtFrm() << endl; */

    const ForceSet& forceSet = osimModel.getForceSet();
    int forceSetSize = forceSet.getSize();
    cout << "forceSetSize = " << forceSetSize << endl;
    cout << "-- force set " << forceSet << endl;

    /* OpenSim::Array< std::string > rNames; */
    /* forceSet.getNames(rNames); */
    /* std::cout << "rNames = " << rNames << endl; */

    /* const GeneralForceSubsystem& generalforce = osimModel.getForceSubsystem(); */
    /* int nbforces = generalforce.getNumForces(); */
    /* cout << "-- nbforces = " << nbforces << endl; */
    /* cout << "-- subsystemName = " << generalforce.getName() << endl; */

    /* OpenSim::HuntCrossleyForce &leftHeel = (OpenSim::HuntCrossleyForce &)osimModel.getForceSet().get("LeftHeelContactForce"); */


    /* std::cout << "nothing wrong yet" << endl; */
    /* ContactSetIndex indexFr = leftHeel.getContactSetIndex(); */
    /* std::cout << "-- indexFr = " << indexFr << endl; */

    // Subsystem
    const SimbodyMatterSubsystem& matter = osimModel.getMatterSubsystem();

    // Multibody system
    const MultibodySystem& mltbd = osimModel.getMultibodySystem();

    /* SpatialVec testSpaVec (Vec3(4.0, 9.0, 3.6), Vec3(5.2, 5.9,1.6)); */
    /* SimTK::Vector testConvertToVec = convertSpatialVecToVector(testSpaVec); */
    /* cout << "-- testConvertToVec = " << testConvertToVec << endl; */
    /* SimTK::Vector testInitVector = (2.3, 4.1, 9.8, 6.1); */
    /* cout << "-- test initialization vector = " << testInitVector << endl; */

    // Mobilizer Reaction Forces
    Vector_<SpatialVec> forcesAtMinG;
    Vector_<SpatialVec> bodyForces;
    /* Vector mobilityForces; */

    /* // Constraints */
    cout << "-- nb of constraints = " << matter.getNumConstraints() << endl;

    // Unilateral contact
    cout << "-- nb of unilateral contacts = " << matter.getNumUnilateralContacts() << endl;
    cout << "-- constraint set (before loop) = " << osimModel.getConstraintSet() << endl;

    // State variables
    cout << "-- State variables = " << osimModel.getStateVariableNames () << endl;

    // Joints
    cout << "-- nb of joints = " << osimModel.getNumJoints() << endl;

    // Contact geometries
    cout << "-- nb of contact geometries = " << osimModel.getNumContactGeometries () << endl;


    // Euler or quaternion
    /* cout << "-- getUseEulerAngles = " << matter.getUseEulerAngles(si) << endl; */

    // Setup integrator and manager
    SimTK::RungeKuttaMersonIntegrator integrator(mltbd);
    integrator.setAccuracy(1.0e-3);
    Manager manager(osimModel, integrator);

    // Create the force reporter
    ForceReporter* reporter = new ForceReporter(&osimModel);
    osimModel.addAnalysis(reporter);

    double t0(0.0), tf(0.1);

    /* cout << " --> check range floating base: " << osimModel.getCoordinateSet().get(4).getRangeMax() << endl; */
    /* cout << " --> check range floating base: " << osimModel.getCoordinateSet().get(4).getRangeMin() << endl; */


    int count = 0;
    while (tf < 0.4) {
      count += 1;
      manager.setInitialTime(t0);
      manager.setFinalTime(tf);
      manager.integrate(si);

      cout << "\n------------------ time = " << t0 << " --------------------------" << endl;

      mltbd.realize(si, Stage::Acceleration);

      /* cout << "-- height = " << osimModel.getCoordinateSet().get(4).getValue(si) << endl; */ 
      /* cout << "-- coordinate set = " << osimModel.getCoordinateSet() << endl; */ 
      /* cout << "-- state values = " << osimModel.getStateValues(si) << endl; */

      matter.calcMobilizerReactionForces(si, forcesAtMinG);
      cout << "-- forcesAtMinG = " << forcesAtMinG << endl;

      cout << "-- record 0) = " << forceSet.get(0).getRecordValues(si) << endl;
      if (modelChoice) {
         cout << "-- record 1) = " << forceSet.get(1).getRecordValues(si) << endl;
         cout << "-- record 2) = " << forceSet.get(2).getRecordValues(si) << endl;
         /* cout << "-- record 3) = " << forceSet.get(3).getRecordValues(si) << endl; */
      }

      /* matter.calcTreeEquivalentMobilityForces(si, bodyForces, mobilityForces); */
      /* cout << "tree calculated" << endl; */
      /* cout << "-- bodyForces = " << bodyForces << endl; */


      /* leftHeel.computeForce(si, bodyForces, mobilityForces); */
      /* cout << "-- mobilityForces = " << mobilityForces << endl; */

      /* if (printContactFrInLoop) { */
      /*   const ForceSet& FSet =  osimModel.getForceSet(); */
      /*   for (int i = 0; i < FSet.getSize(); i++) { */
      /*     OpenSim::Force& fi = FSet.get(i); */
      /*     cout << "Force name: " << fi.getName() << endl; */
      /*     Array<string> labels = fi.getRecordLabels(); */
      /*     Array<double> values = fi.getRecordValues(si); */
      /*     for (int j = 0; j < labels.size(); j++) */
      /*       cout << "-> " << labels.get(j) << "= " << values.get(j) << endl; */
      /*   } */
      /*   cout << "-- contact force func = " << contactForces(si,osimModel) << endl; */
      /* } */

      // Save the forces
      reporter->getForceStorage().print("forceReporter.mot"); 

      // Store State variables
      manager.getStateStorage().print("States.sto");

      cout << "-- si.getY() = " << si.getY() << endl;
      cout << "-- si.getQ() = " << si.getQ() << endl;
      cout << "-- si.getU() = " << si.getU() << endl;
      cout << "-- si.getZ() = " << si.getZ() << endl;

      /* cout << " -- euler angle ? = " << matter.getUseEulerAngles(si) << endl; */


      //---------------------------------------- EoM Terms ---------------------------------------

      // M*dU
      /* SimTK::Vector Mu; */
      /* matter.multiplyByM(si, si.getUDot(), Mu); */

      // ~G.mult
      /* SimTK::Vector f_cons; */
      /* matter.multiplyByGTranspose(si,matter.getConstraintMultipliers(si),f_cons); */

      // Gravity forces - system Jacobian Transpose
      /* SimTK::Vector gr; */
      /* matter.multiplyBySystemJacobianTranspose(si,osimModel.getGravityForce().getBodyForces(si),gr); */
      /* cout << "-- gravity force -sys Jab    = " << gr << endl; */
      /* cout << "-- gravity body forces       = " << osimModel.getGravityForce().getBodyForces(si) << endl; */

      // Gravity forces - system Jacobian Transpose
      /* Vector_<SpatialVec> grBodyForces = osimModel.getGravityForce().getBodyForces(si); */
      /* SpatialVec totalGravityForce = grBodyForces(0) + grBodyForces(1) + grBodyForces(2) + grBodyForces(3); */ 
      /* cout << "-- leg gravity body force = " << grBodyForces(1) << endl; */
      /* SimTK::Vector grGen1; */
      /* matter.multiplyByFrameJacobianTranspose(si,MobilizedBodyIndex(1),Vec3(0),grBodyForces(1),grGen1); */
      /* SimTK::Vector grGen2; */
      /* matter.multiplyByFrameJacobianTranspose(si,MobilizedBodyIndex(2),Vec3(0),grBodyForces(2),grGen2); */
      /* SimTK::Vector grGen3; */
      /* matter.multiplyByFrameJacobianTranspose(si,MobilizedBodyIndex(3),Vec3(0),grBodyForces(3),grGen3); */
      /* cout << "-- gravity force -frame Jab  = " << grGen1 + grGen2 + grGen3 << endl; */


      // Coriolis - totalCentrifugalForces - system Jacobian Transpose 
      /* const int nb = matter.getNumBodies(); */
      /* Vector_<SpatialVec> F_inertial(nb); */
      /* Vector f_inertial; */
      /* for (MobodIndex i(0); i<nb; ++i) */
      /*   F_inertial[i] = matter.getTotalCentrifugalForces(si, i); */
      /* matter.multiplyBySystemJacobianTranspose(si, F_inertial, f_inertial); */
      /* cout << "-- gr - f_inertial           = " << gr - f_inertial << endl; */ 
      // Coriolis - totalCentrifugalForces - frame Jacobian Transpose
      /* SpatialVec totalCentrifugal1 = matter.getTotalCentrifugalForces(si, MobilizedBodyIndex(1)); */
      /* SpatialVec totalCentrifugal2 = matter.getTotalCentrifugalForces(si, MobilizedBodyIndex(2)); */
      /* SpatialVec totalCentrifugal3 = matter.getTotalCentrifugalForces(si, MobilizedBodyIndex(3)); */
      /* SpatialVec totalCentrifugal0 = matter.getTotalCentrifugalForces(si, MobilizedBodyIndex(0)); */
      /* SpatialVec totalCentrifugalForce = totalCentrifugal0 + totalCentrifugal1 + totalCentrifugal2 + totalCentrifugal3; */
      /* cout << "-- total centrifugal leg = " << totalCentrifugal1 << endl; */
      /* cout << "total centrifugal thigh      = " << totalCentrifugal2 << endl; */
      /* cout << "total centrifugal 1+2        = " << totalCentrifugal2 + totalCentrifugal1 << endl; */
      /* cout << "total centrifugal 1+2+0      = " << totalCentrifugal2 + totalCentrifugal1 + totalCentrifugal0 << endl; */
      /* SimTK::Vector centrifugal1gen; */
      /* matter.multiplyByFrameJacobianTranspose(si,MobilizedBodyIndex(1),Vec3(0),totalCentrifugal1,centrifugal1gen); */
      /* SimTK::Vector centrifugal2gen; */
      /* matter.multiplyByFrameJacobianTranspose(si,MobilizedBodyIndex(2),Vec3(0),totalCentrifugal2,centrifugal2gen); */
      /* SimTK::Vector centrifugal3gen; */
      /* matter.multiplyByFrameJacobianTranspose(si,MobilizedBodyIndex(3),Vec3(0),totalCentrifugal3,centrifugal3gen); */

      /* if(printCentrifugalForce) { */
        /* cout << "-- total centrifugal         = " << centrifugal1gen + centrifugal2gen + centrifugal3gen << endl; */
        /* cout << "-- gr - centrifugal          = " << gr - centrifugal1gen - centrifugal2gen - centrifugal3gen << endl; */
      /* } */

      // Coriolis - residualForcesNoConstraint
      /* SimTK::Vector cor; */
      /* matter.calcResidualForceIgnoringConstraints(si,SimTK::Vector(0),SimTK::Vector_<SimTK::SpatialVec>(0),SimTK::Vector(0),cor); */
      /* cout << "-- coriolis force            = " << cor << endl; */
      /* cout << "-- gr - cr                   = " << gr - cor << endl; */ 

      //------------------------------ Tree Equivalent Mobility Forces ---------------------------

      /* Vector_<SpatialVec> rigidBodyForces = mltbd.getRigidBodyForces(si, SimTK::Stage::Dynamics); */
      /* cout << "-- rigidBodyForces           = " << rigidBodyForces << endl; */
      /* SpatialVec totalRigidBodyForces = -rigidBodyForces(0) + rigidBodyForces(1) + rigidBodyForces(2) + rigidBodyForces(3); */
      /* SpatialVec totalRigidBodyForces = rigidBodyForces(1) + rigidBodyForces(2) + rigidBodyForces(3); */
      /* cout << "-- totalRigidBodyForces = " << totalRigidBodyForces << endl; */
      /* SimTK::Vector F1 = mltbd.getMobilityForces(si,SimTK::Stage::Dynamics); */
      /* SimTK::Vector F2; */
      /* matter.calcTreeEquivalentMobilityForces(si, mltbd.getRigidBodyForces(si, SimTK::Stage::Dynamics), F2); */

      /* if (compareRigidBodyForcesToEoMTerms) { */
      /*     cout << "-- EoM groundf frame = " << totalRigidBodyForces - totalGravityForce + totalCentrifugalForce - totalContactForces(si,osimModel) << endl; */
      /* } */

      /* if(printTreeEqMobForces) { */
        /* cout << "-- f_mob = " << F1 << endl; */
        /* cout << "-- J*F_body - f_bias         = " << F2 << endl; */
      /* } */

      /* if (printConstraintMult) { */
      /*   SimTK::Matrix G_; */
      /*   matter.calcG(si,G_); */
      /*   cout << "-- acceleration constraint matrix G = " << G_ << endl; */
      /*   cout << "-- constraint multipliers = " << matter.getConstraintMultipliers(si) << endl; */
      /* } */

      //-------------------------------- Compare with rigidBodyForces ----------------------------

      /* if (compareToRigidBodyForces) { */
      /*   cout << "-- foot forces ground frame = " << rigidBodyForces(1) << endl; */
      /*   cout << "-- total foot force         = " << totalContactForces(si, osimModel) + grBodyForces(1) - totalCentrifugal1 << endl; */

      /*   cout << "-- gr - cor (2) = " << grBodyForces(2) - totalCentrifugal2 << endl; */
      /*   cout << "-- leg forces   = " << rigidBodyForces(2) << endl; */

      /*   cout << "-- gr - cor (3) = " << grBodyForces(3) - totalCentrifugal3 << endl; */
      /*   cout << "-- thigh forces   = " << rigidBodyForces(3) << endl; */
      /* } */

      //------------------------------------- Residual forces ------------------------------------

      /* SimTK::Vector residualForces; */
      /* matter.calcResidualForce(si,SimTK::Vector(0),SimTK::Vector_<SpatialVec>(0),si.getUDot(),matter.getConstraintMultipliers(si),residualForces); */

      /* SimTK::Vector residualForcesNoConstraint; */
      /* matter.calcResidualForceIgnoringConstraints(si,SimTK::Vector(0),SimTK::Vector_<SpatialVec>(0),si.getUDot(),residualForcesNoConstraint); */

      /* if (printResidualForce) { */
      /*   cout << "-- residual force (f_inertial) = " << Mu + f_cons + f_inertial - gr - contactForces(si,osimModel) << endl; */
        /* cout << "-- residual force (cor)        = " << Mu + f_cons + cor - gr - contactForces(si,osimModel) << endl; */
        /* cout << "-- residual force (w/ mult)    = " << Mu + cor - gr - contactForces(si,osimModel) << endl; */
        /* cout << "-- SimTK residual force =     " << residualForces - contactForces(si,osimModel) << endl; */
        /* cout << "-- SimTK residual force        = " << residualForces << endl; */
        /* cout << "-- SimTK resforce NoConstr  = " << residualForcesNoConstraint << endl; */
        /* cout << "-- SimTK residual force NoConstraint = " << residualForcesNoConstraint - contactForces(si,osimModel) << endl; */
        /* cout << "-- residual (centrifugal)    = " << Mu + centrifugal3gen + centrifugal1gen + centrifugal2gen - gr - contactForces(si,osimModel) << endl; */
        /* cout << "-- resForce (mobility+Tree)    = " << Mu - F1 - F2 << endl; */
        /* SimTK::Vector totalFootForceGen; */
        /* matter.multiplyByFrameJacobianTranspose(si,MobilizedBodyIndex(1),Vec3(0),totalContactForces(si,osimModel),totalFootForceGen); */
        /* cout << "--> totalFootForceGen - contactForces = " << totalFootForceGen - contactForces(si,osimModel) << endl; */
      /* } */

      /* cout << "-- constraint set = " << osimModel.getConstraintSet() << endl; */

      // Get state variable values (faster with getStateValues vector)
      /* cout << "-- footGround_ty = " << osimModel.getStateVariable (si,"footGround_ty") << endl; */
      /* cout << "-- footGround_ty_u = " << osimModel.getStateVariable (si,"footGround_ty_u") << endl; */
      /* cout << "-- coordinate set = " << osimModel.getCoordinateSet().get("footGround_ty").getValue(si) << endl; */
      /* cout << "-- getUseEulerAngles = " << matter.getUseEulerAngles(si) << endl; */
      /* cout << "-- getNumQuaternionsInUse = " << matter.getNumQuaternionsInUse(si) << endl; */


      //--------------------------------- FREE-FLOATING JACOBIAN --------------------------------

      SimTK::Matrix Jc;
      matter.calcFrameJacobian (si, MobilizedBodyIndex(3), rPadPt, Jc);
      cout << "\n-- Frame jacob   = " << Jc << endl;

      matter.calcFrameJacobian (si, MobilizedBodyIndex(1), eefPoint, Jc);
      cout << "\n-- Frame jacob   = " << Jc << endl;

      SimTK::Matrix jacob_base = contactJacob_base (si, osimModel, rPadPt);
      cout << "-- Frame jacob_b = " << jacob_base << endl;

      SimTK::Matrix J_eef;
      /* matter.calcStationJacobian (si, MobilizedBodyIndex(1), eefPoint, J_eef); */
      /* cout << "-- Station jacob eef = " << J_eef << endl; */

      /* matter.calcStationJacobian (si, MobilizedBodyIndex(3), rPadPt, J_eef); */
      /* cout << "-- Station jacob eef = " << J_eef << endl; */

      /* SimTK::Matrix Jc; */
      /* matter.calcFrameJacobian (si, MobilizedBodyIndex(1), rPadPt, Jc); */
      /* cout << "\n-- Frame jacob   = " << Jc << endl; */

      /* SimTK::Matrix jacob_base = contactJacob_base (si, osimModel, rPadPt); */
      /* cout << "-- Frame jacob_b = " << jacob_base << endl; */

      /* SimTK::Matrix J_eef; */
      /* /1* matter.calcStationJacobian (si, MobilizedBodyIndex(1), eefPoint, J_eef); *1/ */
      /* /1* cout << "-- Station jacob eef = " << J_eef << endl; *1/ */

      /* matter.calcStationJacobian (si, MobilizedBodyIndex(1), rPadPt, J_eef); */
      /* cout << "-- Station jacob eef = " << J_eef << endl; */

      t0 += 0.1;
      tf += 0.1;
    }
  }
  catch (OpenSim::Exception ex)
  {
    std::cout << ex.getMessage() << std::endl;
    return 1;
  }
  catch (SimTK::Exception::Base ex)
  {
    std::cout << ex.getMessage() << std::endl;
    return 1;
  }
  catch (std::exception ex)
  {
    std::cout << ex.what() << std::endl;
    return 1;
  }
  catch (...)
  {
    std::cout << "UNRECOGNIZED EXCEPTION" << std::endl;
    return 1;
  }
  std::cout << "OpenSim example completed successfully" << std::endl;
  std::cout << "Press return to continue" << std::endl;
  std::cin.get();
  return 0;
}
