\documentclass{article}
\input{structure.tex} % Include the file specifying the document structure and custom commands


\title{One Leg Simulation} 


\date{\today}

%----------------------------------------------------------------------------------------

\begin{document}

\maketitle % Print the title

%----------------------------------------------------------------------------------------
%	CURRENT WORK
%----------------------------------------------------------------------------------------

\section{System}

The model consists of two cylindrical links and a brick representing the thigh and the leg and the foot respectively.
Revolute joints connects the thigh to the leg and the leg to the foot (Fig. \ref{fig:twolinkspathacts}).
The foot pushes on the ground through four spheres (Fig. \ref{fig:foot}), each of them is modeled based on Hunt-Crossley contact model \cite{huntcrossleyopensim}.
The dimensions and mass of different parts are in the appendix.
\begin{figure}
	\centering
	\includegraphics[width=0.7\linewidth]{photos/system_edit.png}
	\caption{Studied system consisting of two rigid links connected to each other by a revolute joint. They are actuated by path actuators which can wrap around cylinders at the joints.}
	\label{fig:twolinkspathacts}
\end{figure}

\begin{figure}
	\centering
	\includegraphics[width=0.7\linewidth]{photos/foot.PNG}
	\caption{Four contact squares based on Hunt-Crossley model}
	\label{fig:foot}
\end{figure}
Each joint is actuated by a pair of proportional path actuators whose behavior follows the equation 
\begin{equation}
	\label{eq:propotionalActuator}
	f = F_{max} \, u
\end{equation}
where $F_{max}$ is the maximum constant generalized force and
$u \in [0,1]$ is the control input. 
They are positioned in an agonist-antagonist configuration and wrap around a cylinder situated at the center of the joint. 


\section{Dynamic modeling}
Let's consider the system as a rigid structure arranged in a tree structure with a floating-base as the root.
The origin of the floating-base frame is located at the center of masse of the foot.
The pose of the floating-base with respect to the inertial frame 
$\mathbf{q_b}$ combined with the joint coordinate space $\mathbf{q_j}$ create the generalized configuration parameterization $\mathbf{q = [q_b^T \; q_j^T]^T}$ of the system.
$\mathbf{\nu = [\nu_b^T \; \dot{q_j}^T]^T}$ and 
$\mathbf{\dot{\nu}=[\dot{\nu_b}^T \; \ddot{q_j}^T]^T} $
are the $n$ generalized velocities and accelerations respectively with 
$\mathbf{\nu_b}$ the twist of the floating-base.
The continuous dynamics can be expressed in the Lagrangian form \textit{\textbf{according to OpenSim convention}}
\begin{equation}
	\label{eq:dynamic}
	\mathbf{
		M(q) \dot{\nu}  +  c (q,\nu) = g (q,\nu) +	\Gamma (q) + \Gamma_c
	}
\end{equation}
where
$\mathbf{M}$ the inertia matrix, 
$\mathbf{c}$ the centrifugal and Coriolis effects vector and
$\mathbf{g}$ the generalized gravity force vector.

Since the floating-base is not actuated, the system is underactuated.
The wrenches applied from $n_a$ actuators are written as
$$\mathbf{\Gamma(q)}=
\left \{
  \begin{tabular}{c}
  $\mathbf{0_6}$ \\
  $\mathbf{S(q)^T \, \tau}$
  \end{tabular}
\right \}
$$
with 
$\mathbf{\tau} \in \mathbf{R}^{n_a}$ the actuator torque vector and
$\mathbf{S(q)}$ the selection matrix.
In case of proportional path actuators, the torque $\tau$ can be expressed as
\begin{equation}
	\mathbf{
		\tau = D(q) \, F_{max} \, u
		}
		\label{eq:qpActForce}
\end{equation}
where
$\mathbf{F_{max}=diag}(f_{max}^i)$ is the diagonal maximal force matrix and
$\mathbf{D(q)}$ is the moment arm matrix which can be written as
$$\mathbf{D}=
\left \{
  \begin{tabular}{c c c c}
  $d_{lankle}$ & $d_{rankle}$ & $0$ & $0$ \\
  $0$ & $0$ & $d_{lknee}$ & $d_{rknee}$
  \end{tabular}
\right \}
$$
with $d_{lankle}$, $d_{rankle}$, $d_{lknee}$, $d_{rknee}$ the moment arm of the actuators \textbf{Left Ankle Muscle}, \textbf{Right Ankle Muscle}, \textbf{Left Knee Muscle} and \textbf{Right Knee Muscle} respectively.

$\mathbf{\Gamma_c = J_c^T(q) \, \gamma_c}$ captures the wrench applied to the foot through the contact with the ground in the generalized coordinates space, 
with $\mathbf{J_c}$ is the Jacobian w.t.r to the \textit{\textbf{center of mass of the foot}} and
$\mathbf{\gamma_c} \in \mathbf{R}^6$ is the total contact wrench  w.t.r to the \textit{\textbf{center of mass of the foot}} which can be written as
$$\mathbf{\gamma_c} = [t_c^x \quad t_c^z \quad t_c^z \quad f_c^x \quad f_c^y \quad f_c^z]^T$$
where $t$ indicates \textit{torque} and $f$ indicates \textit{force}.

\section{Optimization problem with quadratic programming (including generalized accelerations)}

\subsection{Problem formulation}
In this section, the task is to make the end-effector, which is the distal end of the thigh, reach a reference point by optimization process. 
The acceleration of the end-effector
$\mathbf{\ddot{x}}$ 
can be obtained by rearranging the dynamic equation
\begin{equation}
	\mathbf{
		\ddot{x} = 
		J_{eef}(q) \, M^{-1}(q) \, [D(q) \, F_{max} \, u + J_c(q)^T \, \gamma_c]
	  + J_{eef}(q) \, M^{-1}(q) \, [g(q)-c(q,\nu)] + \dot{J}_{eef}(q) \, \nu
	}
	\label{eq:qpAcc}
\end{equation}
with $\mathbf{J_{eef}}$ is the Jacobian w.r.t the end-effector.
On the other hand, the desired acceleration 
$\mathbf{\ddot{x}_{des}}$ 
of the end-effector to maintain on the reference trajectory 
$\mathbf{x_{ref}}$ 
is computed by a PD controller
\begin{equation}
	\mathbf{
		\ddot{x}_{des} = \ddot{x}_{ref} + K_p (x_{ref}-x) + K_v (\dot{x}_{ref} - \dot{x})
	}
	\label{eq:PDcontroller}
\end{equation}
with 
$\mathbf{K_p}$ and 
$\mathbf{K_v}$ 
the position gain and the velocity gain matrices respectively.
If these reference accelerations are achieved, errors between the end-effector desired coordinates and reference coordinates will be driven to zero. 
To drive these errors to zero in a critically damped fashion, the velocity gains can be chosen using the following relation:
\begin{equation}
	\mathbf{K_v = 2 \sqrt{K_p}}
	\label{eq:gains}
\end{equation}

For the optimization process, the performance criterion is to minimize the error between the actual acceleration of the end-effector and its desired acceleration.
The optimization variables (the generalized accelerations, the actuator controls and the foot/ground contact wrench w.t.r to the center of mass of the foot) are arranged in the vector
$$\mathbf{\chi}=
\left \{
  \begin{tabular}{c}
  $\mathbf{\dot{\nu}}$ \\
  \textbf{u} \\
  $\mathbf{\gamma_c}$
  \end{tabular}
\right \}
$$

The optimization problem is formulated as follow:
\begin{equation}
	\begin{aligned}
		& \mathbf{
		\underset{\chi}{\text{arg min}}} & & w_{task} \, \mathbf{||\ddot{x} - \ddot{x}_{des}||_2^2
		+ W_{reg} \, \chi
		}		
		\\
		& \text{s.t} & & \text{dynamic equation (\ref{eq:dynamic})}
		\\	
		& && \text{actuator control limit:} \quad \mathbf{0 \leq u \leq 1} 
		\\
		& && \text{foot/ground non-penetration:} \quad f_c^y \geq 0
		\\
		& && \text{flat foot on the ground:} \quad m_c^x \geq 0 \quad m_c^z \geq 0
		\\
		& && \text{revolute joint limit:} \quad \mathbf{q_j^{max} \leq q_j \leq q_j^{min}}
	\end{aligned}
	\label{eq:optProbCurrentWork}
\end{equation}
where
$w_{task}$ is the weight associated to the task and
$\mathbf{W=diag}(w_i)$ the diagonal matrix of regulation weights $w_i$ associated to the elements of $\chi$.
$$\mathbf{W_{reg} \, \chi} = \sum_{i=1}^{n+n_a+6} w_i \, \chi_i^2 $$

\textit{\textbf{(**) The linear velocity constraint and the friction contraint for the foot/ground contact are NOT taken into account during this optimization process since no significant linear movement is observed during simulation.}}

\subsection{Transformation of the cost function}
The optimization problem is transformed into a quadratic programming problem. It is expressed as: 
\begin{equation}
	\begin{aligned}
		& \mathbf{\underset{u}{\text{min}}} & & 
		\mathbf{\frac{1}{2} \chi^T H \chi + \chi^Tg} 
	\end{aligned}
	\label{eq:qpProblem}
\end{equation}
To obtain $\mathbf{H}$ and $\mathbf{g}$, first 
$\mathbf{\ddot{x} - \ddot{x}_{des}}$ 
is put under a form of 
$\mathbf{P\chi+k}$ where 
\begin{equation}
	\mathbf{
		P = 
		[
			O_{3\times n} \quad
			J_{eef}(q) \, M^{-1} \, (q) \, D(q) \, F_{max} \quad
			J_{eef}(q) \, M^{-1} \, (q) \, J_c(q)^T
		]
	} 
	\label{eq:qpP}
\end{equation}
and 
\begin{equation}
	\mathbf{
		k = J_{eef}(q) \, M^{-1}(q) \, [ \,g(q)-c(q,\nu) \,] + \dot{J}_{eef}(q) \, \nu - \ddot{x}_{des}
	}
	\label{eq:qpk}
\end{equation}
Then $\mathbf{H}$ and $\mathbf{g}$ can be obtained by the following equations:
\begin{equation}
	\mathbf{H = } w_{task} \, \mathbf{P^T P + W_{reg}}
	\label{eq:qpH}
\end{equation}
\begin{equation}
	\mathbf{g = } w_{task} \, \mathbf{P^T k}
	\label{eq:qpG}
\end{equation}

\subsection{Transformation of the constraints}
\textit{\textbf{** This section will be modified later.}
}

The constraints should be put under these forms
$$ \mathbf{lb_A \leq A\chi \leq ub_A}$$
$$ \mathbf{lb \leq \chi \leq ub}$$

The first form expresses the dynamic equation
$$ \mathbf{lb_A = ub_A = g(q) - c(q,\nu)}$$
$$ \mathbf{A=[M(q) \quad -D(q)F_{max} \quad -J_c^T]}$$

The second from expressed the boundary constraints of the optimization variables $\chi$.
\paragraph{Generalized acceleration boundaries}
$$
\left \{
  \begin{tabular}{c}
  $\mathbf{-10^9_{6 x 1}}$ \\
  $\frac{2}{h} (\mathbf{q_{min}-q}-h \mathbf{\nu})$ \\
  \end{tabular}
\right \}
\leq \dot{\nu} \leq 
\left \{
  \begin{tabular}{c}
  $\mathbf{10^9_{6 x 1}}$ \\
  $\frac{2}{h} (\mathbf{q_{max}-q}-h \mathbf{\nu})$ \\
  \end{tabular}
\right \}
$$
where $\mathbf{-10^9_{6 x 1}}$ and $\mathbf{10^9_{6 x 1}}$ are the vectors representing the unlimited boudaries of floating-base generalized coordinates,
$\mathbf{q_{min}}$ and $\mathbf{q_{min}}$ are the vectors of joint limits (knee and ankle) and
$h$ is the time step for the forward dynamic simulation.


\paragraph{Actuator control input boundaries}
Control input for proportional path actuators is limited in the interval $[0,1]$.
$$\mathbf{0 \leq u \leq 1}$$

\paragraph{Contact force boundaries}
The numerical values of the boundaries of $\mathbf{\gamma_c}$ are based on a forward dynamic simulation during which all componants of the contact wrench stay inside an interval $[-100,100]$.
Since $f_c^y$, $m_c^x$ and $m_c^z$ has 0 as their minimum values, the lower bound vector for $\gamma_c$ is $[0 \; -100 \; 0 \; -100 \; 0 \; -100]$.
The upper bound vector is $\mathbf{100_6}$.
$$
\begin{bmatrix}
	0 \\
	-100 \\
	0 \\
	-100 \\
	0 \\
	-100
\end{bmatrix}
\leq \mathbf{\gamma_c}=
\begin{bmatrix}
	m_c^x \\
	m_c^y \\
	m_c^z \\
	t_c^x \\
	t_c^y \\
	t_c^z
\end{bmatrix}
 \leq \mathbf{100_6}
$$

\subsection{Simulation}
\subsubsection{Set up}
\paragraph{Actuators}
The maximal force $\mathbf{F_{max}}$ of every path actuators is \textbf{1000 N}. 

\paragraph{Controler}
The position gains $\mathbf{K_p}$ are all chosen to be \textbf{64} and therefore the value of all velocity gains $\mathbf{K_v}$ are \textbf{16}.

\paragraph{Regulation term of the cost function}
The values of weight are
\begin{itemize}
\item Task $w_{task} = 1$
\item Regulation $\mathbf{W_{reg}=10^{-6}}$
\end{itemize}

\paragraph{Initial conditions}
The initial control inputs of the "Left ankle muscle" and "Right knee muscle" are \textbf{0.063} to attain the static equilibrum of the system.
The intial control inputs of the other actuators are \textbf{0}.
The initial angle of the knee is \textbf{0.1 rad}.
The initial angle of the ankle is \textbf{0 rad}.

\paragraph{Destination point}
Reaching a point whose coordinates is \textbf{(0, 0.8, 0)}.

\subsubsection{Results}
\textit{\textbf{not available for now
}}
\section{Optimization problem with quadratic programming (NOT including generalized accelerations)}

\subsection{Problem formulation}
The equations from (\ref{eq:qpAcc}) - (\ref{eq:gains}) are still taken into account for this section.

The new optimization variable vector is
$$\mathbf{\chi}=
\left \{
  \begin{tabular}{c}
  \textbf{u} \\
  $\mathbf{\gamma_c}$
  \end{tabular}
\right \}
$$

The optimization problem is formulated as follow:
\begin{equation}
	\begin{aligned}
		& \mathbf{
		\underset{\chi}{\text{arg min}}} & & w_{task} \, \mathbf{||\ddot{x} - \ddot{x}_{des}||_2^2
		+ W_{reg} \, \chi
		}		
		\\
		& \text{s.t} & & \text{dynamic equation (\ref{eq:dynamic})}
		\\	
		& && \text{actuator control limit:} \quad \mathbf{0 \leq u \leq 1} 
		\\
		& && \text{foot/ground non-penetration:} \quad f_c^y \geq 0
		\\
		& && \text{flat foot on the ground:} \quad m_c^x \geq 0 \quad m_c^z \geq 0
	\end{aligned}
	\label{eq:optProbCurrentWork}
\end{equation}
where
$w_{task}$ is the weight associated to the task and
$\mathbf{W=diag}(w_i)$ the diagonal matrix of regulation weights $w_i$ associated to the elements of $\chi$.
$$\mathbf{W_{reg} \, \chi} = \sum_{i=1}^{n+n_a+6} w_i \, \chi_i^2 $$

\textit{\textbf{(*) The linear velocity constraint and the friction contraint for the foot/ground contact are NOT taken into account during this optimization process since no significant linear movement is observed during simulation.}}

\textit{\textbf{(**) The joint limits are NOT taken into account for the moment.}}

\subsection{Transformation of the cost function}
The optimization problem is transformed into a quadratic programming problem. It is expressed as: 
\begin{equation}
	\begin{aligned}
		& \mathbf{\underset{u}{\text{min}}} & & 
		\mathbf{\frac{1}{2} \chi^T H \chi + \chi^Tg} 
	\end{aligned}
	\label{eq:qpProblem}
\end{equation}
To obtain $\mathbf{H}$ and $\mathbf{g}$, first 
$\mathbf{\ddot{x} - \ddot{x}_{des}}$ 
is put under a form of 
$\mathbf{P\chi+k}$ where 
\begin{equation}
	\mathbf{
		P = 
		[
			J_{eef}(q) \, M^{-1} \, (q) \, D(q) \, F_{max} \quad
			J_{eef}(q) \, M^{-1} \, (q) \, J_c(q)^T
		]
	} 
	\label{eq:qpP}
\end{equation}
and 
\begin{equation}
	\mathbf{
		k = J_{eef}(q) \, M^{-1}(q) \, [ \,g(q)-c(q,\nu) \,] + \dot{J}_{eef}(q) \, \nu - \ddot{x}_{des}
	}
	\label{eq:qpk}
\end{equation}
Then $\mathbf{H}$ and $\mathbf{g}$ can be obtained by the following equations:
\begin{equation}
	\mathbf{H = } w_{task} \, \mathbf{P^T P + W_{reg}}
	\label{eq:qpH}
\end{equation}
\begin{equation}
	\mathbf{g = } w_{task} \, \mathbf{P^T k}
	\label{eq:qpG}
\end{equation}

\subsection{Transformation of the constraints}
\textit{\textbf{** This section will be modified later.}
}

The constraints should be put under these forms
$$ \mathbf{lb_A \leq A\chi \leq ub_A}$$
$$ \mathbf{lb \leq \chi \leq ub}$$

The first form expresses the dynamic equation
$$ \mathbf{lb_A = ub_A = g(q) - c(q,\nu) - M(q) \, \dot{\nu}}$$
$$ \mathbf{A=[-D(q)F_{max} \quad -J_c^T]}$$

The second from expressed the boundary constraints of the optimization variables $\chi$.

\paragraph{Actuator control input boundaries}
Control input for proportional path actuators is limited in the interval $[0,1]$.
$$\mathbf{0 \leq u \leq 1}$$

\paragraph{Contact force boundaries}
The numerical values of the boundaries of $\mathbf{\gamma_c}$ are based on a forward dynamic simulation during which all componants of the contact wrench stay inside an interval $[-100,100]$.
Since $f_c^y$, $m_c^x$ and $m_c^z$ has 0 as their minimum values, the lower bound vector for $\gamma_c$ is $[0 \; -100 \; 0 \; -100 \; 0 \; -100]$.
The upper bound vector is $\mathbf{100_6}$.
$$
\begin{bmatrix}
	0 \\
	-100 \\
	0 \\
	-100 \\
	0 \\
	-100
\end{bmatrix}
\leq \mathbf{\gamma_c}=
\begin{bmatrix}
	m_c^x \\
	m_c^y \\
	m_c^z \\
	t_c^x \\
	t_c^y \\
	t_c^z
\end{bmatrix}
 \leq \mathbf{100_6}
$$

\subsection{Simulation}
\subsubsection{Set up}
\paragraph{Actuators}
The maximal force $\mathbf{F_{max}}$ of every path actuators is \textbf{1000 N}. 

\paragraph{Controler}
The position gains $\mathbf{K_p}$ are all chosen to be \textbf{64} and therefore the value of all velocity gains $\mathbf{K_v}$ are \textbf{16}.

\paragraph{Regulation term of the cost function}
The values of weight are
\begin{itemize}
\item Task $w_{task} = 1$
\item Actuators at ankle : $10^-2$
\item Actuators at knee: $10^-9$ since I'm struggling to increase their control inputs
\item Regulation $\mathbf{W_{reg}=10^{-6}}$
\end{itemize}

\paragraph{Initial conditions}
The initial control inputs of the "Left ankle muscle" and "Right knee muscle" are \textbf{0.063} to attain the static equilibrum of the system.
The intial control inputs of the other actuators are \textbf{0}.
The initial angle of the knee is \textbf{0.1 rad}.
The initial angle of the ankle is \textbf{0 rad}.

\paragraph{Destination point}
Reaching a point whose coordinates is \textbf{(0, 0.8, 0)}.

\subsubsection{Results}
Despite being associated with a small value of weight, the right knee muscle was not activated, especially at the beginning of the simulation, to pull the thigh to the right and not let it swing down.

At least, the actuators at the ankle still manange to hold the leg up, which is an improvement comparing to the case when $\dot{\nu}$ was included in the optimization vector $\chi$.

(video joint email)

\begin{figure}[H]
	\begin{subfigure}{0.5\textwidth}
		\centering
		\includegraphics[width=\linewidth]{graphs/ankleControls.png}
		\caption{Ankle muscle controls}
	\end{subfigure}
	\begin{subfigure}{0.5\textwidth}
		\centering
		\includegraphics[width=\linewidth]{graphs/ankleForces.png}
		\caption{Ankle muscle forces}
	\end{subfigure}
	\begin{subfigure}{0.5\textwidth}
		\centering
		\includegraphics[width=\linewidth]{graphs/kneeControls.png}
		\caption{Knee muscle controls}
	\end{subfigure}
	\begin{subfigure}{0.5\textwidth}
		\centering
		\includegraphics[width=\linewidth]{graphs/kneeForces.png}
		\caption{Knee muscle forces}
	\end{subfigure}
	\caption{Optimization results with qpOASES}
	\label{fig:test1}
\end{figure}

\begin{figure}[H]
	\begin{subfigure}{0.8\textwidth}
		\centering
		\includegraphics[width=\linewidth]{graphs/trajectory.png}
		\caption{Tracking position (the reference point is red, on the right edge of the graph)}
	\end{subfigure}
	\caption{Tracking position with qpOASES}
	\label{fig:test1}
\end{figure}


\section{Appendix}
\subsection{Part physical properties}
\begin{tabular}{|l|l|l|l|l|l|}
    \hline
    Part & Form & Weight & Length (X) & Height (Y) & Width (Z) \tabularnewline
    \hline
    Leg & Cylinder & 4.75 kg & 77 mm & 410 mm & 77 mm  \tabularnewline
    \hline
    Thigh & Cylinder & 10.5 kg & 105 mm & 430 mm & 105 mm  \tabularnewline
    \hline
    Foot & Brick & 1.43 kg & 240 mm & 90 mm & 93 mm  \tabularnewline
    \hline
    Contact sphere & Sphere & 0 kg & 5 mm & 5 mm & 5 mm  \tabularnewline
    \hline
 \end{tabular}


%----------------------------------------------------------------------------------------

\bibliographystyle{unsrt}
\bibliography{mybib}
\end{document}
