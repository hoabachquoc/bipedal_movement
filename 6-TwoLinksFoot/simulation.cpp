#include <OpenSim/OpenSim.h>
#include <Simbody.h>
#include <cmath>
#include <qpOASES.hpp>

#include <iostream>
#include <fstream>
#include <cstdlib>
#include <vector>

using namespace OpenSim;
using namespace SimTK;
using namespace std;
USING_NAMESPACE_QPOASES


//----------------------------------------- OPTIONS --------------------------------------------
bool simulation = 1;
bool useVisualizer = 0;		// cause segmentation fault !!!
bool verifyInitSys = 0;
bool printRefTrajectory = 0;
bool printInitialCtrl = 0;
bool printInitialState = 0;
bool printlog = 0;
bool printDesAcc = 0;
bool printNewCtrls = 0;
bool printDbgQPForm = 0;    // print it to avoid absurd results (don't know why yet ...)
bool printQPmatrices = 0;
bool printContactFr = 0;
bool printJc = 0;

/** Set initial state
 * default: data 
 * 1: all 0 
 * 2: knee 0.1
 * 3: all 0
 * 4: knee 0.3
 **/
int setInitialState = 2;

/* Set initial control
 * 1: gravity compensation
 * 2: all 0
 * 3: all 0.3
*/
int setInitCtrl = 1;


/** Set test case
 * default: data
 * 1: origin (0,0,0)
 * 2: vertical
 * 3: pose
 **/
int testCase = 2;
int TrajLength = 900;


//-------------------------------------- OPTIMIZATION SETTINGS ---------------------------------
// Control sampling time
double h = 1e-3;
bool dynamicConstraint = 1;

//-------------------------------------- QPOASES OPTIMIZATION ----------------------------------
bool setqpOASES = 1;
bool printqpOAESOptions = 0;
bool modifyqpOASESOptions = 0;
bool printMatrixQPOASES = 0;
bool printqpOASESsolution = 1;

//----------------------------------- BUILT-IN OPTIMIZATION ------------------------------------
/** Set OpenSim built-in optimizer
 * 1: Interior Point
 * 2: Limited-memory Broyden-Fletcher-Goldfarb-Shanno (LBFGS)
 * 3: LBFGS with simple bound constraints (LBFGSB)
 * 4: C implementation of sequential quadratic programming (CFSQP)
 * 5: Covariance matrix adaptation, evolution strategy (CMAES)
 **/
int setOptimizerAlgorithm = 1;
bool setCostFuncFormulationType = 0;		// 1: integrator || 0 : qp

//----------------------------------------- WEIGHTS --------------------------------------------
/* Generalized acceleration */
SimTK::Real fbw0   = 1e-6;
SimTK::Real fbw1   = 1e-6;
SimTK::Real fbw2   = 1e-6;
SimTK::Real fbw3   = 1e-6;
SimTK::Real fbw4   = 1e-6;
SimTK::Real fbw5   = 1e-6;
SimTK::Real anklew = 1e-6;
SimTK::Real kneew  = 1e-6;

/* Control input */
SimTK::Real lanklew = 1e-6;
SimTK::Real ranklew = 1e-6;
SimTK::Real lkneew  = 1e-6;
SimTK::Real rkneew  = 1e-6;

/* Contact wrench */
SimTK::Real wrenchw0 = 1e-6;
SimTK::Real wrenchw1 = 1e-6;
SimTK::Real wrenchw2 = 1e-6;
SimTK::Real wrenchw3 = 1e-6;
SimTK::Real wrenchw4 = 1e-6;
SimTK::Real wrenchw5 = 1e-6;

//---------------------------------------- END-EFFECTOR ----------------------------------------
Vec3 eefPoint(0,0.43,0);		// in body frame

//------------------------------------- CONTACT POINTS -----------------------------------------
/** Contact points in foot frame
 * Contact sphere radius 0.003 m
 * Contact sphere center position in foot frame Y = -0.005
 **/
bool separateContactPoint = 0;
int nc = 1;
/* int nc = 4; */
Vec3 lHeelPt(-0.07,-0.008,-0.02);
Vec3 rHeelPt(-0.07,-0.008,0.02);
Vec3 lPadPt(0.06,-0.008,-0.02);
Vec3 rPadPt(0.06,-0.008,0.02);

//------------------------------------- INTEGRATOR OPTIONS -------------------------------------
unsigned stepNum = 1;
const Real stepSize = 1e-3;
double initialStepTime = 0.0;
double finalStepTime = initialStepTime + stepSize;


//------------------------------------- FEEDBACK CONTROLERS ------------------------------------
Mat33 Kp(64); // position gain
Mat33 Kv(16); // velocity gain

//-------------------------------------- MODEL PROPERTIES --------------------------------------

//---------------------------------------- MISCELLANEOUS ---------------------------------------


//==============================================================================================
//				                    TRAJECTORY GENERATION - KDL
//==============================================================================================
int calcTrajVecLength() {
    string s;
    int sTotal(0);

    ifstream in;
    in.open("velocity.dat");

    while(!in.eof()) {
        getline(in, s);
        sTotal ++;	
    }
    return sTotal - 1;
}

void printvector(vector<double> &vect) {
    for (vector<double>::const_iterator iter = vect.begin(); iter != vect.end(); ++iter)
        cout << *iter << endl;
}

void generateTrajectry ( Vector_<Vec3> &refPos, Vector_<Vec3> &refVel, Vector_<Vec3> &refAcc ) {
    // Read reference acceleration
    ifstream acc_in("acceleration.dat");
    if (!acc_in) {
        cerr << "Uh oh, file .dat could not be opened for reading!" << endl;
        exit(1);
    }
    // Create the vector, initialized from the numbers in the file:
    std::vector<double> accData (( std::istream_iterator<double>(acc_in)),
            std::istream_iterator<double>());
    int i = 0;
    for (int count = 0; count < accData.size(); count+=6) {
        refAcc[i] = Vec3( accData[count], accData[count+1], 0); i++;
    }

    // Read reference velocity
    ifstream vel_in("velocity.dat");
    if (!vel_in) {
        cerr << "Uh oh, file .dat could not be opened for reading!" << endl;
        exit(1);
    }
    // Create the vector, initialized from the numbers in the file:
    std::vector<double> velData (( std::istream_iterator<double>(vel_in)),
            std::istream_iterator<double>());
    i = 0;
    for (int count = 0; count < velData.size(); count+=6) {
        refVel[i] = Vec3( velData[count], velData[count+1], 0);
        i++;
    }

    // Read reference position
    ifstream traj_in("position.dat");
    if (!traj_in) {
        cerr << "Uh oh, file .dat could not be opened for reading!" << endl;
        exit(1);
    }

    std::vector<double> posData (( std::istream_iterator<double>(traj_in)),
            std::istream_iterator<double>());
    int j = 0;
    for (int count = 0; count < posData .size(); count+=16) {
        refPos[j] = Vec3( posData [count+3], posData [count+7], 0);
        j++;
    }
}

//==============================================================================================
// 			                            REARRANGE VECTOR
//==============================================================================================

// Convert Vec3 to Vector
SimTK::Vector vec3ToVector(Vec3& vec){
    SimTK::Vector vecOut(3);
    for (int i = 0; i < 3; i++){
        vecOut(i) = vec(i);
    }
    return vecOut;
}

// Array of bodies on which are located contact points
// Until now the only body is the foot
Array_<MobilizedBodyIndex> bodyCt() {
    Array_<MobilizedBodyIndex> onBodyB(nc);
    onBodyB[0] = MobilizedBodyIndex(1);
    onBodyB[1] = MobilizedBodyIndex(1);
    onBodyB[2] = MobilizedBodyIndex(1);
    onBodyB[3] = MobilizedBodyIndex(1);
    return onBodyB; 
}

// Array of frame origin points of task frames (contact points)
Array_<Vec3> originCtPtFrm() {
    Array_<Vec3> originAoInB(nc);

    if (separateContactPoint) {
      originAoInB[0] = Vec3(0);
      originAoInB[1] = Vec3(0);
      originAoInB[2] = Vec3(0);
      originAoInB[3] = Vec3(0);
    }
    else {
      originAoInB[0] = lHeelPt;
      originAoInB[1] = rHeelPt;
      originAoInB[2] = lPadPt;
      originAoInB[3] = rPadPt;
    }

    return originAoInB;
}

/* void printqpOASESmatrix(real_t Mat) { */
/*   for (int ii = 0; ii < nbTotalOptVar; ++ii) */
/*   { */
/*     printf ("H: "); */
/*     for (int jj = 0; jj < nbTotalOptVar; ++jj ) */
/*       printf ("%8.2e ", H_qp[ii*nbTotalOptVar+jj]); */
/*     printf("\n"); */
/*   } */
/* } */
//==============================================================================================
// 			                          OPTIMIZATION SYSTEM
//==============================================================================================

/** Foot-ground contact forces **/
SimTK::Vector contactForces(State& si, Model& osimModel) {
    // Retrieve contact forces
    Array<double> lHeelForceRec = osimModel.getForceSet().get("LeftHeelContactForce").getRecordValues(si);
    Array<double> rHeelForceRec = osimModel.getForceSet().get("RightHeelContactForce").getRecordValues(si);
    Array<double> lPadForceRec = osimModel.getForceSet().get("LeftPadContactForce").getRecordValues(si);
    Array<double> rPadForceRec = osimModel.getForceSet().get("RightPadContactForce").getRecordValues(si);
    
    Vec3 lHeelForce(lHeelForceRec[6],lHeelForceRec[7],lHeelForceRec[8]);
    Vec3 lHeelMoment(lHeelForceRec[9],lHeelForceRec[10],lHeelForceRec[11]);

    Vec3 rHeelForce(rHeelForceRec[6],rHeelForceRec[7],rHeelForceRec[8]);
    Vec3 rHeelMoment(rHeelForceRec[9],rHeelForceRec[10],rHeelForceRec[11]);

    Vec3 lPadForce(lPadForceRec[6],lPadForceRec[7],lPadForceRec[8]);
    Vec3 lPadMoment(lPadForceRec[9],lPadForceRec[10],lPadForceRec[11]);

    Vec3 rPadForce(rPadForceRec[6],rPadForceRec[7],rPadForceRec[8]);
    Vec3 rPadMoment(rPadForceRec[9],rPadForceRec[10],rPadForceRec[11]);

    if (printContactFr) {
        cout << "-- lHeelForceRec = " << lHeelForceRec << endl;
        cout << "-- lHeelForce = " << lHeelForce << endl;
        cout << "-- lHeelMoment = " << lHeelMoment << endl;
        cout << "-- rHeelForce = " << rHeelForce << endl;
        cout << "-- rHeelMoment = " << rHeelMoment << endl;
        cout << "-- lPadForce = " << lPadForce << endl;
        cout << "-- lPadMoment = " << lPadMoment << endl;
        cout << "-- rPadForce = " << rPadForce << endl;
        cout << "-- rPadMoment = " << rPadMoment << endl;
    }


    // Generalized forces resulting from foot ground contact forces
    const SimbodyMatterSubsystem& matter = osimModel.getMatterSubsystem();

    Array_<MobilizedBodyIndex> onBodyB = bodyCt();

    Array_<Vec3> originAoInB = originCtPtFrm();

    Vector_<SpatialVec> F_GAo(nc);
    F_GAo[0] = SpatialVec(lHeelMoment,lHeelForce);
    F_GAo[1] = SpatialVec(rHeelMoment,rHeelForce);
    F_GAo[2] = SpatialVec(lPadMoment,lPadForce);
    F_GAo[3] = SpatialVec(rPadMoment,rPadForce);
    
    SimTK::Vector contact;
    matter.multiplyByFrameJacobianTranspose(si, onBodyB, originAoInB, F_GAo, contact);
    return contact;
}


/** Moment arms set
**/
SimTK::Vector momentArmVec(State& si, Model& osimModel) {
    // Prepare actuators - dynamic cast
    PathActuator& leftAnkleMuscle = dynamic_cast<PathActuator&>
        (osimModel.getActuators().get(0));
    PathActuator& rightAnkleMuscle = dynamic_cast<PathActuator&>
        (osimModel.getActuators().get(1));
    PathActuator& leftKneeMuscle = dynamic_cast<PathActuator&>
        (osimModel.getActuators().get(2));
    PathActuator& rightKneeMuscle = dynamic_cast<PathActuator&>
        (osimModel.getActuators().get(3));

    // Prepare coordinates
    Coordinate& ankle_ = osimModel.getCoordinateSet().get(6);
    Coordinate& knee_ = osimModel.getCoordinateSet().get(7);

    // Moment arm vector
    int nbAct = osimModel.getNumControls();
    SimTK::Vector momentArms(nbAct);
    momentArms (0) = leftAnkleMuscle.computeMomentArm(si, ankle_);
    momentArms (1) = rightAnkleMuscle.computeMomentArm(si, ankle_);
    momentArms (2) = leftKneeMuscle.computeMomentArm(si, knee_);
    momentArms (3) = rightKneeMuscle.computeMomentArm(si, knee_);
    return momentArms;
}

/** Matrix of maximal force of proportional path actuators
**/
SimTK::Matrix FmMat (State& si, Model& osimModel) {
    int nbAct = osimModel.getNumControls();
    SimTK::Matrix Fm (nbAct,nbAct);
    Fm = 0;
    for (int i = 0; i < nbAct; i++) {
        Fm(i,i) = osimModel.getActuators().get(i).getOptimalForce();
    }
    return Fm;
}

/** Multiply this matrix (nu x na) to associate generalized coordinates 
  * to muscles which actuate them.
  * The moment arms are already assigned a sign (+/-) according to the rotation
  * generated anti-clockwise/clockwise
  * generally to separate the floating base coordinates with the rest
  * Multiply this matrix with muscles forces vector to produce a torque vector.
**/
SimTK::Matrix momentArmMat (State& si, Model& osimModel) {
    int nbU = si.getNU();
    int nbAct = osimModel.getNumControls();
    SimTK::Vector momentArms = momentArmVec(si, osimModel);
    SimTK::Matrix MA_Mat (nbU,nbAct);
    // First 6 rows are 0 - free joint
    MA_Mat = 0;
    MA_Mat (6,0) = momentArms(0);
    MA_Mat (6,1) = momentArms(1);
    MA_Mat (7,2) = momentArms(2);
    MA_Mat (7,3) = momentArms(3);
    return MA_Mat;
} 
/** Selection matrix for propotional muscles
  * To be multiplied with control vector
**/

SimTK::Matrix selectionMatrixForProportionalMuscles (State& si, Model& osimModel) {
    return momentArmMat(si, osimModel) * FmMat(si, osimModel);
}

// Simple regulation matrix (weight matrix) for only control optimization
SimTK::Matrix simpleRegulationMatrix (Model& osimModel){
    int nbAct = osimModel.getNumControls();

    SimTK::Matrix regulationMat(nbAct,nbAct);

    regulationMat(0,0) = lanklew;
    regulationMat(1,1) = ranklew;
    regulationMat(2,2) = lkneew;
    regulationMat(3,3) = rkneew;

    return regulationMat;
}

// Regulation matrix (weight matrix) for all optimization variables
SimTK::Matrix regulationMatrix (Model& osimModel, State& si){
    int nbAct = osimModel.getNumControls();
    int nu = si.getNU();
    int size = nbAct + nu + 6*nc;

    SimTK::Matrix regulationMat(size,size);
    regulationMat(0,0) = fbw0;
    regulationMat(1,1) = fbw1;
    regulationMat(2,2) = fbw2;
    regulationMat(3,3) = fbw3;
    regulationMat(4,4) = fbw4;
    regulationMat(5,5) = fbw5;
    regulationMat(6,6) = anklew;
    regulationMat(7,7) = kneew;

    regulationMat(8,8)   = lanklew;
    regulationMat(9,9)   = ranklew;
    regulationMat(10,10) = lkneew;
    regulationMat(11,11) = rkneew;

    regulationMat(12,12) = wrenchw0;
    regulationMat(13,13) = wrenchw1;
    regulationMat(14,14) = wrenchw2;
    regulationMat(15,15) = wrenchw3;
    regulationMat(16,16) = wrenchw4;
    regulationMat(17,17) = wrenchw5;

    return regulationMat;
}

/** Contact Jacobian composed of only translational part
**/
SimTK::Matrix ContactTranslationalJacobian(State& si, Model& osimModel) {
    const SimbodyMatterSubsystem& matter = osimModel.getMatterSubsystem();
    int nu = si.getNU();
    SimTK::Matrix Jc(3*nc, nu);
    matter.calcStationJacobian(si, bodyCt(), originCtPtFrm(), Jc);
    return Jc;
}


/** Jdotu at contact points composed of only translational part
**/
SimTK::Vector JduContact(State& si, Model& osimModel) {
    const SimbodyMatterSubsystem& matter = osimModel.getMatterSubsystem();
    SimTK::Vector jdotuct(3*nc);
    matter.calcBiasForStationJacobian(si, bodyCt(), originCtPtFrm(), jdotuct);
    return jdotuct;
}


/** Mass matrix
*/
SimTK::Matrix MassMatrix(State& si, Model& osimModel) {
    const SimbodyMatterSubsystem& matter = osimModel.getMatterSubsystem();
    SimTK::Matrix M_;
    matter.calcM(si,M_);
    return M_;
}


/** Inverse Mass matrix
*/
SimTK::Matrix InverseMassMatrix(State& si, Model& osimModel) {
    const SimbodyMatterSubsystem& matter = osimModel.getMatterSubsystem();
    SimTK::Matrix Minv;
    matter.calcMInv(si,Minv);
    return Minv;
}


/** Methods q_max, q_min, q_value, nu_value don't take into account the floating base
**/
SimTK::Vector q_max (Model& osimModel) {
    const CoordinateSet& CoordSet = osimModel.getCoordinateSet();
    SimTK::Vector qMax(CoordSet.getSize()-6);
    for (int i = 0; i < CoordSet.getSize()-6; i++) {
        qMax(i) = CoordSet.get(i+6).getRangeMax();
    }
    return qMax;
}

SimTK::Vector q_min (Model& osimModel) {
    const CoordinateSet& CoordSet = osimModel.getCoordinateSet();
    SimTK::Vector qMin(CoordSet.getSize()-6);
    for (int i = 0; i < CoordSet.getSize()-6; i++) {
        qMin(i) = CoordSet.get(i+6).getRangeMin();
    }
    return qMin;
}

SimTK::Vector q_value (Model& osimModel, State& s) {
    const CoordinateSet& CoordSet = osimModel.getCoordinateSet();
    SimTK::Vector qVal(CoordSet.getSize()-6);
    for (int i = 0; i < CoordSet.getSize()-6; i++) {
        qVal(i) = CoordSet.get(i+6).getValue(s);
    }
    return qVal;
}

SimTK::Vector nu_value (Model& osimModel, State& s) {
    const CoordinateSet& CoordSet = osimModel.getCoordinateSet();
    SimTK::Vector nuVal(CoordSet.getSize()-6);
    for (int i = 0; i < CoordSet.getSize()-6; i++) {
        nuVal(i) = CoordSet.get(i+6).getSpeedValue(s);
    }
    return nuVal;
}



//------------------------------ QUADRATIC PROGRAMING FORMULATION ------------------------------

/** This function calculates
 * Matrix H in which integrated the regulation terms
 * Vector g
 **/

/* M(q).du + ~G.mult = g(q) + T + gamma - cor(q,u)
 * --> du = Minv.(T + g + gamma - ~G.mult - cor)
 * Xd = J.u
 * --> Xdd = dJ.u + J.du
 * --> Xdd = dJ.u + J.Minv.(T + g + gamma - ~G.mult - cor)
 * --> Xdd = J.Minv.(T + gamma) + J.Minv.(g - ~G.mult - cor) + dJ.u
 * --> Xdd = J.Minv.S.u + J.Minv.~Jc.Fc + J.Minv.(g - ~G.mult - cor) + dJ.u
 * Xdd_des = Xdd_traj_ + p_gains_.( X_des - X_curr_) + d_gains_.( Xd_des - Xd_curr_)
 * compute min(u) || Xdd - Xdd_des ||² + u^T * W * u
 * Xdd - Xdd_des = P.x + k
 * With P = [0  J.Minv.S  J.Minv.~Jc]
 *      k = J.Minv.(g - ~G.mult - cor) + dJ.u
*/

/** QP with only actuator controls as optimization parameters
**/
void simpleQPformulation(State& si, Model& osimModel, Vec3& desInput, SimTK::Matrix& H_, SimTK::Vector& g_) {
    const SimbodyMatterSubsystem& matter = osimModel.getMatterSubsystem();
    const int nb = matter.getNumBodies();

    /** Jacobian at the end-effector - Thigh/Body Index 3
      *  Dimension 3 x 8
    **/  
    SimTK::Matrix J_eef;
    matter.calcStationJacobian(si,MobilizedBodyIndex(3),eefPoint,J_eef);

    /** Gravtiy and Coriolis forces are on the rhs of the dynamic equation
      * They have the opposite sign of the actuator forces
      * Dimension: 8 x 1
    **/
    SimTK::Vector gr;
    matter.multiplyBySystemJacobianTranspose(si, osimModel.getGravityForce().getBodyForces(si), gr);
    SimTK::Vector cor;
    matter.calcResidualForceIgnoringConstraints(si, SimTK::Vector(0), SimTK::Vector_<SimTK::SpatialVec>(0), SimTK::Vector(0),cor);

    // gravity - coriolis - ~G.mult
    SimTK::Vector _f =  gr - cor;

    // M^-1*(gravity - coriolis - ~G.mult) || Dimension 8 x 1
    SimTK::Vector MInvf;
    matter.multiplyByMInv(si, _f, MInvf);


    // J_eff*M^-1*(gravity - coriolis - ~G.mult)
    Vec3 J_effMInvf = matter.multiplyByStationJacobian(si, MobilizedBodyIndex(3), eefPoint, MInvf);

    // d(J_eff)*u
    Vec3 J_effDotu = matter.calcBiasForStationJacobian(si, MobilizedBodyIndex(3), eefPoint);

    // Acceleration errors in form of Px + k
    // P = J_eff*M^-1*D*Fm
    SimTK::Matrix P = J_eef * InverseMassMatrix(si,osimModel) * selectionMatrixForProportionalMuscles(si,osimModel);

    // Construct vector k
    Vec3 k3 = J_effMInvf + J_effDotu - desInput;
    SimTK::Vector k = vec3ToVector(k3);


    // Calculate H combined with regulation terms
    H_ = ~P * P;
    /* cout << "--> H = " << H_ << endl; */
    H_ = H_ + simpleRegulationMatrix(osimModel);
    /* cout << "--> new H = " << H_ << endl; */

    // Vector g
    g_ = ~P * k;
}

/******************* Matrix A = [Ad Anuc Awcf]^T *********************/
void A_qpoases(Model& osimModel, State& si, SimTK::Matrix& A_qp) {
    const SimbodyMatterSubsystem& matter = osimModel.getMatterSubsystem();
    int na = osimModel.getNumControls();
    int nu = si.getNU();
    // Frame Jacobian of all contact points (containing both linear and rotational parts)
    SimTK::Matrix Jc;
    if (separateContactPoint) {
        matter.calcFrameJacobian(si, bodyCt(), originCtPtFrm(), Jc);
    }
    else {
        matter.calcFrameJacobian(si,MobilizedBodyIndex(1),Vec3(0),Jc);
    }
    if (printJc) {cout << "-- Jc = " << Jc << endl;}
    A_qp = 0.0;

    // Anuc - linear velocity null - multiply with generalized accelerations
    /* A(0,0,3*nc,nu) = ContactTranslationalJacobian(si,osimModel); */
    /* cout << "------- A(0,0,3*nc,nu) = " << A(0,0,3*nc,nu) << endl; */

    // Ad
    A_qp(0,0,nu,nu) = -1 * MassMatrix(si,osimModel);
    A_qp(0,nu,nu,na) = selectionMatrixForProportionalMuscles(si,osimModel);
    A_qp(0,nu+na,nu,6*nc) = ~Jc;

    // Awcf - pending for friction cone

    if (printDbgQPForm) {cout << "-- A = " << A_qp << endl;}
}


/************************** ubA - lbA  ******************************/
void ubA_lbA(Model& osimModel, State& si, SimTK::Vector& ubA, SimTK::Vector& lbA) {
    const SimbodyMatterSubsystem& matter = osimModel.getMatterSubsystem();
    int na = osimModel.getNumControls();
    int nu = si.getNU();
    // ubA_nuc
    /* ubA(0,3*nc) = JduContact(si, osimModel); */

    // ubA_d (equality constraint) 
    /** Gravtiy and Coriolis forces are on the rhs of the dynamic equation
      * They have the opposite sign of the actuator forces
    **/
    SimTK::Vector gr;
    matter.multiplyBySystemJacobianTranspose(si, osimModel.getGravityForce().getBodyForces(si), gr);
    SimTK::Vector cor;
    matter.calcResidualForceIgnoringConstraints(si, SimTK::Vector(0), SimTK::Vector_<SimTK::SpatialVec>(0), SimTK::Vector(0),cor);
    ubA(0,nu) = cor - gr;
    /* ubA(0,nu) = cor - gr + GMult; */

    // ubA_wcf - pending for friction cone
    if (printDbgQPForm) {cout << "-- ubA = " << ubA << endl;}

    // lbA_nuc; lbA_d
    lbA = ubA;
    // lbA_wcf - pending for friction cone
    if (printDbgQPForm) {cout << "-- lbA = " << lbA << endl;}
}

/************************** ub - lb *******************************/
void ub_lb(Model& osimModel, State& si, SimTK::Vector& ub, SimTK::Vector& lb) {
    int na = osimModel.getNumControls();
    int nu = si.getNU();
    // ub_q
    ub(0,6) = 1e9;
    ub(6,nu-6) = 2/h/h*(q_max(osimModel) - q_value(osimModel,si) - h * nu_value(osimModel,si));
    // ub_a
    ub(nu,na) = 1.0;
    // ub_wc (arbitrary great value)
    ub(nu+na,6*nc) = 1e2;
    if (printDbgQPForm) {cout << "-- ub = " << ub << endl;}

    // lb_q
    lb(0,6) = -1e9; 
    lb(6,nu-6) = 2/h/h*(q_min(osimModel) - q_value(osimModel,si) - h * nu_value(osimModel,si));
    // lb_a
    lb(nu,na) = 0.0;
    // lb_wc
    lb(nu+na,6*nc) = -1e2;
    // mx, mz, ty >= 0
    lb(nu+na) = 0.0;
    lb(nu+na+2) = 0.0;
    lb(nu+na+4) = 0.0;
    if (separateContactPoint) {
        lb(nu+na+10) = 0.0;
        lb(nu+na+16) = 0.0;
        lb(nu+na+22) = 0.0;
    }
    if (printDbgQPForm) {cout << "-- lb = " << lb << endl;}
}

/** QP formulation with new set of optimization variables [nu,control,omega_c]
**/
void QPformulation(State& si, Model& osimModel, Vec3& desInput, SimTK::Matrix& H, SimTK::Vector& g) {
    const SimbodyMatterSubsystem& matter = osimModel.getMatterSubsystem();
    const int nb = matter.getNumBodies();
    int na = osimModel.getNumControls();
    int nu = si.getNU();

    SimTK::Matrix M_;
    matter.calcM(si,M_);
    /* cout << " --> mass matrix : " << M_ << endl; */

    // Jacobian at the end-effector - Thigh/Body Index 3
    SimTK::Matrix J_eef;
    matter.calcStationJacobian(si,MobilizedBodyIndex(3),eefPoint,J_eef);

    // Frame Jacobian of all contact points (containing both linear and rotational parts)
    SimTK::Matrix Jc;
    if (separateContactPoint) {
        matter.calcFrameJacobian(si, bodyCt(), originCtPtFrm(), Jc);
    }
    else {
        matter.calcFrameJacobian(si,MobilizedBodyIndex(1),Vec3(0),Jc);
    }
    if (printJc) {cout << "-- Jc = " << Jc << endl;}

    /** Gravtiy and Coriolis forces are on the rhs of the dynamic equation
      * They have the opposite sign of the actuator forces
    **/
    SimTK::Vector gr;
    matter.multiplyBySystemJacobianTranspose(si, osimModel.getGravityForce().getBodyForces(si), gr);
    SimTK::Vector cor;
    matter.calcResidualForceIgnoringConstraints(si, SimTK::Vector(0), SimTK::Vector_<SimTK::SpatialVec>(0), SimTK::Vector(0),cor);

    // acceleration constraint force
    /* SimTK::Vector GMult; */
    /* matter.multiplyByGTranspose(si,matter.getConstraintMultipliers(si),GMult); */

    // gravity - coriolis - ~G.mult
    /* SimTK::Vector _f =  gr - cor - GMult; */
    SimTK::Vector _f =  gr - cor;

    // M^-1*(gravity - coriolis - ~G.mult)
    SimTK::Vector MInvf;
    matter.multiplyByMInv(si, _f, MInvf);

    // J_eff*M^-1*(gravity - coriolis - ~G.mult)
    Vec3 J_effMInvf = matter.multiplyByStationJacobian(si, MobilizedBodyIndex(3), eefPoint, MInvf);

    // d(J_eff)*u
    Vec3 J_effDotu = matter.calcBiasForStationJacobian(si, MobilizedBodyIndex(3), eefPoint);


    // Acceleration errors in form of Px + k
    // P = [0 J_eff*M^-1*D*Fm J_eff*M^-1*~Jc]
    SimTK::Matrix P(3, nu+na+6*nc);
    P(0,0,3,nu) = 0;
    P(0,nu,3,na) = J_eef * InverseMassMatrix(si,osimModel) * selectionMatrixForProportionalMuscles(si,osimModel);
    P(0,nu+na,3,6*nc) = J_eef * InverseMassMatrix(si,osimModel) * ~Jc;

    // Construct vector k
    Vec3 k3 = J_effMInvf + J_effDotu - desInput;
    SimTK::Vector k = vec3ToVector(k3);

    // Matrix H
    H = ~P * P;
    /* cout << "--> H = " << H << endl; */
    H = H + regulationMatrix(osimModel,si);
    /* cout << "--> new H = " << H << endl; */

    // Vector g
    g = ~P * k;

    if (printDbgQPForm) { cout << "-- full_formulation" << endl;}
}


//----------------------------------- QPOASES CLASSES ------------------------------------------

class SimpleOneLegqpOASESOpt{
    private:
        State& si;
        Model& osimModel;
        Vec3& desAcc;
    public:
        SimpleOneLegqpOASESOpt(State& s, Model& aModel, Vec3& adesAcc): 
            si(s), osimModel(aModel), desAcc(adesAcc) { }

        // Task: Move to a point
        SimTK::Vector optimize(){
            int na = osimModel.getNumControls();
            int nbTotalOptVar = na;

            SimTK::Matrix H_;
            SimTK::Vector g_;

            simpleQPformulation(si, osimModel, desAcc, H_, g_);

            if (printQPmatrices) {
                cout << "-- OpenSim: H_ = " << H_ << endl;
                cout << "-- OpenSim: g_ = " << g_ << endl;
            } 

            // Convert OpenSim matrices to qpOASES matrices
            real_t H_qp[nbTotalOptVar*nbTotalOptVar];
            real_t g_qp[nbTotalOptVar];
            real_t lb_qp[nbTotalOptVar];
            real_t ub_qp[nbTotalOptVar];

            for (int i = 0; i < nbTotalOptVar; i++) {
                for (int j = 0; j < nbTotalOptVar; j++)
                    H_qp[i*nbTotalOptVar+j] = H_(i,j);
                g_qp[i] = g_(i);
                lb_qp[i] = 0;
                ub_qp[i] = 1;
            }

            //-----------------------------------------------

            if (printMatrixQPOASES) {
              printf ("-- qpOASES H: ");
              for (int ii = 0; ii < nbTotalOptVar; ++ii)
              {
                for (int jj = 0; jj < nbTotalOptVar; ++jj )
                  printf ("%8.2e ", H_qp[ii*nbTotalOptVar+jj]);
                printf("\n");
              }

              printf("\n");
              printf("---------------------------------");
              printf("\n");

              printf ("-- qpOASES g: ");
              for (int jj = 0; jj < nbTotalOptVar; ++jj )
                printf ("%8.2e ", g_qp[jj]);
              printf("\n");

              printf("\n");
              printf("---------------------------------");
              printf("\n");

              printf ("-- qpOASES ub: ");
              for (int jj = 0; jj < nbTotalOptVar; ++jj )
                printf ("%8.2e ", ub_qp[jj]);
              printf("\n");

              printf("\n");
              printf("---------------------------------");
              printf("\n");

              printf ("-- qpOASES lb: ");
              for (int jj = 0; jj < nbTotalOptVar; ++jj )
                printf ("%8.2e ", lb_qp[jj]);
              printf("\n");

              printf("\n");
              printf("---------------------------------");
              printf("\n");
            }

            printf("\n");
            printf("---------------------------------");
            printf("\n");

            //-----------------------------------------------

            /* Setting up QProblem object. */
            QProblemB onelegOASES(nbTotalOptVar);

            Options options;
            options.printLevel = PrintLevel(0);
            onelegOASES.setOptions(options);

            /* Solve first QP. */
            real_t cputime = 1.0;
            int_t nWSR = 100;
            onelegOASES.init(H_qp, g_qp, lb_qp, ub_qp, nWSR, &cputime);

            /* Get and print solution of first QP. */
            real_t xOpt[nbTotalOptVar];
            onelegOASES.getPrimalSolution(xOpt);

            if (printqpOASESsolution) {
              printf ("-- qpOASES xOpt: ");
              for (int jj = 0; jj < nbTotalOptVar; ++jj )
                printf ("%8.2e ", xOpt[jj]);
              printf("\n");
            }

            // Retrive actuator controls
            SimTK::Vector controlOpt(na);
            for (int i = 0; i < na; i++) {
                controlOpt(i) = xOpt[i];
            }
            return controlOpt;
        }
        ~SimpleOneLegqpOASESOpt() {}
};


/** QPOASES optimization class with new set of optimization variables [nu,control,omega_c]
**/
class OneLegqpOASESOpt{

    private:
        State& si;
        Model& osimModel;
        Vec3& desAcc;
    public:

        OneLegqpOASESOpt(State& s, Model& aModel, Vec3& adesAcc): 
            si(s), osimModel(aModel), desAcc(adesAcc) { }

        // Task: Move to a point
        SimTK::Vector optimize(){
            int na = osimModel.getNumControls();
            int nu = si.getNU();

            SimTK::Matrix H_;
            SimTK::Vector g_;

            QPformulation(si, osimModel, desAcc, H_, g_);

            // New QP problem with variable [nu,tau,omegac_c]
            int nbRwA = nu;
            int nbTotalOptVar = nu+na+6*nc;
            
            // A, lbA, ubA, lb, ub need to specify dimensions
            SimTK::Matrix A_(nbRwA, nbTotalOptVar);
            SimTK::Vector ubA_(nbRwA);
            SimTK::Vector lbA_(nbRwA);

            A_qpoases(osimModel, si, A_);
            ubA_lbA(osimModel, si, ubA_, lbA_);

            SimTK::Vector ub_(nbTotalOptVar);
            SimTK::Vector lb_(nbTotalOptVar);
            ub_lb(osimModel, si, ub_, lb_);

            if (printQPmatrices) {
                cout << "-- QPOASES: A_ = " << A_ << endl;
                cout << "-- OpenSim: H_ = " << H_ << endl;
                cout << "-- OpenSim: g_ = " << g_ << endl;
                cout << "-- OpenSim: ubA_ = " << ubA_ << endl;
                cout << "-- OpenSim: lbA_ = " << lbA_ << endl;
                cout << "-- OpenSim: ub_ = " << ub_ << endl;
                cout << "-- OpenSim: lb_ = " << lb_ << endl;
            } 

            // Convert OpenSim matrices to qpOASES matrices
            real_t H_qp[nbTotalOptVar*nbTotalOptVar];
            real_t g_qp[nbTotalOptVar];
            real_t lb_qp[nbTotalOptVar];
            real_t ub_qp[nbTotalOptVar];

            real_t A_qp[nbRwA*nbTotalOptVar];
            real_t lbA_qp[nbRwA];
            real_t ubA_qp[nbRwA];

            for (int i = 0; i < nbTotalOptVar; i++) {
                for (int j = 0; j < nbTotalOptVar; j++)
                    H_qp[i*nbTotalOptVar+j] = H_(i,j);
                g_qp[i] = g_(i);
                lb_qp[i] = lb_(i);
                ub_qp[i] = ub_(i);
            }

            for (int i = 0; i < nbRwA; i++) {
                for (int j = 0; j < nbTotalOptVar; j++)
                    A_qp[i*nbRwA+j] = A_(i,j);
                lbA_qp[i] = lbA_(i);
                ubA_qp[i] = ubA_(i);
            }
            //-----------------------------------------------

            if (printMatrixQPOASES) {
              printf ("-- qpOASES H: ");
              for (int ii = 0; ii < nbTotalOptVar; ++ii)
              {
                for (int jj = 0; jj < nbTotalOptVar; ++jj )
                  printf ("%8.2e ", H_qp[ii*nbTotalOptVar+jj]);
                printf("\n");
              }

              printf("\n");
              printf("---------------------------------");
              printf("\n");

              printf ("-- qpOASES g: ");
              for (int jj = 0; jj < nbTotalOptVar; ++jj )
                printf ("%8.2e ", g_qp[jj]);
              printf("\n");

              printf("\n");
              printf("---------------------------------");
              printf("\n");

              printf ("-- qpOASES ub: ");
              for (int jj = 0; jj < nbTotalOptVar; ++jj )
                printf ("%8.2e ", ub_qp[jj]);
              printf("\n");

              printf("\n");
              printf("---------------------------------");
              printf("\n");

              printf ("-- qpOASES lb: ");
              for (int jj = 0; jj < nbTotalOptVar; ++jj )
                printf ("%8.2e ", lb_qp[jj]);
              printf("\n");

              printf("\n");
              printf("---------------------------------");
              printf("\n");

              /* printf ("-- qpOASES A: "); */
              /* for (int ii = 0; ii < nbRwA; ++ii) */
              /* { */
              /*   for (int jj = 0; jj < nbTotalOptVar; ++jj ) */
              /*     printf ("%8.2e ", A_qp[ii*nbRwA+jj]); */
              /*   printf("\n"); */
              /* } */

              /* printf("\n"); */
              /* printf("---------------------------------"); */
              /* printf("\n"); */

              /* printf ("-- qpOASES ub_A: "); */
              /* for (int jj = 0; jj < nbRwA; ++jj ) */
              /*   printf ("%8.2e ", ubA_qp[jj]); */
              /* printf("\n"); */

              /* printf("\n"); */
              /* printf("---------------------------------"); */
              /* printf("\n"); */

              /* printf ("-- qpOASES lb_A: "); */
              /* for (int jj = 0; jj < nbRwA; ++jj ) */
              /*   printf ("%8.2e ", lbA_qp[jj]); */
              /* printf("\n"); */
            }

            printf("\n");
            printf("---------------------------------");
            printf("\n");

            //-----------------------------------------------

            /* Setting up QProblem object. */
            QProblem twolinksfoot(nbTotalOptVar,nbRwA);
            /* QProblemB twolinksfoot(nbTotalOptVar); */

            Options options;
            options.printLevel = PrintLevel(0);
            /* options.printLevel = PrintLevel(PL_DEBUG_ITER); */
            twolinksfoot.setOptions(options);

            /* Solve first QP. */
            real_t cputime = 1.0;
            int_t nWSR = 100;
            twolinksfoot.init(H_qp,g_qp,A_qp,lb_qp,ub_qp,lbA_qp,ubA_qp,nWSR,&cputime);
            /* twolinksfoot.init(H_qp,g_qp,lb_qp,ub_qp,nWSR,&cputime); */

            /* Get and print solution of first QP. */
            real_t xOpt[nbTotalOptVar];
            twolinksfoot.getPrimalSolution(xOpt);

            if (printqpOASESsolution) {
              printf ("-- qpOASES xOpt: ");
              for (int jj = 0; jj < nbTotalOptVar; ++jj )
                printf ("%8.2e ", xOpt[jj]);
              printf("\n");
            }

            // Retrive actuator controls - starting from xOpt[nu]
            SimTK::Vector controlOpt(na);
            for (int i = 0; i < na; i++) {
                controlOpt(i) = xOpt[i+nu];
            }
            return controlOpt;
        }
        ~OneLegqpOASESOpt() {}
};


//-------------------------------- OPENSIM BUILT IN OPTIMIZATION -------------------------------

class OpensimOptimizationSystem : public OptimizerSystem {

    private:
        int numControls;
        State& si;
        Model& osimModel;
        Vec3& desInput;

    public:
        OpensimOptimizationSystem(int numParameters, State& s, Model& aModel, Vec3& desAcc): 
            numControls(numParameters), OptimizerSystem(numParameters), 
            si(s), osimModel(aModel), desInput(desAcc) { }

        int objectiveFunc(const SimTK::Vector &newControls, bool new_coefficients, Real& f) const {

            if (setCostFuncFormulationType) 
            {
                // make a copy of out initial states
                State s = si;

                // Update the control values
                //newControls.dump("New Controls In:");
                osimModel.updDefaultControls() = newControls;

                // Create the integrator for the simulation.
                RungeKuttaMersonIntegrator integrator(osimModel.getMultibodySystem());
                integrator.setAccuracy(1.0e-4);
                integrator.setAllowInterpolation(false);

                // Integration without manager
                /* integrator.initialize(si); */
                /* integrator.stepBy(stepSize); */

                // Create a manager to run the simulation
                Manager manager(osimModel, integrator);
                manager.setInitialTime(initialStepTime);
                manager.setFinalTime(finalStepTime);
                manager.integrate(s);

                //osimModel.getControls(s).dump("Model Controls:");
                osimModel.getMultibodySystem().realize(s, Stage::Acceleration);

                Vec3 curAcc;
                // Don't forget state s for god sake !!!
                osimModel.getSimbodyEngine().getAcceleration
                    (s, osimModel.getBodySet().get("linkage2"), eefPoint, curAcc);

                Vec3 err;
                err = desInput - curAcc;

                // Sum of forces produced by actuators
                for (int actNb = 0; actNb < osimModel.getNumControls(); actNb ++)
                {
                    f += osimModel.getActuators().get(actNb).getControl(s) * 
                        osimModel.getActuators().get(actNb).getOptimalForce();
                }
            }
            else
            {
                int na = osimModel.getNumControls();
                int nu = si.getNU();
                int nbRwA = nu;
                int nbTotalOptVar = nu+na+6*nc;

                SimTK::Matrix H_;
                SimTK::Vector g_;

                QPformulation(si, osimModel, desInput, H_, g_);

                // SimTK bullshit bug: vector*matrix*vector can't give an double/int
                SimTK::Vector stupidVector = ~newControls * H_ * newControls;
                f = 1/2 * stupidVector(0) + ~newControls * g_ ;
            }
            return(0);
        }

};

//==============================================================================================
// 				                                MAIN
//==============================================================================================


int main()
{
    try {
        std::clock_t startTime = std::clock();

        //------------------------------------- LOAD MODEL -------------------------------------

        Model osimModel("OneLeg1.osim");
        osimModel.setUseVisualizer(useVisualizer);

        //----------------------------------- INITIALIZATION -----------------------------------

        SimTK::State& si = osimModel.initSystem();
        if (verifyInitSys){ cout << "-- Initializing system - done" << endl;}

        //--------------------------------- REFERENCE TRAJECTORY -------------------------------

        /* Choose a type of reference
         * default : trajectory from data files
         * 1 : destination point at the origin
         * 2 : destination point at the top of the linkage 2 when the system is straight up vertically
         **/ 

        int trajVecLength;
        if (testCase == 0) { trajVecLength = calcTrajVecLength(); }
        else { trajVecLength = TrajLength; }
        Vector_<Vec3> refPos(trajVecLength);
        Vector_<Vec3> refVel(trajVecLength);
        Vector_<Vec3> refAcc(trajVecLength);

        switch (testCase) {
            default:
                generateTrajectry(refPos, refVel, refAcc);
                break;
            case 1:
                for (int count = 0; count < trajVecLength; ++count) {
                    refPos[count] = Vec3(0); 
                    refVel[count] = Vec3(0);
                    refAcc[count] = Vec3(0); 
                }
                break;
            case 2:
                for (int count = 0; count < trajVecLength; ++count) {
                    refPos[count] = Vec3(-0.01,0.800,0); 
                    refVel[count] = Vec3(0);
                    refAcc[count] = Vec3(0); 
                }
                break;
            case 3:
                for (int count = 0; count < trajVecLength; ++count) {
                    refPos[count] = Vec3(-0.05,0.905,0); 
                    refVel[count] = Vec3(0);
                    refAcc[count] = Vec3(0); 
                }
                break;
        }

        if (printRefTrajectory)
        {
            cout << "-- Print trajectories from data files" << endl;
            for (int count = 0; count < trajVecLength; ++count) {
                cout << "refPos " << count << " " << refPos[count] << endl;
                cout << "refVel " << count << " " << refVel[count] << endl;
                cout << "refAcc " << count << " " << refAcc[count] << endl;
            }
        }

        //------------------------------- INITIAL ANGLE POSITIONS ------------------------------

        double anklePos_init;
        double ankleVel_init;
        double kneePos_init;
        double kneeVel_init;

        switch (setInitialState) {
            default:
                anklePos_init = -20*SimTK::Pi/180;
                ankleVel_init = 0;
                kneePos_init = -30*SimTK::Pi/180;
                kneeVel_init = 0;
                break;
            case 1:
                anklePos_init = 0;
                ankleVel_init = 0;
                kneePos_init = 0;
                kneeVel_init = 0;
                break;
            case 2:
                anklePos_init = 0;
                ankleVel_init = 0;
                kneePos_init = 0.1;
                kneeVel_init = 0;
                break;
            case 3:
                anklePos_init = 0;
                ankleVel_init = 0;
                kneePos_init = 0;
                kneeVel_init = 0;
                break;
            case 4:
                anklePos_init = 0;
                ankleVel_init = 0;
                kneePos_init = 0.3;
                kneeVel_init = 0;
                break;
        }

        osimModel.getCoordinateSet().get("ankle_rotZ").setValue(si, anklePos_init);
        osimModel.getCoordinateSet().get("knee_rotZ").setValue(si, kneePos_init);
        osimModel.getCoordinateSet().get("ankle_rotZ").setSpeedValue(si, ankleVel_init);
        osimModel.getCoordinateSet().get("knee_rotZ").setSpeedValue(si, kneeVel_init);

        cout << "--nbU = " << si.getNU() << endl;
        cout << "--nbQ = " << si.getNQ() << endl;
        cout << "--nbY = " << si.getNY() << endl;
        cout << "-- actuator set = " << osimModel.getActuators() << endl;
        cout << "-- coordinate set = " << osimModel.getCoordinateSet() << endl;
        cout << "-- joint set = " << osimModel.getJointSet() << endl;
        cout << "-- body set = " << osimModel.getBodySet() << endl;
        cout << "-- class object = " << (osimModel.getActuators().get(2).getConcreteClassName() == "PathActuator") << endl;
        cout << "-- force set = " << osimModel.getForceSet() << endl;

        //------------------------------------ ACTUATORS ---------------------------------------

        int numControls = osimModel.getNumControls();
        cout << "numControls = " << numControls << endl;

        // Get control bound set and initialize controls
        int bound_size = numControls;
        /* int bound_size = numControls + 6*nc + si.getNU(); */
        SimTK::Vector lower_bounds(bound_size);
        SimTK::Vector upper_bounds(bound_size);
        int na = osimModel.getNumControls();
        int nu = si.getNU();

        /*// ub_q*/
        /*upper_bounds(0,6) = 1e9;*/
        /*upper_bounds(6,nu-6) = 2/h/h*(q_max(osimModel) - q_value(osimModel,si) - h * nu_value(osimModel,si));*/
        /*// ub_a*/
        /*upper_bounds(nu,na) = 1.0;*/
        /*// ub_wc (arbitrary great value)*/
        /*upper_bounds(nu+na,6*nc) = 1e2;*/

        /*// lb_q*/
        /*lower_bounds(0,6) = -1e9;*/ 
        /*lower_bounds(6,nu-6) = 2/h/h*(q_min(osimModel) - q_value(osimModel,si) - h * nu_value(osimModel,si));*/
        /*// lb_a*/
        /*lower_bounds(nu,na) = 0.0;*/
        /*// lb_wc*/
        /*lower_bounds(nu+na,6*nc) = -1e2;*/
        /*// only Y componant of contact forces*/
        /*lower_bounds(nu+na+4) = 0.0;*/
        /*if (separateContactPoint) {*/
        /*    lower_bounds(nu+na+10) = 0.0;*/
        /*    lower_bounds(nu+na+16) = 0.0;*/
        /*    lower_bounds(nu+na+22) = 0.0;*/
        /*}*/

        lower_bounds = 0;
        upper_bounds = 1; 

        cout << "--------------------------------------------pass?" << endl;

        // Set initial controls
        SimTK::Vector initialControls(numControls);

        for (int i = 0; i < numControls; i++) {
            lower_bounds(i) = osimModel.getActuators().get(i).getMinControl();
            upper_bounds(i) = osimModel.getActuators().get(i).getMaxControl();
        }

        switch (setInitCtrl) {
          case 1:
            initialControls(0) = 0;
            initialControls(1) = 0.063;
            initialControls(2) = 0;
            initialControls(3) = 0.063;
            break;
          case 2:
            initialControls(0) = 0;
            initialControls(1) = 0;
            initialControls(2) = 0;
            initialControls(3) = 0;
            break;
          case 3:
            initialControls(0) = 0.3;
            initialControls(1) = 0.3;
            initialControls(2) = 0.3;
            initialControls(3) = 0.3;
            break;
        }
        osimModel.updDefaultControls() = initialControls;

        if (printInitialCtrl)
        { cout << "Initial controls = " << osimModel.getDefaultControls() << endl; }

        //------------------------------------ VISUALIZER --------------------------------------
        if (useVisualizer) {
            osimModel.updMatterSubsystem().setShowDefaultGeometry(true);
            Visualizer& viz = osimModel.updVisualizer().updSimbodyVisualizer();
            viz.setBackgroundType(viz.SolidColor);
            viz.setBackgroundColor(Cyan);
        }

        // Angular state variables
        Vec3 eefPos;
        Vec3 eefVel;
        osimModel.getMultibodySystem().realize(si, Stage::Acceleration);
        osimModel.getSimbodyEngine().getPosition
            (si, osimModel.getBodySet().get("linkage2"), eefPoint, eefPos);
        osimModel.getSimbodyEngine().getVelocity
            (si, osimModel.getBodySet().get("linkage2"), eefPoint, eefVel);

        if (printInitialState) {
            cout << "Initial end-effector position  = " << eefPos << endl;
            cout << "Initial end-effector velocity  = " << eefVel << endl;
        }


        //------------------------------------- LOG FILES --------------------------------------
        // Construct control storage
        Array<string> columnLabels;
        Storage* _controlStore = new Storage(1023,"controls");
        columnLabels.append("time");
        for(int i=0;i<osimModel.getActuators().getSize();i++)
            columnLabels.append(osimModel.getActuators().get(i).getName());
        _controlStore->setColumnLabels(columnLabels);

        // Construct force storage
        Storage* _forceStore = new Storage(1023,"forces");
        _forceStore->setColumnLabels(columnLabels);

        // Initialize storage files
        SimTK::Vector updctrls(osimModel.getActuators().getSize());
        SimTK::Vector updforces(osimModel.getActuators().getSize());
        osimModel.getMultibodySystem().realize(si, Stage::Acceleration);

        for (int i = 0; i < osimModel.getActuators().getSize(); i++) {
            updctrls[i] = osimModel.getActuators().get(i).getControl(si);
            updforces[i] = updctrls[i] * osimModel.getActuators().get(i).getOptimalForce();
        } 
        /* cout << updctrls << endl; */

        _controlStore->store(stepNum, 0.0,osimModel.getActuators().getSize(), &updctrls[0]);
        _controlStore->print("controls.sto");
        _forceStore->store(stepNum, 0.0,osimModel.getActuators().getSize(), &updforces[0]);
        _forceStore->print("forces.sto");

        // Construct Cartesian state storage of the end-effector
        Array<string> posColumnLabels;
        Storage* _posCartesianStore = new Storage(1023,"PosCartesian");
        posColumnLabels.append("time");
        posColumnLabels.append("pX");
        posColumnLabels.append("pY");
        posColumnLabels.append("pZ");
        _posCartesianStore->setColumnLabels(posColumnLabels);

        Array<string> velColumnLabels;
        Storage* _velCartesianStore = new Storage(1023,"VelCartesian");
        velColumnLabels.append("time");
        velColumnLabels.append("vX");
        velColumnLabels.append("vY");
        velColumnLabels.append("vZ");
        _velCartesianStore->setColumnLabels(velColumnLabels);

        Array<string> accColumnLabels;
        Storage* _accCartesianStore = new Storage(1023,"AccCartesian");
        accColumnLabels.append("time");
        accColumnLabels.append("aX");
        accColumnLabels.append("aY");
        accColumnLabels.append("aZ");
        _accCartesianStore->setColumnLabels(accColumnLabels);

        Array<string> refPosColumnLabels;
        Storage* _refPosCartesianStore = new Storage(1023,"refstatePos");
        refPosColumnLabels.append("time");
        refPosColumnLabels.append("pXr");
        refPosColumnLabels.append("pYr");
        refPosColumnLabels.append("pZr");
        _refPosCartesianStore->setColumnLabels(refPosColumnLabels);

        Array<string> refVelColumnLabels;
        Storage* _refVelCartesianStore = new Storage(1023,"refstateVel");
        refVelColumnLabels.append("time");
        refVelColumnLabels.append("vXr");
        refVelColumnLabels.append("vYr");
        refVelColumnLabels.append("vZr");
        _refVelCartesianStore->setColumnLabels(refVelColumnLabels);

        Array<string> refAccColumnLabels;
        Storage* _refAccCartesianStore = new Storage(1023,"refstateAcc");
        refAccColumnLabels.append("time");
        refAccColumnLabels.append("aXr");
        refAccColumnLabels.append("aYr");
        refAccColumnLabels.append("aZr");
        _refAccCartesianStore->setColumnLabels(refAccColumnLabels);

        // Initialize state storage
        Vec3 updPosCartesian(0);
        Vec3 updVelCartesian(0);
        Vec3 updAccCartesian(0);
        osimModel.getSimbodyEngine().getPosition
            (si, osimModel.getBodySet().get("linkage2"), eefPoint, updPosCartesian);
        osimModel.getSimbodyEngine().getVelocity
            (si, osimModel.getBodySet().get("linkage2"), eefPoint, updVelCartesian);
        osimModel.getSimbodyEngine().getAcceleration
            (si, osimModel.getBodySet().get("linkage2"), eefPoint, updAccCartesian);

        _posCartesianStore->store(stepNum, 0.0, 3, &updPosCartesian[0]);
        _posCartesianStore->print("posCartesian.sto");
        _velCartesianStore->store(stepNum, 0.0, 3, &updVelCartesian[0]);
        _velCartesianStore->print("velCartesian.sto");
        _accCartesianStore->store(stepNum, 0.0, 3, &updAccCartesian[0]);
        _accCartesianStore->print("accCartesian.sto");

        _refPosCartesianStore->store(stepNum, 0.0, 3, &refPos[0].get(0));
        _refPosCartesianStore->print("refPosCartesian.sto");
        _refVelCartesianStore->store(stepNum, 0.0, 3, &refVel[0].get(0));
        _refVelCartesianStore->print("refVelCartesian.sto");
        _refAccCartesianStore->store(stepNum, 0.0, 3, &refAcc[0].get(0));
        _refAccCartesianStore->print("refAccCartesian.sto");

        // Moment arm storage
        Array<string> momentArms;
        Storage* _momentArmsStore = new Storage(1023,"momentArms");
        momentArms.append("time");
        momentArms.append("leftAnkleMuscle");
        momentArms.append("rightAnkleMuscle");
        momentArms.append("leftKneeMuscle");
        momentArms.append("rightKneeMuscle");
        _momentArmsStore->setColumnLabels(momentArms);

        // Initialize moment arm storage
        SimTK::Vector updMomentArms = momentArmVec(si, osimModel);
        int nbMomentArms = numControls;
        _momentArmsStore->store(stepNum, 0.0, nbMomentArms, &updMomentArms(0));
        _momentArmsStore->print("momentArms.sto");

        // Torque storage
        Array<string> torques;
        Storage* _torqueStore = new Storage(1023,"torques");
        torques.append("time");
        torques.append("ankle");
        torques.append("knee");
        _torqueStore->setColumnLabels(torques);

        // Initialize torque storage
        SimTK::Vector updTorques(numControls/2);
        updTorques(0) = updctrls(0) * updMomentArms(0) + updctrls(1) * updMomentArms(1);
        updTorques(1) = updctrls(2) * updMomentArms(2) + updctrls(3) * updMomentArms(3);
        _torqueStore->store(stepNum, 0.0, numControls/2, &updTorques(0));
        _torqueStore->print("torques.sto");

        // Gravity storage
        Array<string> gravity;
        Storage* _gravityStore = new Storage(1023,"gravity");
        gravity.append("time");
        gravity.append("ankle");
        gravity.append("knee");
        _gravityStore->setColumnLabels(gravity);

        const SimbodyMatterSubsystem& matter = osimModel.getMatterSubsystem();
        SimTK::Vector gr;
        matter.multiplyBySystemJacobianTranspose(
            si, osimModel.getGravityForce().getBodyForces(si), gr);
        _gravityStore->store(stepNum, 0.0, numControls/2, &gr(6));
        _gravityStore->print("gravity.sto");

        /* // Coriolis storage */
        /* Array<string> coriolis; */
        /* Storage* _coriolisStore = new Storage(1023,"coriolis"); */
        /* coriolis.append("time"); */
        /* coriolis.append("ankle"); */
        /* coriolis.append("knee"); */
        /* _coriolisStore->setColumnLabels(coriolis); */

        /* SimTK::Vector cor; */
        /* matter.calcResidualForceIgnoringConstraints( */
        /*     si, SimTK::Vector(0), SimTK::Vector_<SimTK::SpatialVec>(0), */
        /*     SimTK::Vector(0),cor); */

        /* _coriolisStore->store(stepNum, 0.0, numControls/2, &cor(6)); */
        /* _coriolisStore->print("coriolis.sto"); */

        // Create the force reporter
        ForceReporter* reporter = new ForceReporter(&osimModel);
        osimModel.addAnalysis(reporter);

        //-------------------------------- INTEGRATOR MANAGER ----------------------------------
        SimTK::RungeKuttaMersonIntegrator integ(osimModel.getMultibodySystem());
        integ.setAccuracy(1.0e-4);
        Manager manager(osimModel, integ);

        //------------------------------------ SIMULATION --------------------------------------
        if (simulation) {
            // start timing
            clock_t startTime = clock();

            for (int count = 0; count < trajVecLength; ++count) {

                //--------------------------- TASK SERVOING ------------------------------------

                // Compute desired trajectory from PD controller
                Vec3 desAcc = Kp * (refPos[count] - eefPos) 
                    + Kv * (refVel[count] - eefVel) 
                    + refAcc[count];

                if (printDesAcc)
                    cout << "desAcc = " << desAcc <<endl;

                /* cout << "Message: End task servoing" << endl; */

                //---------------------------- OPTIMIZATION ------------------------------------

                SimTK::Vector newControls(osimModel.getActuators().getSize());
                if (setqpOASES) {
                    /* SimpleOneLegqpOASESOpt sys(si, osimModel, desAcc); */
                    OneLegqpOASESOpt sys(si, osimModel, desAcc);
                    newControls = sys.optimize();
                }
                else {
                    OptimizerAlgorithm optimizerAlg;
                    switch (setOptimizerAlgorithm) {
                        case 1:
                            optimizerAlg = InteriorPoint;
                            break;
                        case 2:
                            optimizerAlg = LBFGS;
                            break; 
                        case 3:
                            optimizerAlg = LBFGSB;
                            break;
                        case 4:
                            optimizerAlg = CFSQP;
                            break;
                        case 5:
                            optimizerAlg = CMAES;
                            break;
                    }
                    OpensimOptimizationSystem sys(bound_size, si, osimModel, desAcc);
                    sys.setParameterLimits(lower_bounds, upper_bounds);

                    Optimizer opt(sys, optimizerAlg);

                    // Specify settings for the optimizer
                    opt.setConvergenceTolerance(0.001);
                    opt.useNumericalGradient(true);
                    opt.setMaxIterations(200);
                    opt.setLimitedMemoryHistory(500);

                    Real f;	
                    f = opt.optimize(newControls);
                    /* cout <<  "f " << count << " = " << f << endl; */
                }
                osimModel.updDefaultControls() = newControls;

                if (printNewCtrls)
                    cout << "newControls " << count << " = " << newControls << endl;


                //-------------------------- ADVANCE TO NEXT STEP ------------------------------

                manager.setInitialTime(initialStepTime);
                manager.setFinalTime(finalStepTime);
                manager.integrate(si);

                // Update current state
                osimModel.getMultibodySystem().realize(si, Stage::Acceleration);
                osimModel.getSimbodyEngine().getPosition
                    (si, osimModel.getBodySet().get("linkage2"), eefPoint, eefPos);
                osimModel.getSimbodyEngine().getVelocity
                    (si, osimModel.getBodySet().get("linkage2"), eefPoint, eefVel);

                /* cout << "Message: Advanced to next step" << endl; */

                //---------------------------- UPDATE LOG FILES --------------------------------

                // Save the forces
                reporter->getForceStorage().print("forceReporter.mot"); 

                // Store State variables
                manager.getStateStorage().print("states.sto");
                /* cout << "Message: Stored State variables" << endl; */

                // Store controls, forces and moment arms
                for (int i = 0; i < osimModel.getActuators().getSize(); i++) {
                    updctrls[i] = osimModel.getActuators().get(i).getControl(si); 
                    updforces[i] = updctrls[i] * osimModel.getActuators().get(i).getOptimalForce();
                }
                updMomentArms = momentArmVec(si, osimModel);
                updTorques(0) = updctrls(0) * updMomentArms(0) + updctrls(1) * updMomentArms(1);
                updTorques(1) = updctrls(2) * updMomentArms(2) + updctrls(3) * updMomentArms(3);
                /* cout << "Message: Moment arm update loop ended" << endl; */

                _controlStore->store(stepNum, si.getTime(), numControls, &updctrls[0]);
                _controlStore->print("controls.sto");
                _forceStore->store(stepNum, si.getTime(), numControls, &updforces[0]);
                _forceStore->print("forces.sto");
                _momentArmsStore->store(stepNum, si.getTime(), nbMomentArms, &updMomentArms(0));
                _momentArmsStore->print("momentArms.sto");
                _torqueStore->store(stepNum, si.getTime(), numControls/2, &updTorques(0));
                _torqueStore->print("torques.sto");

                /* cout << "Message: Stored controls, forces and moment arms" << endl; */

                // Store Cartesian state of the end-effector
                osimModel.getSimbodyEngine().getPosition
                    (si, osimModel.getBodySet().get("linkage2"), eefPoint, updPosCartesian);
                osimModel.getSimbodyEngine().getVelocity
                    (si, osimModel.getBodySet().get("linkage2"), eefPoint, updVelCartesian);
                osimModel.getSimbodyEngine().getAcceleration
                    (si, osimModel.getBodySet().get("linkage2"), eefPoint, updAccCartesian);

                if (printlog) {
                    cout << "pos " << count << "= " << updPosCartesian << 
                        "\t" << "ref pos " << count << "= " << refPos[count] <<
                        "\t" << "vel " << count << "= " << updVelCartesian << "\t" << "ref vel " << count << "= " << refVel[count] <<
                        endl;
                }

                _posCartesianStore->store(stepNum, si.getTime(), 3, &updPosCartesian[0] );
                _posCartesianStore->print("posCartesian.sto");
                _velCartesianStore->store(stepNum, si.getTime(), 3, &updVelCartesian[0] );
                _velCartesianStore->print("velCartesian.sto");
                _accCartesianStore->store(stepNum, si.getTime(), 3, &updAccCartesian[0] );
                _accCartesianStore->print("accCartesian.sto");
                _refPosCartesianStore->store(stepNum, si.getTime(), 3, &refPos[count].get(0) );
                _refPosCartesianStore->print("refPosCartesian.sto");
                _refVelCartesianStore->store(stepNum, si.getTime(), 3, &refVel[count].get(0));
                _refVelCartesianStore->print("refVelCartesian.sto");
                _refAccCartesianStore->store(stepNum, si.getTime(), 3, &refAcc[count].get(0));
                _refAccCartesianStore->print("refAccCartesian.sto");

                matter.multiplyBySystemJacobianTranspose(
                    si, osimModel.getGravityForce().getBodyForces(si), gr);
                _gravityStore->store(stepNum, si.getTime(), numControls/2, &gr(6));
                _gravityStore->print("gravity.sto");

                /* matter.calcResidualForceIgnoringConstraints( */
                /*     si, SimTK::Vector(0), SimTK::Vector_<SimTK::SpatialVec>(0), */
                /*     SimTK::Vector(0),cor); */
                /* _coriolisStore->store(stepNum, si.getTime(), numControls/2, &cor(6)); */
                /* _coriolisStore->print("coriolis.sto"); */

                /* cout << "Message: Updated log files" << endl; */

                //-------------------------------- UPDATE TIME ---------------------------------

                initialStepTime += stepSize;
                finalStepTime = initialStepTime + stepSize;
            }		
            // end timing
            cout << "simulation time = " << 1.e3*(clock()-startTime)/CLOCKS_PER_SEC << "ms" << endl;
        }
    }
    catch (OpenSim::Exception ex)
    {
        std::cout << ex.getMessage() << std::endl;
        return 1;
    }
    catch (SimTK::Exception::Base ex)
    {
        std::cout << ex.getMessage() << std::endl;
        return 1;
    }
    catch (std::exception ex)
    {
        std::cout << ex.what() << std::endl;
        return 1;
    }
    catch (...)
    {
        std::cout << "UNRECOGNIZED EXCEPTION" << std::endl;
        return 1;
    }
}
