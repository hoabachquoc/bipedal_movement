reset
# set term postscript eps size 1024, 720 color blacktext "Helvetica" 24
# set output "PosEef.eps"
# set title 'Position of the end-effector'
# set ylabel 'Position (m)'
# set xlabel 'Time (s)'
# set grid
# set key outside below; set key title "Legend"; set key box reverse; set key box lw 2 lc 4
# plot "build/PosCartesian.sto" using 1:2 with lines lc rgb "red" title "X",\
#      "build/RefPosCartesian.sto" using 1:2 with lines lc rgb "cyan" title "refX",\
#      "build/PosCartesian.sto" using 1:3 with lines lc rgb "blue" title "Y",\
#      "build/RefPosCartesian.sto" using 1:3 with lines lc rgb "orange" title "refY" 


# reset
# set terminal png 
# set output "PosEef.png"
# set title 'Position of the end-effector'
# set ylabel 'Position (m)'
# set xlabel 'Time (s)'
# set grid
# set key outside below; set key title "Legend"; set key box reverse; set key box lw 2 lc 4
# plot "build/RefPosCartesian.sto" using 1:2 with lines lw 4 lc rgb "cyan" title "refX",\
#      "build/PosCartesian.sto" using 1:2 with lines lw 3 lc rgb "red" title "X",\
#      "build/RefPosCartesian.sto" using 1:3 with lines dt '-.' lw 3 lc rgb "orange" title "refY",\
#      "build/PosCartesian.sto" using 1:3 with lines lw 2 lc rgb "blue" title "Y"

reset
set terminal png 
set output "trajectory.png"
set title 'Trajectory of the end-effector'
set ylabel 'X (m)'
set xlabel 'Y (m)'
set grid
set key outside below; set key title "Legend"; set key box reverse; set key box lw 2 lc 4
plot "build/refPosCartesian.sto" using 2:3 with lines lw 4 lc rgb "red" title "reference",\
     "build/posCartesian.sto" using 2:3 with lines lw 2 lc rgb "blue" title "result"

# reset
# set terminal png
# set output "VelEefX.png"
# set title 'Velocity X of the end-effector'
# set ylabel 'Velocity (m/s)'
# set xlabel 'Time (s)'
# set grid
# plot "build/VelCartesian.sto" using 1:2 with lines lw 2 lc rgb "cyan" title "X",\
#      "build/RefVelCartesian.sto" using 1:2 with lines dt '-.' lw 2 lc rgb "orange" title "refX" 


# reset
# set terminal png
# set output "VelEefY.png"
# set title 'Velocity Y of the end-effector'
# set ylabel 'Velocity (m/s)'
# set xlabel 'Time (s)'
# set grid
# plot "build/VelCartesian.sto" using 1:3 with lines lw 2 lc rgb "cyan" title "Y",\
#      "build/RefVelCartesian.sto" using 1:3 with lines dt '-.' lw 2 lc rgb "orange" title "refY" 


# reset
# set terminal png'
# set output "AccEef.png"
# set title 'Acceleration of the end-effector'
# set ylabel 'Acceleration (m/s²)'
# set xlabel 'Time (s)'
# set grid
# plot "build/AccCartesian.sto" using 1:2 with lines lc rgb "red" title "X",\
#      "build/AccCartesian.sto" using 1:3 with lines lc rgb "blue" title "Y"


# reset
# set terminal png
# set output "AccEefX.png"
# set title 'Acceleration X of the end-effector'
# set ylabel 'Acceleration (m/s²)'
# set xlabel 'Time (s)'
# set yrange [-0.5:0.5]
# set grid
# plot "build/AccCartesian.sto" using 1:2 with lines lw 2 lc rgb "blue" title "X",\
#      "build/RefAccCartesian.sto" using 1:2 with lines dt '-.' lw 2 lc rgb "red" title "refX"


# reset
# set terminal png
# set output "AccEefY.png"
# set title 'Acceleration Y of the end-effector'
# set ylabel 'Acceleration (m/s²)'
# set xlabel 'Time (s)'
# set yrange [-0.5:0.5]
# set grid
# plot "build/AccCartesian.sto" using 1:3 with lines lw 2 lc rgb "blue" title "Y",\
#      "build/RefAccCartesian.sto" using 1:3 with lines dt '-.' lw 2 lc rgb "red" title "refY"


reset
set terminal png
set output "ankleControls.png"
set title 'Ankle Controls'
set ylabel 'control'
set xlabel 'Time (s)'
set grid
plot "build/controls.sto" using 1:2 with lines lw 2 lc rgb "red" title "Left ankle muscle",\
     "build/controls.sto" using 1:3 with lines lw 2 lc rgb "blue" title "Right ankle muscle",\

reset
set terminal png
set output "kneeControls.png"
set title 'Knee Controls'
set ylabel 'control'
set xlabel 'Time (s)'
set grid
plot "build/controls.sto" using 1:4 with lines lw 2 lc rgb "orange" title "Left knee muscle",\
     "build/controls.sto" using 1:5 with lines lw 2 lc rgb "cyan" title "Right knee muscle"


reset
set terminal png
set output "ankleForces.png"
set title 'Ankle muscle forces'
set ylabel 'Force (N)'
set xlabel 'Time (s)'
set grid
plot "build/forces.sto" using 1:2 with lines lw 2 lc rgb "red" title "Left ankle muscle",\
     "build/forces.sto" using 1:3 with lines lw 2 lc rgb "blue" title "Right ankle muscle",\

reset
set terminal png
set output "kneeForces.png"
set title 'Knee muscle forces'
set ylabel 'Force (N)'
set xlabel 'Time (s)'
set grid
plot "build/forces.sto" using 1:4 with lines lw 2 lc rgb "red" title "Left knee muscle",\
     "build/forces.sto" using 1:5 with lines lw 2 lc rgb "blue" title "Right knee muscle"

reset
set terminal png
set output "torques.png"
set title 'torques'
set ylabel 'torque (Nm)'
set xlabel 'Time (s)'
set grid
plot "build/torques.sto" using 1:2 with lines lw 2 lc rgb "red" title "ankle",\
     "build/torques.sto" using 1:3 with lines lw 2 lc rgb "blue" title "knee"

reset
set terminal png
set output "gravity.png"
set title 'gravity'
set ylabel 'torques (Nm)'
set xlabel 'Time (s)'
set grid
plot "build/gravity.sto" using 1:2 with lines lw 2 lc rgb "red" title "ankle",\
     "build/gravity.sto" using 1:3 with lines lw 2 lc rgb "blue" title "knee"

# reset
# set terminal png
# set output "coriolis.png"
# set title 'coriolis'
# set ylabel 'torques (Nm)'
# set xlabel 'Time (s)'
# set grid
# plot "build/coriolis.sto" using 1:2 with lines lw 2 lc rgb "red" title "ankle",\
#      "build/coriolis.sto" using 1:3 with lines lw 2 lc rgb "blue" title "knee"

reset
set terminal png
set output "angles.png"
set title 'Joint angles'
set ylabel 'Angle (rad)'
set xlabel 'Time (s)'
set grid
plot "build/states.sto" using 1:7 with lines lw 2 lc rgb "red" title "Ankle",\
     "build/states.sto" using 1:8 with lines lw 2 lc rgb "blue" title "Knee"

# reset
# set terminal png
# set output "AngularVel.png"
# set title 'Angular Velocity'
# set ylabel 'Angular Velocity (rad/s)'
# set xlabel 'Time (s)'
# set grid
# plot "build/States.sto" using 1:4 with lines lw 2 lc rgb "red" title "Ankle",\
#      "build/States.sto" using 1:5 with lines lw 2 lc rgb "blue" title "Knee"
