// Created on January 5th 2016
// author: Kevin Tanghe, KU Leuven University

#include <OpenSim/OpenSim.h>
#include <iostream>
#include <OpenSim/Simulation/InverseDynamicsSolver.h>

void computeDerivatives(OpenSim::Model &osimModel, OpenSim::Storage &Q, OpenSim::Storage &U, OpenSim::Storage &Udot) {
	SimTK::State s = osimModel.initSystem();

	Udot.setColumnLabels(Q.getColumnLabels());
	Udot.setDescription(Q.getDescription());
	Udot.setHeaderToken(Q.getHeaderToken());
	U.setColumnLabels(Q.getColumnLabels());
	U.setDescription(Q.getDescription());
	U.setHeaderToken(Q.getHeaderToken());

	OpenSim::FunctionSet *coordFunctions = NULL;
	coordFunctions = new OpenSim::GCVSplineSet(5, &Q);
	const OpenSim::CoordinateSet &coords = osimModel.getCoordinateSet();
	int nq = osimModel.getNumCoordinates();

	for (int i = 0; i<nq; i++) {
		if (coordFunctions->contains(coords[i].getName())) {
			coordFunctions->insert(i, coordFunctions->get(coords[i].getName()));
		}
		else {
			coordFunctions->insert(i, new OpenSim::Constant(coords[i].getDefaultValue()));
		}
	}

	if (coordFunctions->getSize()>nq) {
		coordFunctions->setSize(nq);
	}

	double currentTime = 0;
	OpenSim::StateVector *stateAtTimeIndex;
	for (int timeLoop = 0; timeLoop < Q.getSize(); ++timeLoop) {
		stateAtTimeIndex = Q.getStateVector(timeLoop);
		currentTime = stateAtTimeIndex->getTime();
		SimTK::Vector &u = s.updU();
		SimTK::Vector &udot = s.updUDot();

		for (int i = 0; i < nq; i++) {
			u[i] = coordFunctions->evaluate(i, 1, currentTime);
			udot[i] = coordFunctions->evaluate(i, 2, currentTime);
		}
		U.append(currentTime, u);
		Udot.append(currentTime, udot);
	}
}

void readForces(OpenSim::Model &osimModel, OpenSim::Storage &Q, OpenSim::Storage external_forces_c3d,
	OpenSim::Array<SimTK::Vec3> &Fp, OpenSim::Array<SimTK::Vec3> &Fl, OpenSim::Array<SimTK::Vec3> &Fr,
	OpenSim::Array<SimTK::Vec3> &rp, OpenSim::Array<SimTK::Vec3> &rl, OpenSim::Array<SimTK::Vec3> &rr,
	OpenSim::Array<SimTK::Vec3> &Mp, OpenSim::Array<SimTK::Vec3> &Ml, OpenSim::Array<SimTK::Vec3> &Mr) {

	OpenSim::CoordinateSet modelCoordinateSet = osimModel.getCoordinateSet();
	SimTK::Vector q(modelCoordinateSet.getSize(), 0.0);
	const SimTK::MultibodySystem* mbs = &osimModel.getMultibodySystem();
	SimTK::State s = osimModel.initSystem();

	double currentTime = 0;
	OpenSim::StateVector *stateAtTimeIndex;

	const SimTK::MobilizedBodyIndex calcn_r_index = osimModel.getBodySet().get("calcn_r").getIndex();
	SimTK::MobilizedBody calcn_r = osimModel.getMatterSubsystem().getMobilizedBody(calcn_r_index);
	const SimTK::MobilizedBodyIndex calcn_l_index = osimModel.getBodySet().get("calcn_l").getIndex();
	SimTK::MobilizedBody calcn_l = osimModel.getMatterSubsystem().getMobilizedBody(calcn_l_index);
	const SimTK::MobilizedBodyIndex pelvis_index = osimModel.getBodySet().get("pelvis").getIndex();
	SimTK::MobilizedBody pelvis = osimModel.getMatterSubsystem().getMobilizedBody(pelvis_index);

	SimTK::Vec3 calcn_r_location(0);
	SimTK::Vec3 calcn_l_location(0);
	SimTK::Vec3 pelvis_location(0);
	SimTK::Vector v(external_forces_c3d.getSmallestNumberOfStates(), 0.0);

	for (int i = 0; i<Q.getSize(); ++i) {

		stateAtTimeIndex = Q.getStateVector(i);
		currentTime = stateAtTimeIndex->getTime();
		Q.getData(i, modelCoordinateSet.getSize(), q);
		s.setTime(currentTime);
		s.setQ(q);
		osimModel.getMultibodySystem().realize(s, SimTK::Stage::Position);

		calcn_r_location = calcn_r.getBodyOriginLocation(s);
		calcn_l_location = calcn_l.getBodyOriginLocation(s);
		pelvis_location = pelvis.getBodyOriginLocation(s);

		external_forces_c3d.getDataAtTime(currentTime, external_forces_c3d.getSmallestNumberOfStates(), v);


		rp[i](0) = v[external_forces_c3d.getStateIndex("2_ground_force_px")] - pelvis_location(0);
		rp[i](1) = v[external_forces_c3d.getStateIndex("2_ground_force_py")] - pelvis_location(1);
		rp[i](2) = v[external_forces_c3d.getStateIndex("2_ground_force_pz")] - pelvis_location(2);
		rr[i](0) = v[external_forces_c3d.getStateIndex("1_ground_force_px")] - calcn_r_location(0);
		rr[i](1) = v[external_forces_c3d.getStateIndex("1_ground_force_py")] - calcn_r_location(1);
		rr[i](2) = v[external_forces_c3d.getStateIndex("1_ground_force_pz")] - calcn_r_location(2);
		rl[i](0) = v[external_forces_c3d.getStateIndex("ground_force_px")] - calcn_l_location(0);
		rl[i](1) = v[external_forces_c3d.getStateIndex("ground_force_py")] - calcn_l_location(1);
		rl[i](2) = v[external_forces_c3d.getStateIndex("ground_force_pz")] - calcn_l_location(2);

		Fp[i](0) = v[external_forces_c3d.getStateIndex("2_ground_force_vx")];
		Fp[i](1) = v[external_forces_c3d.getStateIndex("2_ground_force_vy")];
		Fp[i](2) = v[external_forces_c3d.getStateIndex("2_ground_force_vz")];
		Fr[i](0) = v[external_forces_c3d.getStateIndex("1_ground_force_vx")];
		Fr[i](1) = v[external_forces_c3d.getStateIndex("1_ground_force_vy")];
		Fr[i](2) = v[external_forces_c3d.getStateIndex("1_ground_force_vz")];
		Fl[i](0) = v[external_forces_c3d.getStateIndex("ground_force_vx")];
		Fl[i](1) = v[external_forces_c3d.getStateIndex("ground_force_vy")];
		Fl[i](2) = v[external_forces_c3d.getStateIndex("ground_force_vz")];

		Mp[i](0) = v[external_forces_c3d.getStateIndex("2_ground_torque_x")];
		Mp[i](1) = v[external_forces_c3d.getStateIndex("2_ground_torque_y")];
		Mp[i](2) = v[external_forces_c3d.getStateIndex("2_ground_torque_z")];
		Mr[i](0) = v[external_forces_c3d.getStateIndex("1_ground_torque_x")];
		Mr[i](1) = v[external_forces_c3d.getStateIndex("1_ground_torque_y")];
		Mr[i](2) = v[external_forces_c3d.getStateIndex("1_ground_torque_z")];
		Ml[i](0) = v[external_forces_c3d.getStateIndex("ground_torque_x")];
		Ml[i](1) = v[external_forces_c3d.getStateIndex("ground_torque_y")];
		Ml[i](2) = v[external_forces_c3d.getStateIndex("ground_torque_z")];
	}

}

SimTK::Array_<SimTK::Vector> totalID(OpenSim::Model &osimModel, OpenSim::Storage storage_q,
	OpenSim::Storage storage_qdot, OpenSim::Storage &Udot,
	OpenSim::Array<SimTK::Vec3> &Fl, OpenSim::Array<SimTK::Vec3> &Fp, OpenSim::Array<SimTK::Vec3> &Fr,
	OpenSim::Array<SimTK::Vec3> &rl, OpenSim::Array<SimTK::Vec3> &rp, OpenSim::Array<SimTK::Vec3> &rr,
	OpenSim::Array<SimTK::Vec3> &Mp, OpenSim::Array<SimTK::Vec3> &Ml, OpenSim::Array<SimTK::Vec3> &Mr) {
	//calculate the joint torques (=mobility forces) using the inversedynamicssolver
	//can be faster, since the inversedynamicssolver realizes the stage up to dynamics, but the velocity stage is sufficient

	//initializations
	int nt = Udot.getSize();
	int nq = osimModel.getNumCoordinates();
	SimTK::Vec3 g = osimModel.getGravity();
	const SimTK::MultibodySystem* mbs = &osimModel.getMultibodySystem();
	SimTK::Array_<SimTK::Vector> genForceTraj(nt, SimTK::Vector(nq, 0.0));

	SimTK::Vector q(osimModel.getCoordinateSet().getSize(), 0.0);
	SimTK::Vector qdot(osimModel.getCoordinateSet().getSize(), 0.0);
	SimTK::Vector udot(osimModel.getCoordinateSet().getSize(), 0.0);

	SimTK::Vector_<SimTK::SpatialVec> appliedBodyForces;
	appliedBodyForces.resize(osimModel.getNumBodies());
	appliedBodyForces.setToZero();
	SimTK::Vector mobilityForces(nq, 0.0);

	OpenSim::InverseDynamicsSolver ivdSolver(osimModel);
	SimTK::State s = osimModel.initSystem();

	const SimTK::MobilizedBodyIndex calcn_l_index = osimModel.getBodySet().get("calcn_l").getIndex();
	SimTK::MobilizedBody calcn_l = osimModel.getMatterSubsystem().getMobilizedBody(calcn_l_index);
	const SimTK::MobilizedBodyIndex calcn_r_index = osimModel.getBodySet().get("calcn_r").getIndex();
	SimTK::MobilizedBody calcn_r = osimModel.getMatterSubsystem().getMobilizedBody(calcn_r_index);
	const SimTK::MobilizedBodyIndex pelvis_index = osimModel.getBodySet().get("pelvis").getIndex();
	SimTK::MobilizedBody pelvis = osimModel.getMatterSubsystem().getMobilizedBody(pelvis_index);
	SimTK::MobilizedBody mobod;

	//calculations
	for (int timeLoop = 0; timeLoop<nt; ++timeLoop) {
		appliedBodyForces.setToZero();
		storage_q.getData(timeLoop, nq, q);
		storage_qdot.getData(timeLoop, nq, qdot);
		Udot.getData(timeLoop, nq, udot);
		s.setQ(q);
		s.setU(qdot);
		mbs->realize(s, SimTK::Stage::Position);

		appliedBodyForces[pelvis_index] = SimTK::SpatialVec(rp[timeLoop] % Fp[timeLoop] + Mp[timeLoop], Fp[timeLoop]);
		appliedBodyForces[calcn_l_index] = SimTK::SpatialVec(rl[timeLoop] % Fl[timeLoop] + Ml[timeLoop], Fl[timeLoop]);
		appliedBodyForces[calcn_r_index] = SimTK::SpatialVec(rr[timeLoop] % Fr[timeLoop] + Mr[timeLoop], Fr[timeLoop]);

		for (SimTK::MobilizedBodyIndex i(1); i < osimModel.getMatterSubsystem().getNumBodies(); ++i) {
			mobod = osimModel.getMatterSubsystem().getMobilizedBody(i);
			osimModel.getMatterSubsystem().addInStationForce(s, i, mobod.getBodyMassCenterStation(s), mobod.getBodyMass(s)*g, appliedBodyForces);
		}

		genForceTraj[timeLoop] = ivdSolver.solve(s, udot, mobilityForces, appliedBodyForces);
	}

	return genForceTraj;
}

SimTK::Array_<SimTK::Vector> totalID_matrixStyle(OpenSim::Model &osimModel, OpenSim::Storage Q,
	OpenSim::Storage U, OpenSim::Storage &Udot,
	OpenSim::Array<SimTK::Vec3> &Fl, OpenSim::Array<SimTK::Vec3> &Fp, OpenSim::Array<SimTK::Vec3> &Fr,
	OpenSim::Array<SimTK::Vec3> &rl, OpenSim::Array<SimTK::Vec3> &rp, OpenSim::Array<SimTK::Vec3> &rr,
	OpenSim::Array<SimTK::Vec3> &Mp, OpenSim::Array<SimTK::Vec3> &Ml, OpenSim::Array<SimTK::Vec3> &Mr,
	bool stageDynamics) {

	int nt = Udot.getSize();
	int nq = osimModel.getNumCoordinates();
	int nb = osimModel.getNumBodies();
	SimTK::Vec3 g = osimModel.getGravity();
	const SimTK::MultibodySystem* mbs = &osimModel.getMultibodySystem();
	SimTK::Array_<SimTK::Vector> genForceTraj(nt, SimTK::Vector(nq, 0.0));

	SimTK::Vector q(osimModel.getCoordinateSet().getSize(), 0.0);
	SimTK::Vector u(osimModel.getCoordinateSet().getSize(), 0.0);
	SimTK::Vector udot(osimModel.getCoordinateSet().getSize(), 0.0);
	SimTK::State s = osimModel.initSystem();

	SimTK::MobilizedBody mobod = osimModel.getMatterSubsystem().getGround();;
	SimTK::SpatialVec gyrForce = SimTK::SpatialVec(0);
	SimTK::SpatialVec colAcc = SimTK::SpatialVec(0);
	SimTK::SpatialInertia I;
	SimTK::Vector_<SimTK::SpatialVec> totCorForce_bodyFrame; //coriolis and gyroscopic wrenches of each body expressed in body origin
	SimTK::Vector_<SimTK::SpatialVec> gravForces_bodyFrame;
	totCorForce_bodyFrame.resize(nb);
	gravForces_bodyFrame.resize(nb);

	const SimTK::MobilizedBodyIndex calcn_l_index = osimModel.getBodySet().get("calcn_l").getIndex();
	SimTK::MobilizedBody calcn_l = osimModel.getMatterSubsystem().getMobilizedBody(calcn_l_index);
	const SimTK::MobilizedBodyIndex calcn_r_index = osimModel.getBodySet().get("calcn_r").getIndex();
	SimTK::MobilizedBody calcn_r = osimModel.getMatterSubsystem().getMobilizedBody(calcn_r_index);
	const SimTK::MobilizedBodyIndex pelvis_index = osimModel.getBodySet().get("pelvis").getIndex();
	SimTK::MobilizedBody pelvis = osimModel.getMatterSubsystem().getMobilizedBody(pelvis_index);
	SimTK::Vector_<SimTK::SpatialVec> totF_frame(osimModel.getMatterSubsystem().getNumBodies(), SimTK::SpatialVec());
	SimTK::Vector totF_system(nq, 0.0);
	SimTK::Vector m_udot(nq, 0.0);

	for (int timeLoop = 0; timeLoop < Q.getSize(); ++timeLoop) {
		totCorForce_bodyFrame.setToZero();
		gravForces_bodyFrame.setToZero();
		Q.getData(timeLoop, nq, q);
		U.getData(timeLoop, nq, u);
		Udot.getData(timeLoop, nq, udot);
		s.setQ(q);
		s.setU(u);

		if (stageDynamics) {
			mbs->realize(s, SimTK::Stage::Dynamics);
		}
		else {
			mbs->realize(s, SimTK::Stage::Velocity);
		}

		for (SimTK::MobilizedBodyIndex i(1); i < osimModel.getMatterSubsystem().getNumBodies(); ++i) {
			mobod = osimModel.getMatterSubsystem().getMobilizedBody(i);
			//calculate total coriolis forces in body frame
			gyrForce = osimModel.getMatterSubsystem().getGyroscopicForce(s, i);
			colAcc = osimModel.getMatterSubsystem().getTotalCoriolisAcceleration(s, i);
			I = mobod.getBodySpatialInertiaInGround(s);
			totCorForce_bodyFrame[i] = I*colAcc + gyrForce;

			//calculate all gravity forces in the body frame
			osimModel.getMatterSubsystem().addInStationForce(s, i, mobod.getBodyMassCenterStation(s), mobod.getBodyMass(s)*g, gravForces_bodyFrame);

			totF_frame = -totCorForce_bodyFrame + gravForces_bodyFrame;
			//add external forces
			totF_frame[calcn_l_index] += SimTK::SpatialVec(rl[timeLoop] % Fl[timeLoop] + Ml[timeLoop], Fl[timeLoop]);
			totF_frame[calcn_r_index] += SimTK::SpatialVec(rr[timeLoop] % Fr[timeLoop] + Mr[timeLoop], Fr[timeLoop]);
			totF_frame[pelvis_index] += SimTK::SpatialVec(rp[timeLoop] % Fp[timeLoop] + Mp[timeLoop], Fp[timeLoop]);
			osimModel.getMatterSubsystem().multiplyBySystemJacobianTranspose(s, totF_frame, totF_system);
			osimModel.getMatterSubsystem().multiplyByM(s, udot, m_udot);
			genForceTraj[timeLoop] = m_udot - totF_system;
		}
	}
	return genForceTraj;

}

void printIdResults(std::string outputFileDir, std::string outputFileName, SimTK::Array_<SimTK::Vector> &genForceTraj, OpenSim::Model &osimModel, OpenSim::Storage &storage_q) {

	int nq = osimModel.getNumCoordinates();
	int nt = storage_q.getSize();
	const OpenSim::CoordinateSet &coords = osimModel.getCoordinateSet();
	OpenSim::Array<std::string> labels("time", nq + 1);
	for (int i = 0; i<nq; i++) {
		labels[i + 1] = coords[i].getName();
		labels[i + 1] += (coords[i].getMotionType() == OpenSim::Coordinate::Rotational) ? "_moment" : "_force";
	}

	OpenSim::Storage genForceResults(nt);
	OpenSim::Storage bodyForcesResults(nt);
	SimTK::SpatialVec equivalentBodyForceAtJoint;

	for (int i = 0; i<nt; i++) {
		OpenSim::StateVector genForceVec(storage_q.getStateVector(i)->getTime(), nq, &((genForceTraj[i])[0]));
		genForceResults.append(genForceVec);
	}

	genForceResults.setColumnLabels(labels);
	genForceResults.setName("Inverse Dynamics Generalized Forces");
	OpenSim::Storage::printResult(&genForceResults, outputFileName, outputFileDir, -1, ".sto");

}

int main()
{
	std::string projectPath = "C:/Users/u0074271/Documents/doctoraat/tests/opensim_cpp/slowID/";

	////////Load all model and motion files
	OpenSim::Model osimModel(projectPath + "inputFiles/osimModel_calc.osim");
	OpenSim::Storage external_forces_c3d(projectPath + "inputFiles/extForces.mot");
	OpenSim::Storage Q(projectPath + "inputFiles/ik.mot");

	Q.lowpassIIR(6);
	if (Q.isInDegrees()) {
		osimModel.getSimbodyEngine().convertDegreesToRadians(Q);
	}
	OpenSim::Storage U, Udot;
	computeDerivatives(osimModel, Q, U, Udot);
	
	//////Read forces from inputfile
	// Fp, Fl, Fr: force on pelvis, left foot and right foot
	// rp, rl, rr: position of the force, relative to the body origin, expressed in world frame
	// Mp, Ml, Mr: torque on pelvis, left foot and right foot
	OpenSim::Array<SimTK::Vec3> Fp, Fl, Fr;
	OpenSim::Array<SimTK::Vec3> rp, rl, rr;
	OpenSim::Array<SimTK::Vec3> Mp, Ml, Mr;
	Fp.setSize(Q.getSize()); Fl.setSize(Q.getSize()); Fr.setSize(Q.getSize());
	rp.setSize(Q.getSize()); rl.setSize(Q.getSize()); rr.setSize(Q.getSize());
	Mp.setSize(Q.getSize()); Ml.setSize(Q.getSize()); Mr.setSize(Q.getSize());

	readForces(osimModel, Q, external_forces_c3d, Fp, Fl, Fr, rp, rl, rr, Mp, Ml, Mr);

	/////Inverse dynamics
	int nt = Q.getSize();
	int nq = osimModel.getNumCoordinates();
	time_t tstart, tend;

	//slow method with ID solver
	SimTK::Array_<SimTK::Vector> genForceTraj(nt, SimTK::Vector(nq, 0.0));
	tstart = clock();
	genForceTraj = totalID(osimModel, Q, U, Udot, Fl, Fp, Fr, rl, rp, rr, Mp, Ml, Mr);
	tend = clock();
	std::cout << "ID with ID solver took " << difftime(tend, tstart) << " millisecond(s)." << std::endl;
	std::string output_fileName = "idSolver";
	std::string output_Dir = projectPath + "outputFiles";
	printIdResults(output_Dir, output_fileName, genForceTraj, osimModel, Q);

	//fast method using matrix method and realizing velocity stage
	bool stageDynamics = false; //do not realize to dynamic stage
	SimTK::Array_<SimTK::Vector> genForceTraj2(nt, SimTK::Vector(nq, 0.0));
	tstart = clock();
	genForceTraj2 = totalID_matrixStyle(osimModel, Q, U, Udot, Fl, Fp, Fr, rl, rp, rr, Mp, Ml, Mr, stageDynamics);
	tend = clock();
	std::cout << "ID with matrix method took " << difftime(tend, tstart) << " millisecond(s)." << std::endl;
	output_fileName = "fastMatrix";
	output_Dir = projectPath + "outputFiles";
	printIdResults(output_Dir, output_fileName, genForceTraj2, osimModel, Q);

	//slow method using matrix method and realizing dynamics stage
	stageDynamics = true; //realize to dynamic stage
	SimTK::Array_<SimTK::Vector> genForceTraj3(nt, SimTK::Vector(nq, 0.0));
	tstart = clock();
	genForceTraj3 = totalID_matrixStyle(osimModel, Q, U, Udot, Fl, Fp, Fr, rl, rp, rr, Mp, Ml, Mr, stageDynamics);
	tend = clock();
	std::cout << "ID with matrix method, but realizing to dynamic stage, took " << difftime(tend, tstart) << " millisecond(s)." << std::endl;
	output_fileName = "slowMatrix";
	output_Dir = projectPath + "outputFiles";
	printIdResults(output_Dir, output_fileName, genForceTraj3, osimModel, Q);

	std::cout << "Succes! \n\n\n\n\n" << std::endl;
	return 0;
}
